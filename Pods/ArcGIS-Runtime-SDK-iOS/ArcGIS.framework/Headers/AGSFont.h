/*
 COPYRIGHT 2013 ESRI
 
 TRADE SECRETS: ESRI PROPRIETARY AND CONFIDENTIAL
 Unpublished material - all rights reserved under the
 Copyright Laws of the United States and applicable international
 laws, treaties, and conventions.
 
 For additional information, contact:
 Environmental Systems Research Institute, Inc.
 Attn: Contracts and Legal Services Department
 380 New York Street
 Redlands, California, 92373
 USA
 
 email: contracts@esri.com
 */

/** @file AGSColor.h */ //Required for Globals API doc

#if __has_include(<UIKit/UIFont.h>)
#import <UIKit/UIFont.h>
/**
 @c AGSFont is defined as @c UIFont on iOS platform.
 
 @since 100
 */
#define AGSFont UIFont
#elif __has_include(<AppKit/NSFont.h>)
#import <AppKit/NSFont.h>
/**
 @c AGSFont is defined as @c NSFont on macOS platform.
 @since 100
 */
#define AGSFont NSFont
#endif
