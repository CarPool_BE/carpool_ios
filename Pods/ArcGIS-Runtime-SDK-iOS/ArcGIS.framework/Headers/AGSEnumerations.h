/*
 COPYRIGHT 2011 ESRI

 TRADE SECRETS: ESRI PROPRIETARY AND CONFIDENTIAL
 Unpublished material - all rights reserved under the
 Copyright Laws of the United States and applicable international
 laws, treaties, and conventions.

 For additional information, contact:
 Environmental Systems Research Institute, Inc.
 Attn: Contracts and Legal Services Department
 380 New York Street
 Redlands, California, 92373
 USA

 email: contracts@esri.com
 */

/** @file AGSEnumerations.h */

#import <Foundation/Foundation.h>
#import <ArcGIS/AGSDefines.h>

NS_ASSUME_NONNULL_BEGIN

#pragma mark - Geometry

/** Supported geometry types.
 @since 100
 */
typedef NS_ENUM(NSInteger, AGSGeometryType)  {
    AGSGeometryTypeUnknown = -1,    /*!< Undefined */
    AGSGeometryTypePoint = 1,           /*!< Point */
    AGSGeometryTypeEnvelope = 2,          /*!< Envelope */
    AGSGeometryTypePolyline = 3,        /*!< Polyline */
    AGSGeometryTypePolygon = 4,         /*!< Polygon */
    AGSGeometryTypeMultipoint = 5,      /*!< Multipoint */
};

/** Supported @p offsetHow types.
 Square only applicable with the AGSGeometryEngine
 @since 100
 */
typedef NS_ENUM(NSInteger, AGSGeometryOffsetType) {
    AGSGeometryOffsetTypeMitered = 0,       /*!< Mietered */
    AGSGeometryOffsetTypeBevelled = 1,      /*!< Bevelled  */
    AGSGeometryOffsetTypeRounded = 2,       /*!< Rounded */
    AGSGeometryOffsetTypeSquared = 3       /**  Squared  */
};

/** Dimension classification of the geometry
 @since 100
 */
typedef NS_ENUM(NSInteger, AGSGeometryDimension) {
    AGSGeometryDimensionPoint = 0,          /*!<  */
    AGSGeometryDimensionCurve = 1,          /*!<  */
    AGSGeometryDimensionArea = 2,           /*!<  */
    AGSGeometryDimensionVolume = 3,         /*!<  */
    AGSGeometryDimensionUnknown = -1,       /*!<  */
};

/** Extend geometry options
 @since 100.1
 */
typedef NS_OPTIONS(NSInteger, AGSGeometryExtendOption)
{
    AGSGeometryExtendOptionDefault = 0,                             /*!<  */
    AGSGeometryExtendOptionRelocateEnds = 1 << 0,                   /*!<  */
    AGSGeometryExtendOptionKeepEndAttributes = 1 << 1,              /*!<  */
    AGSGeometryExtendOptionNoEndAttributes = 1 << 2,                /*!<  */
    AGSGeometryExtendOptionDoNotExtendFromStartPoint = 1 << 3,      /*!<  */
    AGSGeometryExtendOptionDoNotExtendFromEndPoint = 1 << 4,        /*!<  */
};

/**
 Types of Geodetic curves.
 @since 100
 @see [About geodetic features](http://resources.arcgis.com/en/help/main/10.1/index.html#/About_geodetic_features/01m70000003q000000/)
 */
typedef NS_ENUM(NSInteger, AGSGeodeticCurveType) {
    AGSGeodeticCurveTypeGeodesic = 0,       /*!< The shortest line between any two points on the Earth's surface on a spheroid (ellipsoid) */
    AGSGeodeticCurveTypeLoxodrome,          /*!< A line of constant bearing, or azimuth.*/
    AGSGeodeticCurveTypeGreatElliptic,      /*!< The line on a spheroid (ellipsoid) defined by the intersection at the surface by a plane that passes through the center of the spheroid and the start and end points of a segment. This is also known as a great circle when a sphere is used */
    AGSGeodeticCurveTypeNormalSection,      /*!<  */
    AGSGeodeticCurveTypeShapePreserving    /*!< Keeps the original shape of the geometry or curve  */
};

/** Indicates the location of a point relative to a GARS cell.
 @since 100.1
 */
typedef NS_ENUM(NSInteger, AGSGARSConversionMode) {
    AGSGARSConversionModeLowerLeft = 0,     /*!< Represents a GARS cell by the coordinate of its south-west corner. */
    AGSGARSConversionModeCenter = 1,        /*!< Represents a GARS cell by the coordinate of its center. */
};

/** Determines the lettering scheme and treatment of coordinates at 180 degrees longitude when converting MGRS coordinates.
 @since 100.1
 */
typedef NS_ENUM(NSInteger, AGSMGRSConversionMode) {
    AGSMGRSConversionModeAutomatic = 0,         /*!< The choice of MGRS lettering scheme is based on the datum and ellipsoid of the spatial reference provided. Spatial references with new datums (e.g. WGS 84) assume new lettering scheme (AA scheme). This is equivalent to `AGSMGRSConversionModeNew180InZone60`. Spatial references with older datums (e.g. Clarke 1866, Bessel 1841, Clarke 1880) assume old lettering scheme (AL scheme). This is equivalent to `AGSMGRSConversionModeOld180InZone60`. When converted, points with longitude of exactly 180deg are placed in zone 60. */
    AGSMGRSConversionModeNew180InZone01 = 1,    /*!< The MGRS notation uses the new lettering scheme (AA scheme) and, when converted, places points with longitude of 180deg in zone 01. */
    AGSMGRSConversionModeNew180InZone60 = 2,    /*!< The MGRS notation uses the new lettering scheme (AA scheme) and, when converted, places points with longitude of 180deg in zone 60.*/
    AGSMGRSConversionModeOld180InZone01 = 3,    /*!< The MGRS notation uses the old lettering scheme (AL scheme) and, when converted, places points with longitude of 180deg in zone 01.*/
    AGSMGRSConversionModeOld180InZone60 = 4,    /*!< The MGRS notation uses the old lettering scheme (AL scheme) and, when converted, places points with longitude of 180deg in zone 60. */
};

/** Determines how latitude is designated in UTM notation.

 Within a single longitudinal zone within the UTM system, two points share the same grid position: one in the northern hemisphere and one in the south. Two schemes are used to resolve this ambiguity. In the first, the point is designated a latitude band, identified with letters C through X (omitting I and O). In the second, in place of the latitude band, a hemisphere indicator (N or S) is used.

 @since 100.1
 */
typedef NS_ENUM(NSInteger, AGSUTMConversionMode) {
    AGSUTMConversionModeLatitudeBandIndicators = 0, /*!<  The letter after the UTM zone number represents a latitudinal band (C through X, omitting I and O).*/
    AGSUTMConversionModeNorthSouthIndicators = 1,   /*!<  The letter after the UTM zone number represents a hemisphere (N or S).*/
};

/** Supported formats for representing latitude-longitude geographical coordinates as a string.
 @since 100.1
 */
typedef NS_ENUM(NSInteger, AGSLatitudeLongitudeFormat) {
    AGSLatitudeLongitudeFormatDecimalDegrees = 0,           /*!< The geographical coordinates are represented in decimal degrees. */
    AGSLatitudeLongitudeFormatDegreesDecimalMinutes = 1,    /*!< The geographical coordinates are represented in degrees and decimal minutes.*/
    AGSLatitudeLongitudeFormatDegreesMinutesSeconds = 2,    /*!< The geographical coordinates are represented in degrees and minutes and decimal seconds. */
};

/** Different ways to treat x/y coordinates order.
 @since 100.5
 */
typedef NS_ENUM(NSInteger, AGSOGCAxisOrder) {
    AGSOGCAxisOrderAuto = 0,           /*!< Swapping of the axis order depends on OGC standard specification, version, and spatial reference. */
    AGSOGCAxisOrderSwap = 1,           /*!< Order of x/y coordinates is swapped. */
    AGSOGCAxisOrderNoSwap = 2          /*!< Indicates the order of x/y coordinates will stay as is, and all swapping rules by the OGC standards will be ignored. */
};

#pragma mark - Network Request

/** The HTTP method being used by the network request.
 @since 100
 */
typedef NS_ENUM(NSInteger, AGSRequestHTTPMethod) {
    AGSRequestHTTPMethodGet = 0,                        /*!< The query parameters are sent as name/value pairs in the URL of a GET request*/
    AGSRequestHTTPMethodPostFormEncodeParameters = 1,   /*!< The query parameters are form encoded and sent in the body of a POST request. This is the method used if the URL of a GET request gets too long.*/
    AGSRequestHTTPMethodPostJSON = 2                    /*!< The query parameters are serialized to JSON and sent in the body of a POST request with the content type of @c application/json.*/
};



#pragma mark - Errors

/** Constant representing domain for errors originating from web services.
 @since 100
 */
AGS_EXTERN NSString *const AGSServicesErrorDomain;

/** Constants representing error codes coming from web services. These errors belongs to @c #AGSServicesErrorDomain.
 The values in this enumeration are not comprehensive. You may get error codes belonging to the #AGSServicesErrorDomain 
 that are not in this enumeration.
 @since 100
 */
typedef NS_ENUM(NSInteger, AGSServicesErrorCode) {
    AGSServicesErrorCodeBadRequest = 400,                   /*!< Bad Request */
    AGSServicesErrorCodeUnauthorized = 401,                 /*!< Authorization Required */
    AGSServicesErrorCodeURLNotFound = 404,                  /*!< Requested URL Not Found */
    AGSServicesErrorCodeInvalidToken = 498,                 /*!< Invalid Token */
    AGSServicesErrorCodeTokenRequired = 499,                /*!< Token Required */
    AGSServicesErrorCodeUnknownError = 500                  /*!< Unknown Error */
};

/** Constant representing domain for errors originating from ArcGIS Runtime.
 @since 100
 @see `AGSServicesErrorDomain`
 */
AGS_EXTERN NSString *const AGSErrorDomain;

/** Constants representing core error codes. These errors belongs to @c #AGSErrorDomain.
 @since 100
 */
typedef NS_ENUM(NSInteger, AGSErrorCode) {
    AGSErrorCodeUnknown = -1,                                              /*!< This is the catch all for unknown errors. */
    AGSErrorCodeCommonNullPtr = 1,                                         /*!< Null pointer exception. */
    AGSErrorCodeCommonInvalidArgument = 2,                                 /*!< Invalid argument exception. */
    AGSErrorCodeCommonNotImplemented = 3,                                  /*!< Not implemented exception. */
    AGSErrorCodeCommonOutOfRange = 4,                                      /*!< Out of range exception. */
    AGSErrorCodeCommonInvalidAccess = 5,                                   /*!< Invalid access exception. */
    AGSErrorCodeCommonIllegalState = 6,                                    /*!< Illegal state exception. */
    AGSErrorCodeCommonNotFound = 7,                                        /*!< Not found exception. */
    AGSErrorCodeCommonExists = 8,                                          /*!< Exists exception. */
    AGSErrorCodeCommonTimeout = 9,                                         /*!< Timeout exception. */
    AGSErrorCodeCommonRegularExpression = 10,                              /*!< Regular expression exception. */
    AGSErrorCodeCommonPropertyNotSupported = 11,                           /*!< Property not supported exception. */
    AGSErrorCodeCommonNoPermission = 12,                                   /*!< No permission exception. */
    AGSErrorCodeCommonFile = 13,                                           /*!< File exception. */
    AGSErrorCodeCommonFileNotFound = 14,                                   /*!< File not found exception. */
    AGSErrorCodeCommonInvalidCall = 15,                                    /*!< Invalid call exception. */
    AGSErrorCodeCommonIO = 16,                                             /*!< IO exception. */
    AGSErrorCodeCommonUserCanceled = 17,                                   /*!< User canceled exception. */
    AGSErrorCodeCommonInternalError = 18,                                  /*!< Internal error exception. */
    AGSErrorCodeCommonConversionFailed = 19,                               /*!< Conversion failed exception. */
    AGSErrorCodeCommonNoData = 20,                                         /*!< No data. */
    AGSErrorCodeCommonInvalidJSON = 21,                                    /*!< Attempted to use invalid JSON. */
    AGSErrorCodeCommonUserDefinedFailure = 22,                             /*!< Error was propagated. */
    AGSErrorCodeCommonBadXml = 23,                                         /*!< Attempted to use invalid XML. */
    AGSErrorCodeCommonObjectAlreadyOwned = 24,                             /*!< Object is already owned. */
    AGSErrorCodeCommonExpired = 26,                                        /*!< The resource is past its expiry date. @since 100.5 */
    AGSErrorCodeSqliteError = 1001,                                        /*!< SQLite error exception. */
    AGSErrorCodeSqliteInternal = 1002,                                     /*!< SQLite Internal exception. */
    AGSErrorCodeSqlitePerm = 1003,                                         /*!< SQLite Perm exception. */
    AGSErrorCodeSqliteAbort = 1004,                                        /*!< SQLite Abort exception. */
    AGSErrorCodeSqliteBusy = 1005,                                         /*!< SQLite Busy exception. */
    AGSErrorCodeSqliteLocked = 1006,                                       /*!< SQLite Locked exception. */
    AGSErrorCodeSqliteNoMem = 1007,                                        /*!< SQLite NoMem exception. */
    AGSErrorCodeSqliteReadonly = 1008,                                     /*!< SQLite Read only exception. */
    AGSErrorCodeSqliteInterrupt = 1009,                                    /*!< SQLite Interrupt exception. */
    AGSErrorCodeSqliteIOErr = 1010,                                        /*!< SQLite IO Error exception. */
    AGSErrorCodeSqliteCorrupt = 1011,                                      /*!< SQLite Corrupt exception. */
    AGSErrorCodeSqliteNotFound = 1012,                                     /*!< SQLite Not found exception. */
    AGSErrorCodeSqliteFull = 1013,                                         /*!< SQLite Full exception. */
    AGSErrorCodeSqliteCantOpen = 1014,                                     /*!< SQLite Can't open exception. */
    AGSErrorCodeSqliteProtocol = 1015,                                     /*!< SQLite Protocol exception. */
    AGSErrorCodeSqliteEmpty = 1016,                                        /*!< SQLite Empty exception. */
    AGSErrorCodeSqliteSchema = 1017,                                       /*!< SQLite Schema exception. */
    AGSErrorCodeSqliteTooBig = 1018,                                       /*!< SQLite Too big exception. */
    AGSErrorCodeSqliteConstraint = 1019,                                   /*!< SQLite Constraint exception. */
    AGSErrorCodeSqliteMismatch = 1020,                                     /*!< SQLite Mismatch exception. */
    AGSErrorCodeSqliteMisuse = 1021,                                       /*!< SQLite Misuse exception. */
    AGSErrorCodeSqliteNolfs = 1022,                                        /*!< SQLite Nolfs exception. */
    AGSErrorCodeSqliteAuth = 1023,                                         /*!< SQLite Auth exception. */
    AGSErrorCodeSqliteFormat = 1024,                                       /*!< SQLite Format exception. */
    AGSErrorCodeSqliteRange = 1025,                                        /*!< SQLite Range exception. */
    AGSErrorCodeSqliteNotadb = 1026,                                       /*!< SQLite Notadb exception. */
    AGSErrorCodeSqliteNotice = 1027,                                       /*!< SQLite Notice exception. */
    AGSErrorCodeSqliteWarning = 1028,                                      /*!< SQLite Warning exception. */
    AGSErrorCodeSqliteRow = 1029,                                          /*!< SQLite Row exception. */
    AGSErrorCodeSqliteDone = 1030,                                         /*!< SQLite Done exception. */
    AGSErrorCodeGeometryUnknownError = 2000,                               /*!< Geometry Exception. */
    AGSErrorCodeGeometryCorruptedGeometry = 2001,                          /*!< Geometry Corrupted geometry exception. */
    AGSErrorCodeGeometryEmptyGeometry = 2002,                              /*!< Geometry Empty geometry exception. */
    AGSErrorCodeGeometryMathSingularity = 2003,                            /*!< Geometry Math singularity exception. */
    AGSErrorCodeGeometryBufferIsTooSmall = 2004,                           /*!< Geometry Buffer is too small exception. */
    AGSErrorCodeGeometryInvalidShapeType = 2005,                           /*!< Geometry Invalid shape type exception. */
    AGSErrorCodeGeometryProjectionOutOfSupportedRange = 2006,              /*!< Geometry Projection out of supported range exception. */
    AGSErrorCodeGeometryNonSimpleGeometry = 2007,                          /*!< Geometry Non simple geometry exception. */
    AGSErrorCodeGeometryCannotCalculateGeodesic = 2008,                    /*!< Geometry Cannot calculate geodesic exception. */
    AGSErrorCodeGeometryNotationConversion = 2009,                         /*!< Geometry Notation conversion exception. */
    AGSErrorCodeGeometryMissingGridFile = 2010,                            /*!< Geometry Missing grid file exception. */
    AGSErrorCodeGDBValueOutOfRange = 3001,                                 /*!< Geodatabase Value out of range exception. */
    AGSErrorCodeGDBDataTypeMismatch = 3002,                                /*!< Geodatabase Data type mismatch exception. */
    AGSErrorCodeGDBBadXml = 3003,                                          /*!< Geodatabase Bad XML exception. */
    AGSErrorCodeGDBDatabaseAlreadyExists = 3004,                           /*!< Geodatabase Database already exists exception. */
    AGSErrorCodeGDBDatabaseDoesNotExist = 3005,                            /*!< Geodatabase Database does not exist exception. */
    AGSErrorCodeGDBNameLongerThan128Characters = 3006,                     /*!< Geodatabase Name longer than 128 characters exception. */
    AGSErrorCodeGDBInvalidShapeType = 3007,                                /*!< Geodatabase Invalid shape type exception. */
    AGSErrorCodeGDBRasterNotSupported = 3008,                              /*!< Geodatabase Raster not supported exception. */
    AGSErrorCodeGDBRelationshipClassOneToOne = 3009,                       /*!< Geodatabase Relationship class one to one exception. */
    AGSErrorCodeGDBItemNotFound = 3010,                                    /*!< Geodatabase Item not found exception. */
    AGSErrorCodeGDBDuplicateCode = 3011,                                   /*!< Geodatabase Duplicate code exception. */
    AGSErrorCodeGDBMissingCode = 3012,                                     /*!< Geodatabase Missing code exception. */
    AGSErrorCodeGDBWrongItemType = 3013,                                   /*!< Geodatabase Wrong item type exception. */
    AGSErrorCodeGDBIDFieldNotNullable = 3014,                              /*!< Geodatabase Id field not nullable exception. */
    AGSErrorCodeGDBDefaultValueNotSupported = 3015,                        /*!< Geodatabase Default value not supported exception. */
    AGSErrorCodeGDBTableNotEditable = 3016,                                /*!< Geodatabase Table not editable exception. */
    AGSErrorCodeGDBFieldNotFound = 3017,                                   /*!< Geodatabase Field not found exception. */
    AGSErrorCodeGDBFieldExists = 3018,                                     /*!< Geodatabase Field exists exception. */
    AGSErrorCodeGDBCannotAlterFieldType = 3019,                            /*!< Geodatabase Cannot alter field type exception. */
    AGSErrorCodeGDBCannotAlterFieldWidth = 3020,                           /*!< Geodatabase Cannot alter field width exception. */
    AGSErrorCodeGDBCannotAlterFieldToNullable = 3021,                      /*!< Geodatabase Cannot alter field to nullable exception. */
    AGSErrorCodeGDBCannotAlterFieldToEditable = 3022,                      /*!< Geodatabase Cannot alter field to editable exception. */
    AGSErrorCodeGDBCannotAlterFieldToDeletable = 3023,                     /*!< Geodatabase Cannot alter field to deletable exception. */
    AGSErrorCodeGDBCannotAlterGeometryProperties = 3024,                   /*!< Geodatabase Cannot alter geometry properties exception. */
    AGSErrorCodeGDBUnnamedTable = 3025,                                    /*!< Geodatabase Unnamed table exception. */
    AGSErrorCodeGDBInvalidTypeForDomain = 3026,                            /*!< Geodatabase Invalid type for domain exception. */
    AGSErrorCodeGDBMinMaxReversed = 3027,                                  /*!< Geodatabase Min max reversed exception. */
    AGSErrorCodeGDBFieldNotSupportedOnRelationshipClass = 3028,            /*!< Geodatabase Field not supported on relationship class exception. */
    AGSErrorCodeGDBRelationshipClassKey = 3029,                            /*!< Geodatabase Relationship class key exception. */
    AGSErrorCodeGDBValueIsNull = 3030,                                     /*!< Geodatabase Value is null exception. */
    AGSErrorCodeGDBMultipleSqlStatements = 3031,                           /*!< Geodatabase Multiple SQL statements exception. */
    AGSErrorCodeGDBNoSqlStatements = 3032,                                 /*!< Geodatabase No SQL statements exception. */
    AGSErrorCodeGDBGeometryFieldMissing = 3033,                            /*!< Geodatabase Geometry field missing exception. */
    AGSErrorCodeGDBTransactionStarted = 3034,                              /*!< Geodatabase Transaction started exception. */
    AGSErrorCodeGDBTransactionNotStarted = 3035,                           /*!< Geodatabase Transaction not started exception. */
    AGSErrorCodeGDBShapeRequiresZ = 3036,                                  /*!< Geodatabase Shape requires z exception. */
    AGSErrorCodeGDBShapeRequiresM = 3037,                                  /*!< Geodatabase shape requires m exception. */
    AGSErrorCodeGDBShapeNoZ = 3038,                                        /*!< Geodatabase Shape no z exception. */
    AGSErrorCodeGDBShapeNoM = 3039,                                        /*!< Geodatabase Shape no m exception. */
    AGSErrorCodeGDBShapeWrongType = 3040,                                  /*!< Geodatabase Shape wrong type exception. */
    AGSErrorCodeGDBCannotUpdateFieldType = 3041,                           /*!< Geodatabase Cannot update field type exception. */
    AGSErrorCodeGDBNoRowsAffected = 3042,                                  /*!< Geodatabase No rows affected exception. */
    AGSErrorCodeGDBSubtypeInvalid = 3043,                                  /*!< Geodatabase Subtype invalid exception. */
    AGSErrorCodeGDBSubtypeMustBeInteger = 3044,                            /*!< Geodatabase Subtype must be integer exception. */
    AGSErrorCodeGDBSubtypesNotEnabled = 3045,                              /*!< Geodatabase Subtypes not enabled exception. */
    AGSErrorCodeGDBSubtypeExists = 3046,                                   /*!< Geodatabase Subtype exists exception. */
    AGSErrorCodeGDBDuplicateFieldNotAllowed = 3047,                        /*!< Geodatabase Duplicate field not allowed exception. */
    AGSErrorCodeGDBCannotDeleteField = 3048,                               /*!< Geodatabase Cannot delete field exception. */
    AGSErrorCodeGDBIndexExists = 3049,                                     /*!< Geodatabase Index exists exception. */
    AGSErrorCodeGDBIndexNotFound = 3050,                                   /*!< Geodatabase Index not found exception. */
    AGSErrorCodeGDBCursorNotOnRow = 3051,                                  /*!< Geodatabase Cursor not on row exception. */
    AGSErrorCodeGDBInternalError = 3052,                                   /*!< Geodatabase Internal error exception. */
    AGSErrorCodeGDBCannotWriteGeodatabaseManagedFields = 3053,             /*!< Geodatabase Cannot write geodatabase managed fields exception. */
    AGSErrorCodeGDBItemAlreadyExists = 3054,                               /*!< Geodatabase Item already exists exception. */
    AGSErrorCodeGDBInvalidSpatialIndexName = 3055,                         /*!< Geodatabase Invalid spatial index name exception. */
    AGSErrorCodeGDBRequiresSpatialIndex = 3056,                            /*!< Geodatabase Requires spatial index exception. */
    AGSErrorCodeGDBReservedName = 3057,                                    /*!< Geodatabase Reserved name exception. */
    AGSErrorCodeGDBCannotUpdateSchemaIfChangeTracking = 3058,              /*!< Geodatabase Cannot update schema if change tracking exception. */
    AGSErrorCodeGDBInvalidDate = 3059,                                     /*!< Geodatabase Invalid date exception. */
    AGSErrorCodeGDBDatabaseDoesNotHaveChanges = 3060,                      /*!< Geodatabase Database does not have changes exception. */
    AGSErrorCodeGDBReplicaDoesNotExist = 3061,                             /*!< Geodatabase Replica does not exists exception. */
    AGSErrorCodeGDBStorageTypeNotSupported = 3062,                         /*!< Geodatabase Storage type not supported exception. */
    AGSErrorCodeGDBReplicaModelError = 3063,                               /*!< Geodatabase Replica model error exception. */
    AGSErrorCodeGDBReplicaClientGenError = 3064,                           /*!< Geodatabase Replica client gen error exception. */
    AGSErrorCodeGDBReplicaNoUploadToAcknowledge = 3065,                    /*!< Geodatabase Replica no upload to acknowledge exception. */
    AGSErrorCodeGDBLastWriteTimeInTheFuture = 3066,                        /*!< Geodatabase Last write time in the future exception. */
    AGSErrorCodeGDBInvalidArgument = 3067,                                 /*!< Geodatabase Invalid argument exception. */
    AGSErrorCodeGDBTransportationNetworkCorrupt = 3068,                    /*!< Geodatabase Transportation network corrupt exception. */
    AGSErrorCodeGDBTransportationNetworkFileIO = 3069,                     /*!< Geodatabase Transportation network file IO exception. */
    AGSErrorCodeGDBFeatureHasPendingEdits = 3070,                          /*!< Geodatabase Feature has pending edits exception. */
    AGSErrorCodeGDBChangeTrackingNotEnabled = 3071,                        /*!< Geodatabase Change tracking not enabled exception. */
    AGSErrorCodeGDBTransportationNetworkFileOpen = 3072,                   /*!< Geodatabase Transportation network file open exception. */
    AGSErrorCodeGDBTransportationNetworkUnsupported = 3073,                /*!< Geodatabase Transportation network unsupported exception. */
    AGSErrorCodeGDBCannotSyncCopy = 3074,                                  /*!< Geodatabase Cannot sync copy exception. */
    AGSErrorCodeGDBAccessControlDenied = 3075,                             /*!< Geodatabase Access control denied exception. */
    AGSErrorCodeGDBGeometryOutsideReplicaExtent = 3076,                    /*!< Geodatabase Geometry outside replica extent exception. */
    AGSErrorCodeGDBUploadAlreadyInProgress = 3077,                         /*!< Geodatabase Upload already in progress exception. */
    AGSErrorCodeGDBDatabaseClosed = 3078,                                  /*!< Geodatabase Database is closed exception. */
    AGSErrorCodeGDBDomainAlreadyExists = 3079,                             /*!< Domain Already Exists exception. */
    AGSErrorCodeGDBGeometryTypeNotSupported = 3080,                        /*!< Geometry type not supported exception. */
    AGSErrorCodeGeocodeUnsupportedFileFormat = 4001,                       /*!< Geocode Unsupported file format exception. */
    AGSErrorCodeGeocodeUnsupportedSpatialReference = 4002,                 /*!< Geocode Unsupported spatial reference exception. */
    AGSErrorCodeGeocodeUnsupportedProjectionTransformation = 4003,         /*!< Geocode Unsupported projection transformation exception. */
    AGSErrorCodeGeocodeGeocoderCreation = 4004,                            /*!< Geocode Geocoder creation exception. */
    AGSErrorCodeGeocodeIntersectionsNotSupported = 4005,                   /*!< Geocode Intersections not supported exception. */
    AGSErrorCodeGeocodeUninitializedGeocodeTask = 4006,                    /*!< Geocode Uninitialized geocode task exception. */
    AGSErrorCodeGeocodeInvalidLocatorProperties = 4007,                    /*!< Geocode Invalid locator properties exception. */
    AGSErrorCodeGeocodeRequiredFieldMissing = 4008,                        /*!< Geocode Required field missing exception. */
    AGSErrorCodeGeocodeCannotReadAddress = 4009,                           /*!< Geocode Cannot read address exception. */
    AGSErrorCodeGeocodeReverseGeocodingNotSupported = 4010,                /*!< Geocode Geocoding not supported exception. */
    AGSErrorCodeNAInvalidRouteSettings = 5001,                             /*!< Network Analyst Invalid route settings exception. */
    AGSErrorCodeNANoSolution = 5002,                                       /*!< Network Analyst No solution exception. */
    AGSErrorCodeNATaskCanceled = 5003,                                     /*!< Network Analyst Task canceled exception. */
    AGSErrorCodeNAInvalidNetwork = 5004,                                   /*!< Network Analyst Invalid network exception. */
    AGSErrorCodeNADirectionsError = 5005,                                  /*!< Network Analyst Directions error exception. */
    AGSErrorCodeNAInsufficientNumberOfStops = 5006,                        /*!< Network Analyst Insufficient number of stops exception. */
    AGSErrorCodeNAStopUnlocated = 5007,                                    /*!< Network Analyst Stop unlocated exception. */
    AGSErrorCodeNAStopLocatedOnNonTraversableElement = 5008,               /*!< Network Analyst Stop located on non traversable element exception. */
    AGSErrorCodeNAPointBarrierInvalidAddedCostAttributeName = 5009,        /*!< Network Analyst Point barrier invalid added cost attribute name exception. */
    AGSErrorCodeNALineBarrierInvalidScaledCostAttributeName = 5010,        /*!< Network Analyst Line barrier invalid scaled cost attribute name exception. */
    AGSErrorCodeNAPolygonBarrierInvalidScaledCostAttributeName = 5011,     /*!< Network Analyst Polygon barrier invalid scaled cost attribute name. */
    AGSErrorCodeNAPolygonBarrierInvalidScaledCostAttributeValue = 5012,    /*!< Network Analyst Polygon barrier invalid scaled cost attribute value. */
    AGSErrorCodeNAPolylineBarrierInvalidScaledCostAttributeValue = 5013,   /*!< Network Analyst Polyline barrier invalid scaled cost attribute value. */
    AGSErrorCodeNAInvalidImpedanceAttribute = 5014,                        /*!< Network Analyst Invalid impedance attribute exception. */
    AGSErrorCodeNAInvalidRestrictionAttribute = 5015,                      /*!< Network Analyst Invalid restriction attribute exception. */
    AGSErrorCodeNAInvalidAccumulateAttribute = 5016,                       /*!< Network Analyst Invalid accumulate attribute exception. */
    AGSErrorCodeNAInvalidDirectionsTimeAttribute = 5017,                   /*!< Network Analyst Invalid directions time attribute exception. */
    AGSErrorCodeNAInvalidDirectionsDistanceAttribute = 5018,               /*!< Network Analyst Invalid directions distance attribute exception. */
    AGSErrorCodeNAInvalidAttributeParametersAttributeName = 5019,          /*!< Network Analyst Invalid attribute parameters attribute name exception. */
    AGSErrorCodeNAInvalidAttributeParametersParameterName = 5020,          /*!< Network Analyst Invalid attributes parameters parameter name exception. */
    AGSErrorCodeNAInvalidAttributeParametersValueType __deprecated_msg("No longer used.") = 5021, /*!< Network Analyst Invalid attributes parameters value type exception. */
    AGSErrorCodeNAInvalidAttributeParametersRestrictionUsageValue = 5022,  /*!< Network Analyst Invalid attribute parameters restriction usage value exception. */
    AGSErrorCodeNANetworkHasNoHierarchyAttribute = 5023,                   /*!< Network Analyst Network has no hierarchy attribute exception. */
    AGSErrorCodeNANoPathFoundBetweenStops = 5024,                          /*!< Network Analyst No path found between stops exception. */
    AGSErrorCodeNAUndefinedInputSpatialReference __deprecated_msg("No longer used.") = 5025, /*!< Network Analyst Undefined input spatial reference exception. */
    AGSErrorCodeNAUndefinedOutputSpatialReference = 5026,                  /*!< Network Analyst Undefined output spatial reference exception. */
    AGSErrorCodeNAInvalidDirectionsStyle __deprecated_msg("No longer used.") = 5027, /*!< Network Analyst Invalid directions style exception. */
    AGSErrorCodeNAInvalidDirectionsLanguage = 5028,                        /*!< Deprecated. Network Analyst Invalid directions language exception. */
    AGSErrorCodeNADirectionsTimeAndImpedanceAttributeMismatch = 5029,      /*!< Network Analyst Directions time and impedance attribute mismatch exception. */
    AGSErrorCodeNAInvalidDirectionsRoadClassAttribute = 5030,              /*!< Network Analyst Invalid directions road class attribute exception. */
    AGSErrorCodeNAStopIsUnreachable = 5031,                                /*!< Network Analyst Stop can not be reached. */
    AGSErrorCodeNAStopTimeWindowStartsBeforeUnixEpoch = 5032,              /*!< Network Analyst Stop time window starts before unix epoch exception. */
    AGSErrorCodeNAStopTimeWindowIsInverted = 5033,                         /*!< Network Analyst Stop time window is inverted exception. */
    AGSErrorCodeNAWalkingModeRouteTooLarge = 5034,                         /*!< Walking mode route too large exception. */
    AGSErrorCodeNAStopHasNullGeometry = 5035,                              /*!< Stop has null geometry exception. */
    AGSErrorCodeNAPointBarrierHasNullGeometry = 5036,                      /*!< Point barrier has null geometry exception. */
    AGSErrorCodeNAPolylineBarrierHasNullGeometry = 5037,                   /*!< Polyline barrier has null geometry exception. */
    AGSErrorCodeNAPolygonBarrierHasNullGeometry = 5038,                    /*!< Polygon barrier has null geometry. */
    AGSErrorCodeNAUnsupportedSearchWhereClause = 5039,                     /*!< Online route task does not support search_where_clause condition exception. */
    AGSErrorCodeNAInsufficientNumberOfFacilities = 5040,                   /*!< Network Analyst Insufficient number of facilities exception. */
    AGSErrorCodeNAFacilityHasNullGeometry = 5041,                          /*!< Network Analyst Facility has null geometry exception. */
    AGSErrorCodeNAFacilityHasInvalidAddedCostAttributeName = 5042,         /*!< Network Analyst Facility has invalid added cost attribute name exception. */
    AGSErrorCodeNAFacilityHasNegativeAddedCostAttribute = 5043,            /*!< Network Analyst Facility has negative added cost attribute exception. */
    AGSErrorCodeNAFacilityHasInvalidImpedanceCutoff = 5044,                /*!< Network Analyst Facility has invalid impedance cutoff exception. */
    AGSErrorCodeNAInsufficientNumberOfIncidents = 5045,                    /*!< Network Analyst insufficient number of incidents exception. */
    AGSErrorCodeNAIncidentHasNullGeometry = 5046,                          /*!< Network Analyst Incident has null geometry exception. */
    AGSErrorCodeNAIncidentHasInvalidAddedCostAttributeName = 5047,         /*!< Network Analyst Incident has invalid added cost attribute name exception. */
    AGSErrorCodeNAIncidentHasNegativeAddedCostAttribute = 5048,            /*!< Network Analyst Incident has negative added cost attribute exception. */
    AGSErrorCodeNAInvalidTargetFacilityCount = 5049,                       /*!< Network Analyst Invalid target facility count exception. */
    AGSErrorCodeNAIncidentHasInvalidImpedanceCutoff = 5050,                /*!< Network Analyst Incident has invalid impedance cutoff exception. */
    AGSErrorCodeNAStartTimeIsBeforeUnixEpoch = 5051,                       /*!< Network Analyst start time is before Unix epoch exception. */
    AGSErrorCodeNAInvalidDefaultImpedanceCutoff = 5052,                    /*!< Network Analyst Invalid default impedance cutoff exception. */
    AGSErrorCodeNAInvalidDefaultTargetFacilityCount = 5053,                /*!< Network Analyst Invalid default target facility count exception. */
    AGSErrorCodeNAInvalidPolygonBufferDistance = 5054,                     /*!< Network Analyst Invalid polygon buffer distance exception. */
    AGSErrorCodeNAPolylinesCannotBeReturned = 5055,                        /*!< Network Analyst Polylines cannot be returned. */
    AGSErrorCodeNATimeWindowsWithNonTimeImpedance = 5056,                  /*!< Network Analyst Solving non time impedance, but time windows applied. */
    AGSErrorCodeNAUnsupportedStopType = 5057,                              /*!< One or more stops have unsupported type. */
    AGSErrorCodeNARouteStartsOrEndsOnWaypoint = 5058,                      /*!< Network Analyst Route starts or ends on a waypoint. */
    AGSErrorCodeNAWaypointsAndRestBreaksForbiddenReordering = 5059,        /*!< Network Analyst Reordering stops (Traveling Salesman Problem) is not supported when the collection of stops contains waypoints or rest breaks. */
    AGSErrorCodeNAWaypointHasTimeWindows = 5060,                           /*!< Network Analyst The waypoint contains time windows. */
    AGSErrorCodeNAWaypointHasAddedCostAttribute = 5061,                    /*!< Network Analyst The waypoint contains added cost attribute. */
    AGSErrorCodeNAStopHasInvalidCurbApproach = 5062,                       /*!< Network Analyst The stop has unknown curb approach. */
    AGSErrorCodeNAPointBarrierHasInvalidCurbApproach = 5063,               /*!< Network Analyst The point barrier has unknown curb approach. */
    AGSErrorCodeNAFacilityHasInvalidCurbApproach = 5064,                   /*!< Network Analyst The facility has unknown curb approach. */
    AGSErrorCodeNAIncidentHasInvalidCurbApproach = 5065,                   /*!< Network Analyst The incident has unknown curb approach. */
    AGSErrorCodeNANetworkDoesNotSupportDirections = 5066,                  /*!< Network dataset has no directions attributes. */
    AGSErrorCodeNADirectionsLanguageNotFound = 5067,                       /*!< Desired direction language not supported by platform. */
    AGSErrorCodeJSONParserInvalidToken = 6001,                             /*!< JSON parser invalid token exception. */
    AGSErrorCodeJSONParserInvalidCharacter = 6002,                         /*!< JSON parser invalid character exception. */
    AGSErrorCodeJSONParserInvalidUnicode = 6003,                           /*!< JSON parser invalid unicode exception. */
    AGSErrorCodeJSONParserInvalidJSONStart = 6004,                         /*!< JSON parser invalid start of JSON exception. */
    AGSErrorCodeJSONParserInvalidEndOfPair = 6005,                         /*!< JSON parser invalid end of pair exception. */
    AGSErrorCodeJSONParserInvalidEndOfElement = 6006,                      /*!< JSON parser invalid end of element exception. */
    AGSErrorCodeJSONParserInvalidEscapeSequence = 6007,                    /*!< JSON parser invalid escape sequence exception. */
    AGSErrorCodeJSONParserInvalidEndOfFieldName = 6008,                    /*!< JSON parser invalid end of field name exception. */
    AGSErrorCodeJSONParserInvalidStartOfFieldName = 6009,                  /*!< JSON parser invalid start of field name exception. */
    AGSErrorCodeJSONParserInvalidStartOfComment = 6010,                    /*!< JSON parser invalid start of comment exception. */
    AGSErrorCodeJSONParserInvalidDecDigit = 6011,                          /*!< JSON parser invalid decimal digit exception. */
    AGSErrorCodeJSONParserInvalidHexDigit = 6012,                          /*!< JSON parser invalid hex digit. */
    AGSErrorCodeJSONParserExpectingNull = 6013,                            /*!< JSON parser expecting null exception. */
    AGSErrorCodeJSONParserExpectingTrue = 6014,                            /*!< JSON parser expecting true exception. */
    AGSErrorCodeJSONParserExpectingFalse = 6015,                           /*!< JSON parser expecting false exception. */
    AGSErrorCodeJSONParserExpectingClosingQuote = 6016,                    /*!< JSON parser expecting closing quote exception. */
    AGSErrorCodeJSONParserExpectingNan = 6017,                             /*!< JSON parser expecting not a number exception. */
    AGSErrorCodeJSONParserExpectingEndOfComment = 6018,                    /*!< JSON parser expecting end of comment exception. */
    AGSErrorCodeJSONParserUnexpectedEndOfData = 6019,                      /*!< JSON parser unexpected end of data exception. */
    AGSErrorCodeJSONObjectExpectingStartObject = 6020,                     /*!< JSON object expecting start object exception. */
    AGSErrorCodeJSONObjectExpectingStartArray = 6021,                      /*!< JSON object expecting start array exception. */
    AGSErrorCodeJSONObjectExpectingValueObject = 6022,                     /*!< JSON object expecting value object exception. */
    AGSErrorCodeJSONObjectExpectingValueArray = 6023,                      /*!< JSON object expecting value array exception. */
    AGSErrorCodeJSONObjectExpectingValueInt32 = 6024,                      /*!< JSON object expecting value int32 exception. */
    AGSErrorCodeJSONObjectExpectingIntegerType = 6025,                     /*!< JSON object expecting integer type exception. */
    AGSErrorCodeJSONObjectExpectingNumberType = 6026,                      /*!< JSON object expecting number type exception. */
    AGSErrorCodeJSONObjectExpectingValueString = 6027,                     /*!< JSON object expecting value string exception. */
    AGSErrorCodeJSONObjectExpectingValueBool = 6028,                       /*!< JSON object expecting value bool exception. */
    AGSErrorCodeJSONObjectIteratorNotStarted = 6029,                       /*!< JSON object iterator not started exception. */
    AGSErrorCodeJSONObjectIteratorIsFinished = 6030,                       /*!< JSON object iterator is finished exception. */
    AGSErrorCodeJSONObjectKeyNotFound = 6031,                              /*!< JSON object key not found exception. */
    AGSErrorCodeJSONObjectIndexOutOfRange = 6032,                          /*!< JSON object index out of range exception. */
    AGSErrorCodeJSONStringWriterJSONIsComplete = 6033,                     /*!< JSON string writer JSON is complete exception. */
    AGSErrorCodeJSONStringWriterInvalidJSONInput = 6034,                   /*!< JSON string writer invalid JSON input exception. */
    AGSErrorCodeJSONStringWriterExpectingContainer = 6035,                 /*!< JSON string writer expecting container exception. */
    AGSErrorCodeJSONStringWriterExpectingKeyOrEndObject = 6036,            /*!< JSON string writer expecting key or end object exception. */
    AGSErrorCodeJSONStringWriterExpectingValueOrEndArray = 6037,           /*!< JSON string writer expecting value or end array exception. */
    AGSErrorCodeJSONStringWriterExpectingValue = 6038,                     /*!< JSON string writer expecting value exception. */
    AGSErrorCodeMappingMissingSpatialReference = 7001,                     /*!< The spatial reference is missing. */
    AGSErrorCodeMappingMissingInitialViewpoint = 7002,                     /*!< The initial viewpoint is missing. */
    AGSErrorCodeMappingInvalidResponse = 7003,                             /*!< Expected a different response to the request. */
    AGSErrorCodeMappingMissingBingMapsKey = 7004,                          /*!< The Bing maps key is missing. */
    AGSErrorCodeMappingUnsupportedLayerType = 7005,                        /*!< The layer type is not supported. */
    AGSErrorCodeMappingSyncNotEnabled = 7006,                              /*!< Cannot sync because it is not enabled. */
    AGSErrorCodeMappingTileExportNotEnabled = 7007,                        /*!< Cannot export tiles because it is not enabled. */
    AGSErrorCodeMappingMissingItemProperty = 7008,                         /*!< Required item property is missing. */
    AGSErrorCodeMappingWebmapNotSupported = 7009,                          /*!< Webmap version is not supported. */
    AGSErrorCodeMappingSpatialReferenceInvalid = 7011,                     /*!< Spatial Reference invalid or incompatible */
    AGSErrorCodeMappingPackageUnpackRequired = 7012,                       /*!< The package needs to be unpacked before it can be used. */
    AGSErrorCodeMappingUnsupportedElevationFormat = 7013,                  /*!< The elevation source data format is not supported. */
    AGSErrorCodeMappingWebsceneNotSupported = 7014,                        /*!< Webscene version or viewing mode is not supported. */
    AGSErrorCodeMappingNotLoaded = 7015,                                   /*!< Loadable object is not loaded when it is expected to be loaded. */
    AGSErrorCodeMappingScheduledUpdatesNotSupported = 7016,                /*!< Scheduled Updates for an offline preplanned map area, are not supported. */
    AGSErrorCodeLicensingUnlicensedFeature = 8001,                         /*!< Unlicensed feature exception. */
    AGSErrorCodeLicensingLicenseLevelFixed = 8002,                         /*!< License level fixed exception. */
    AGSErrorCodeLicensingLicenseLevelAlreadySet = 8003,                    /*!< License level is already set exception. */
    AGSErrorCodeLicensingMainLicenseNotSet = 8004,                         /*!< Main license is not set exception. */
    AGSErrorCodeLicensingUnlicensedExtension = 8005,                       /*!< Unlicensed extension exception. */
    AGSErrorCodeLocalServerServerFailedToStart = 9001,                     /*!< Local server failed to start. */
    AGSErrorCodeLocalServerServiceFailedToStart = 9002,                    /*!< A local server's service failed to start. */
    AGSErrorCodeLocalServerServiceTerminatedUnexpectedly = 9003,           /*!< A local server's service terminated unexpectedly. */
    AGSErrorCodeLocalServerServerFailed = 9004,                            /*!< The local server has failed. */
    AGSErrorCodeLocalServerServiceFailed = 9005,                           /*!< A local server's service has failed. */
    AGSErrorCodeStdIosBaseFailure = 10001,                                 /*!< std::ios_base::failure exception. */
    AGSErrorCodeStdBadArrayNewLength = 10002,                              /*!< std::bad_array_new_length exception. */
    AGSErrorCodeStdUnderflowError = 10003,                                 /*!< std::underflow_error exception. */
    AGSErrorCodeStdSystemError = 10004,                                    /*!< std::system_error exception. */
    AGSErrorCodeStdRangeError = 10005,                                     /*!< std::range_error exception. */
    AGSErrorCodeStdOverflowError = 10006,                                  /*!< std::overflow_error exception. */
    AGSErrorCodeStdOutOfRange = 10007,                                     /*!< std::out_of_range exception. */
    AGSErrorCodeStdLengthError = 10008,                                    /*!< std::length_error exception. */
    AGSErrorCodeStdInvalidArgument = 10009,                                /*!< std::invalid_argument exception. */
    AGSErrorCodeStdFutureError = 10010,                                    /*!< std::future_error exception. */
    AGSErrorCodeStdDomainError = 10011,                                    /*!< std::domain_error exception. */
    AGSErrorCodeStdRuntimeError = 10012,                                   /*!< std::runtime_error exception. */
    AGSErrorCodeStdLogicError = 10013,                                     /*!< std::logic_error exception. */
    AGSErrorCodeStdBadWeakPtr = 10014,                                     /*!< std::bad_weak_ptr exception. */
    AGSErrorCodeStdBadTypeId = 10015,                                      /*!< std::bad_typeid exception. */
    AGSErrorCodeStdBadFunctionCall = 10016,                                /*!< std::bad_function_call exception. */
    AGSErrorCodeStdBadException = 10017,                                   /*!< std::bad_exception exception. */
    AGSErrorCodeStdBadCast = 10018,                                        /*!< std::bad_cast exception. */
    AGSErrorCodeStdBadAlloc = 10019,                                       /*!< std::bad_alloc exception. */
    AGSErrorCodeStdException = 10020,                                      /*!< std::exception exception. */
    AGSErrorCodeNavigationReroutingNotSupportedByService = 13001           /*!< Service doesn't support rerouting. */
};


/** Constant representing domain for popup validation errors.
 @since 100
 */
AGS_EXTERN NSString *const AGSPopupValidationErrorDomain;

/** Constants representing popup validation error codes. These errors belongs to @c #AGSPopupValidationErrorDomain.
 @since 100
 */
typedef NS_ENUM(NSInteger, AGSPopupValidationErrorCode) {
    AGSPopupValidationErrorCodeInvalidGeoElement = 11000,          /*!< Invalid attributes or geometry on the associated AGSGeoElement */
    AGSPopupValidationErrorCodeNullNotAllowed = 11001,          /*!< Null value not allowed */
    AGSPopupValidationErrorCodeValueOutOfRange = 11002,         /*!< Value out of range domain. */
    AGSPopupValidationErrorExceedsMaxLength = 11003,             /*!< Value exceeds maximum field length */
    AGSPopupValidationErrorInvalidNumericString = 11004,             /*!< String input could not be parsed into the numeric value required by the field */
    AGSPopupValidationErrorExceedsNumericMaximum = 11005,             /*!< Numeric field value greater than the field type allows */
    AGSPopupValidationErrorLessThanNumericMinimum = 11006,             /*!< Numeric field value less than the field type allows */
    AGSPopupValidationErrorIllegalDomainCode = 11007                   /*!< Coded value domain code is not defined in the domain list. */
};

/** The edit state of attachments in an AGSPopupAttachmentManager.
 @since 100
 */
typedef NS_ENUM(NSInteger, AGSPopupAttachmentEditState) {
    AGSPopupAttachmentEditStateOriginal = 0,    /*!<  */
    AGSPopupAttachmentEditStateAdded,           /*!<  */
    AGSPopupAttachmentEditStateDeleted,         /*!<  */
};

/** The type of attachments in an AGSPopupAttachmentManager.
 @since 100
 */
typedef NS_ENUM(NSInteger, AGSPopupAttachmentType) {
    AGSPopupAttachmentTypeImage = 0,/*!< Any attachment whose MIME type contains 'image' prefix */
    AGSPopupAttachmentTypeVideo,    /*!< Any attachment whose MIME type contains 'video' prefix */
    AGSPopupAttachmentTypeDocument, /*!< PDF, MS-Word, MS-Excel, MS-Powerpoint, HTML */
    AGSPopupAttachmentTypeOther     /*!< */
};

/** Size options for an image attachment
 @since 100
 */
typedef NS_ENUM(NSInteger, AGSPopupAttachmentSize) {
    AGSPopupAttachmentSizeActual = 0,        /*!<  full resolution */
    AGSPopupAttachmentSizeSmall,             /*!<  240x320 */
    AGSPopupAttachmentSizeMedium,            /*!<  480x640 */
    AGSPopupAttachmentSizeLarge,             /*!<  960x1280 */
    AGSPopupAttachmentSizeExtraLarge,        /*!<  1126x1500 */
};

typedef NS_ENUM(NSInteger, AGSPopupExpressionReturnType) {
    AGSPopupExpressionReturnTypeString = 0,
    AGSPopupExpressionReturnTypeNumber = 1
};

#pragma mark - Portal

/** The type of content represented by the local item
 @since 100
 */
typedef NS_ENUM(NSInteger, AGSLocalItemType) {
    AGSLocalItemTypeUnknown = -1,           /*!<  */
    AGSLocalItemTypeMobileMap = 0,          /*!<  */
    AGSLocalItemTypeMobileScene = 1,        /*!<  */
    AGSLocalItemTypeMobileMapPackage = 2,   /*!<  */
    AGSLocalItemTypeMobileScenePackage = 3  /*!<  */
};

/** The type of content represented by the portal item.
 @since 100
 */
typedef NS_ENUM(NSInteger, AGSPortalItemType) {
    AGSPortalItemTypeUnknown = -1,                                      /*!< An unknown portal item type. */
    AGSPortalItemTypeArcGISProAddIn = 0,                                /*!< A Pro Add In (esriaddinx). */
    AGSPortalItemTypeArcPadPackage = 1,                                 /*!< An ArcPad Package (zip). */
    AGSPortalItemTypeCADDrawing = 2,                                    /*!< A Computer-Aided Design drawing comprised of a single file. */
    AGSPortalItemTypeCSV = 3,                                           /*!< A text file of data values separated by commas or other delimiters. Can be published as a feature service using the Portal API Publish call. */
    AGSPortalItemTypeCityEngineWebScene = 4,			                /*!< A city engine web scene. */
    AGSPortalItemTypeCodeAttachment = 5,				                /*!< The sample code associated with an application whose purpose is code sample. */
    AGSPortalItemTypeCodeSample = 6,					                /*!< A code sample. */
    AGSPortalItemTypeColorSet = 7,                                      /*!< A color set. */
    AGSPortalItemTypeDesktopAddIn = 8,                                  /*!< An ArcGIS Add-In (esriaddin). */
    AGSPortalItemTypeDesktopApplication = 9,                            /*!< A Desktop Application (zip). */
    AGSPortalItemTypeDesktopApplicationTemplate = 10,                   /*!< A Desktop Application Template (zip). Includes either a desktop add in or a toolbar.exe. */
    AGSPortalItemTypeDesktopStyle = 11,                                 /*!< An ArcGIS Pro Style file (stylx). */
    AGSPortalItemTypeDocumentLink = 12,                                 /*!< A link to a web resource. */
    AGSPortalItemTypeExplorerAddIn = 13,				                /*!< An ArcGIS Explorer Layer (eaz). */
    AGSPortalItemTypeExplorerLayer = 14,				                /*!< An ArcGIS Explorer Layer (nmc). */
    AGSPortalItemTypeExplorerMap = 15,                                  /*!< An ArcGIS Explorer Document (nmf). */
    AGSPortalItemTypeFeatureCollection = 16,			                /*!< A feature collection, which is a saved web map feature layer with layer definition (types, symbols, fields, and so on) and a feature set (the actual features). */
    AGSPortalItemTypeFeatureCollectionTemplate = 17,	                /*!< A feature collection that includes the layer definition component. */
    AGSPortalItemTypeFeatureService = 18,                               /*!< A feature service.
                                                                         
                                                                         The URL to the service is stored in the @c serviceURL property of the item (see @c AGSPortalItem).
                                                                         Optional JSON data contains overridden service properties. */
    AGSPortalItemTypeFileGeodatabase = 19,                              /*!< A File Geodatabase. */
    AGSPortalItemTypeForm = 20,                                         /*!< A form authored using Survey123. */
    AGSPortalItemTypeGeocodingService = 21,                             /*!< An ArcGIS Server Geocoding Service.
                                                                         
                                                                         The URL to the service is stored in the @c serviceURL property of the item (see @c AGSPortalItem). */
    AGSPortalItemTypeGeodataService = 22,                               /*!< An ArcGIS Server Geodata Service.
                                                                         
                                                                         The URL to the service is stored in the @c serviceURL property of the item (see @c AGSPortalItem). */
    AGSPortalItemTypeGeometryService = 23,                              /*!< ArcGIS Server Geometry Service
                                                                         
                                                                         The URL to the service is stored in the @c serviceURL property of the item (see @c AGSPortalItem). */
    AGSPortalItemTypeGeoprocessingPackage = 24,                         /*!< An ArcGIS Geoprocessing Package (gpk). */
    AGSPortalItemTypeGeoprocessingPackageProVersion = 25,               /*!< An ArcGIS Pro Geoprocessing Package (gpkx) */
    AGSPortalItemTypeGeoprocessingSample = 26,                          /*!< A Geoprocessing Sample. */
    AGSPortalItemTypeGeoprocessingService = 27,                         /*!< An ArcGIS Server Geoprocessing Service.
                                                                         
                                                                         The URL to the service is stored in the @c serviceURL property of the item (see @c AGSPortalItem). */
    AGSPortalItemTypeGlobeDocument = 28,				                /*!< An ArcGlobe Document (3dd) */
    AGSPortalItemTypeGlobeService = 29,                                 /*!< An ArcGIS Server Globe Service.
                                                                         
                                                                         The URL to the service is stored in the @c serviceURL property of the item (see @c AGSPortalItem). */
    AGSPortalItemTypeImage = 30,						                /*!< An image (.jpg, .jpeg, .tif, .tiff, .png). */
    AGSPortalItemTypeImageCollection = 31,                              /*!< A portable file that contains one or more images that can be published as an image service for imagery visualization and analysis. */
    AGSPortalItemTypeImageService = 32,                                 /*!< An image service.
                                                                         
                                                                         The URL to the service is stored in the @c serviceURL property of the item (see @c AGSPortalItem).
                                                                         Optional JSON data contains overridden service properties. */
    AGSPortalItemTypeInsightsModel = 33,                                /*!< An Insights model records analysis steps on an insights pages, including adding and joining datasets, spatial analysis, data analytics and styling. */
    AGSPortalItemTypeInsightsPage = 34,                                 /*!< An Insights Page resides in Insights Workbook, used to connect the data and analyze related content and themes with interactive visualization. */
    AGSPortalItemTypeInsightsWorkbook = 35,                             /*!< Insights Workbook collects or associates all data and analytical activity for a project,
                                                                         capturing and maintaining relationships such as data locations and storing result layers, models, pages and cards. */
    AGSPortalItemTypeiWorkKeynote NS_SWIFT_NAME(iWorkKeynote) = 36,     /*!< An iWork Keynote file (.key). */
    AGSPortalItemTypeiWorkNumbers NS_SWIFT_NAME(iWorkNumbers) = 37,     /*!< An iWork Numbers file (.numbers). */
    AGSPortalItemTypeiWorkPages NS_SWIFT_NAME(iWorkPages) = 38,         /*!< An iWork Pages file (.pages) */
    AGSPortalItemTypeIWorkKeynote __deprecated_enum_msg("Please use AGSPortalItemTypeiWorkKeynote. Available at 100.3") = AGSPortalItemTypeiWorkKeynote,   /*!<  */
    AGSPortalItemTypeIWorkNumbers __deprecated_enum_msg("Please use AGSPortalItemTypeiWorkNumbers. Available at 100.3") = AGSPortalItemTypeiWorkNumbers,   /*!<  */
    AGSPortalItemTypeIWorkPages __deprecated_enum_msg("Please use AGSPortalItemTypeiWorkPages. Available at 100.3") = AGSPortalItemTypeiWorkPages,         /*!<  */
    AGSPortalItemTypeKML = 39,                                          /*!< KML Network Link or KML file.
                                                                         
                                                                         If a file, then the data resource retrieves the file and can be used as a network link.
                                                                         If a network link, then the @c serviceURL property of the item (see @c AGSPortalItem) contains the URL for the network link. */
    AGSPortalItemTypeKMLCollection = 40,                                /*!< A zip file containing a collection of KML/KMZ files. */
    AGSPortalItemTypeLayer = 41,						                /*!< A Layer File (lyr) or ArcGIS Pro layer file (lyrx). */
    AGSPortalItemTypeLayerPackage = 42,                                 /*!< A Layer Package (lpk). */
    AGSPortalItemTypeLayout = 43,                                       /*!< An ArcGIS Pro Layout File (pagx). */
    AGSPortalItemTypeLocatorPackage = 44,                               /*!< An ArcGIS Locator Package (gcpk). */
    AGSPortalItemTypeMapDocument = 45,                                  /*!< An ArcMap Document (mxd). */
    AGSPortalItemTypeMapPackage = 46,                                   /*!< An ArcGIS Map Package (mpk). */
    AGSPortalItemTypeMapService = 47,                                   /*!< A map service.
                                                                         
                                                                         The URL to the service is stored in the @c serviceURL property of the item (see @c AGSPortalItem).
                                                                         Optional JSON data contains overridden service properties. */
    AGSPortalItemTypeMapTemplate = 48,                                  /*!< A Map Template (.zip) contains documentation, a map, and GDB folder. */
    AGSPortalItemTypeMicrosoftExcel = 49,                               /*!< A Microsoft Excel Document (.xls, .xlsx). */
    AGSPortalItemTypeMicrosoftPowerpoint = 50,                          /*!< A Microsoft Powerpoint (.ppt, .pptx). */
    AGSPortalItemTypeMicrosoftWord = 51,                                /*!< A Microsoft Word Document (.doc, .docx). */
    AGSPortalItemTypeMobileApplication = 52,                            /*!< A mobile application.
                                                                         
                                                                         The URL to the application in the app store is stored in the @c serviceURL property of the item (see @c AGSPortalItem). */
    AGSPortalItemTypeMobileBasemapPackage = 53,                         /*!< An ArcGIS Mobile Basemap Package (.bpk). */
    AGSPortalItemTypeMobileMapPackage = 54,                             /*!< An ArcGIS Mobile Map Package (mmpk). */
    AGSPortalItemTypeNativeApplication = 55,                            /*!< A Native Application for AppStudio for ArcGIS. */
    AGSPortalItemTypeNativeApplicationInstaller = 56,                   /*!< A Native Application Installer for AppStudio for ArcGIS. */
    AGSPortalItemTypeNativeApplicationTemplate = 57,                    /*!< A Native Application Template for AppStudio for ArcGIS. */
    AGSPortalItemTypeNetCDF = 58,                                       /*!< A Self-describing, portable and scalable file format for storing multidimensional scientific data usable by GeoAnalytics tools. */
    AGSPortalItemTypeNetworkAnalysisService = 59,                       /*!< An ArcGIS Server Network Analyst Service.
                                                                         
                                                                         The URL to the service is stored in the @c serviceURL property of the item (see @c AGSPortalItem). */
    AGSPortalItemTypeOperationView = 60,                                /*!< An operation view. */
    AGSPortalItemTypeOperationsDashboardAddIn = 61,                     /*!< An ArcGIS Operations Dashboard Add In (opdashboardaddin). */
    AGSPortalItemTypeOperationsDashboardExtension = 62,                 /*!< An ArcGIS Operations Dashboard Extension. */
    AGSPortalItemTypePDF = 63,                                          /*!< A Portable Document Format (.pdf). */
    AGSPortalItemTypeProjectPackage = 64,                               /*!< An ArcGIS Project Package (ppkx). */
    AGSPortalItemTypeProjectTemplate = 65,                              /*!< An ArcGIS Project Template (aptx). */
    AGSPortalItemTypeProMap = 66,                                       /*!< An ArcGIS Pro map file (mapx). */
    AGSPortalItemTypePublishedMap = 67,                                 /*!< An ArcReader Document (pmf). */
    AGSPortalItemTypeRasterFunctionTemplate = 68,                       /*!< An ArcGIS Pro raster function template. */
    AGSPortalItemTypeRelationalDatabaseConnection = 69,                 /*!< An ArcGIS server relational catalog service. Item represents a connection to a database for the purpose of viewing, querying, and analyzing its contents. */
    AGSPortalItemTypeReportTemplate = 70,                               /*!< A report template used by the geoenrichment service and Business Analyst applications to generate custom reports. */
    AGSPortalItemTypeRulePackage = 71,                                  /*!< An ArcGIS Rule Package (lpk). */
    AGSPortalItemTypeSceneDocument = 72,				                /*!< An ArcScene Document (sxd). */
    AGSPortalItemTypeScenePackage = 73,                                 /*!< A Scene Layer Package (spk or slpk). */
    AGSPortalItemTypeSceneService = 74,                                 /*!< Cached web layers that are optimized for displaying a large amount of 2D or 3D features. */
    AGSPortalItemTypeServiceDefinition = 75,			                /*!< A Service Definition that can be published to create a geo spatial web service using the Portal API Publish call. */
    AGSPortalItemTypeShapeFile = 76,					                /*!< A shape file. */
    AGSPortalItemTypeStatisticalDataCollection = 77,                    /*!< Data collection used by the geoenrichment service and Business Analyst applications to perform data aggregation with statistical feature data. */
    AGSPortalItemTypeSymbolSet = 78,					                /*!< A symbol set. */
    AGSPortalItemTypeTaskFile = 79,                                     /*!< An ArcGIS Task File (esriTasks). */
    AGSPortalItemTypeTilePackage = 80,                                  /*!< A Tile Package (tpk). */
    AGSPortalItemTypeVectorTilePackage = 81,			                /*!< A Vector Tile Package (.vtpk). */
    AGSPortalItemTypeVectorTileService = 82,			                /*!< A Vector Tile Service.
                                                                         
                                                                         The URL to the service is stored in the @c serviceURL property of the item (see @c AGSPortalItem). */
    AGSPortalItemTypeVisioDocument = 83,				                /*!< A Visio Document (.vsd). */
    AGSPortalItemTypeVr360Experience = 84,                              /*!< A A 360-degrees virtual reality experience that lets you explore a set of connected geo-located panoramic 3D visualizations or photos. */
    AGSPortalItemTypeWFS = 85,                                          /*!< An OGC Web Feature Service.
                                                                         
                                                                         The URL of the service is stored in the @c serviceURL property of the item (see @c AGSPortalItem). */
    AGSPortalItemTypeWMS = 86,                                          /*!< An OGC Web Map Service.
                                                                         
                                                                         The URL to the service is stored in the @c serviceURL property of the item (see @c AGSPortalItem). */
    AGSPortalItemTypeWMTS = 87,                                         /*!< A WMTS Service. */
    AGSPortalItemTypeWebMap = 88,                                       /*!< A web map. */
    AGSPortalItemTypeWebMappingApplication = 89,		                /*!< A web mapping application. */
    AGSPortalItemTypeWebScene = 90,                                     /*!< A web scene. */
    AGSPortalItemTypeWindowsMobilePackage = 91,                         /*!< A windows mobile package. */
    AGSPortalItemTypeWorkflowManagerPackage = 92,                       /*!< A Workflow Manager Package (wpk). */
    AGSPortalItemTypeWorkflowManagerService = 93,                       /*!< An ArcGIS Server Workflow Manager Service. */
    AGSPortalItemTypeWorkforceProject = 94,                             /*!< A Workforce Project. */
    AGSPortalItemTypeSQLiteGeodatabase = 95,                            /*!< A runtime sqlite geodatabase. */
    AGSPortalItemTypeMapArea = 96,                                      /*!< A map area defining a preplanned offline map. */
    AGSPortalItemTypeHubInitiative = 97,                                /*!< Initiatives organize data and tools with an organization goal. */
    AGSPortalItemTypeHubSiteApplication = 98,                           /*!< A customizable website that provides a focused view of an organization's items. */
    AGSPortalItemTypeHubPage = 99,                                      /*!< Hub pages provide web site pages to market Hub Initiatives and provide chart and app content. */
    AGSPortalItemTypeAppBuilderExtension = 100,                         /*!< An AppBuilder Extension for Web AppBuilder for ArcGIS.
                                                                         
                                                                         URL that references custom widgets for use in Web AppBuilder apps within Portal. */
    AGSPortalItemTypeAppBuilderWidgetPackage = 101,                     /*!< AppBuilder Widget Package for Web AppBuilder for ArcGIS.
                                                                         
                                                                         Custom widget in a zip file that can be downloaded for use in Web AppBuilder Developer Edition or Portal for ArcGIS 10.5.1 or above. */
    AGSPortalItemTypeDashboard = 102,                                   /*!< Dashboards integrate maps, lists, charts, and gauges that help monitoring and managing daily operations. */
    AGSPortalItemTypeArcGISProConfiguration = 103,                      /*!< A customization of Pro to include a custom splash screen and startup page as well as tools, dock panes, and menus. */
    AGSPortalItemTypeContentCategorySet = 104,                          /*!< A content category set is used as templates for creating the category set for an organization or a group. */
    AGSPortalItemTypeInsightsTheme = 105,                               /*!< An Insights Theme is a collection of properties set on cards and pages in an Insights Workbook. */
    AGSPortalItemTypeMobileScenePackage = 106,                          /*!< A Mobile Scene Package (mspk). */
    AGSPortalItemTypeOrientedImageryCatalog = 107,                      /*!< A collection of images stored as a catalog where data is added with specific parameters and auxiliary information
                                                                         to allow exploring non-nadir imagery on map and see the camera's field of view dynamically. */
    AGSPortalItemTypeOrthoMappingProject = 108,                         /*!< An Ortho mapping project. */
    AGSPortalItemTypeOrthoMappingTemplate = 109,                        /*!< An Ortho mapping template. */
    AGSPortalItemTypeSolution = 110,                                    /*!< A solution is comprised of one or more related items and groups that work together as part of a workflow.
                                                                         It can be deployed to create a new copy of all the items and groups that make up the solution. */
    AGSPortalItemTypeBuildingSceneLayer = 111,                          /*!< A building scene layer. Building scene layers allow you to explore a building's composition, properties, and location of structures within a building digital model.
                                                                         @since 100.5 */
    AGSPortalItemTypeCompactTilePackage = 112,                          /*!< A Compact Tile Package (tpkx).
                                                                         @since 100.5 */
    AGSPortalItemTypeDataStore = 113,                                   /*!< A geodatabase or file share data store.
                                                                         @since 100.5 */
    AGSPortalItemTypeDeepLearningPackage = 114,                         /*!< A Deep Learning model package.
                                                                         @since 100.5 */
    AGSPortalItemTypeExcaliburImageryProject = 115,                     /*!< An Excalibur Imagery Project, created and maintained by the ArcGIS Excalibur web app.
                                                                         @since 100.5 */
    AGSPortalItemTypeGeoPackage = 116,                                  /*!< A SQLite based data file, compliant with OGC GeoPackage specification, containing both vector geospatial features and tile matrix sets.
                                                                         @since 100.5 */
    AGSPortalItemTypeMission = 117,                                     /*!< A collection of portal items created and maintained by the ArcGIS Mission Command web app. Provides a focused user experience for mission focused situational awareness.
                                                                         @since 100.5 */
    AGSPortalItemTypeSiteApplication = 118,                             /*!< A customizable website that provides a focused view of an organization's items. Applicable to ArcGIS Enterprise, for ArcGIS Online use @c AGSPortalItemTypeHubSiteApplication.
                                                                         @since 100.5 */
    AGSPortalItemTypeSitePage = 119,                                    /*!< A Site Page belongs to a Site Application and displays related, more detailed information. Applicable to ArcGIS Enterprise, for ArcGIS Online use @c AGSPortalItemTypeHubPage.
                                                                         @since 100.5 */
    AGSPortalItemTypeBigDataAnalytic = 120,                /*!< A component used by the ArcGIS Analytics for IoT application for processing high volumes of data. */
    AGSPortalItemTypeFeed = 121,                           /*!< A component used by the ArcGIS Analytics for IoT application for bringing in a real-time data feed.
                                                            Behaves like a stream layer when added to a map.
                                                            */
    AGSPortalItemTypeRealTimeAnalytic = 122,               /*!< A component used by the ArcGIS Analytics for IoT application for processing real-time data.
                                                            Includes references to data feeds, analytic operations, and destinations (outputs) where the results should be sent.
                                                            */
    AGSPortalItemTypeProReport = 123,                      /*!< ArcGIS Pro Report File (rptx). */
    AGSPortalItemTypeQuickCaptureProject = 124,            /*!< A project created by the QuickCapture Designer tool for use by the the QuickCapture mobile app. */
    AGSPortalItemTypeSurvey123AddIn = 125,                 /*!< Survey123 Add-Ins are created and published via Survey123 Connect and are downloaded and used by the Survey123 field app. */
    AGSPortalItemTypeUrbanModel = 126,                     /*!< Contains all information needed to start up an Urban App including links to the Urban database services. */
    AGSPortalItemTypeWebExperience = 127                   /*!< Used by Experience Builder for ArcGIS to store the information needed for a web experience. */
};



/** Access types on Portal, Item, Group or User.

 <b> For Portal or Organization </b>

 Determines who can view your organization as an anonymous user. <code>AGSPortalAccessPublic</code>
 means it allows anonymous users to access your organization's custom URL. <code>AGSPortalAccessPrivate </code> restricts access to only members of your organization.

 <b> For Portal Item </b>

 Indicates the access level of the item. If <code>AGSPortalAccessPrivate</code>, only the item owner can
 access. <code>AGSPortalAccessShared</code> allows the item to be shared with a specific group. <code>
 AGSPortalAccessOrganization</code> restricts item access to members of your organization. If <code>
 AGSPortalAccessPublic</code>, all users can access the item.

 <b> For Portal Group </b>

 Determines who can access the group. <code>AGSPortalAccessPrivate</code> gives access to only the group
 members. If <code>AGSPortalAccessOrganization</code>, all members of this organization can access the
 group. <code>AGSPortalAccessPublic</code> makes the group accessible to all.

 <b> For Portal User </b>

 Determines if other users can search for this user's name to find content and groups owned by this user
 and to invite this user to join their groups. <code>AGSPortalAccessPrivate</code> hides the user from
 user searches and invites. If <code>AGSPortalAccessOrganization</code>, only members of this user's
 organization can search for this user, the content and groups. <code>AGSPortalAccessPublic</code> makes
 the user findable.

 @since 100
 */
typedef NS_ENUM(NSInteger, AGSPortalAccess) {
    AGSPortalAccessUnknown = -1,
    AGSPortalAccessOrganization = 0,                    /*!< Applicable for Items, Groups and Users  */
    AGSPortalAccessPrivate = 1,                     /*!< Applicable for All  */
    AGSPortalAccessPublic = 2,                           /*!< Applicable for All  */
    AGSPortalAccessShared = 3                          /*!< Applicable for only Items  */
};

/** Indicates the order of results of the portal query.
 @since 100
 */
typedef NS_ENUM(NSInteger, AGSPortalQuerySortOrder){
    AGSPortalQuerySortOrderAscending = 0,   	/*!< Ascending */
    AGSPortalQuerySortOrderDescending,			/*!< Descending */
} ;


/** Indicating whether the portal contains multiple organizations or not.
 @since 100
 */
typedef NS_ENUM(NSInteger, AGSPortalMode) {
    AGSPortalModeSingleTenant = 0,   	/*!< Only one organization*/
    AGSPortalModeMultiTenant,			/*!< Multiple organizations*/
} ;

/** Indicates the role of the portal user within an organization.
 @since 100
 */
typedef NS_ENUM(NSInteger, AGSPortalUserRole) {
    AGSPortalUserRoleUnknown = 0,           /*!< The user does not belong to an organization */
    AGSPortalUserRoleUser,                  /*!< Information worker */
    AGSPortalUserRolePublisher,             /*!< Publisher */
    AGSPortalUserRoleAdmin,                 /*!< Administrator */
} ;

/** Indicates the desired sorting criterion for group items.
 @since 100
 */
typedef NS_ENUM(NSInteger, AGSPortalGroupSortField) {
    AGSPortalGroupSortFieldUnknown = 0,
    AGSPortalGroupSortFieldTitle,           /*!< Title */
    AGSPortalGroupSortFieldOwner,           /*!< Ownder */
    AGSPortalGroupSortFieldAvgRating,       /*!< Average Rating */
    AGSPortalGroupSortFieldNumViews,        /*!< Number of Views */
    AGSPortalGroupSortFieldCreated,         /*!< Created Date */
    AGSPortalGroupSortFieldModified,        /*!< Modified Date */
} ;

/** Supported login types for portal.
 @since 100
 */
typedef NS_ENUM(NSInteger, AGSPortalLoginType) {
    AGSPortalLoginTypeOAuth = 0,                   /*!<  */
    AGSPortalLoginTypeClientCertificate,           /*!<  */
    AGSPortalLoginTypeUsernamePassword,            /*!<  */
    AGSPortalLoginTypeUnknown,                     /*!<  */
} ;

/** Loading status of an object implementing `<AGSLoadable>` protocol.
 @since 100
 */
typedef NS_ENUM(NSInteger, AGSLoadStatus) {
    AGSLoadStatusLoaded = 0,            /*!< Loading completed successfully */
    AGSLoadStatusLoading = 1,           /*!< Loading in progress */
    AGSLoadStatusFailedToLoad = 2,      /*!< Loading completed with error */
    AGSLoadStatusNotLoaded = 3,         /*!< Loading not started */
    AGSLoadStatusUnknown = -1           /*!< Unknwon */
} ;

/** Supported expiration types for packages
 @since 100.5
 */
typedef NS_ENUM(NSInteger, AGSExpirationType) {
    AGSExpirationTypeAllowExpiredAccess = 0,            /*!< Expiration is provided for warning purposes only. The package can still be used. */
    AGSExpirationTypePreventExpiredAccess = 1           /*!< Expiration is mandatory and the package can no longer be used. For example, it will fail to load. */
} ;

#pragma mark - Portal Privileges

/** Supported portal privilege realms.
 @since 100
 */
typedef NS_ENUM(NSInteger, AGSPortalPrivilegeRealm) {
    AGSPortalPrivilegeRealmUnknown = 0,         /*!<  */
    AGSPortalPrivilegeRealmFeatures,            /*!<  */
    AGSPortalPrivilegeRealmMarketplace,         /*!<  */
    AGSPortalPrivilegeRealmOpenData,            /*!<  */
    AGSPortalPrivilegeRealmPortal,              /*!<  */
    AGSPortalPrivilegeRealmPremium,             /*!<  */
};

/** Supported portal privilege roles.
 @since 100
 */
typedef NS_ENUM(NSInteger, AGSPortalPrivilegeRole) {
    AGSPortalPrivilegeRoleUnknown = 0,          /*!<  */
    AGSPortalPrivilegeRoleUser,                 /*!<  */
    AGSPortalPrivilegeRoleAdmin,                /*!<  */
    AGSPortalPrivilegeRolePublisher,            /*!<  */
};

/** Supported portal privilege types.
 @since 100.0
 */
typedef NS_ENUM(NSInteger, AGSPortalPrivilegeType) {
    AGSPortalPrivilegeTypeUnknown = 0,               /*!< An unknown portal privilege type. */
    AGSPortalPrivilegeTypeEdit,                      /*!< Grants the ability to edit features in editable layers, according to the edit options enabled on the layer. */
    AGSPortalPrivilegeTypeFullEdit,                  /*!< Grants the ability to add, delete, and update features in a hosted feature layer regardless of the editing options enabled on the layer. */
    AGSPortalPrivilegeTypeManage,                    /*!< Grants the ability to create listings, list items and manage subscriptions in ArcGIS Marketplace. */
    AGSPortalPrivilegeTypePurchase,                  /*!< Grants the ability to request purchase information about apps and data in ArcGIS Marketplace. */
    AGSPortalPrivilegeTypeStartTrial,                /*!< Grants the ability to start trial subscriptions in ArcGIS Marketplace. */
    AGSPortalPrivilegeTypeDesignateGroup,            /*!< Grants the ability to designate groups within the organization as being available for use in Open Data. */
    AGSPortalPrivilegeTypeOpenDataAdmin,             /*!< Grants the ability to manage Open Data Sites for the organization. */
    AGSPortalPrivilegeTypeAssignToGroups,            /*!< Grants the ability to assign members to, and remove members from, groups within the organization. */
    AGSPortalPrivilegeTypeChangeUserRoles,           /*!< Grants the ability to change the role a member is assigned within the organization. */
    AGSPortalPrivilegeTypeDeleteGroups,              /*!< Grants the ability to delete groups within the organization. */
    AGSPortalPrivilegeTypeDeleteItems,               /*!< Grants the ability to delete content within the organization. */
    AGSPortalPrivilegeTypeDeleteUsers,               /*!< Grants the ability to delete member accounts within the organization. */
    AGSPortalPrivilegeTypeDisableUsers,              /*!< Grants the ability to enable and disable member accounts within the organization. */
    AGSPortalPrivilegeTypeInviteUsers,               /*!< Grants the ability to invite members to the organization. */
    AGSPortalPrivilegeTypeManageEnterpriseGroups,    /*!< Grants the ability to link group membership to an enterprise group. */
    AGSPortalPrivilegeTypeManageLicenses,            /*!< Grants the ability to assign licenses to members of the organization. */
    AGSPortalPrivilegeTypeReassignGroups,            /*!< Grants the ability to reassign groups to other members within the organization. */
    AGSPortalPrivilegeTypeReassignItems,             /*!< Grants the ability to reassign content to other members within the organization. */
    AGSPortalPrivilegeTypeReassignUsers,             /*!< Grants the ability to assign all groups and content of a member to another within the organization. */
    AGSPortalPrivilegeTypeUpdateGroups,              /*!< Grants the ability to update groups within the organization. */
    AGSPortalPrivilegeTypeUpdateItems,               /*!< Grants the ability to update and categorize content within the organization. */
    AGSPortalPrivilegeTypeUpdateUsers,               /*!< Grants the ability to update member account information within the organization. */
    AGSPortalPrivilegeTypeViewGroups,                /*!< Grants the ability to view all groups within the organization. */
    AGSPortalPrivilegeTypeViewItems,                 /*!< Grants the ability to view all content within the organization. */
    AGSPortalPrivilegeTypeViewUsers,                 /*!< Grants the ability to view full member account information within the organization. */
    AGSPortalPrivilegeTypePublishFeatures,           /*!< Grants the ability to publish hosted feature layers from shapefiles, CSVs, etc. */
    AGSPortalPrivilegeTypePublishScenes,             /*!< Grants the ability to publish hosted scene layers. */
    AGSPortalPrivilegeTypePublishTiles,              /*!< Grants the ability to publish hosted tile layers from tile packages, features, etc. */
    AGSPortalPrivilegeTypeCreateGroup,               /*!< Grants the ability for a member to create, edit, and delete their own groups. */
    AGSPortalPrivilegeTypeCreateItem,                /*!< Grants the ability for a member to create, edit, and delete their own content. */
    AGSPortalPrivilegeTypeJoinGroup,                 /*!< Grants the ability to join groups within the organization. */
    AGSPortalPrivilegeTypeJoinNonOrgGroup,           /*!< Grants the ability to join groups external to the organization. */
    AGSPortalPrivilegeTypeShareGroupToOrg,           /*!< Grants the ability to make groups discoverable by the organization. */
    AGSPortalPrivilegeTypeShareGroupToPublic,        /*!< Grants the ability to make groups discoverable by all users of the portal. */
    AGSPortalPrivilegeTypeShareToGroup,              /*!< For a user, grants the ability to share content to groups.
                                                      For an administrator, grants the ability to share other member's content to groups the user belongs to.
                                                      */
    AGSPortalPrivilegeTypeShareToOrg,                /*!< For a user, grants the ability to share content to the organization.
                                                      For an administrator, grants the ability to share other member's content to the organization.
                                                      */
    AGSPortalPrivilegeTypeShareToPublic,             /*!< For a user, grants the ability to share content to all users of the portal.
                                                      For an administrator, grants the ability to share other member's content to all users of the portal.
                                                      */
    AGSPortalPrivilegeTypeDemographics,              /*!< Grants the ability to make use of premium demographic data. */
    AGSPortalPrivilegeTypeElevation,                 /*!< Grants the ability to perform analytical tasks on elevation data. */
    AGSPortalPrivilegeTypeGeocode,                   /*!< Grants the ability to perform large-volume geocoding tasks with the Esri World Geocoder such as publishing a CSV of addresses as a hosted feature layer. */
    AGSPortalPrivilegeTypeGeoEnrichment,             /*!< Grants the ability to geoenrich features. */
    AGSPortalPrivilegeTypeNetworkAnalysis,           /*!< Grants the ability to perform network analysis tasks such as routing and drive-time areas. */
    AGSPortalPrivilegeTypeSpatialAnalysis,           /*!< Grants the ability to perform spatial analysis tasks. */
    AGSPortalPrivilegeTypeCreateUpdateCapableGroup,  /*!< Grants the ability to create and own groups with item update capabilities. */
    AGSPortalPrivilegeTypeViewOrgGroups,             /*!< Grants the ability to view groups shared with the organization. */
    AGSPortalPrivilegeTypeViewOrgItems,              /*!< Grants the ability to view content shared with the organization. */
    AGSPortalPrivilegeTypeViewOrgUsers,              /*!< Grants the ability to view members of the organization. */
    AGSPortalPrivilegeTypeGeoAnalytics,              /*!< Grants the ability to use big data geoanalytics. */
    AGSPortalPrivilegeTypeRasterAnalysis,            /*!< Grants the ability to use raster analysis. */
    AGSPortalPrivilegeTypePublishServerGPServices,   /*!< Grants the ability to publish non-hosted server geoprocessing services. */
    AGSPortalPrivilegeTypePublishServerServices,     /*!< Grants the ability to publish non-hosted server services. */
    AGSPortalPrivilegeTypeUpdateItemCategorySchema,  /*!< Grants the ability to configure the organization content category schema. */
    AGSPortalPrivilegeTypeFeatureReport,             /*!< Grants the ability to generate feature reports. */
    AGSPortalPrivilegeTypeManageCollaborations,       /*!< Grants the ability to manage the organization's collaborations. */
    AGSPortalPrivilegeTypeManageCredits,             /*!< Grants the ability to manage the organization's credit settings. */
    AGSPortalPrivilegeTypeManageRoles,               /*!< Grants the ability to manage the organization's member roles. */
    AGSPortalPrivilegeTypeManageSecurity,            /*!< Grants the ability to manage the organization's security settings. */
    AGSPortalPrivilegeTypeManageServers,             /*!< Grants the ability to manage the portal's server settings. */
    AGSPortalPrivilegeTypeManageUtilityServices,     /*!< Grants the ability to manage the organization's utility service settings. */
    AGSPortalPrivilegeTypeManageWebsite,              /*!< Grants the ability to manage the organization's website settings. */
    AGSPortalPrivilegeTypeManageReplications,         /*!< Grants the ability to manage replications and utilize the collaborations API. */
    AGSPortalPrivilegeTypeCreateNotebooks,            /*!< Grants the ability to create and edit interactive notebook documents. */
    AGSPortalPrivilegeTypeCreateAdvancedNotebooks,    /*!< Grants the ability to publish a notebook as a geoprocessing service. */
    AGSPortalPrivilegeTypeBulkPublishFromDataStores,  /*!< Grants the ability to publish web layers from a registered data store. */
    AGSPortalPrivilegeTypeEnumerateDataStores,        /*!< Grants the ability to get the list of datasets from a registered data store. */
    AGSPortalPrivilegeTypeRegisterDataStores,         /*!< Grants the ability to register data stores to the portal. */
    AGSPortalPrivilegeTypeCategorizeItems,            /*!< Grants the ability to categorize items in groups. */
    AGSPortalPrivilegeTypeViewTracks                  /*!< Grants the ability to view members' location tracks via shared track views when location tracking is enabled. */
};

#pragma mark - popups

/** Style options for the Popups VC
 @since 100
 */
typedef NS_ENUM(NSInteger, AGSPopupsViewControllerContainerStyle) {
    AGSPopupsViewControllerContainerStyleNavigationController,  /*!< The popups view controller will be pushed onto a navigation controller stack by the client */
    AGSPopupsViewControllerContainerStyleNavigationBar,         /*!< The popups view controller will add a navigation bar for the client (useful for when presenting modally) */
    AGSPopupsViewControllerContainerStyleCustom                 /*!< The popups view controller will be embedded in a custom container by the client, who is then responsible for having an edit button */
};

/** Options specifying how to handle existing field values when a feature's type is changed.
 @since 100
 */
typedef NS_ENUM(NSInteger, AGSPopupFeatureTypeChangeMode) {
    AGSPopupFeatureTypeChangeModeResetDefaultValues = 0,        /*!< Reset all fields to default values when feature type is changed */
    AGSPopupFeatureTypeChangeModeKeepValues = 1                 /*!< Keep existing field values even when the feature type is changed */
};

/** An enumeration that is used for defining the editing style of an AGSPopupsViewController.
 @since 100
 */
typedef NS_ENUM(NSInteger, AGSPopupsViewControllerGeometryEditingStyle) {
    AGSPopupsViewControllerGeometryEditingStyleToolbar,     /*!< Shows a toolbar with a button for attachments and a button for collecting the geometry */
    AGSPopupsViewControllerGeometryEditingStyleInline    /*!< Has a view to switch between attributes and attachments.
                                                          There is no button for the collecting the geometry, as this is assumed
                                                          the user has access to the map while the popup is up (think ipad), or
                                                          the user will not be collecting a geometry at all. */
};

/** Enumeration that specifies how date fields are formatted in an AGSPopup.
 The LE (Little Endian) formats are the same as their counterparts. We will not force the LE format,
 but instead honor the format of the current locale. This is so we give the user a string value for the date
 that they expect based on their current locale.
 @since 100
 */
typedef NS_ENUM(NSInteger, AGSPopupDateFormat) {
    AGSPopupDateFormatDayShortMonthYear = 0,           /*!< Date with d MMM y */
    AGSPopupDateFormatLongDate = 1,                    /*!< Date with EEEE, MMMM d, y */
    AGSPopupDateFormatLongMonthDayYear = 2,            /*!< Date with  MMMM d y */
    AGSPopupDateFormatLongMonthYear = 3,               /*!< Date with MMMM y */
    AGSPopupDateFormatShortDate = 4,                   /*!< Date with M/d/y */
    AGSPopupDateFormatShortDateLE = 5,                 /*!< Date with M/d/y */
    AGSPopupDateFormatShortDateLELongTime = 6,         /*!< Date with M/d/y h:mm:ss a */
    AGSPopupDateFormatShortDateLELongTime24 = 7,       /*!< Date with M/d/y H:mm:ss */
    AGSPopupDateFormatShortDateLEShortTime = 8,        /*!< Date with M/d/y h:mm a */
    AGSPopupDateFormatShortDateLEShortTime24 = 9,      /*!< Date with M/d/y H:mm */
    AGSPopupDateFormatShortDateLongTime = 10,          /*!< Date with M/d/y h:mm:ss a */
    AGSPopupDateFormatShortDateLongTime24 = 11,        /*!< Date with M/d/y H:mm:ss */
    AGSPopupDateFormatShortDateShortTime = 12,         /*!< Date with M/d/y h:mm a*/
    AGSPopupDateFormatShortDateShortTime24 = 13,       /*!< Date with M/d/y H:mm */
    AGSPopupDateFormatShortMonthYear = 14,             /*!< Date with MMM y */
    AGSPopupDateFormatYear = 15,                       /*!< Date with y */
    AGSPopupDateFormatUnknown = -1,
};

/** Enumeration that specifies different types of media in an AGSPopup.
 @since 100
 */
typedef NS_ENUM(NSInteger, AGSPopupMediaType){
    AGSPopupMediaTypeImage = 0,                       /*!< Image */
    AGSPopupMediaTypeBarChart = 1,                    /*!< Bar Chart */
    AGSPopupMediaTypeColumnChart = 2,                 /*!< Column Chart */
    AGSPopupMediaTypeLineChart = 3,                   /*!< Line Chart */
    AGSPopupMediaTypePieChart = 4,                    /*!< Pie Chart */
    AGSPopupMediaTypeUnknown = -1                     /*!< Unknown */
};

/** The type of view within @c AGSPopupsViewController
 @since 100
 */
typedef NS_ENUM(NSInteger, AGSPopupViewControllerType){
    AGSPopupViewControllerTypeContainer = 0,  /*!< The default starting view controller */
    AGSPopupViewControllerTypeUIImagePicker,  /*!< View controller that displays the AGSImagePicker */
    AGSPopupViewControllerTypeFullMedia,      /*!< View controller that displays a single media item (image/chart)  */
    AGSPopupViewControllerTypeWeb,            /*!< View controller that displays an embedded `WKWebView` */
    AGSPopupViewControllerTypeMovie,          /*!< View controller that displays movies */
    AGSPopupViewControllerTypeAttributeInput,  /*!< View controller that displays interface for inputting an attribute value */
    AGSPopupViewControllerTypeAlert           /*!< View controller of type UIAlertController */
};

/** Options for string fields in AGSPopupsViewController
 @since 100
 */
typedef NS_ENUM(NSInteger, AGSPopupStringFieldOption) {
    AGSPopupStringFieldOptionSingleLine = 0,        /*!<  */
    AGSPopupStringFieldOptionMultiLine = 1,         /*!<  */
    AGSPopupStringFieldOptionRichText = 2,          /*!<  */
    AGSPopupStringFieldOptionUnknown = -1,          /*!<  */
};

/** The scale mode for images.
 @since 100.3
 */
typedef NS_ENUM(NSInteger, AGSImageScaleMode) {
    AGSImageScaleModeFill = 0,        /*!< Scale to fill, not maintaining aspect ratio. */
    AGSImageScaleModeAspectFit,       /*!< Scales the image to fit in the desired size, maintains aspect ratio. No image cropping will occur. */
    AGSImageScaleModeAspectFill       /*!< Scales the image to fill in the desired size both width and height-wise, maintains aspect ratio. Some cropping may occur. */
};

#pragma mark - License

/** Status for license operations
 @since 100
 */
typedef NS_ENUM(NSInteger, AGSLicenseStatus) {
    AGSLicenseStatusInvalid = 0,                /*!< License is invalid */
    AGSLicenseStatusExpired	= 1,	    		/*!< License has expired */
    AGSLicenseStatusLoginRequired = 2,          /*!< License has passed the 30-day timeout period for a named user. User will need to login in again */
    AGSLicenseStatusValid = 3					/*!< License is valid */
};

/** Available license levels
 @since 100
 */
typedef NS_ENUM(NSInteger, AGSLicenseLevel) {
    AGSLicenseLevelDeveloper = 0,           /*!< No license set. Application will run in developer mode. Full functionality but with a watermarked view. Not suitable for production deployment. */
    AGSLicenseLevelLite = 1,                /*!< Lite license set. No watermark. Functionality available at the lowest level.*/
    AGSLicenseLevelBasic = 2,				/*!< Basic license set. No watermark. Functionality available at the basic level. */
    AGSLicenseLevelStandard = 3,			/*!< Standard license set. No watermark. Functionality available at the standard level. */
    AGSLicenseLevelAdvanced = 4             /*!< Advanced license set. No watermark. Functionality available at the advanced level.*/
};

/** The type of license being used by the application.
 @since 100
 */
typedef NS_ENUM(NSInteger, AGSLicenseType) {
    AGSLicenseTypeDeveloper = 0,             /*!< A license has not been set and application is running in developer mode. Not suitable for production deployment.*/
    AGSLicenseTypeNamedUser = 1,             /*!< Using a subscription license from a named user account.*/
    AGSLicenseTypeLicenseKey = 2             /*!< Licensed from a license key.*/
};

#pragma mark - Load status

/** Return load status for status string.
 @param loadStatus The string representation of a load status.
 @return @c AGSLoadStatus for @p loadStatus string.
 @since 100
 @see AGSLoadStatus
 */
AGS_EXTERN AGSLoadStatus AGSLoadStatusFromString(NSString *loadStatus);

/** Return load status string for @c AGSLoadStatus.
 @param loadStatus The load status.
 @return String representation of @p loadStatus.
 @since 100
 @see AGSLoadStatus
 */
AGS_EXTERN NSString *AGSLoadStatusAsString(AGSLoadStatus loadStatus);




#pragma mark - Mapping

/**
 @since 100
 */
typedef NS_ENUM(NSInteger, AGSBasemapType) {
    AGSBasemapTypeImagery = 0,                  /*!< */
    AGSBasemapTypeImageryWithLabels = 1,        /*!< */
    AGSBasemapTypeStreets = 2,                  /*!< */
    AGSBasemapTypeTopographic = 3,              /*!< */
    AGSBasemapTypeTerrainWithLabels = 4,        /*!< */
    AGSBasemapTypeLightGrayCanvas = 5,          /*!< */
    AGSBasemapTypeNationalGeographic = 6,       /*!< */
    AGSBasemapTypeOceans = 7,                   /*!< */
    AGSBasemapTypeOpenStreetMap = 8,            /*!< */
    AGSBasemapTypeImageryWithLabelsVector = 9,  /*!< */
    AGSBasemapTypeStreetsVector = 10,           /*!< */
    AGSBasemapTypeTopographicVector = 11,       /*!< */
    AGSBasemapTypeTerrainWithLabelsVector = 12, /*!< */
    AGSBasemapTypeLightGrayCanvasVector = 13,   /*!< */
    AGSBasemapTypeNavigationVector = 14,        /*!< */
    AGSBasemapTypeStreetsNightVector = 15,      /*!< */
    AGSBasemapTypeStreetsWithReliefVector = 16, /*!< */
    AGSBasemapTypeDarkGrayCanvasVector = 17     /*!< */
};

/**
 @since 100
 @deprecated 100.1.  AGSGridType is not used.
 */
typedef __deprecated_msg("AGSGridType is not used") NS_ENUM(NSInteger, AGSGridType){
    AGSGridTypeLatitudeLongitudeGrid __deprecated_msg("AGSGridType is not used") = 0, /*!< */
    AGSGridTypeUTM __deprecated_msg("AGSGridType is not used") = 1,                   /*!< */
    AGSGridTypeMGRS __deprecated_msg("AGSGridType is not used") = 2,                  /*!< */
    AGSGridTypeUSNG __deprecated_msg("AGSGridType is not used") = 3,                  /*!< */
    AGSGridTypeUnknown __deprecated_msg("AGSGridType is not used") = -1,              /*!< */
};

/** Bing Maps style
 @since 100
 */
typedef NS_ENUM(NSInteger, AGSBingMapsLayerStyle){
    AGSBingMapsLayerStyleAerial = 0,     /*!< Aerial */
    AGSBingMapsLayerStyleHybrid = 1,     /*!< Hybrid */
    AGSBingMapsLayerStyleRoad = 2,       /*!< Road */
    AGSBingMapsLayerStyleUnknown = -1    /*!< Unknown */
};

/** The draw status
 @since 100
 */
typedef NS_ENUM(NSInteger, AGSDrawStatus) {
    AGSDrawStatusInProgress = 0,    /*!< In progress*/
    AGSDrawStatusCompleted = 1,     /*!< Completed */
};

/** The status could be a combination of any of these individual states
 @since 100
 */
typedef NS_OPTIONS(NSInteger, AGSLayerViewStatus) {
    AGSLayerViewStatusActive = 1 << 0,       /*!< */
    AGSLayerViewStatusNotVisible = 1 << 1,   /*!< */
    AGSLayerViewStatusOutOfScale = 1 << 2,   /*!< */
    AGSLayerViewStatusLoading = 1 << 3,      /*!< */
    AGSLayerViewStatusError = 1 << 4        /*!< */
};

/**
 @since 100
 */
typedef NS_ENUM(NSInteger, AGSWrapAroundMode) {
    AGSWrapAroundModeEnabledWhenSupported = 0,  /*!< */
    AGSWrapAroundModeDisabled = 1,              /*!< */
};

/**
 @since 100
 */
typedef NS_ENUM(NSInteger, AGSViewpointType) {
    AGSViewpointTypeCenterAndScale = 0,         /*!< */
    AGSViewpointTypeBoundingGeometry = 1,       /*!< */
    AGSViewpointTypeUnknown = -1,               /*!< */
};

/** Rendering modes available for feature layers.
 @since 100.2
 */
typedef NS_ENUM(NSInteger, AGSFeatureRenderingMode) {
    AGSFeatureRenderingModeAutomatic = 0,       /*!< The rendering mode for the layer will be automatically chosen based on the geometry type and renderer. For point geometries it is typically Dynamic, for polyline and polgyon it is Static. */
    AGSFeatureRenderingModeStatic = 1,          /*!< Static rendering mode - Features are updated after zoom and pan operations complete. This mode is good for complex geometries or features to be rendered with cartographic quality symbology for example. It may also be suited for rendering features when low end graphics hardware is used. */
    AGSFeatureRenderingModeDynamic = 2          /*!< Dynamic rendering mode - Features will be updated continuously while pan and zoom operations are performed for a smoothly rendered display. This mode is not supported by all renderers. A heat map renderer is an example of a renderer not supported by the dynamic rendering mode. */
};

/**
 @since 100
 */
typedef NS_ENUM(NSInteger, AGSCacheStorageFormat)
{
    AGSCacheStorageFormatCompact = 0,       /*!<  */
    AGSCacheStorageFormatCompactV2 = 1,     /*!<  */
    AGSCacheStorageFormatExploded = 2,      /*!<  */
    AGSCacheStorageFormatUnknown = -1       /*!<  */
};

/** Represents options for positioning grid labels on screen. `AGSGridLabelPositionGeographic`  means labels are anchored to a
 geographical position on the map, whereas the remaining enum values are screen-positioned, with the labels anchored
 relative to the edge of the map.
 @since 100
 */
typedef NS_ENUM(NSInteger, AGSGridLabelPosition) {
    AGSGridLabelPositionGeographic = 0,     /*!< Labels anchored to a geographical position on the map view. */
    AGSGridLabelPositionBottomLeft = 1,     /*!< x-labels anchored to bottom, y-labels anchored to left of map view.*/
    AGSGridLabelPositionBottomRight = 2,    /*!< x-labels anchored to bottom, y-labels anchored to right of map view.*/
    AGSGridLabelPositionTopLeft = 3,        /*!< x-labels anchored to top, y-labels anchored to left of map view. */
    AGSGridLabelPositionTopRight = 4,       /*!< x-labels anchored to top, y-labels anchored to right of map view.*/
    AGSGridLabelPositionCenter = 5,         /*!< x- and y-labels are anchored in center of map view*/
    AGSGridLabelPositionAllSides = 6,       /*!< Labels are anchored to top, right, bottom and left of map view.*/
};

/** Supported formats for Latitude-Longitude values used in grid labels.
 @since 100
 */
typedef NS_ENUM(NSInteger, AGSLatitudeLongitudeGridLabelFormat) {
    AGSLatitudeLongitudeGridLabelFormatDecimalDegrees = 0,          /*!< Label the grid lines (graticules) in decimal degrees. */
    AGSLatitudeLongitudeGridLabelFormatDegreesMinutesSeconds = 1,   /*!< Label the grid lines (graticules) in degrees, minutes and seconds. */
};

/** Supported units for values displayed in MGRS grid labels
 @since 100
 */
typedef NS_ENUM(NSInteger, AGSMGRSGridLabelUnit) {
    AGSMGRSGridLabelUnitKilometersMeters = 0,   /*!< Labels are displayed in kilometers or meters based on view's scale */
    AGSMGRSGridLabelUnitMeters = 1,             /*!< Labels are always displayed in meters. */
};

/** Supported units for values displayed in USNG grid labels
 @since 100
 */
typedef NS_ENUM(NSInteger, AGSUSNGGridLabelUnit) {
    AGSUSNGGridLabelUnitKilometersMeters = 0,   /*!< Labels are displayed in kilometers or meters based on view's scale */
    AGSUSNGGridLabelUnitMeters = 1,             /*!< Labels are always displayed in meters. */
};

/**
 @since 100
 */
typedef NS_ENUM(NSInteger, AGSNoDataTileBehavior) {
    AGSNoDataTileBehaviorUpSample = 0,      /*!<  */
    AGSNoDataTileBehaviorBlank = 1,         /*!<  */
    AGSNoDataTileBehaviorShow = 2,          /*!<  */
    AGSNoDataTileBehaviorUnknown = -1       /*!<  */
};

/** The location data source status
 @since 100.6
 */
typedef NS_ENUM(NSInteger, AGSLocationDataSourceStatus) {
    AGSLocationDataSourceStatusStopped = 0,    /*!< Location Data source is stopped */
    AGSLocationDataSourceStatusStarting = 1,     /*!< Location Data source is starting */
    AGSLocationDataSourceStatusStarted = 2,     /*!< Location Data source has started */
    AGSLocationDataSourceStatusFailedToStart = 4,     /*!< Location Data source failed to start */
};

#pragma mark - Symbology

/** Determines whether the symbol should rotate with the map/scene or not.
 @since 100
 */
typedef NS_ENUM(NSInteger, AGSMarkerSymbolAngleAlignment)
{
    AGSMarkerSymbolAngleAlignmentMap = 0,       /*!< Top edge of the symbol stays aligned with the North direction of the map/scene. When the map/scene is rotated, symbols will also rotate along with them. */
    AGSMarkerSymbolAngleAlignmentScreen = 1    /*!< Top edge of the symbol stays aligned with the top-edge of the screen. Even when the map/scene is rotated, symbols will appear to remain face up. */
};

/**
 @since 100
 */
typedef NS_ENUM(NSInteger, AGSSymbolRotationType)
{
    AGSSymbolRotationTypeArithmetic = 0,    /*!< Rotation is performed starting from East in a counter-clockwise direction where East is the 0° axis */
    AGSSymbolRotationTypeGeographic = 1     /*!< Rotation is performed starting from North in a clockwise direction where North is the 0° axis */
};

/**
 @since 100
 */
typedef NS_ENUM(NSInteger, AGSSimpleMarkerSymbolStyle)
{
    AGSSimpleMarkerSymbolStyleCircle = 0,       /*!< */
    AGSSimpleMarkerSymbolStyleCross = 1,        /*!< */
    AGSSimpleMarkerSymbolStyleDiamond = 2,      /*!< */
    AGSSimpleMarkerSymbolStyleSquare = 3,       /*!< */
    AGSSimpleMarkerSymbolStyleTriangle = 4,     /*!< */
    AGSSimpleMarkerSymbolStyleX = 5             /*!< */
};

/**
 @since 100
 */
typedef NS_ENUM(NSInteger, AGSSimpleLineSymbolStyle) {
    AGSSimpleLineSymbolStyleDash = 0,           /*!< */
    AGSSimpleLineSymbolStyleDashDot = 1,        /*!< */
    AGSSimpleLineSymbolStyleDashDotDot = 2,     /*!< */
    AGSSimpleLineSymbolStyleDot = 3,            /*!< */
    AGSSimpleLineSymbolStyleNull = 4,           /*!< */
    AGSSimpleLineSymbolStyleSolid = 5           /*!< */
};


/**
 @since 100
 */
typedef NS_ENUM(NSInteger, AGSFontDecoration) {
    AGSFontDecorationLineThrough = 0,           /*!< */
    AGSFontDecorationNone = 1,                  /*!< */
    AGSFontDecorationUnderline = 2              /*!< */
};


/**
 @since 100
 */
typedef NS_ENUM(NSInteger, AGSFontStyle) {
    AGSFontStyleItalic = 0,     /*!< */
    AGSFontStyleNormal = 1,     /*!< */
    AGSFontStyleOblique = 2    /*!< */
};

/**
 @since 100
 */

typedef NS_ENUM(NSInteger, AGSFontWeight) {
    AGSFontWeightBold = 0,      /*!< */
    AGSFontWeightNormal = 1    /*!< */
};

/**
 @since 100
 */
typedef NS_ENUM(NSInteger, AGSHorizontalAlignment) {
    AGSHorizontalAlignmentCenter = 0,       /*!< */
    AGSHorizontalAlignmentJustify = 1,      /*!< */
    AGSHorizontalAlignmentLeft = 2,         /*!< */
    AGSHorizontalAlignmentRight = 3         /*!< */
};

/**
 @since 100
 */
typedef NS_ENUM(NSInteger, AGSVerticalAlignment) {
    AGSVerticalAlignmentBaseline = 0,       /*!< */
    AGSVerticalAlignmentBottom = 1,         /*!< */
    AGSVerticalAlignmentMiddle = 2,         /*!< */
    AGSVerticalAlignmentTop = 3             /*!< */
};

/**
 @since 100
 */
typedef NS_ENUM(NSInteger, AGSSimpleFillSymbolStyle) {
    AGSSimpleFillSymbolStyleBackwardDiagonal = 0,       /*!< */
    AGSSimpleFillSymbolStyleCross = 1,                  /*!< */
    AGSSimpleFillSymbolStyleDiagonalCross = 2,          /*!< */
    AGSSimpleFillSymbolStyleForwardDiagonal = 3,        /*!< */
    AGSSimpleFillSymbolStyleHorizontal = 4,             /*!< */
    AGSSimpleFillSymbolStyleNull = 5,                   /*!< */
    AGSSimpleFillSymbolStyleSolid = 6,                  /*!< */
    AGSSimpleFillSymbolStyleVertical = 7               /*!< */
};

typedef NS_ENUM(NSInteger, AGSSimpleLineSymbolMarkerPlacement) {
    AGSSimpleLineSymbolMarkerPlacementBegin = 0,
    AGSSimpleLineSymbolMarkerPlacementEnd = 1,
    AGSSimpleLineSymbolMarkerPlacementBeginAndEnd = 2
};

typedef NS_ENUM(NSInteger, AGSSimpleLineSymbolMarkerStyle) {
    AGSSimpleLineSymbolMarkerStyleNone = 0,
    AGSSimpleLineSymbolMarkerStyleArrow = 1
};

/**
 @since 100
 */
typedef NS_ENUM(NSInteger, AGSSceneSymbolAnchorPosition) {
    AGSSceneSymbolAnchorPositionTop = 0,        /*!<  */
    AGSSceneSymbolAnchorPositionBottom = 1,     /*!<  */
    AGSSceneSymbolAnchorPositionCenter = 2,     /*!<  */
    AGSSceneSymbolAnchorPositionOrigin = 3,     /*!<  */
};

/**
 @since 100
 */
typedef NS_ENUM(NSInteger, AGSSimpleMarkerSceneSymbolStyle) {
    AGSSimpleMarkerSceneSymbolStyleCone = 0,        /*!<  */
    AGSSimpleMarkerSceneSymbolStyleCube = 1,        /*!<  */
    AGSSimpleMarkerSceneSymbolStyleCylinder = 2,    /*!<  */
    AGSSimpleMarkerSceneSymbolStyleDiamond = 3,     /*!<  */
    AGSSimpleMarkerSceneSymbolStyleSphere = 4,      /*!<  */
    AGSSimpleMarkerSceneSymbolStyleTetrahedron = 5  /*!<  */
};

/** Possible geometric effects for symbol layers
 @since 100.5
 */
typedef NS_ENUM(NSInteger, AGSGeometricEffectType) {
    AGSGeometricEffectTypeUnknown = -1,        /*!< Unknown geometric effect type */
    AGSGeometricEffectTypeDashGeometricEffect = 0        /*!< Dash pattern to be applied to a line */
};

/** The list of possible cap styles. The cap style describes the way that line symbol layers will terminate when combined with a geometry. The term "cap" refers to the end of the line. These options control the shape the cap takes.
 @note In dynamic rendering mode, caps are always rendered with the `butt` style.
 @since 100.5
 */
typedef NS_ENUM(NSInteger, AGSStrokeSymbolLayerCapStyle) {
    AGSStrokeSymbolLayerCapStyleButt = 0,        /*!< The line ending is terminated exactly where the geometry ends. The end cap will be squared off, i.e. two 90 degree angles form a butted ending to the symbol. */
    AGSStrokeSymbolLayerCapStyleRound = 1,        /*!< The line ending is terminated with a semicircle of radius equal to the stroke width. That semicircle will be centered at the line endpoint. */
    AGSStrokeSymbolLayerCapStyleSquare = 2        /*!< The termination of the line ending is extended past the end of the geometry. The end cap will be squared off, i.e. two 90 degree angles form a squared ending to the symbol. */
};

/** The list of possible 3D line styles. The 3D line style describes the way that line symbol layers will render in 3D. It refers to the rendering style of a 3D stroke layer. In a scene view, changing this property will change the appearance of the stroke layer.
 @note Only the `strip` line style is supported in static rendering mode.
 @since 100.5
 */
typedef NS_ENUM(NSInteger, AGSStrokeSymbolLayerLineStyle3D) {
    AGSStrokeSymbolLayerLineStyle3DTube = 0,        /*!< The stroke appears as a 3D tube. A "tube" refers to a solid circular cylinder, following the given geometry. */
    AGSStrokeSymbolLayerLineStyle3DStrip = 1        /*!< The stroke appears as a flat strip of surface. Note that the cap style for this line is always `butt`. A "strip" refers to a flat 2D surface with more significant width than a line, though it is presented in 3D. */
};

/** The list of possible symbol anchor placement modes.
 @since 100.5
 */
typedef NS_ENUM(NSInteger, AGSSymbolAnchorPlacementMode) {
    AGSSymbolAnchorPlacementModeRelative = 0,        /*!< Specifies anchor with relative values. This mode causes anchor values to be interpreted as percentages, relative to the origin of the symbol. Percentage values must be expressed as fractions between [0, 1]. A 50% (.5) x-anchor, for example, moves the symbol layer anchor in the positive-x direction an amount equal to 50% of the symbol layer size. */
    AGSSymbolAnchorPlacementModeAbsolute = 1        /*!< Specifies anchor with absolute values. This mode causes anchor values to be interpreted as absolute units (DIPs) rather than percentages. */
};

/** The list of possible symbol layer types.
 @since 100.5
 */
typedef NS_ENUM(NSInteger, AGSSymbolLayerType) {
    AGSSymbolLayerTypeUnknown = -1,        /*!< Generic unknown value that indicates a malformed or useless symbol layer. */
    AGSSymbolLayerTypeSymbolLayer = 0,        /*!< Symbol layer base class */
    AGSSymbolLayerTypeMarkerSymbolLayer = 1,        /*!< Marker symbol layer. */
    AGSSymbolLayerTypeVectorMarkerSymbolLayer = 2,        /*!< Vector marker symbol layer. */
    AGSSymbolLayerTypeStrokeSymbolLayer = 3,        /*!< Stroke symbol layer. */
    AGSSymbolLayerTypeSolidStrokeSymbolLayer = 4,        /*!< Solid Stroke symbol layer. */
    AGSSymbolLayerTypePictureMarkerSymbolLayer = 5,        /*!< Picture marker symbol layer. */
    AGSSymbolLayerTypeFillSymbolLayer = 6,        /*!< Fill symbol layer. */
    AGSSymbolLayerTypeSolidFillSymbolLayer = 7,        /*!< Solid fill symbol layer. */
    AGSSymbolLayerTypePictureFillSymbolLayer = 8,        /*!< Picture fill symbol layer. */
    AGSSymbolLayerTypeHatchFillSymbolLayer = 9        /*!< Hatch fill symbol layer. */
};

/** The list of possible size units for symbols.
 It describes the size units that can be applied to the symbols.
 For instance using DIPs for @c AGSModelSceneSymbol.
 @since 100.5
 */
typedef NS_ENUM(NSInteger, AGSSymbolSizeUnits) {
    AGSSymbolSizeUnitsDIPs = 0,   /*!<  */
    AGSSymbolSizeUnitsMeters = 1  /*!<  */
};

#pragma mark - Renderer

/**
 @since 100
 @deprecated 100.1. AGSRendererNormalizationTypeUnknown is not supported
 */
typedef NS_ENUM(NSInteger, AGSRendererNormalizationType) {
    AGSRendererNormalizationTypeByField = 0,            /*!< */
    AGSRendererNormalizationTypeByLog = 1,              /*!< */
    AGSRendererNormalizationTypeByPercentOfTotal = 2,   /*!< */
    AGSRendererNormalizationTypeNone = 3,               /*!< */
    AGSRendererNormalizationTypeUnknown __deprecated_enum_msg("AGSRendererNormalizationTypeUnknown is no longer supported") = -1            /*!< */
};

/**
 @since 100
 */
typedef NS_ENUM(NSInteger, AGSPresetColorRampType) {
    AGSPresetColorRampTypeNone = -1,                          /*!<  */
    AGSPresetColorRampTypeElevation = 0,                      /*!<  */
    AGSPresetColorRampTypeDEMScreen = 1,                      /*!<  */
    AGSPresetColorRampTypeDEMLight = 2,                       /*!<  */
};

/**
 @since 100
 */
typedef NS_ENUM(NSInteger, AGSPansharpenType) {
    AGSPansharpenTypeNone = -1,                         /*!<  */
    AGSPansharpenTypeIHS = 0,                           /*!<  */
    AGSPansharpenTypeBrovey = 1,                        /*!<  */
    AGSPansharpenTypeMean = 2,                          /*!<  */
    AGSPansharpenTypeEsri = 3,                          /*!<  */
    AGSPansharpenTypeGramSchmidt = 4,                   /*!<  */
};

/** Type of data, such as signed integer, unsigned integer, or floating point, contained in each pixel of a raster
 @since 100.1
 */
typedef NS_ENUM(NSInteger, AGSPixelType) {
    AGSPixelTypeUnknown = -1,       /*!<  */
    AGSPixelTypeUInt1 = 0,          /*!<  */
    AGSPixelTypeUInt2 = 1,          /*!<  */
    AGSPixelTypeUInt4 = 2,          /*!<  */
    AGSPixelTypeUInt8 = 3,          /*!<  */
    AGSPixelTypeInt8 = 4,           /*!<  */
    AGSPixelTypeUInt16 = 5,         /*!<  */
    AGSPixelTypeInt16 = 6,          /*!<  */
    AGSPixelTypeUInt32 = 7,         /*!<  */
    AGSPixelTypeInt32 = 8,          /*!<  */
    AGSPixelTypeFloat32 = 9,        /*!<  */
    AGSPixelTypeFloat64 = 10        /*!<  */
};

typedef NS_ENUM(NSInteger, AGSSlopeType) {
    AGSSlopeTypeNone = -1,                              /*!<  */
    AGSSlopeTypeDegree = 0,                             /*!<  Inclination of slope is calculated in degrees. The values range from 0 to 90 */
    AGSSlopeTypePercentRise = 1,                        /*!<  Inclination of slope is calculated as percentage values. The values range from 0 to essentially infinity. A flat surface is 0 percent and a 45-degree surface is 100 percent, and as the surface becomes more vertical, the percent rise becomes increasingly larger */
    AGSSlopeTypeScaled = 2,                             /*!<  Same as Degree, but the z-factor is adjusted for scale using Pixel Size Power and Pixel Size Factor */
};

/**
 @since 100
 */
typedef NS_ENUM(NSInteger, AGSExtrusionMode) {
    AGSExtrusionModeNone = 0,                           /*!<  No extrusion */
    AGSExtrusionModeMinimum = 1,                        /*!<  A z-value is calculated by adding the extrusion height to the minimum z-value of the geo-element, and the geo-element is extruded to a flat top at that value.*/
    AGSExtrusionModeMaximum = 2,                        /*!<  A z-value is calculated by adding the extrusion height to the maximum z-value of the geo-element, and the geo-element is extruded to a flat top at that value.*/
    AGSExtrusionModeAbsoluteHeight = 3,                 /*!<  The geo-element is extruded to the specified z-value as a flat top, regardless of the z-values of the geo-element.*/
    AGSExtrusionModeBaseHeight = 4,                     /*!<  A z-value is calculated for each vertex of the geo-element's base, and the geo-element is extruded to the various z-values. This is the only extrusion mode that doesn't have a flat top.*/
};

/**
 @since 100.2
 */
typedef NS_ENUM(NSInteger, AGSRendererClassificationMethod)
{
    AGSRendererClassificationMethodDefinedInterval = 0,        /*!<  */
    AGSRendererClassificationMethodEqualInterval = 1,          /*!<  */
    AGSRendererClassificationMethodGeometricalInterval = 2,    /*!<  */
    AGSRendererClassificationMethodNaturalBreaks = 3,          /*!<  */
    AGSRendererClassificationMethodQuantile = 4,               /*!<  */
    AGSRendererClassificationMethodStandardDeviation = 5,      /*!<  */
    AGSRendererClassificationMethodManual = 6                  /*!<  */
};

#pragma mark - Animation

/**
 @since 100
 */
typedef NS_ENUM(NSInteger, AGSAnimationCurve) {
    AGSAnimationCurveLinear = 0,                /*!< */
    AGSAnimationCurveEaseInQuad = 1,            /*!< */
    AGSAnimationCurveEaseOutQuad = 2,           /*!< */
    AGSAnimationCurveEaseInOutQuad = 3,         /*!< */
    AGSAnimationCurveEaseInCubic = 4,           /*!< */
    AGSAnimationCurveEaseOutCubic = 5,          /*!< */
    AGSAnimationCurveEaseInOutCubic = 6,        /*!< */
    AGSAnimationCurveEaseInQuart = 7,           /*!< */
    AGSAnimationCurveEaseOutQuart = 8,          /*!< */
    AGSAnimationCurveEaseInOutQuart = 9,        /*!< */
    AGSAnimationCurveEaseInQuint = 10,          /*!< */
    AGSAnimationCurveEaseOutQuint = 11,         /*!< */
    AGSAnimationCurveEaseInOutQuint = 12,       /*!< */
    AGSAnimationCurveEaseInSine = 13,           /*!< */
    AGSAnimationCurveEaseOutSine = 14,          /*!< */
    AGSAnimationCurveEaseInOutSine = 15,        /*!< */
    AGSAnimationCurveEaseInExpo = 16,           /*!< */
    AGSAnimationCurveEaseOutExpo = 17,          /*!< */
    AGSAnimationCurveEaseInOutExpo = 18,        /*!< */
    AGSAnimationCurveEaseInCirc = 19,           /*!< */
    AGSAnimationCurveEaseOutCirc = 20,          /*!< */
    AGSAnimationCurveEaseInOutCirc = 21        /*!< */
};



#pragma mark - Units

/** Supported time units.
 @since 100
 */
typedef NS_ENUM(NSInteger, AGSTimeUnit) {
    AGSTimeUnitUnknown = -1,               /*!< Unknown */
    AGSTimeUnitCenturies = 0,              /*!< Centuries */
    AGSTimeUnitDays = 1,                   /*!< Days */
    AGSTimeUnitDecades = 2,                /*!< Decades*/
    AGSTimeUnitHours = 3,                  /*!< Hours */
    AGSTimeUnitMilliseconds = 4,           /*!< Milliseconds */
    AGSTimeUnitMinutes = 5,                /*!< Minutes */
    AGSTimeUnitMonths = 6,                 /*!< Months */
    AGSTimeUnitSeconds = 7,                /*!< Seconds */
    AGSTimeUnitWeeks = 8,                  /*!< Weeks */
    AGSTimeUnitYears = 9                   /*!< Years */
};

/** Supported time relation.
 @since 100
 */
typedef NS_ENUM(NSInteger, AGSTimeRelation) {
    AGSTimeRelationUnknown = -1,                /*!<  */
    AGSTimeRelationOverlaps = 0,                /*!<  */
    AGSTimeRelationAfterStartOverlapsEnd = 1,   /*!<  */
    AGSTimeRelationOverlapsStartWithinEnd = 2,  /*!<  */
};

/**
 Angular units.
 
 @since 100
 */
typedef NS_ENUM(NSInteger, AGSAngularUnitID)
{
    AGSAngularUnitIDDegrees = 9102,     /*!< Degrees */
    AGSAngularUnitIDGrads = 9105,       /*!< Gradians */
    AGSAngularUnitIDMinutes = 9103,     /*!< Minutes */
    AGSAngularUnitIDRadians = 9101,     /*!< Radians */
    AGSAngularUnitIDSeconds = 9104,     /*!< Seconds */
    AGSAngularUnitIDOther = 0           /*!< Other */
};

/**
 Area units.
 
 @since 100
 */
typedef NS_ENUM(NSInteger, AGSAreaUnitID)
{
    AGSAreaUnitIDAcres = 109402,                /*!< Acres */
    AGSAreaUnitIDHectares = 109401,             /*!< Hectares */
    AGSAreaUnitIDSquareCentimeters = 109451,    /*!< Sq. Centimeters */
    AGSAreaUnitIDSquareDecimeters = 109450,     /*!< Sq. Decimeters */
    AGSAreaUnitIDSquareFeet = 109405,           /*!< Sq. Feet */
    AGSAreaUnitIDSquareKilometers = 109414,     /*!< Sq. Kilometers */
    AGSAreaUnitIDSquareMeters = 109404,         /*!< Sq. Meters */
    AGSAreaUnitIDSquareMillimeters = 109452,    /*!< Sq. Millimeters */
    AGSAreaUnitIDSquareMiles = 109439,          /*!< Sq. Miles */
    AGSAreaUnitIDSquareYards = 109442,          /*!< Sq. Yards */
    AGSAreaUnitIDOther = 0                      /*!< Other */
};

/**
 Linear units.
 
 @since 100
 */

typedef NS_ENUM(NSInteger, AGSLinearUnitID)
{
    AGSLinearUnitIDCentimeters = 1033,          /*!< Centimeters. */
    AGSLinearUnitIDFeet = 9002,                 /*!< International foot. */
    AGSLinearUnitIDInches = 109008,             /*!< International inch. */
    AGSLinearUnitIDKilometers = 9036,           /*!< Kilometer. */
    AGSLinearUnitIDMeters = 9001,               /*!< International meter. */
    AGSLinearUnitIDMiles = 9093,                /*!< Statute Miles. */
    AGSLinearUnitIDMillimeters = 1025,          /*!< Millimeters */
    AGSLinearUnitIDNauticalMiles = 9030,		/*!< International nautical mile. */
    AGSLinearUnitIDYards = 9096,                /*!< Yards. */
    AGSLinearUnitIDOther = 0,                   /*!< Other */
};

#pragma mark - Image Format

/**
 @since 100
 */
typedef NS_ENUM(NSInteger, AGSMapServiceImageFormat) {
    AGSMapServiceImageFormatDefault = 0,      /*!< */
    AGSMapServiceImageFormatPNG = 1,          /*!< */
    AGSMapServiceImageFormatPNG8 = 2,         /*!< */
    AGSMapServiceImageFormatPNG24 = 3,        /*!< */
    AGSMapServiceImageFormatPNG32 = 4,        /*!< */
    AGSMapServiceImageFormatJPG = 5,          /*!< */
    AGSMapServiceImageFormatJPGPNG = 6,       /*!< */
    AGSMapServiceImageFormatBMP = 7,          /*!< */
    AGSMapServiceImageFormatGIF = 8,          /*!< */
    AGSMapServiceImageFormatTIFF = 9,         /*!< */
    AGSMapServiceImageFormatUnknown = -1,     /*!< */
};

/**
 @since 100
 */
typedef NS_ENUM(NSInteger, AGSTileImageFormat)
{
    AGSTileImageFormatPNG = 0,      /*!<  */
    AGSTileImageFormatPNG8 = 1,     /*!<  */
    AGSTileImageFormatPNG24 = 2,    /*!<  */
    AGSTileImageFormatPNG32 = 3,    /*!<  */
    AGSTileImageFormatJPG = 4,      /*!<  */
    AGSTileImageFormatMIXED = 5,    /*!<  */
    AGSTileImageFormatLERC = 6,     /*!<  */
    AGSTileImageFormatUnknown = -1  /*!<  */
};

#pragma mark - Labeling

/**
 @since 100
 */
typedef NS_ENUM(NSInteger, AGSLabelingPlacement) {
    AGSLabelingPlacementUnknown = -1,           /*!< */
    AGSLabelingPlacementLineAboveAfter = 0,     /*!< */
    AGSLabelingPlacementLineAboveAlong = 1,     /*!< */
    AGSLabelingPlacementLineAboveBefore = 2,    /*!< */
    AGSLabelingPlacementLineAboveEnd = 3,       /*!< */
    AGSLabelingPlacementLineAboveStart = 4,     /*!< */
    AGSLabelingPlacementLineBelowAfter = 5,     /*!< */
    AGSLabelingPlacementLineBelowAlong = 6,     /*!< */
    AGSLabelingPlacementLineBelowBefore = 7,    /*!< */
    AGSLabelingPlacementLineBelowEnd = 8,       /*!< */
    AGSLabelingPlacementLineBelowStart = 9,     /*!< */
    AGSLabelingPlacementLineCenterAfter = 10,   /*!< */
    AGSLabelingPlacementLineCenterAlong = 11,   /*!< */
    AGSLabelingPlacementLineCenterBefore = 12,  /*!< */
    AGSLabelingPlacementLineCenterEnd = 13,     /*!< */
    AGSLabelingPlacementLineCenterStart = 14,   /*!< */
    AGSLabelingPlacementPointAboveCenter = 15,  /*!< */
    AGSLabelingPlacementPointAboveLeft = 16,    /*!< */
    AGSLabelingPlacementPointAboveRight = 17,   /*!< */
    AGSLabelingPlacementPointBelowCenter = 18,  /*!< */
    AGSLabelingPlacementPointBelowLeft = 19,    /*!< */
    AGSLabelingPlacementPointBelowRight = 20,   /*!< */
    AGSLabelingPlacementPointCenterCenter = 21, /*!< */
    AGSLabelingPlacementPointCenterLeft = 22,   /*!< */
    AGSLabelingPlacementPointCenterRight = 23,  /*!< */
    AGSLabelingPlacementPolygonAlwaysHorizontal = 24,/*!< */
};



#pragma mark - Feature

/**
 @since 100
 */
typedef NS_ENUM(NSInteger, AGSFieldType) {
    AGSFieldTypeUnknown = -1,       /*!< */
    AGSFieldTypeInt16 = 0,          /*!< */
    AGSFieldTypeInt32 = 1,          /*!< */
    AGSFieldTypeGUID = 3,           /*!< */
    AGSFieldTypeFloat = 4,          /*!< */
    AGSFieldTypeDouble = 5,         /*!< */
    AGSFieldTypeDate = 6,           /*!< */
    AGSFieldTypeText = 7,           /*!< */
    AGSFieldTypeOID = 8,            /*!< */
    AGSFieldTypeGlobalID = 9,       /*!< */
    AGSFieldTypeBlob = 10,          /*!< */
    AGSFieldTypeGeometry = 11,      /*!< */
    AGSFieldTypeRaster = 12,        /*!< */
    AGSFieldTypeXML = 13            /*!< */
};

/**
 @since 100
 */
typedef NS_ENUM(NSInteger, AGSSpatialRelationship) {
    AGSSpatialRelationshipUnknown = -1,             /*!< */
    AGSSpatialRelationshipRelate = 0,               /*!< */
    AGSSpatialRelationshipEquals = 1,               /*!< */
    AGSSpatialRelationshipDisjoint = 2,             /*!< */
    AGSSpatialRelationshipIntersects = 3,           /*!< */
    AGSSpatialRelationshipTouches = 4,              /*!< */
    AGSSpatialRelationshipCrosses = 5,              /*!< */
    AGSSpatialRelationshipWithin = 6,               /*!< */
    AGSSpatialRelationshipContains = 7,             /*!< */
    AGSSpatialRelationshipOverlaps = 8,             /*!< */
    AGSSpatialRelationshipEnvelopeIntersects = 9,   /*!< */
    AGSSpatialRelationshipIndexIntersects = 10,     /*!< */
};

/** Specifies how `AGSServiceFeatureTable` should retrieve data from its service.
 @since 100
 */
typedef NS_ENUM(NSInteger, AGSFeatureRequestMode) {
    AGSFeatureRequestModeUndefined = 0,                 /*!< */
    AGSFeatureRequestModeOnInteractionCache = 1,        /*!< Data is automatically requested as the user navigates and interacts with the map. All data that is requested is cached locally. Cached data is never requested again even when the map is navigated to areas that have been already visited. Suitable for fairly static data that does not change on the server.*/
    AGSFeatureRequestModeOnInteractionNoCache = 2,      /*!< The features are explicitly requested for the layer work in this mode. It is suitable for always working against the latest data and will therefore have a high network bandwidth. Queries always go to the server for all interactions (pans, zooms, selects or queries). */
    AGSFeatureRequestModeManualCache = 3,               /*!< Data is not automatically requested upon map interaction or navigation. The developer needs to explicitly request data using `AGSServiceFeatureTable#populateFromServiceWithParameters:clearCache:completion:` */
};

/** The type of join between the sources
 @since 100.1
 */
typedef NS_ENUM(NSInteger, AGSJoinType) {
    AGSJoinTypeInnerJoin = 0,           /*!< Inner join:  keeps only those rows that are common between the two sources */
    AGSJoinTypeLeftOuterJoin = 1,       /*!< Left join: keeps all rows from the left source, some values from the right source may be null when there isn't a match */
    AGSJoinTypeUnknown = -1             /*!< Unknown */
};

/** Order in which to sort query results
 @since 100
 */
typedef NS_ENUM(NSInteger, AGSSortOrder) {
    AGSSortOrderAscending = 0,      /*!< */
    AGSSortOrderDescending = 1,     /*!< */
};

/**
 @since 100
 */
typedef NS_ENUM(NSInteger, AGSEditOperation) {
    AGSEditOperationAdd = 0,        /*!< */
    AGSEditOperationUpdate = 1,     /*!< */
    AGSEditOperationDelete = 2,     /*!< */
    AGSEditOperationUnknown = -1,   /*!< */
};

/** Selection mode for handling results
 @see `AGSFeatureLayer#selectFeaturesWithQuery:mode:completion:`
 @since 100
 */
typedef NS_ENUM(NSInteger, AGSSelectionMode) {
    AGSSelectionModeAdd = 0,            /*!< Add results to the existing set of selected features */
    AGSSelectionModeNew = 1,            /*!< Replace existing set of selected features with the results  */
    AGSSelectionModeSubtract = 2,       /*!< Remove results from the existing set of selected features */
};

/** Various options that can be specified with calling @c AGSServiceFeatureTable#queryFeaturesWithParameters:queryFeatureFields:completion: and @c AGSServiceFeatureTable#queryRelatedFeaturesWithFeature:parameters:queryFeatureFields:completion:
 @since 100
 */
typedef NS_ENUM(NSInteger, AGSQueryFeatureFields) {
    AGSQueryFeatureFieldsIDsOnly = 0,       /*!<  */
    AGSQueryFeatureFieldsMinimum = 1,       /*!<  */
    AGSQueryFeatureFieldsLoadAll = 2,       /*!<  */
};

/** Supported drawing tools.
 @since 100
 */
typedef NS_ENUM(NSInteger, AGSDrawingTool) {
    AGSDrawingToolUnknown = -1,             /*!< */
    AGSDrawingToolAutoCompletePolygon = 0,  /*!< */
    AGSDrawingToolCircle = 1,               /*!< */
    AGSDrawingToolDownArrow = 2,            /*!< */
    AGSDrawingToolEllipse = 3,              /*!< */
    AGSDrawingToolFreeHand = 4,             /*!< */
    AGSDrawingToolLeftArrow = 5,            /*!< */
    AGSDrawingToolLine = 6,                 /*!< */
    AGSDrawingToolNone = 7,                 /*!< */
    AGSDrawingToolPoint = 8,                /*!< */
    AGSDrawingToolPolygon = 9,              /*!< */
    AGSDrawingToolRectangle = 10,           /*!< */
    AGSDrawingToolRightArrow = 11,          /*!< */
    AGSDrawingToolText = 12,                /*!< */
    AGSDrawingToolTriangle = 13,            /*!< */
    AGSDrawingToolUpArrow = 14,             /*!< */
};

/** Supported service type of feature layer info.
 @since 100
 */
typedef NS_ENUM(NSInteger, AGSArcGISFeatureLayerInfoServiceType) {
    AGSArcGISFeatureLayerInfoServiceTypeLayer = 0,              /*!<  */
    AGSArcGISFeatureLayerInfoServiceTypeTable = 1,              /*!<  */
    AGSArcGISFeatureLayerInfoServiceTypeGroupLayer = 2,         /*!<  */
    AGSArcGISFeatureLayerInfoServiceTypeAnnotationLayer = 3,    /*!<  */
    AGSArcGISFeatureLayerInfoServiceTypeUnknown = -1,           /*!<  */
};

/** Represents the various statistics that can be calculated for values in a field in a table.
 @since 100.2
 */
typedef NS_ENUM(NSInteger, AGSStatisticType) {
    AGSStatisticTypeAverage = 0,                /*!< the average for all non-null values in a column. */
    AGSStatisticTypeCount = 1,                  /*!< the number of non-null values in a column. */
    AGSStatisticTypeMaximum = 2,                /*!< the maximum value found within a column. */
    AGSStatisticTypeMinimum = 3,                /*!< the minimum value found within a column. */
    AGSStatisticTypeStandardDeviation = 4,      /*!< the standard deviation of the values within a column. */
    AGSStatisticTypeSum = 5,                    /*!< the sum of all non-null values within a column. */
    AGSStatisticTypeVariance = 6,               /*!< the variance of the values within a column. */
};

#pragma mark - Location Display

/**
 @since 100
 */
typedef NS_ENUM(NSInteger, AGSLocationDisplayAutoPanMode) {
    AGSLocationDisplayAutoPanModeOff = 0,                       /*!< */
    AGSLocationDisplayAutoPanModeRecenter = 1,                  /*!< */
    AGSLocationDisplayAutoPanModeNavigation = 2,                /*!< */
    AGSLocationDisplayAutoPanModeCompassNavigation = 3          /*!< */
};

#pragma mark - Layer

/** Cardinality of the relationship
 @since 100.1
 */
typedef NS_ENUM(NSInteger, AGSRelationshipCardinality) {
    AGSRelationshipCardinalityManyToMany = 0,       /*!< Each feature in the origin table may be related to many features in the destination table and vice versa */
    AGSRelationshipCardinalityOneToMany = 1,        /*!< Each feature in the origin table may be related to many features in the destination table */
    AGSRelationshipCardinalityOneToOne = 2,          /*!< Each feature in the origin table may be related to only one feature in the destination table */
    AGSRelationshipCardinalityUnknown = -1          /*!< */
};

/** Role of the feature table in the relationship
 @since 100.1
 */
typedef NS_ENUM(NSInteger, AGSRelationshipRole) {
    AGSRelationshipRoleDestination = 0,     /*!< Table serves as the destination in the relationship */
    AGSRelationshipRoleOrigin = 1,           /*!< Table serves as the origin in the relationship */
    AGSRelationshipRoleUnknown = -1          /*!< */
};

#pragma mark - Service Info

/**
 @since 100
 */
typedef NS_ENUM(NSInteger, AGSTextAntialiasingMode) {
    AGSTextAntialiasingModeNone = 0,                /*!<  */
    AGSTextAntialiasingModeNormal = 1,              /*!<  */
    AGSTextAntialiasingModeForce = 2,               /*!<  */
};

/**
 @since 100
 */
typedef NS_ENUM(NSInteger, AGSAntialiasingMode) {
    AGSAntialiasingModeNone = 0,                    /*!<  */
    AGSAntialiasingModeFastest = 1,                 /*!<  */
    AGSAntialiasingModeFast = 2,                    /*!<  */
    AGSAntialiasingModeNormal = 3,                  /*!<  */
    AGSAntialiasingModeBest = 4,                    /*!<  */
};

/**
 @since 100
 */
typedef NS_ENUM(NSInteger, AGSServiceType) {
    AGSServiceTypeUnknown = -1,             /*!<  */
    AGSServiceTypeFeatureService = 0,       /*!<  */
    AGSServiceTypeImageService = 1,         /*!<  */
    AGSServiceTypeMapService = 2,           /*!<  */
};

/** The type of sublayer in the ArcGIS Map Service
 @since 100.1
 */
typedef NS_ENUM(NSInteger, AGSArcGISMapServiceSublayerType) {
    AGSArcGISMapServiceSublayerTypeFeatureLayer = 0,         /*!< Feature layer */
    AGSArcGISMapServiceSublayerTypeTable = 1,                /*!< Non-spatial table */
    AGSArcGISMapServiceSublayerTypeGroupLayer = 2,           /*!< Group layer */
    AGSArcGISMapServiceSublayerTypeRasterLayer = 3,          /*!< Raster layer */
    AGSArcGISMapServiceSublayerTypeNetworkAnalysisLayer = 4, /*!< Network Analysis layer */
    AGSArcGISMapServiceSublayerTypeUnknown = -1,             /*!< Other */
};

/** Represents the version of WMS services.
 @since 100.2.1
 */
typedef NS_ENUM(NSInteger, AGSWMSVersion) {
    AGSWMSVersionV110 = 110,    /*!< Version 1.1.0 */
    AGSWMSVersionV111 = 111,    /*!< Version 1.1.1 */
    AGSWMSVersionV130 = 130,    /*!< Version 1.3.0 */
};

#pragma mark - Graphics Overlay

/**  Specifies how the `AGSGraphicsOverlay` renders its content.
 Dynamic mode is better suited for a small number of graphics that are constantly changing or moving and need to
 be redrawn frequently.
 If your content is more static then use static mode. Static mode does not provide the same level
 of interactiveness or user experience as dynamic mode,
 but it allows the overlay to render a larger number of graphics.
 @since 100
 */
typedef NS_ENUM(NSInteger, AGSGraphicsRenderingMode) {
    AGSGraphicsRenderingModeDynamic = 0,    /*!< */
    AGSGraphicsRenderingModeStatic = 1,     /*!< */
};

#pragma mark - Network Tasks

/**
 @since 100
 */
typedef NS_ENUM(NSInteger, AGSBarrierType) {
    AGSBarrierTypeRestriction = 0,          /*!<  */
    AGSBarrierTypeCostAdjustment = 1,       /*!<  */
};

/**
 @since 100
 */
typedef NS_ENUM(NSInteger, AGSCurbApproach) {
    AGSCurbApproachEitherSide = 0,      /*!<  */
    AGSCurbApproachLeftSide = 1,        /*!<  */
    AGSCurbApproachRightSide = 2,       /*!<  */
    AGSCurbApproachNoUTurn = 3,         /*!<  */
    AGSCurbApproachUnknown = 4          /*!<  */
};

/**
 @since 100
 */
typedef NS_ENUM(NSInteger, AGSLocationStatus) {
    AGSLocationStatusNotLocated = 0,                /*!< The element's location on the network dataset can't be determined. */
    AGSLocationStatusOnClosest = 1,                 /*!< The element has been located on the closest network location */
    AGSLocationStatusOnClosestNotRestricted = 2,    /*!< The closest network location to the element is not traversable because of a restriction or barrier, so the element has been located on the closest traversable network feature instead  */
    AGSLocationStatusNotReached = 3,                /*!< The element can't be reached during analysis */
};

/**
 @since 100
 */
typedef NS_ENUM(NSInteger, AGSUTurnPolicy) {
    AGSUTurnPolicyNotAllowed = 0,                           /*!<  */
    AGSUTurnPolicyAllowedAtDeadEnds = 1,                    /*!<  */
    AGSUTurnPolicyAllowedAtIntersections = 2,               /*!<  */
    AGSUTurnPolicyAllowedAtDeadEndsAndIntersections = 3,    /*!<  */
};

/**
 @since 100
 */
typedef NS_ENUM(NSInteger, AGSDirectionManeuverType) {
    AGSDirectionManeuverTypeUnknown = 0,			/*!<  */
    AGSDirectionManeuverTypeStop = 1,               /*!<  */
    AGSDirectionManeuverTypeStraight = 2,			/*!<  */
    AGSDirectionManeuverTypeBearLeft = 3,			/*!<  */
    AGSDirectionManeuverTypeBearRight = 4,			/*!<  */
    AGSDirectionManeuverTypeTurnLeft = 5,			/*!<  */
    AGSDirectionManeuverTypeTurnRight = 6,			/*!<  */
    AGSDirectionManeuverTypeSharpLeft = 7,			/*!<  */
    AGSDirectionManeuverTypeSharpRight = 8,			/*!<  */
    AGSDirectionManeuverTypeUTurn = 9,              /*!<  */
    AGSDirectionManeuverTypeFerry = 10,             /*!<  */
    AGSDirectionManeuverTypeRoundabout = 11,		/*!<  */
    AGSDirectionManeuverTypeHighwayMerge = 12,		/*!<  */
    AGSDirectionManeuverTypeHighwayExit = 13,		/*!<  */
    AGSDirectionManeuverTypeHighwayChange = 14,		/*!<  */
    AGSDirectionManeuverTypeForkCenter = 15,		/*!<  */
    AGSDirectionManeuverTypeForkLeft = 16,			/*!<  */
    AGSDirectionManeuverTypeForkRight = 17,			/*!<  */
    AGSDirectionManeuverTypeDepart = 18,			/*!<  */
    AGSDirectionManeuverTypeTripItem = 19,			/*!<  */
    AGSDirectionManeuverTypeEndOfFerry = 20,		/*!<  */
    AGSDirectionManeuverTypeRampRight = 21,			/*!<  */
    AGSDirectionManeuverTypeRampLeft = 22,			/*!<  */
    AGSDirectionManeuverTypeTurnLeftRight = 23,		/*!<  */
    AGSDirectionManeuverTypeTurnRightLeft = 24,		/*!<  */
    AGSDirectionManeuverTypeTurnRightRight = 25,	/*!<  */
    AGSDirectionManeuverTypeTurnLeftLeft = 26,		/*!<  */
    AGSDirectionManeuverTypePedestrianRamp = 27,	/*!<  */
    AGSDirectionManeuverTypeElevator = 28,			/*!<  */
    AGSDirectionManeuverTypeEscalator = 29,			/*!<  */
    AGSDirectionManeuverTypeStairs = 30,			/*!<  */
    AGSDirectionManeuverTypeDoorPassage = 31,		/*!<  */
};

/** Type of stop along the route
 @since 100.1
 */
typedef NS_ENUM(NSUInteger, AGSStopType) {
    AGSStopTypeStop = 0,            /*!<  */
    AGSStopTypeWaypoint = 1,        /*!<  */
    AGSStopTypeRestBreak = 2,       /*!<  */
};

/** The system of units/measurement
 @since 100
 */
typedef NS_ENUM(NSInteger, AGSUnitSystem) {
    AGSUnitSystemUnknown = -1,
    AGSUnitSystemImperial = 0,                      /*!<  */
    AGSUnitSystemMetric = 1,                        /*!<  */
};

/**
 @since 100
 */
typedef NS_ENUM(NSInteger, AGSDirectionsStyle) {
    AGSDirectionsStyleDesktop = 0,      /*!<  */
    AGSDirectionsStyleNavigation = 1,   /*!<  */
    AGSDirectionsStyleCampus = 2,       /*!<  */
};

/**
 @since 100
 */
typedef NS_ENUM(NSInteger, AGSRouteShapeType) {
    AGSRouteShapeTypeNone = 0,                      /*!<  */
    AGSRouteShapeTypeStraightLine = 1,              /*!<  */
    AGSRouteShapeTypeTrueShapeWithMeasures = 2,     /*!<  */
};

/**
 @since 100
 */
typedef NS_ENUM(NSInteger, AGSAttributeUnit) {
    AGSAttributeUnitUnknown = 0,              /*!<  */
    AGSAttributeUnitInches = 1,               /*!<  */
    AGSAttributeUnitFeet = 2,                 /*!<  */
    AGSAttributeUnitYards = 3,                /*!<  */
    AGSAttributeUnitMiles = 4,                /*!<  */
    AGSAttributeUnitMillimeters = 5,          /*!<  */
    AGSAttributeUnitCentimeters = 6,          /*!<  */
    AGSAttributeUnitDecimeters = 7,           /*!<  */
    AGSAttributeUnitMeters = 8,               /*!<  */
    AGSAttributeUnitKilometers = 9,           /*!<  */
    AGSAttributeUnitNauticalMiles = 10,       /*!<  */
    AGSAttributeUnitDecimalDegrees = 11,      /*!<  */
    AGSAttributeUnitSeconds = 12,             /*!<  */
    AGSAttributeUnitMinutes = 13,             /*!<  */
    AGSAttributeUnitHours = 14,               /*!<  */
    AGSAttributeUnitDays = 15,                /*!<  */
};

/**
 @since 100
 @deprecated 100.2 AGSDirectionMessageTypeLength is not supported, use AGSDirectionManeuver#length instead
 @deprecated 100.2 AGSDirectionMessageTypeTime is not supported, use AGSDirectionManeuver#duration instead
 @deprecated 100.2 AGSDirectionMessageTypeSummary is not supported, use AGSDirectionManeuver#duration and AGSDirectionManeuver#length instead
 @deprecated 100.2 AGSDirectionMessageTypeTimeWindow is not supported, use AGSStop#timeWindowEnd instead
 @deprecated 100.2 AGSDirectionMessageTypeViolationTime is not supported, use AGSStop#violationTime instead
 @deprecated 100.2 AGSDirectionMessageTypeWaitTime is not supported, use AGSStop#waitTime instead
 @deprecated 100.2 AGSDirectionMessageTypeServiceTime is not supported, use the difference between AGSDirectionManeuver#duration and AGSStop#waitTime instead
 @deprecated 100.2 AGSDirectionMessageTypeEstimatedArrivalTime is not supported, use AGSDirectionManeuver#estimatedArriveTime instead
 @deprecated 100.2 AGSDirectionMessageTypeCumulativeLength is not supported, use the sum of previous AGSDirectionManeuver#length instead
 */
typedef NS_ENUM(NSInteger, AGSDirectionMessageType) {
    AGSDirectionMessageTypeLength __deprecated_enum_msg("AGSDirectionMessageTypeLength is no longer supported, use AGSDirectionManeuver.length instead") = 0,                  /*!<  */
    AGSDirectionMessageTypeTime __deprecated_enum_msg("AGSDirectionMessageTypeTime is no longer supported, use AGSDirectionManeuver.duration instead") = 1,                    /*!<  */
    AGSDirectionMessageTypeSummary __deprecated_enum_msg("AGSDirectionMessageTypeSummary is no longer supported, use AGSDirectionManeuver.duration and AGSDirectionManeuver.length instead") = 2,                 /*!<  */
    AGSDirectionMessageTypeTimeWindow __deprecated_enum_msg("AGSDirectionMessageTypeTimeWindow is no longer supported, use AGSStop.timeWindowStart and AGSStop.timeWindowEnd instead") = 3,              /*!<  */
    AGSDirectionMessageTypeViolationTime __deprecated_enum_msg("AGSDirectionMessageTypeViolationTime is no longer supported, use AGSStop.violationTime instead") = 4,			/*!<  */
    AGSDirectionMessageTypeWaitTime __deprecated_enum_msg("AGSDirectionMessageTypeWaitTime is no longer supported, use AGSStop.waitTime instead") = 5,                /*!<  */
    AGSDirectionMessageTypeServiceTime __deprecated_enum_msg("AGSDirectionMessageTypeServiceTime is no longer supported, use the difference between AGSDirectionManeuver.duration and AGSStop.waitTime instead") = 6,             /*!<  */
    AGSDirectionMessageTypeEstimatedArrivalTime __deprecated_enum_msg("AGSDirectionMessageTypeEstimatedArrivalTime is no longer supported, use AGSDirectionManeuver.estimatedArriveTime instead") = 7,	/*!<  */
    AGSDirectionMessageTypeCumulativeLength __deprecated_enum_msg("AGSDirectionMessageTypeCumulativeLength is no longer supported, use the sum of previous AGSDirectionManeuver.length instead") = 8,		/*!<  */
    AGSDirectionMessageTypeStreetName = 9,              /*!<  */
    AGSDirectionMessageTypeAlternativeName = 10,		/*!<  */
    AGSDirectionMessageTypeBranch = 11,                 /*!<  */
    AGSDirectionMessageTypeToward = 12,                 /*!<  */
    AGSDirectionMessageTypeCrossStreet = 13,			/*!<  */
    AGSDirectionMessageTypeExit = 14,                   /*!<  */
};

#pragma mark - Service Area Task

/** Type of overlap between service area geometries
 @since 100.1
 */
typedef NS_ENUM(NSInteger, AGSServiceAreaOverlapGeometry)
{
    AGSServiceAreaOverlapGeometryOverlap = 0,   /*!< Creates individual polygons for each facility. The polygons can overlap each other. For overlapping lines, portions of the network that are within reach of more than one facility will have a line for each facility*/
    AGSServiceAreaOverlapGeometryDissolve = 1,  /*!< Merges the polygons of multiple facilities that have the same cutoff values into one polygon. If the polygons of a given break value don't touch, they are nonetheless merged into one multipart polygon. This option does not apply to lines */
    AGSServiceAreaOverlapGeometrySplit = 2      /*!< Creates individual polygons that are closest for each facility. The polygons do not overlap each other. For split lines, none of the lines overlap each other, and a line is always assigned to the nearest facility even if more than one facility can reach it */
};

/** Type of geometry for concentric service areas
 @since 100.1
 */
typedef NS_ENUM(NSInteger, AGSServiceAreaPolygonCutoffGeometry)
{
    AGSServiceAreaPolygonCutoffGeometryRings = 0,   /*!< Output polygons extend between the nearest cutoff values only. They do not include the area of smaller breaks. This creates polygons between consecutive breaks */
    AGSServiceAreaPolygonCutoffGeometryDisks = 1    /*!< Output polygons extend from the facility to the cutoff. If you create 5- and 10-minute service areas, the 10-minute service area polygon will include the area under the 5-minute service area polygon as well as the area from the 5-minute to the 10-minute area */
};

/** Level of detail for service area geometries
 @since 100.1
 */
typedef NS_ENUM(NSInteger, AGSServiceAreaPolygonDetail)
{
    AGSServiceAreaPolygonDetailGeneralized = 0, /*!< Prefers higher-order edges to the lowest-order edges in the transportation network, for example primary roads, then secondary roads, and finally local roads in that order.  */
    AGSServiceAreaPolygonDetailStandard = 1,    /*!< Polygons are generated quickly and are fairly accurate, but quality deteriorates somewhat as you move toward the borders of the service area polygons. Outputting standard-detail polygons may result in islands of unreached network elements, such as roads, being covered. */
    AGSServiceAreaPolygonDetailHigh = 2         /*!< Most detailed polygons. Holes within the polygon may exist; they represent islands of network elements that couldn't be reached due to travel restrictions for example. Expect high-precision polygons to take the longest amount of time to generate. */
};

/** Specifies the direction of travel between facilities and incidents
 @since 100.1
 */
typedef NS_ENUM(NSInteger, AGSTravelDirection)
{
    AGSTravelDirectionFromFacility = 0, /*!< Direction of travel is from facilities to incidents. Fire departments commonly use this setting, since they are concerned with the time it takes to travel from the fire station (facility) to the location of the emergency (incident) */
    AGSTravelDirectionToFacility = 1 /*!< Direction of travel is from incidents to facilities. Retail stores commonly use this setting, since they are concerned with the time it takes the shoppers (incidents) to reach the store (facility) */
};

#pragma mark - Closest Facility Task

/** Determines whether the specified time is departure time or arrival time
 @since 100.1
 */
typedef NS_ENUM(NSInteger, AGSStartTimeUsage)
{
    AGSStartTimeUsageDepartureTime = 0, /*!< Departure time from incident or facility */
    AGSStartTimeUsageArrivalTime = 1 /*!< Arrival time at incident or facility */
};

#pragma mark - Offline Map

/** Indicates the type of parameters object used to take layers offline.
 @since 100.4
 */
typedef NS_ENUM(NSInteger, AGSOfflineMapParametersType) {
    AGSOfflineMapParametersTypeGenerateGeodatabase = 0, /*!< Parameters used to generate a geodatabase */
    AGSOfflineMapParametersTypeExportVectorTiles = 1, /*!< Parameters used for exporting vector tiles */
    AGSOfflineMapParametersTypeExportTileCache = 2, /*!< Parameters used for exporting a tile cache */
    AGSOfflineMapParametersTypeUnknown = -1, /*!< Unknown paramters type */
};

/** Indicates whether tables in relationships will contain all rows or can be filtered to a smaller set of related rows.
 @since 100.4
 */
typedef NS_ENUM(NSInteger, AGSDestinationTableRowFilter) {
    AGSDestinationTableRowFilterAll = 0,            /*!< All rows of a table will be take offline */
    AGSDestinationTableRowFilterRelatedOnly = 1     /*!< Where appropriate, a table will be filtered to only related rows when taking the table offline. */
};

/** Enumerates options for downloading read-only preplanned updates from an online map area.
 @since 100.6
 */
typedef NS_ENUM(NSInteger, AGSPreplannedScheduledUpdatesOption) {
    AGSPreplannedScheduledUpdatesOptionNoUpdates = 0,           /*!< No updates will be downloaded. */
    AGSPreplannedScheduledUpdatesOptionDownloadAllUpdates = 1   /*!< All available updates for feature data will be downloaded. */
};

/** Different modes for how updates are obtained for a preplanned map area.
 @since 100.6
 */
typedef NS_ENUM(NSInteger, AGSPreplannedUpdateMode) {
    AGSPreplannedUpdateModeNoUpdates = 0,                 /*!< No feature updates will be performed. */
    AGSPreplannedUpdateModeSyncWithFeatureServices = 1,   /*!< Changes, including local edits, will be synced directly with the underlying feature services. */
    AGSPreplannedUpdateModeDownloadScheduledUpdates = 2   /*!< Scheduled, read-only updates will be downloaded from the online map area and applied to the local mobile geodatabases. */
};

/** Enumerates whether offline data has updates, has no updates, or that the availability of updates cannot be determined.
 @since 100.6
 */
typedef NS_ENUM(NSInteger, AGSOfflineUpdateAvailability) {
    AGSOfflineUpdateAvailabilityAvailable = 0,       /*!< There are updates available. */
    AGSOfflineUpdateAvailabilityNone = 1,            /*!< There are no updates available. */
    AGSOfflineUpdateAvailabilityIndeterminate = -1   /*!< It is not possible to determine whether updates are available, for example, because the operation is not supported. */
};

#pragma mark - Geodatabase

/** The sync model defines how feature layers in a sync-enabled geodatabase can be synced.
 @since 100
 */
typedef NS_ENUM(NSInteger, AGSSyncModel) {
    AGSSyncModelNone = 0,           /*!< Not a valid value */
    AGSSyncModelGeodatabase = 1,    /*!< Layers within a geodatabase cannot be synchronized independently, the whole geodatabase must be synced. The sync operation and sync direction applies to all the layers in the geodatabase. */
    AGSSyncModelLayer = 2,          /*!< Layers within a geodatabase can be synchronized independently of one another. Any subset of the layers can be synchronized when running the sync operation. Also, each layer can independently choose its sync direction */
};

/** Direction in which changes should be synced
 @since 100
 */
typedef NS_ENUM(NSInteger, AGSSyncDirection) {
    AGSSyncDirectionNone = 0,           /*!< No changes are synced */
    AGSSyncDirectionDownload = 1,       /*!< Only download changes from the service during sync */
    AGSSyncDirectionUpload = 2,         /*!< Only upload changes from the client to the service during sync */
    AGSSyncDirectionBidirectional = 3,  /*!< Both download and upload changes */
};

/**
 @since 100
 */
typedef NS_ENUM(NSInteger, AGSAttachmentSyncDirection) {
    AGSAttachmentSyncDirectionNone = 0,             /*!< Data may not be uploaded or downloaded */
    AGSAttachmentSyncDirectionUpload = 1,           /*!< Data may only be uploaded to the service, but not downloaded */
    AGSAttachmentSyncDirectionBidirectional = 2,    /*!< Data may either be uploaded or downloaded */
};

/** Options specifying whether or not to include attachments for feature layers when taking a map offline
 @since 100.1
 */
typedef NS_ENUM(NSInteger, AGSReturnLayerAttachmentOption) {
    AGSReturnLayerAttachmentOptionNone = 0,              /*!< Do not include attachments for any feature layers */
    AGSReturnLayerAttachmentOptionAllLayers = 1,         /*!< Include attachments for all feature layers */
    AGSReturnLayerAttachmentOptionReadOnlyLayers = 2,    /*!< Include attachments only for read-only feature layers */
    AGSReturnLayerAttachmentOptionEditableLayers = 3     /*!< Include attachments only for editable feature layers. Editable layers are those that support all the editing capabilities - Create, Update, and Delete. */
};

/** Type of relationship constraint violated by the edit
 @since 100.1
 */
typedef NS_ENUM(NSInteger, AGSRelationshipConstraintViolationType) {
    AGSRelationshipConstraintViolationTypeNone = 0,             /*!<  No constraints violated */
    AGSRelationshipConstraintViolationTypeCardinality = 1,      /*!<  Edit leads to relationship cardinality being violated. */
    AGSRelationshipConstraintViolationTypeOrphaned = 2          /*!<  Edit leads to orphaned feature in the destination table. */
};

#pragma mark - Authentication

/** Supported authentication challenge types.
 @since 100
 */
typedef NS_ENUM(NSInteger, AGSAuthenticationChallengeType) {
    AGSAuthenticationChallengeTypeUsernamePassword = 0,          /*!<  */
    AGSAuthenticationChallengeTypeOAuth,                         /*!<  */
    AGSAuthenticationChallengeTypeClientCertificate,             /*!<  */
    AGSAuthenticationChallengeTypeUntrustedHost,                 /*!<  */
    AGSAuthenticationChallengeTypeUnknown,                       /*!<  */
} ;

/** Error types returned
 @since 100
 */
typedef NS_ENUM(NSInteger, AGSCredentialErrorType) {
    AGSCredentialErrorTypeNone = 0,                 /*!<  */
    AGSCredentialErrorTypeInvalidCredentials,       /*!<  */
    AGSCredentialErrorTypeSSLRequired,              /*!<  */
    AGSCredentialErrorTypeUnknown                   /*!<  */
} ;

#pragma mark - Keychain Item

/** Indicates when a keychain item is accessible.
 @since 100.2
 */
typedef NS_ENUM(NSInteger, AGSKeychainItemAccessible) {
    AGSKeychainItemAccessibleAfterFirstUnlock = 0,                  /*!<  */
    AGSKeychainItemAccessibleAfterFirstUnlockThisDeviceOnly,        /*!<  */
    AGSKeychainItemAccessibleAlways,                                /*!<  */
    AGSKeychainItemAccessibleWhenPasscodeSetThisDeviceOnly,         /*!<  */
    AGSKeychainItemAccessibleAlwaysThisDeviceOnly,                  /*!<  */
    AGSKeychainItemAccessibleWhenUnlocked,                          /*!<  */
    AGSKeychainItemAccessibleWhenUnlockedThisDeviceOnly,            /*!<  */
};

#pragma mark - AGSJob

/**
 @since 100
 */
typedef NS_ENUM(NSInteger, AGSJobType) {
    AGSJobTypeGenerateGeodatabase = 0,      /*!<  */
    AGSJobTypeSyncGeodatabase = 1,          /*!<  */
    AGSJobTypeExportTileCache = 2,          /*!<  */
    AGSJobTypeEstimateTileCacheSize = 3,    /*!<  */
    AGSJobTypeGeoprocessingJob = 4,         /*!<  */
    AGSJobTypeGenerateOfflineMap = 5,       /*!<  */
    AGSJobTypeOfflineMapSync = 7            /*!<  */
};

/** Status of a job
 @since 100
 */
typedef NS_ENUM(NSInteger, AGSJobStatus) {
    AGSJobStatusNotStarted = 0,             /*!<  */
    AGSJobStatusStarted = 1,                /*!<  */
    AGSJobStatusPaused = 2,                 /*!<  */
    AGSJobStatusSucceeded = 3,              /*!<  */
    AGSJobStatusFailed = 4                  /*!<  */
};

/** Job message severity
 @since 100.4
 */
typedef NS_ENUM(NSInteger, AGSJobMessageSeverity) {
    AGSJobMessageSeverityUnknown = -1,         /*!<  */
    AGSJobMessageSeverityInfo = 0,             /*!<  */
    AGSJobMessageSeverityWarning = 1,          /*!<  */
    AGSJobMessageSeverityError = 2             /*!<  */
};

/** Job message source
 @since 100.4
 */
typedef NS_ENUM(NSInteger, AGSJobMessageSource) {
    AGSJobMessageSourceClient = 0,             /*!<  */
    AGSJobMessageSourceService = 1             /*!<  */
};

/** Defines whether and how filters will be applied to a layer/table when including its data in a geodatabase.
 @since 100.1
 */
typedef NS_ENUM(NSInteger, AGSGenerateLayerQueryOption) {
    AGSGenerateLayerQueryOptionUnknown = -1,    /*!<  */
    AGSGenerateLayerQueryOptionAll = 0,         /*!< All the features from the layer are included regardless of what is specified in `AGSGenerateLayerOption#includeRelated`, `AGSGenerateLayerOption#whereClause`, or `AGSGenerateLayerOption#useGeometry`  */
    AGSGenerateLayerQueryOptionNone = 1,        /*!< No features are included, unless they are related to a feature in another layer in the geodatabase and `AGSGenerateLayerOption#includeRelated` is `YES`. When combined with a sync direction of `AGSAttachmentSyncDirectionUpload` this option can be used for an efficient upload-only work-flow.*/
    AGSGenerateLayerQueryOptionUseFilter = 2    /*!< Only those features are included that satisfy filtering based on `AGSGenerateLayerOption#whereClause` and optionally, the specified extent for the geodatabase (`AGSGenerateGeodatabaseParameters#extent`)  if `AGSGenerateLayerOption#useGeometry` is `YES`  */
};

#pragma mark - AGSScene

/** Visual effect for a scene view's atmosphere
 @since 100
 */
typedef NS_ENUM(NSInteger, AGSAtmosphereEffect) {
    AGSAtmosphereEffectNone = 0,            /*!<  */
    AGSAtmosphereEffectHorizonOnly = 1,     /*!<  */
    AGSAtmosphereEffectRealistic = 2        /*!<  */
};

/** Ambient lighting options for a scene view
 @since 100
 */
typedef NS_ENUM(NSInteger, AGSLightingMode) {
    AGSLightingModeNoLight = 0,             /*!<  */
    AGSLightingModeLight = 1,               /*!<  */
    AGSLightingModeLightAndShadows = 2      /*!<  */
};

/**
 @since 100
 */
typedef NS_ENUM(NSInteger, AGSSurfacePlacement) {
    AGSSurfacePlacementDraped = 0,          /*!< Altitude is based on the underlying surface elevation. Object's Z value is ignored. */
    AGSSurfacePlacementAbsolute = 1,        /*!< Altitude is based on object's Z value and doesn't take surface elevation into consideration. */
    AGSSurfacePlacementRelative = 2,        /*!< Altitude is based on a combination of object's Z value and the underlying surface elevation */
    AGSSurfacePlacementRelativeToScene = 3, /*!< Altitude is based on a combination of object's Z value and the underlying scene elevation */
};

/** Determines whether outer space is black with stars or transparent.
 @see @c AGSSceneView
 @since 100.6
 */
typedef NS_ENUM(NSInteger, AGSSpaceEffect) {
    AGSSpaceEffectStars = 0,        /*!< Outer space is black with stars. */
    AGSSpaceEffectTransparent = 1   /*!< Outer space is transparent. */
};

/** The configuration for setting a scene tiling scheme.
 @since 100.2.1
 */
typedef NS_ENUM(NSInteger, AGSSceneViewTilingScheme) {
    AGSSceneViewTilingSchemeGeographic = 0,     /*!< Use a geographic tiling scheme for the tiled layers. */
    AGSSceneViewTilingSchemeWebMercator = 1,    /*!< Use a Web Mercator tiling scheme for tiled layers. */
};

/** The list of options for constraining navigation based on the surface elevation.
 @since 100.5
 */
typedef NS_ENUM(NSInteger, AGSNavigationConstraint) {
    AGSNavigationConstraintNone = 0,     /*!< Camera navigation is unconstrained. In this mode the camera may pass above and below the elevation surface. */
    AGSNavigationConstraintStayAbove = 1,    /*!< Camera navigation is constrained to remaining above the elevation surface. The altitude of the camera may not fall below the elevation of the surface. If the elevation is added or updates resulting in the camera altitude being below the elevation the camera will be pushed up to be at the surface. */
};

/** Identifies the data type contained within a scene layer.
 @since 100.5
 */
typedef NS_ENUM(NSInteger, AGSSceneLayerDataType)
{
    AGSSceneLayerDataTypeUnknown = 0,           /*!< The type of the scene layer is unknown. */
    AGSSceneLayerDataTypeSceneObject = 1,       /*!< The type of the scene layer is 3D scene objects. */
    AGSSceneLayerDataTypePoint = 2,             /*!< The type of the scene layer is point objects. */
    AGSSceneLayerDataTypeIntegratedMesh = 3     /*!< The type of the scene layer is an integrated mesh. */
};

#pragma mark - Raster


/**
 @since 100
 */
typedef NS_ENUM(NSInteger, AGSScreenLocationVisibility) {
    AGSScreenLocationVisibilityVisible = 0,                /*!<  */
    AGSScreenLocationVisibilityHiddenByBaseSurface = 1,    /*!<  */
    AGSScreenLocationVisibilityHiddenByEarth = 2,          /*!<  */
    AGSScreenLocationVisibilityHiddenByElevation = 3,      /*!<  */
    AGSScreenLocationVisibilityNotOnScreen = 4,            /*!<  */
};

#pragma mark - Sketch

/** Specifies what type of geometry is to be created and the manner in which it is created
 @since 100
 */
typedef NS_ENUM(NSInteger, AGSSketchCreationMode) {
    AGSSketchCreationModeUnset = -1,            /*!<  */
    AGSSketchCreationModePoint,                 /*!< Create Point geometry */
    AGSSketchCreationModeMultipoint,            /*!< Create Multipoint geometry incrementally one vertex at a time  */
    AGSSketchCreationModePolyline,              /*!< Create Polyline geometry incrementally one vertex at a time */
    AGSSketchCreationModePolygon,               /*!< Create Polygon geometry incrementally one vertex at a time */
    AGSSketchCreationModeFreehandPolyline,      /*!< Create Polyline geometry using a continuous freehand gesture */
    AGSSketchCreationModeFreehandPolygon,       /*!< Create Polygon geometry using a continuous freehand gesture */
    AGSSketchCreationModeRectangle,             /*!< Create Polygon geometry shaped as a rectangle. Use together with `AGSSketchResizeModeUniform` to create a square */
    AGSSketchCreationModeEllipse,               /*!< Create Polygon geometry shaped as an ellipse. Use together with `AGSSketchResizeModeUniform` to create a circle */
    AGSSketchCreationModeTriangle,              /*!< Create Polygon geometry shaped as a triangle */
    AGSSketchCreationModeArrow,                 /*!< Create Polygon geometry shaped as an arrow */
};

/** Specifies permitted operations on the vertices of the geometry
 @since 100.2
 */
typedef NS_ENUM(NSInteger, AGSSketchVertexEditMode) {
    AGSSketchVertexEditModeInteractionEdit,     /*!< Users can interactively select, add, remove, and move vertices */
    AGSSketchVertexEditModeSelectOnly           /*!< Users can interactively only select vertices. Vertices cannot be added, moved, or removed interactively. */
};

/** Specifies how geometries can be resized
 @since 100.3
 */
typedef NS_ENUM(NSInteger, AGSSketchResizeMode) {
    AGSSketchResizeModeNone,            /*!< Resizing not allowed */
    AGSSketchResizeModeStretch,         /*!< Resizing is free-form and may change the aspect-ratio of the geometry */
    AGSSketchResizeModeUniform          /*!< Resizing is proportional, preserving the aspect-ratio of the geometry */
};

#pragma mark - Geoprocessing

/**
 @since 100
 */
typedef NS_ENUM(NSInteger, AGSGeoprocessingParameterType) {
    AGSGeoprocessingParameterTypeBoolean = 0,       /*!<  */
    AGSGeoprocessingParameterTypeDataFile = 1,      /*!<  */
    AGSGeoprocessingParameterTypeDate = 2,          /*!<  */
    AGSGeoprocessingParameterTypeDouble = 3,        /*!<  */
    AGSGeoprocessingParameterTypeFeatures = 4,      /*!<  */
    AGSGeoprocessingParameterTypeLinearUnit = 5,    /*!<  */
    AGSGeoprocessingParameterTypeLong = 6,          /*!<  */
    AGSGeoprocessingParameterTypeMultiValue = 7,    /*!<  */
    AGSGeoprocessingParameterTypeRaster = 8,        /*!<  */
    AGSGeoprocessingParameterTypeString = 9,        /*!<  */
    AGSGeoprocessingParameterTypeUnknown = 10,      /*!<  */
};

/**
 @since 100
 */
typedef NS_ENUM(NSInteger, AGSGeoprocessingLinearUnits) {
    AGSGeoprocessingLinearUnitsCentimeter = 0,      /*!<  */
    AGSGeoprocessingLinearUnitsDecimeter = 1,       /*!<  */
    AGSGeoprocessingLinearUnitsKilometer = 2,       /*!<  */
    AGSGeoprocessingLinearUnitsMeter = 3,           /*!<  */
    AGSGeoprocessingLinearUnitsMillimeter = 4,      /*!<  */
    AGSGeoprocessingLinearUnitsPoint = 5,           /*!<  */
    AGSGeoprocessingLinearUnitsUSNauticalMile = 6,  /*!<  */
    AGSGeoprocessingLinearUnitsUSSurveyFoot = 7,    /*!<  */
    AGSGeoprocessingLinearUnitsUSSurveyInch = 8,    /*!<  */
    AGSGeoprocessingLinearUnitsUSSurveyMile = 9,    /*!<  */
    AGSGeoprocessingLinearUnitsUSSurveyYard = 10,   /*!<  */
    AGSGeoprocessingLinearUnitsUnknown = -1,        /*!<  */
};

/**
 @since 100
 */
typedef NS_ENUM(NSInteger, AGSGeoprocessingExecutionType) {
    AGSGeoprocessingExecutionTypeAsynchronousSubmit = 0,    /*!<  */
    AGSGeoprocessingExecutionTypeSynchronousExecute = 1,    /*!<  */
    AGSGeoprocessingExecutionTypeUnknown = -1               /*!<  */
};

typedef NS_ENUM(NSInteger, AGSGeoprocessingParameterDirection) {
    AGSGeoprocessingParameterDirectionInput = 0,
    AGSGeoprocessingParameterDirectionOutput = 1,
};

#pragma mark - ENC

/**
 @since 100.2
 */
typedef NS_ENUM(NSInteger, AGSENCAreaSymbolizationType)
{
    AGSENCAreaSymbolizationTypePlain = 4,       /*!<  */
    AGSENCAreaSymbolizationTypeSymbolized = 5   /*!<  */
};

/**
 @since 100.2
 */
typedef NS_ENUM(NSInteger, AGSENCColorScheme)
{
    AGSENCColorSchemeDay = 1,   /*!<  */
    AGSENCColorSchemeDusk = 2,  /*!<  */
    AGSENCColorSchemeNight = 3  /*!<  */
};

/**
 @since 100.2
 */
typedef NS_ENUM(NSInteger, AGSENCDisplayDepthUnits)
{
    AGSENCDisplayDepthUnitsMeters = 1,  /*!<  */
    AGSENCDisplayDepthUnitsFeet = 2,    /*!<  */
    AGSENCDisplayDepthUnitsFathoms = 3  /*!<  */
};

/**
 @since 100.2
 */
typedef NS_ENUM(NSInteger, AGSENCPointSymbolizationType)
{
    AGSENCPointSymbolizationTypeSimplified = 2, /*!<  */
    AGSENCPointSymbolizationTypePaperChart = 1  /*!<  */
};

#pragma mark - Scene Analysis

typedef NS_ENUM(NSInteger, AGSLineOfSightTargetVisibility)
{
    AGSLineOfSightTargetVisibilityVisible = 0,
    AGSLineOfSightTargetVisibilityObstructed = 1,
    AGSLineOfSightTargetVisibilityUnknown = 2
};

#pragma mark - KML

/** KML Geometry types
 @since 100.4
 */
typedef NS_ENUM(NSInteger, AGSKMLGeometryType)
{
    AGSKMLGeometryTypePoint = 0,
    AGSKMLGeometryTypePolyline = 1,
    AGSKMLGeometryTypePolygon = 2,
    AGSKMLGeometryTypeModel = 3,
    AGSKMLGeometryTypeUnknown = -1
};

/** KML Altitude modes
 @since 100.4
 */
typedef NS_ENUM(NSInteger, AGSKMLAltitudeMode)
{
    AGSKMLAltitudeModeClampToGround = 0,        /*!< The `<altitude>` value is ignored, and the object will be draped over the ground. */
    AGSKMLAltitudeModeRelativeToGround = 1,     /*!< (default) Interprets the `<altitude>` as a value in meters above the ground. If the point is over water, the `<altitude>` will be interpreted as a value in meters above sea level. */
    AGSKMLAltitudeModeAbsolute = 2,             /*!< Interprets the `<altitude>` as a value in meters above sea level, regardless of the actual terrain elevation beneath the feature. */
    AGSKMLAltitudeModeUnknown = -1              /*!< Unknown altitude mode. */
};

/** KML Viewpoint types
 @since 100.4
 */
typedef NS_ENUM(NSInteger, AGSKMLViewpointType)
{
    AGSKMLViewpointTypeCamera = 0,      /*!< A camera viewpoint defines the position of the camera directly. */
    AGSKMLViewpointTypeLookAt = 1,      /*!< A 'LookAt' viewpoint, positions the camera relative to an object, like an `AGSKMLPlacemark`. For more information about LookAt, including a diagram, see https://developers.google.com/kml/documentation/kmlreference#lookat. */
    AGSKMLViewpointTypeUnknown = -1     /*!< Unknown viewpoint type. */
};

/** KML graphic types.
 @since 100.4
 */
typedef NS_ENUM(NSInteger, AGSKMLGraphicType)
{
    AGSKMLGraphicTypeNone = 0,              /*!<  */
    AGSKMLGraphicTypePoint = 1,             /*!<  */
    AGSKMLGraphicTypePolyline = 2,          /*!<  */
    AGSKMLGraphicTypePolygon = 3,           /*!<  */
    AGSKMLGraphicTypeExtrudedPoint = 4,     /*!<  */
    AGSKMLGraphicTypeExtrudedPolyline = 5,  /*!<  */
    AGSKMLGraphicTypeExtrudedPolygon = 6,   /*!<  */
    AGSKMLGraphicTypeModel = 7,             /*!<  */
    AGSKMLGraphicTypeMultiGeometry = 8,     /*!<  */
    AGSKMLGraphicTypeUnknown = -1           /*!<  */
};

/** Defines how visibility selection should work, enabling either single or multiple
 selection of child nodes of a document, folder, or network link.
 @note Values of 'checkOffOnly' are represented as `#AGSKMLListItemTypeCheck`.
 @since 100.4
 @see [Google's KML reference documentation](https://developers.google.com/kml/documentation/kmlreference#liststyle)
 for more information about list item types.
 */
typedef NS_ENUM(NSInteger, AGSKMLListItemType)
{
    AGSKMLListItemTypeCheck = 0,                /*!< All child nodes can be enabled for display. In a table of contents, nodes should be shown with checkboxes to enable multiple selection. */
    AGSKMLListItemTypeRadioFolder = 1,          /*!< Only one child node can be enabled for display at any one time. In a table of contents, nodes should be shown with radio buttons. When a child node is selected for display, sibling nodes will be hidden automatically. */
    AGSKMLListItemTypeCheckHideChildren = 2,    /*!< All child nodes should be enabled for display, but they should not be shown in the table of contents. */
    AGSKMLListItemTypeUnknown = -1              /*!<  */
};

/** Refresh status of a KML node.
 @since 100.4
 */
typedef NS_ENUM(NSInteger, AGSKMLRefreshStatus)
{
    AGSKMLRefreshStatusNone = 0,            /*!< No status */
    AGSKMLRefreshStatusInProgress = 1,      /*!< Refresh in progress */
    AGSKMLRefreshStatusCompleted = 2,       /*!< Refresh completed */
    AGSKMLRefreshStatusFailed = 3           /*!< Refresh failed */
};

/** Defines the time-based refresh behavior of a KML network link.
 @since 100.5
 */
typedef NS_ENUM(NSInteger, AGSKMLRefreshMode) {
    AGSKMLRefreshModeOnChange = 0,          /*!< Refresh when the file is loaded and whenever the Link parameters change. */
    AGSKMLRefreshModeOnInterval = 1,        /*!< Refresh every n milliseconds, where n is specified by refresh interval. If no refresh interval is set, the default value is 4000 milliseconds. */
    AGSKMLRefreshModeOnExpire = 2,           /*!< Refresh the file when the expiration time is reached. If a fetched file has a NetworkLinkControl, the "expires" time takes precedence over expiration times specified in HTTP headers. If no "expires" time is specified, the HTTP max-age header is used (if present). If max-age is not present, the Expires HTTP header is used (if present). Currently this is treated the same as if the mode were onChange. */
};

/** Defines the view-based refresh behavior of a KML network link.
 @since 100.5
 */
typedef NS_ENUM(NSInteger, AGSKMLViewRefreshMode) {
    AGSKMLViewRefreshModeNever = 0,         /*!< Ignore changes in the view. */
    AGSKMLViewRefreshModeOnStop = 1,        /*!< Refresh the file n milliseconds after movement stops, where n is specified by view refresh time. If no view refresh time is set, the default value is 4000 milliseconds. */
    AGSKMLViewRefreshModeOnRequest = 2,     /*!< Refresh the file only when the user explicitly requests it. */
    AGSKMLViewRefreshModeOnRegion = 3,      /*!< Refresh the file when the Region becomes active. */
};

/** The tour execution status.
 @since 100.5
 */
typedef NS_ENUM(NSInteger, AGSKMLTourStatus) {
    AGSKMLTourStatusNotInitialized = 0,         /*!< Not Initialized */
    AGSKMLTourStatusInitializing = 1,           /*!< Initializing */
    AGSKMLTourStatusInitialized = 2,            /*!< Initialized */
    AGSKMLTourStatusPlaying = 3,                /*!< Playing */
    AGSKMLTourStatusPaused = 4,                 /*!< Paused */
    AGSKMLTourStatusCompleted = 5               /*!< Completed */
};

/** A KML color mode object.
 @since 100.6
 */
typedef NS_ENUM(NSInteger, AGSKMLColorMode) {
    AGSKMLColorModeNormal = 0,  /*!< Normal is the default value. This means that whatever Color property is set for the KMLColorStyle will be used. */
    AGSKMLColorModeRandom = 1   /*!< Random means whatever Color property is set for the KMLColorStyle will be overwritten by a random color. */
};

/** A KML units type object.
 Use the @c AGSKMLUnitsType enumeration (either via the @c AGSKMLImageCoordinate#xUnits and @c AGSKMLImageCoordinate#yUnits properties or
 via the @c AGSKMLImageCoordinate#initWithX:y:xUnits:yUnits: constructor) to establish the KML
 image coordinate system. Values of type fraction must be between @c 0.0 and @c 1.0. Values below @c 0.0 will be set to @c 0.0, and values above
 @c 1.0 will be set to @c 1.0. If a non-integral number is specified with a unit type of pixels or insetPixels, it will be rounded down to
 a whole number (e.g. 2.243 will be rounded to 2.0).
 @since 100.6
 */
typedef NS_ENUM(NSInteger, AGSKMLUnitsType) {
    AGSKMLUnitsTypeFraction = 0,     /*!< Fraction is the default value. Treat the x/y value as a fraction of the width/height. */
    AGSKMLUnitsTypePixels = 1,       /*!< Treat the x/y value as an integral number of pixels. */
    AGSKMLUnitsTypeInsetPixels = 2   /*!< Treat the x/y value as an integral number of pixels from the top right corner. */
};

#pragma mark - Navigation

/** The list of route tracking destination statuses
 Used to determine how close the current location is to the next destination.
 @since 100.6
 */
typedef NS_ENUM(NSInteger, AGSDestinationStatus) {
    AGSDestinationStatusNotReached = 0,   /*!< Not reached destination status */
    AGSDestinationStatusApproaching = 1,  /*!< Approaching destination status */
    AGSDestinationStatusReached = 2       /*!< Reached destination status */
};

/** The route tracker's rerouting strategy
 Strategies determine which locations will be used during rerouting
 and/or if new route is optimized.
 @since 100.6
 */
typedef NS_ENUM(NSInteger, AGSReroutingStrategy) {
    AGSReroutingStrategyToNextWaypoint = 0,       /*!< Reroute to next unvisited waypoint, rest break, or stop */
    AGSReroutingStrategyToNextStop = 1,           /*!< Reroute to next unvisited stop */
    AGSReroutingStrategyResequenceStopsOnly = 2   /*!< Re-sequence (optimize) all remaining stops, which will drop all waypoints and rest breaks */
};

/** The list of @c AGSVoiceGuidance notification types
 Used to determine type of voice guidance notification.
 @since 100.6
 */
typedef NS_ENUM(NSInteger, AGSVoiceGuidanceType) {
    AGSVoiceGuidanceTypeApproachingManeuver = 0,     /*!< Approaching maneuver */
    AGSVoiceGuidanceTypeAtManeuver = 1,              /*!< At maneuver */
    AGSVoiceGuidanceTypeApproachingDestination = 2   /*!< Approaching destination */
};

#pragma mark - Utility Network

/** An enumeration of the various association roles
 A feature's association role specifies how the feature can associate
 with other features, such as containment or attachment.
 @see @c AGSUtilityAssetType
 @since 100.6
 */
typedef NS_ENUM(NSInteger, AGSUtilityAssociationRole) {
    AGSUtilityAssociationRoleNone = 0,       /*!< These are features that are neither a container nor a structure but can connect to or be contained by other structures */
    AGSUtilityAssociationRoleContainer = 1,  /*!< Features of this asset type can contain other features as contents */
    AGSUtilityAssociationRoleStructure = 2   /*!< Features of this asset type can have other features attached to
                                              them
                                              */
};

/** An enumeration of the various @c AGSUtilityNetworkAttribute data types
 The @c AGSUtilityNetworkAttribute objects in a topological index are limited to specific data types.
 @since 100.6
 */
typedef NS_ENUM(NSInteger, AGSUtilityNetworkAttributeDataType) {
    AGSUtilityNetworkAttributeDataTypeInteger = 0,  /*!< A signed 64-bit integer value */
    AGSUtilityNetworkAttributeDataTypeFloat = 1,    /*!< A floating point value */
    AGSUtilityNetworkAttributeDataTypeDouble = 2,   /*!< A double precision floating point value */
    AGSUtilityNetworkAttributeDataTypeBoolean = 3   /*!< A Boolean value */
};

/** An enumeration of the various network source types
 @see @c AGSUtilityNetworkSource
 @since 100.6
 */
typedef NS_ENUM(NSInteger, AGSUtilityNetworkSourceType) {
    AGSUtilityNetworkSourceTypeJunction = 0,  /*!< The network source references junctions in a feature class or table. */
    AGSUtilityNetworkSourceTypeEdge = 1       /*!< The network source references edges in a feature class or table. */
};

/** An enumeration of the various usage types of an @c AGSUtilityNetworkSource
 These values indicate how an @c AGSUtilityNetworkSource is used in a utility network.
 @since 100.6
 */
typedef NS_ENUM(NSInteger, AGSUtilityNetworkSourceUsageType) {
    AGSUtilityNetworkSourceUsageTypeDevice = 0,             /*!< Network devices, such as transformers or valves */
    AGSUtilityNetworkSourceUsageTypeJunction = 1,           /*!< Miscellaneous junctions not typically tracked as assets, such as taps */
    AGSUtilityNetworkSourceUsageTypeLine = 2,               /*!< Lines such as conductors, pipes, or fiber strands */
    AGSUtilityNetworkSourceUsageTypeAssembly = 3,           /*!< Assemblies such as switchgear */
    AGSUtilityNetworkSourceUsageTypeSubnetLine = 4,         /*!< System-generated subnetwork lines */
    AGSUtilityNetworkSourceUsageTypeStructureJunction = 5,  /*!< Point structures, such as poles */
    AGSUtilityNetworkSourceUsageTypeStructureLine = 6,      /*!< Linear structures, such as ducts, conduits, or buffer tubes */
    AGSUtilityNetworkSourceUsageTypeStructureBoundary = 7   /*!< Polygonal structures, such as substations or town border stations */
};

/** An enumeration of the various result types returned by a trace operation
 @since 100.6
 */
typedef NS_ENUM(NSInteger, AGSUtilityTraceResultType) {
    AGSUtilityTraceResultTypeElements = 0   /*!< An array of @c AGSUtilityElement objects */
};

/** An enumeration of the various types of traces that may be performed with a utility network
 @since 100.6
 */
typedef NS_ENUM(NSInteger, AGSUtilityTraceType) {
    AGSUtilityTraceTypeConnected = 0   /*!< A connected trace */
};

NS_ASSUME_NONNULL_END

