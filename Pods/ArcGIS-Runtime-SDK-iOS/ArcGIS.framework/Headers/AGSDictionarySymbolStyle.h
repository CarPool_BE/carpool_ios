/*
 COPYRIGHT 2016 ESRI
 
 TRADE SECRETS: ESRI PROPRIETARY AND CONFIDENTIAL
 Unpublished material - all rights reserved under the
 Copyright Laws of the United States and applicable international
 laws, treaties, and conventions.
 
 For additional information, contact:
 Environmental Systems Research Institute, Inc.
 Attn: Contracts and Legal Services Department
 380 New York Street
 Redlands, California, 92373
 USA
 
 email: contracts@esri.com
 */

#import <ArcGIS/AGSSymbolStyle.h>

@class AGSDictionarySymbolStyleConfiguration;
@class AGSSymbol;

/** @file AGSDictionarySymbolStyle.h */ //Required for Globals API doc

/** @brief A dictionary symbol style object containing symbol primitives and rules for generating symbols from attribute values.
 
 An @c AGSDictionarySymbolStyle is created with a style file (an SQLite database with a .stylx extension,
 created with ArcGIS Pro). The style file contains a set of symbol primitives and a rule engine that
 parses input fields. The @c AGSDictionarySymbolStyle assembles new symbols based on the input attribute values.
 @c AGSDictionarySymbolStyle is often used to render symbols from a military specification (such as Mil2525D or App6B)
 but can also be used with a custom style.
 An @c AGSDictionarySymbolStyle is used in conjunction with an @c AGSDictionaryRenderer to symbolize geoelements in a
 @c AGSFeatureLayer or @c AGSGraphicsOverlay.
 
 @see `AGSDictionaryRenderer`
 @since 100
 */
@interface AGSDictionarySymbolStyle : AGSSymbolStyle

NS_ASSUME_NONNULL_BEGIN

#pragma mark -
#pragma mark initializers

-(instancetype)init NS_UNAVAILABLE;

/** Creates a new dictionary symbol style object from the style file at the provided location.
 @param fileURL The physical location the .stylx file.
 @since 100.6
 */
-(instancetype)initWithFileURL:(NSURL *)fileURL;

/** Creates a new dictionary symbol style object from the style file at the provided location.
 @param fileURL The physical location the .stylx file.
 @since 100.6
 */
+(instancetype)dictionarySymbolStyleWithFileURL:(NSURL *)fileURL;

/** Creates a new dictionary symbol style object from the name style file
 within the application bundle or shared documents directory.
 @param name The name of the stylx file (excluding the .stylx file extension).
 @since 100.6
 */
-(instancetype)initWithName:(NSString*)name;

/** Creates a new dictionary symbol style object from the name style file
 within the application bundle or shared documents directory.
 @param name The name of the stylx file (excluding the .stylx file extension).
 @since 100.6
 */
+(instancetype)dictionarySymbolStyleWithName:(NSString*)name;

#pragma mark -
#pragma mark properties

/** Gets the configuration settings from the Custom Dictionary.
 @since 100.6
 */
@property (nonatomic, copy, readonly) NSArray<AGSDictionarySymbolStyleConfiguration *> *configurations;

/** Gets the name of the custom Arcade-based style. This function returns an empty string on older version style files.
 @since 100.6
 */
@property (nonatomic, copy, readonly) NSString *dictionaryName;

/** Gets the string list of symbology attributes required to obtain a symbol from a rule engine associated to an @c AGSDictionarySymbolStyle.
 For example, mil2525d would include "identity", "symbolset", "symbolentity", and "modifier1", among
 many others.
 @since 100.0
 */
@property (nonatomic, copy, readonly) NSArray<NSString*> *symbologyFieldNames;

/** Gets the string list of text attributes required to obtain text and placement for symbol from a rule engine associated to a DictionarySymbolStyle.
 For example, mil2525d would include "combateffectiveness", "credibility", "reliability", and
 "staffcomment", among many others.
 @since 100.0
 */
@property (nonatomic, copy, readonly) NSArray<NSString*> *textFieldNames;

#pragma mark -
#pragma mark methods


/** Creates a symbol based on the provided attributes. For example, with mil2525d, you may want to obtain a symbol where
 "symbolset" is 40, "modifier1" is "Incident Qualifier : Accident", "echelon" is "Army", and so on. All of these
 key/value pairs can be used to obtain the specific symbol that you need. Once you have that symbol, you can apply
 it to a graphic or renderer, obtain its swatch image, or serialize to JSON.
 @param attributes used to create the symbol
 @param completion block that is invoked when the operation completes. The symbol is populated if the operation succeeds, else the error is populated if the operation fails.
 @since 100
 */
-(id<AGSCancelable>)symbolWithAttributes:(NSDictionary<NSString*, id> *)attributes completion:(void(^)(AGSSymbol * __nullable symbol, NSError * __nullable error))completion;

@end

@interface AGSDictionarySymbolStyle (AGSDeprecated)

/** Creates a new dictionary symbol style object for a known military standard (such as Mil2525D).
 @param specificationType The name of supported military standard to use for the dictionary style.
 @since 100.0
 @deprecated 100.6.0. The specification definition has been moved from the Runtime source code to an Arcade expression in the style file. Creating a DictionarySymbolStyle from a specification is no longer supported with the new military symbol styles.
 */
-(instancetype)initWithSpecificationType:(NSString *)specificationType __deprecated_msg("The specification definition has been moved from the Runtime source code to an Arcade expression in the style file. Creating a DictionarySymbolStyle from a specification is no longer supported with the new military symbol styles.");

/** Creates a new dictionary symbol style object for a known military standard (such as Mil2525D).
 @param specificationType The name of supported military standard to use for the dictionary style.
 @since 100.0
 @deprecated 100.6.0. The specification definition has been moved from the Runtime source code to an Arcade expression in the style file. Creating a DictionarySymbolStyle from a specification is no longer supported with the new military symbol styles.
 */
+(instancetype)dictionarySymbolStyleWithSpecificationType:(NSString *)specificationType __deprecated_msg("The specification definition has been moved from the Runtime source code to an Arcade expression in the style file. Creating a DictionarySymbolStyle from a specification is no longer supported with the new military symbol styles.");

/** Creates a new Symbol dictionary object using the style file identified by the specification type or style location.
 @param specificationType The name of supported military standard to use for the dictionary style. Can be NULL.
 @param styleURL The physical location of the .stylx file. You must provide the full path (e.g. ../../../mil2525c/mil2525c.stylx). Can be NULL.
 @since 100.1
 @deprecated 100.6.0. The specification definition has been moved from the Runtime source code to an Arcade expression in the style file. Creating a DictionarySymbolStyle from a specification is no longer supported with the new military symbol styles.
 */
-(instancetype)initWithSpecificationType:(NSString *)specificationType
                                styleURL:(NSURL *)styleURL __deprecated_msg("The specification definition has been moved from the Runtime source code to an Arcade expression in the style file. Creating a DictionarySymbolStyle from a specification is no longer supported with the new military symbol styles.");

/** Creates a new Symbol dictionary object using the style file identified by the specification type or style location.
 @param specificationType The name of supported military standard to use for the dictionary style. Can be NULL.
 @param styleURL The physical location of the .stylx file. You must provide the full path (e.g. ../../../mil2525c/mil2525c.stylx). Can be NULL.
 @since 100.1
 @deprecated 100.6.0. The specification definition has been moved from the Runtime source code to an Arcade expression in the style file. Creating a DictionarySymbolStyle from a specification is no longer supported with the new military symbol styles.
 */
+(instancetype)dictionarySymbolStyleWithSpecificationType:(NSString *)specificationType
                                                 styleURL:(NSURL *)styleURL __deprecated_msg("The specification definition has been moved from the Runtime source code to an Arcade expression in the style file. Creating a DictionarySymbolStyle from a specification is no longer supported with the new military symbol styles.");

/** Gets or sets the map of configuration attributes and their current values. It is specific to a rule engine and its standard implementation associated to the DictionarySymbolStyle.
 @since 100.0
 @deprecated 100.6.0. These configuration settings are specific to the older style files.  These have no effect on the newer style files where the symbol creation comes from the Arcade expression in the style.
 */
@property (nonatomic, copy, readwrite) NSDictionary<NSString *, NSString *> *configurationProperties __deprecated_msg("These configuration settings are specific to the older style files.  These have no effect on the newer style files where the symbol creation comes from the Arcade expression in the style.");

/** Gets or sets the name of the dictionary (.stylx file), such as "Mil2525D".
 @since 100.0
 @deprecated 100.6.0. With the new Arcade-based styles, DictionarySymbolStyles are no longer specific to military specifications and can be customized by the end user. Use the @c AGSDictionarySymbolStyle#dictionaryName property instead.
 */
@property (nonatomic, copy, readonly) NSString *specificationType __deprecated_msg("With the new Arcade-based styles, DictionarySymbolStyles are no longer specific to military specifications and can be customized by the end user. Use the @c AGSDictionarySymbolStyle#dictionaryName property instead.");

@end

NS_ASSUME_NONNULL_END
