/*
 COPYRIGHT 1995-2019 ESRI
 
 TRADE SECRETS: ESRI PROPRIETARY AND CONFIDENTIAL
 Unpublished material - all rights reserved under the
 Copyright Laws of the United States and applicable international
 laws, treaties, and conventions.
 
 For additional information, contact:
 Environmental Systems Research Institute, Inc.
 Attn: Contracts and Legal Services Department
 380 New York Street
 Redlands, California, 92373
 USA
 
 email: contracts@esri.com
 */

/** @file AGSDictionarySymbolStyleConfiguration.h */ //Required for Globals API doc

#import <ArcGIS/AGSObject.h>

NS_ASSUME_NONNULL_BEGIN

/** @brief Configuration settings for a custom @c AGSDictionarySymbolStyle.
 
 These configuration settings are returned from an @c AGSDictionarySymbolStyle.
 @since 100.6
 */
@interface AGSDictionarySymbolStyleConfiguration : AGSObject

#pragma mark -
#pragma mark initializers

-(instancetype)init NS_UNAVAILABLE;

#pragma mark -
#pragma mark properties

/** Returns the list of possible values for this configuration property.
 @since 100.6
 */
@property (nonatomic, copy, readonly) NSArray<id> *domain;

/** Returns a description of the configuration as defined by the style author.
 @since 100.6
 */
@property (nonatomic, copy, readonly) NSString *info;

/** Returns the name of the property as defined by the style configuration.
 @since 100.6
 */
@property (nonatomic, copy, readonly) NSString *name;

/** Gets or sets the value of the configuration property.
 The configuration may be defined with a default value.
 @since 100.6
 */
@property (nonatomic, strong, readwrite) id value;

#pragma mark -
#pragma mark methods

@end

NS_ASSUME_NONNULL_END
