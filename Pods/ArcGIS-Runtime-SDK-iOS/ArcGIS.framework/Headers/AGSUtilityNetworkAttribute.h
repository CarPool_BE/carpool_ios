/*
 COPYRIGHT 1995-2019 ESRI
 
 TRADE SECRETS: ESRI PROPRIETARY AND CONFIDENTIAL
 Unpublished material - all rights reserved under the
 Copyright Laws of the United States and applicable international
 laws, treaties, and conventions.
 
 For additional information, contact:
 Environmental Systems Research Institute, Inc.
 Attn: Contracts and Legal Services Department
 380 New York Street
 Redlands, California, 92373
 USA
 
 email: contracts@esri.com
 */

/** @file AGSUtilityNetworkAttribute.h */ //Required for Globals API doc

#import <ArcGIS/AGSObject.h>

NS_ASSUME_NONNULL_BEGIN

/** @brief A network attribute in a utility network
 
 A Network Attribute is an attribute that is copied and stored in the topological index.
 The utility network tracing task can read and make decisions using network attributes that are
 stored directly in the topological index. Without needing to fetch the individual features to get their attributes,
 the topological index provides performance that is orders of magnitude faster.
 
 The network attributes in a topological index are limited to specific data types and aggregate size.
 @since 100.6
 */
@interface AGSUtilityNetworkAttribute : AGSObject

#pragma mark -
#pragma mark initializers

-(instancetype)init NS_UNAVAILABLE;

#pragma mark -
#pragma mark properties

/** The @c AGSUtilityNetworkAttributeDataType for the @c AGSUtilityNetworkAttribute
 The network attributes in a topological index are limited to specific data types.
 @since 100.6
 */
@property (nonatomic, assign, readonly) AGSUtilityNetworkAttributeDataType dataType;

/** The name of the @c AGSUtilityNetworkAttribute
 @since 100.6
 */
@property (nonatomic, copy, readonly) NSString *name;

#pragma mark -
#pragma mark methods

@end

NS_ASSUME_NONNULL_END
