/*
 COPYRIGHT 1995-2019 ESRI
 
 TRADE SECRETS: ESRI PROPRIETARY AND CONFIDENTIAL
 Unpublished material - all rights reserved under the
 Copyright Laws of the United States and applicable international
 laws, treaties, and conventions.
 
 For additional information, contact:
 Environmental Systems Research Institute, Inc.
 Attn: Contracts and Legal Services Department
 380 New York Street
 Redlands, California, 92373
 USA
 
 email: contracts@esri.com
 */

/** @file AGSUtilityElementTraceResult.h */ //Required for Globals API doc

#import <ArcGIS/AGSUtilityTraceResult.h>

NS_ASSUME_NONNULL_BEGIN

@class AGSUtilityElement;

/** @brief A trace result set comprised of an array of @c AGSUtilityElement
 objects

 @since 100.6
 */
@interface AGSUtilityElementTraceResult : AGSUtilityTraceResult

#pragma mark -
#pragma mark initializers

-(instancetype)init NS_UNAVAILABLE;

#pragma mark -
#pragma mark properties

/** The array of @c AGSUtilityElement that comprise the trace 
 results
 @since 100.6
 */
@property (nonatomic, copy, readonly) NSArray<AGSUtilityElement *> *elements;

#pragma mark -
#pragma mark methods

@end

NS_ASSUME_NONNULL_END
