/*
 COPYRIGHT 1995-2019 ESRI
 
 TRADE SECRETS: ESRI PROPRIETARY AND CONFIDENTIAL
 Unpublished material - all rights reserved under the
 Copyright Laws of the United States and applicable international
 laws, treaties, and conventions.
 
 For additional information, contact:
 Environmental Systems Research Institute, Inc.
 Attn: Contracts and Legal Services Department
 380 New York Street
 Redlands, California, 92373
 USA
 
 email: contracts@esri.com
 */

/** @file AGSKMLPolygonStyle.h */ //Required for Globals API doc

#import <ArcGIS/AGSKMLColorStyle.h>

NS_ASSUME_NONNULL_BEGIN

/** @brief Specifies the drawing style for all polygons in a KML node
 
 Specifies the drawing style for all polygons, including polygon extrusions (which look like the walls of buildings) and line extrusions (which look like solid fences).

 Controls how the fill of an @c AGSKMLNode is displayed and whether or not the outline of an @c AGSKMLNode is displayed. If the outline is displayed, the outline
 will use the current @c AGSKMLLineStyle. Corresponds to a <PolyStyle> in a KML document.
 @since 100.6
 */
@interface AGSKMLPolygonStyle : AGSKMLColorStyle

#pragma mark -
#pragma mark initializers

-(instancetype)init NS_UNAVAILABLE;

/** Creates a KML polygon style with a specified fill color.
 @param color The style's fill color.
 @since 100.6
 */
-(instancetype)initWithColor:(AGSColor *)color;

/** Creates a KML polygon style with a specified fill color.
 @param color The style's fill color.
 @since 100.6
 */
+(instancetype)KMLPolygonStyleWithColor:(AGSColor *)color;

#pragma mark -
#pragma mark properties

/** Boolean value. Specifies whether or not to fill the polygon.
 @since 100.6
 */
@property (nonatomic, assign, readwrite, getter=isFilled) BOOL filled;

/** Specifies whether or not to outline the polygon. Polygon outlines use the current @c AGSKMLLineStyle of the KML node.
 @since 100.6
 */
@property (nonatomic, assign, readwrite, getter=isOutlined) BOOL outlined;

#pragma mark -
#pragma mark methods

@end

NS_ASSUME_NONNULL_END
