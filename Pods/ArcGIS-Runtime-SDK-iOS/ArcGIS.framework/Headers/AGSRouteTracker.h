/*
 COPYRIGHT 1995-2019 ESRI
 
 TRADE SECRETS: ESRI PROPRIETARY AND CONFIDENTIAL
 Unpublished material - all rights reserved under the
 Copyright Laws of the United States and applicable international
 laws, treaties, and conventions.
 
 For additional information, contact:
 Environmental Systems Research Institute, Inc.
 Attn: Contracts and Legal Services Department
 380 New York Street
 Redlands, California, 92373
 USA
 
 email: contracts@esri.com
 */

/** @file AGSRouteTracker.h */ //Required for Globals API doc

#import <ArcGIS/AGSObject.h>

NS_ASSUME_NONNULL_BEGIN

@class AGSLocation;
@class AGSRouteParameters;
@class AGSRouteResult;
@class AGSRouteTask;
@class AGSTrackingStatus;
@class AGSVoiceGuidance;

@protocol AGSRouteTrackerDelegate;

/** @brief Tracks the vehicle location along the route, checking status and reporting progress
 
 Instances of this class represent methods for processing navigation.
 The generic workflow would be:
 1. Create Route Tracker.
 2. Enable rerouting @c AGSRouteTracker#enableReroutingWithRouteTask:routeParameters:strategy:visitFirstStopOnStart:completion:.
 3. Subscribe @c AGSRouteTracker#trackLocation:completion: to Location Provider (GPS, simulation, etc. Usual tracking happens each 1 second).
 4. Subscribe Map to updating location after @c AGSRouteTracker#trackLocation:completion: (Use display location).
 5. Call @c AGSRouteTracker#switchToNextDestinationWithCompletion: after obtaining @c AGSDestinationStatusReached status.
 Before calling @c AGSRouteTracker#switchToNextDestinationWithCompletion: make sure that @c AGSTrackingStatus#remainingDestinationCount > 1,
 a value of 1 means we are proceeding to last destination and there are no other destinations to switch to.
 @since 100.6
 @license{Basic}
 */
@interface AGSRouteTracker : AGSObject

#pragma mark -
#pragma mark initializers

-(instancetype)init NS_UNAVAILABLE;

/** Creates a Route Tracker
 Will return nil if Route, Stops or Directions are missing.
 @param routeResult The Route Result object
 @param routeIndex The zero-based index of the Route in Route Result which will use for tracking
 @see @c AGSRouteResult
 @since 100.6
 */
-(nullable instancetype)initWithRouteResult:(AGSRouteResult *)routeResult
                                 routeIndex:(NSInteger)routeIndex;

/** Creates a Route Tracker
 Will return nil if Route, Stops or Directions are missing.
 @param routeResult The Route Result object
 @param routeIndex The zero-based index of the Route in Route Result which will use for tracking
 @see @c AGSRouteResult
 @since 100.6
 */
+(nullable instancetype)routeTrackerWithRouteResult:(AGSRouteResult *)routeResult
                                         routeIndex:(NSInteger)routeIndex;

#pragma mark -
#pragma mark properties

/** The delegate that receives callbacks for the @c AGSRouteTracker.
 @see @c AGSRouteTrackerDelegate
 @since 100.6
 */
@property (nullable, nonatomic, weak, readwrite) id<AGSRouteTrackerDelegate> delegate;

/** Reports state of automatic rerouting property. True if rerouting is enabled, otherwise False
 @see @c AGSRouteTracker#enableReroutingWithRouteTask:routeParameters:strategy:visitFirstStopOnStart:completion:, @c AGSRouteTracker#disableRerouting
 @since 100.6
 */
@property (nonatomic, assign, readonly, getter=isReroutingEnabled) BOOL reroutingEnabled;

/** Contains tracker status for current location
 @see @c AGSTrackingStatus
 @since 100.6
 */
@property (nullable, nonatomic, strong, readonly) AGSTrackingStatus *trackingStatus;

/** The unit system used for generating voice guidance
 Default value is @c AGSUnitSystemMetric. Supported values are @c AGSUnitSystemMetric and @c AGSUnitSystemImperial.
 @since 100.6
 */
@property (nonatomic, assign, readwrite) AGSUnitSystem voiceGuidanceUnitSystem;

#pragma mark -
#pragma mark methods

/** Cancels rerouting task of route tracker.
 If rerouting is started this method will interrupt rerouting background process.
 @since 100.6
 */
-(void)cancelRerouting;

/** Disables automatic rerouting functionality of route tracker.
 After disabling rerouting tracker will not automatically re-calculate route 
 in case isOnRoute = false status.
 @see @c AGSRouteTracker#enableReroutingWithRouteTask:routeParameters:strategy:visitFirstStopOnStart:completion:
 @since 100.6
 */
-(void)disableRerouting;

/** Enables the automatic off route rerouting functionality of route tracker.
 Tracker will start a re-routing calculation automatically as necessary (in general when vehicle isOnRoute = false).
 Note, tracker validates that the passed in GPS location is "on" the transportation network, if it is not (e.g. in a parking lot) 
 re-routing will not occur.  Re-routing will occur once the passed in GPS location is "on" the transportation network.
 @param routeTask A route task.
 @param routeParameters A route parameters.
 @param strategy A rerouting strategy that determinate how route tracker should behave during reroute process (to next waypoint by default).
 @param visitFirstStopOnStart A visit first stop flag.
 @param completion A block that is invoked when the operation finishes. The #error parameter is populated on failure.
 @return The operation which can be canceled
 @see @c AGSRouteTask, @c AGSRouteParameters, @c AGSReroutingStrategy
 @since 100.6
 */
-(id<AGSCancelable>)enableReroutingWithRouteTask:(AGSRouteTask *)routeTask 
                                 routeParameters:(AGSRouteParameters *)routeParameters 
                                        strategy:(AGSReroutingStrategy)strategy 
                           visitFirstStopOnStart:(BOOL)visitFirstStopOnStart 
                                      completion:(void(^)(NSError * __nullable error))completion;

/** Gets voice guidance object.
 Latest voice guidance based on last @c AGSLocation that was passed to @c AGSRouteTracker#trackLocation:completion:.
 Can be used to repeat last/latest voice guidance, where distances will be based on current location.
 @return Voice guidance.
 @see @c AGSTrackingStatus
 @since 100.6
 */
-(nullable AGSVoiceGuidance *)generateVoiceGuidance;

/** Switches route tracker to the next destination.
 Call @c AGSRouteTracker#switchToNextDestinationWithCompletion: after obtaining @c AGSDestinationStatusReached status.
 Before calling @c AGSRouteTracker#switchToNextDestinationWithCompletion: make sure that @c AGSTrackingStatus#remainingDestinationCount > 1,
 a value of 1 means we are proceeding to last destination and there are no other destinations to switch to.
 Also this method can be called after @c AGSDestinationStatusApproaching if vehicle can not get near enough to destination 
 for @c AGSDestinationStatusReached to be raised (for example destination located in center of inaccessible building).
 The delegate is called when the @c AGSTrackingStatus is updated accordingly.
 @param completion A block that is invoked when the operation finishes. The #error parameter is populated on failure.
 @return The operation which can be canceled
 @since 100.6
 */
-(id<AGSCancelable>)switchToNextDestinationWithCompletion:(nullable void(^)(NSError * __nullable error))completion;

/** Tracks location and updates route tracking status.
 Location must have valid values for XY, speed (in meters per second), course (in degrees) and timestamp.
 The delegate is called when the @c AGSTrackingStatus is updated for this location.
 @param location A GPS location.
 @param completion A block that is invoked when the operation finishes. The #error parameter is populated on failure.
 @return The operation which can be canceled
 @see @c AGSLocation
 @since 100.6
 */
-(id<AGSCancelable>)trackLocation:(AGSLocation *)location 
                       completion:(nullable void(^)(NSError * __nullable error))completion;

@end

/**
 Methods for managing route tracker changes.
 @since 100.6
 */
@protocol AGSRouteTrackerDelegate <NSObject>

@optional

/**
 Tells the delegate that the route tracker has a new voice guidance available
 
 @param routeTracker The route tracker object informing the delegate of this impending event.
 @param voiceGuidance The new voice guidance object that was generated.
 @since 100.6
 */
-(void)routeTracker:(AGSRouteTracker*)routeTracker
didGenerateNewVoiceGuidance:(AGSVoiceGuidance*)voiceGuidance;


/**
 Tells the delegate that the route tracker started re-routing.
 
 @param routeTracker The route tracker object informing the delegate of this impending event.
 @since 100.6
 */
-(void)routeTrackerRerouteDidStart:(AGSRouteTracker*)routeTracker;


/**
 Tells the delegate that the route tracker completed re-routing.
 
 @param routeTracker The route tracker object informing the delegate of this impending event.
 @param trackingStatus The tracking status after the re-route completed.
 @param error The error if the re-route failed.
 @since 100.6
 */
-(void)routeTracker:(AGSRouteTracker*)routeTracker
rerouteDidCompleteWithTrackingStatus:(nullable AGSTrackingStatus*)trackingStatus
              error:(nullable NSError*)error;


/**
 Tells the delegate that the route tracker has updated it's tracking status.
 
 @param routeTracker The route tracker object informing the delegate of this impending event.
 @param trackingStatus The latest tracking status.
 @since 100.6
 */
-(void)routeTracker:(AGSRouteTracker*)routeTracker
didUpdateTrackingStatus:(AGSTrackingStatus*)trackingStatus;

@end

NS_ASSUME_NONNULL_END
