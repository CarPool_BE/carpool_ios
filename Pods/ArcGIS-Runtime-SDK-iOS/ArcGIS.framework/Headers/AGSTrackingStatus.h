/*
 COPYRIGHT 1995-2019 ESRI
 
 TRADE SECRETS: ESRI PROPRIETARY AND CONFIDENTIAL
 Unpublished material - all rights reserved under the
 Copyright Laws of the United States and applicable international
 laws, treaties, and conventions.
 
 For additional information, contact:
 Environmental Systems Research Institute, Inc.
 Attn: Contracts and Legal Services Department
 380 New York Street
 Redlands, California, 92373
 USA
 
 email: contracts@esri.com
 */

/** @file AGSTrackingStatus.h */ //Required for Globals API doc

#import <ArcGIS/AGSObject.h>

NS_ASSUME_NONNULL_BEGIN

@class AGSLocation;
@class AGSRouteResult;
@class AGSTrackingProgress;

/** @brief Defines route tracking current status data

 Contains information about route tracker status.
 @see @c AGSRouteTrackerDelegate#routeTracker:didUpdateTrackingStatus:
 @since 100.6
 */
@interface AGSTrackingStatus : AGSObject

#pragma mark -
#pragma mark initializers

-(instancetype)init NS_UNAVAILABLE;

#pragma mark -
#pragma mark properties

/** The current maneuver index
 The index of current maneuver in @c AGSRoute#directionManeuvers.
 @since 100.6
 */
@property (nonatomic, assign, readonly) NSInteger currentManeuverIndex;

/** The tracking progress until next destination (stop)
 @see @c AGSTrackingProgress
 @since 100.6
 */
@property (nonatomic, strong, readonly) AGSTrackingProgress *destinationProgress;

/** The destination status
 @see @c AGSDestinationStatus
 @since 100.6
 */
@property (nonatomic, assign, readonly) AGSDestinationStatus destinationStatus;

/** The display location
 If current location isOnRoute = true then this will be an adjusted location (snapped to route) and bearing (smoothed). 
If current location isOnRoute = false then this will be the passed in GPS location, but with an adjusted bearing (smoothed). 
This location should be used for displaying current position on a map.
 @see @c AGSLocation
 @since 100.6
 */
@property (nonatomic, strong, readonly) AGSLocation *displayLocation;

/** The location on route
 Snapped to route location of last GPS location passed in to @c AGSRouteTracker#trackLocation:completion:.
If current status is isOnRoute = true this will be the same as the @c AGSTrackingStatus#displayLocation.
If current status is isOnRoute = false this value is not updated and last location that was on 
the route will be returned.
 @see @c AGSLocation
 @since 100.6
 */
@property (nonatomic, strong, readonly) AGSLocation *locationOnRoute;

/** The tracking progress along current maneuver
 @see @c AGSTrackingProgress
 @since 100.6
 */
@property (nonatomic, strong, readonly) AGSTrackingProgress *maneuverProgress;

/** Boolean indicating whether or not the current location is on the route
 @since 100.6
 */
@property (nonatomic, assign, readonly, getter=isOnRoute) BOOL onRoute;

/** The remaining destination count
 The number of (routed) stops yet to be visited.  This value does not include waypoints, 
unlocated/unrouted locations.
Calling @c AGSRouteTracker#switchToNextDestinationWithCompletion: will decrease value.
The invalid value for remaining destination count is -1.
 @see @c AGSRouteTracker#switchToNextDestinationWithCompletion:
 @since 100.6
 */
@property (nonatomic, assign, readonly) NSInteger remainingDestinationCount;

/** Boolean indicating whether the route is currently being calculated
 For notification about route calculating implement @c AGSRouteTrackerDelegate#routeTrackerRerouteDidStart:.
 @see @c AGSRouteTrackerDelegate#routeTrackerRerouteDidStart:, @c AGSRouteTrackerDelegate:routeTracker:rerouteDidCompleteWithTrackingStatus:error:
 @since 100.6
 */
@property (nonatomic, assign, readonly, getter=isRouteCalculating) BOOL routeCalculating;

/** The tracking progress along entire route
 @see @c AGSTrackingProgress
 @since 100.6
 */
@property (nonatomic, strong, readonly) AGSTrackingProgress *routeProgress;

/** The route result presently being used by route tracker
 If rerouting happened during tracking, this @c AGSRouteResult can be different 
than the @c AGSRouteResult originally set in the @c AGSRouteTracker.
For being notified when the routeResult is updated via callback, implement @c AGSRouteTrackerDelegate:routeTracker:rerouteDidCompleteWithTrackingStatus:error:.
 @see @c AGSRouteTrackerDelegate#routeTrackerRerouteDidStart:, @c AGSRouteTrackerDelegate:routeTracker:rerouteDidCompleteWithTrackingStatus:error:
 @since 100.6
 */
@property (nonatomic, strong, readonly) AGSRouteResult *routeResult;

#pragma mark -
#pragma mark methods

@end

NS_ASSUME_NONNULL_END
