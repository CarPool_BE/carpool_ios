/*
 COPYRIGHT 2017 ESRI
 
 TRADE SECRETS: ESRI PROPRIETARY AND CONFIDENTIAL
 Unpublished material - all rights reserved under the
 Copyright Laws of the United States and applicable international
 laws, treaties, and conventions.
 
 For additional information, contact:
 Environmental Systems Research Institute, Inc.
 Attn: Contracts and Legal Services Department
 380 New York Street
 Redlands, California, 92373
 USA
 
 email: contracts@esri.com
 */

#import <ArcGIS/AGSObject.h>

@class AGSLayer;
@class AGSOfflineMapSyncLayerResult;
@class AGSFeatureTable;

/** @file AGSOfflineMapSyncResult.h */ //Required for Globals API doc

/** @brief Result of a sync operation by `AGSOfflineMapSyncJob`
 
 Instances of this class represent results of a sync operation initiated by `AGSOfflineMapSyncJob`
 
 @since 100.1
 */
@interface AGSOfflineMapSyncResult : AGSObject

NS_ASSUME_NONNULL_BEGIN

#pragma mark -
#pragma mark initializers

- (instancetype)init NS_UNAVAILABLE;

#pragma mark -
#pragma mark properties

/** Indicates if any sync operations for feature layers and tables in the offline map encountered an error. If `true`, you should inspect `#layerResults` and `#tableResults` to find out which layer or table encountered an error and to get more information about the error.
 @since 100.1
 */
@property (nonatomic, assign, readonly) BOOL hasErrors;

/** Results of the sync operations for feature layers
 @since 100.1
 */
@property (nonatomic, copy, readonly) NSDictionary<AGSLayer *, AGSOfflineMapSyncLayerResult *> *layerResults;

/** Indicates whether the mobile map package must be closed and reopened with a new instance to allow the updates to take effect.
 In some cases, applying updates may require files such as mobile geodatabases to be replaced
 with a new version. When reopen is required, this property will be true and you need to:
 
 - release all instances of the @c AGSMobileMapPackage and its maps and layers. For example, you
 should remove any @c AGSMap instances from the @c AGSMapView and remove layers and geodatabases from
 custom views such as lists and tables of contents. This will
 allow files to be closed.
 - call @c AGSMobileMapPackage#close to close the mobile map package instance and files.
 - create a new @c AGSMobileMapPackage instance with the same path.
 - load the new @c AGSMobileMapPackage. This will apply updates to geodatabases
 that have been replaced via a scheduled updates workflow.
 @since 100.6
 */
@property (nonatomic, assign, readonly, getter=isMobileMapPackageReopenRequired) BOOL mobileMapPackageReopenRequired;

/** Results of the sync operations for feature tables
 @since 100.1
 */
@property (nonatomic, copy, readonly) NSDictionary<AGSFeatureTable *, AGSOfflineMapSyncLayerResult *> *tableResults;

#pragma mark -
#pragma mark methods

@end

NS_ASSUME_NONNULL_END
