/*
 COPYRIGHT 1995-2019 ESRI
 
 TRADE SECRETS: ESRI PROPRIETARY AND CONFIDENTIAL
 Unpublished material - all rights reserved under the
 Copyright Laws of the United States and applicable international
 laws, treaties, and conventions.
 
 For additional information, contact:
 Environmental Systems Research Institute, Inc.
 Attn: Contracts and Legal Services Department
 380 New York Street
 Redlands, California, 92373
 USA
 
 email: contracts@esri.com
 */

/** @file AGSUtilityElement.h */ //Required for Globals API doc

#import <ArcGIS/AGSObject.h>

NS_ASSUME_NONNULL_BEGIN

@class AGSUtilityAssetGroup;
@class AGSUtilityAssetType;
@class AGSUtilityNetworkSource;
@class AGSUtilityTerminal;

/** @brief A entity in a utility network that corresponds to an @c AGSFeature

 @c AGSUtilityElement includes a reference to an @c AGSFeature inside a utility network source, plus a terminal (if applicable). @c AGSUtilityElement objects are used across the utility network API. Some places where they are used are to specify starting points and barriers for use with tracing, and returned as results from tracing.
 @since 100.6
 */
@interface AGSUtilityElement : AGSObject

#pragma mark -
#pragma mark initializers

-(instancetype)init NS_UNAVAILABLE;

#pragma mark -
#pragma mark properties

/** The asset group the element belongs to
 @since 100.6
 */
@property (nonatomic, strong, readonly) AGSUtilityAssetGroup *assetGroup;

/** The asset type the element belongs to
 @since 100.6
 */
@property (nonatomic, strong, readonly) AGSUtilityAssetType *assetType;

/** How far the starting point or barrier is located along an edge in the
 utility network feature, from @c 0.0 (edge's start) to @c 1.0 (edge's end)
 This property is only valid when the @c AGSUtilityElement#globalID 
 property refers to an edge feature that is an input to a tracing
 operation.
 @since 100.6
 */
@property (nonatomic, assign, readwrite) double fractionAlongEdge;

/** The global ID of the element
 @since 100.6
 */
@property (nonatomic, strong, readonly) NSUUID *globalID;

/** The @c AGSUtilityNetworkSource the element is from
 @since 100.6
 */
@property (nonatomic, strong, readonly) AGSUtilityNetworkSource *networkSource;

/** The object ID of the element's corresponding feature in the @c AGSUtilityElement#networkSource
 Only valid on results from a tracing operation, or with elements that are created from an @c AGSArcGISFeature.  Otherwise the value @c -1 is returned.
 @since 100.6
 */
@property (nonatomic, assign, readonly) NSInteger objectID;

/** The @c AGSUtilityTerminal of the starting point or barrier utility 
 network feature
 Only valid when the @c AGSUtilityElement#globalID refers to a 
 device junction feature that has terminals
 @since 100.6
 */
@property (nullable, nonatomic, strong, readonly) AGSUtilityTerminal *terminal;

#pragma mark -
#pragma mark methods

@end

NS_ASSUME_NONNULL_END
