/*
 COPYRIGHT 1995-2019 ESRI
 
 TRADE SECRETS: ESRI PROPRIETARY AND CONFIDENTIAL
 Unpublished material - all rights reserved under the
 Copyright Laws of the United States and applicable international
 laws, treaties, and conventions.
 
 For additional information, contact:
 Environmental Systems Research Institute, Inc.
 Attn: Contracts and Legal Services Department
 380 New York Street
 Redlands, California, 92373
 USA
 
 email: contracts@esri.com
 */

/** @file AGSUtilityAssetGroup.h */ //Required for Globals API doc

#import <ArcGIS/AGSObject.h>

NS_ASSUME_NONNULL_BEGIN

@class AGSUtilityAssetType;

/** @brief A utility network asset group
 
 @c AGSUtilityAssetGroup is the first-level categorization of an @c AGSUtilityNetworkSource. (@c AGSUtilityAssetType is the second-level categorization.) This type provides information about the @c AGSUtilityAssetGroup, including the @c AGSUtilityAssetType set that is contained therein.
 @since 100.6
 */
@interface AGSUtilityAssetGroup : AGSObject

#pragma mark -
#pragma mark initializers

-(instancetype)init NS_UNAVAILABLE;

#pragma mark -
#pragma mark properties

/** The collection of @c AGSUtilityAssetType objects defining the asset types contained in the @c AGSUtilityAssetGroup
 @since 100.6
 */
@property (nonatomic, copy, readonly) NSArray<AGSUtilityAssetType *> *assetTypes;

/** The code of the @c AGSUtilityAssetGroup
 This property is the subtype code value.
 @since 100.6
 */
@property (nonatomic, assign, readonly) NSInteger code;

/** The name of the @c AGSUtilityAssetGroup
 @since 100.6
 */
@property (nonatomic, copy, readonly) NSString *name;

#pragma mark -
#pragma mark methods

/** Gets an @c AGSUtilityAssetType from the @c AGSUtilityAssetGroup by name
 For example, an @c AGSUtilityAssetGroup describing electrical transformers may contain an @c AGSUtilityAssetType for a specific type of electrical transformer with a name such as "Single-phase Distribution Transformer".
 @param name The name of the @c AGSUtilityAssetType to return
 @return An @c AGSUtilityAssetType corresponding to the name provided, or nil if the @c AGSUtilityAssetType is not found in the @c AGSUtilityAssetGroup.
 @since 100.6
 */
-(nullable AGSUtilityAssetType *)assetTypeWithName:(NSString *)name;

@end

NS_ASSUME_NONNULL_END
