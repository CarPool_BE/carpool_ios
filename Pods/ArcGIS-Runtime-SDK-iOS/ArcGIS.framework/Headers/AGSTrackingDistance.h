/*
 COPYRIGHT 1995-2019 ESRI
 
 TRADE SECRETS: ESRI PROPRIETARY AND CONFIDENTIAL
 Unpublished material - all rights reserved under the
 Copyright Laws of the United States and applicable international
 laws, treaties, and conventions.
 
 For additional information, contact:
 Environmental Systems Research Institute, Inc.
 Attn: Contracts and Legal Services Department
 380 New York Street
 Redlands, California, 92373
 USA
 
 email: contracts@esri.com
 */

/** @file AGSTrackingDistance.h */ //Required for Globals API doc

#import <ArcGIS/AGSObject.h>

NS_ASSUME_NONNULL_BEGIN

@class AGSLinearUnit;

/** @brief Defines tracking distance

 Tracking distance contains two types of distances - raw and display.
Raw distance is always in meters (fixed units).
Display distance is in @c AGSTrackingDistance#displayTextUnits (@c AGSLinearUnit).
 @since 100.6
 */
@interface AGSTrackingDistance : AGSObject

#pragma mark -
#pragma mark initializers

-(instancetype)init NS_UNAVAILABLE;

#pragma mark -
#pragma mark properties

/** Formatted and rounded distance, ready for displaying
 Use this property to have consistent distances/units with voice guidances.
The distance is in @c AGSTrackingDistance#displayTextUnits (@c AGSLinearUnit).
The following rules are used for rounding and formatting:
IMPERIAL
- 0 -> 50' => Round to increments of 10' (e.g. "10", "20", "30", "40")
- 50' -> 0.1 mi (528') => Round to increments of 50' (e.g. "50", "100", "150", etc.)
- 0.1 -> 1.0 mi => Round to decimal mile (e.g "0.1", "0.2", "0.3", etc.)
- 1.0 mi -> 10 mi => Round to half mile (e.g. "1.0", "1.5", "2.0", etc.)
- 10+ mi => Round to mile (e.g. "10", "11", "12", etc.)
METRIC
- 0 -> 50 m => Round to increments of 10 m (e.g. "10", "20", "30", "40")
- 50 m -> 500 m => Round to increments of 50 m (e.g. "50", "100", "150", etc.)
- 500 m -> 10 km => Round to half km (e.g. "1.0", "1.5", "2.0", etc.)
- 10+ km => Round to whole km (e.g. "10", "11", "12", etc.)
 @see @c AGSVoiceGuidance
 @since 100.6
 */
@property (nonatomic, copy, readonly) NSString *displayText;

/** The units used in the @c AGSTrackingDistance#displayText which vary base on distance from maneuver/destination
 @since 100.6
 */
@property (nonatomic, strong, readonly) AGSLinearUnit *displayTextUnits;

/** The distance in meters
 The raw distance without rounding and formatting, in meters.
 @since 100.6
 */
@property (nonatomic, assign, readonly) double distance;

#pragma mark -
#pragma mark methods

@end

NS_ASSUME_NONNULL_END
