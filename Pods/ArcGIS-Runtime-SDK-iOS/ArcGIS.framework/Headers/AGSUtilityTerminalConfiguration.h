/*
 COPYRIGHT 1995-2019 ESRI
 
 TRADE SECRETS: ESRI PROPRIETARY AND CONFIDENTIAL
 Unpublished material - all rights reserved under the
 Copyright Laws of the United States and applicable international
 laws, treaties, and conventions.
 
 For additional information, contact:
 Environmental Systems Research Institute, Inc.
 Attn: Contracts and Legal Services Department
 380 New York Street
 Redlands, California, 92373
 USA
 
 email: contracts@esri.com
 */

/** @file AGSUtilityTerminalConfiguration.h */ //Required for Globals API doc

#import <ArcGIS/AGSObject.h>

NS_ASSUME_NONNULL_BEGIN

@class AGSUtilityTerminal;

/** @brief A utility network terminal configuration
 
 @since 100.6
 */
@interface AGSUtilityTerminalConfiguration : AGSObject

#pragma mark -
#pragma mark initializers

-(instancetype)init NS_UNAVAILABLE;

#pragma mark -
#pragma mark properties

/** The name of the terminal configuration
 @since 100.6
 */
@property (nonatomic, copy, readonly) NSString *name;

/** The ID of the terminal configuration
 @since 100.6
 */
@property (nonatomic, assign, readonly) NSInteger terminalConfigurationID;

/** The collection of terminals of the terminal configuration
 @since 100.6
 */
@property (nonatomic, copy, readonly) NSArray<AGSUtilityTerminal *> *terminals;

#pragma mark -
#pragma mark methods

@end

NS_ASSUME_NONNULL_END
