/*
 COPYRIGHT 1995-2019 ESRI
 
 TRADE SECRETS: ESRI PROPRIETARY AND CONFIDENTIAL
 Unpublished material - all rights reserved under the
 Copyright Laws of the United States and applicable international
 laws, treaties, and conventions.
 
 For additional information, contact:
 Environmental Systems Research Institute, Inc.
 Attn: Contracts and Legal Services Department
 380 New York Street
 Redlands, California, 92373
 USA
 
 email: contracts@esri.com
 */

/** @file AGSUtilityNetworkSource.h */ //Required for Globals API doc

#import <ArcGIS/AGSObject.h>

NS_ASSUME_NONNULL_BEGIN

@class AGSArcGISFeatureTable;
@class AGSUtilityAssetGroup;

/** @brief A network source in a utility network
 
 Various sources of related information are associated to comprise the utility network. The most obvious sources are the structures and network features that are included with each domain network. Other sources are the set of associations and the system junctions.
 
 The @c AGSUtilityNetworkDefinition#networkSources property contains the collection of @c AGSUtilityNetworkSource objects in the utility network definition.
 @since 100.6
 */
@interface AGSUtilityNetworkSource : AGSObject

#pragma mark -
#pragma mark initializers

-(instancetype)init NS_UNAVAILABLE;

#pragma mark -
#pragma mark properties

/** The array of asset groups for the utility network source
 @c AGSUtilityAssetGroup is the first-level categorization of an @c AGSUtilityNetworkSource.
 @since 100.6
 */
@property (nonatomic, copy, readonly) NSArray<AGSUtilityAssetGroup *> *assetGroups;

/** The @c AGSArcGISFeatureTable that corresponds to the utility network source
 @since 100.6
 */
@property (nonatomic, strong, readonly) AGSArcGISFeatureTable *featureTable;

/** The name of the utility network source
 @since 100.6
 */
@property (nonatomic, copy, readonly) NSString *name;

/** The ID of the utility network source
 @since 100.6
 */
@property (nonatomic, assign, readonly) NSInteger sourceID;

/** The source type of the utility network source
 This property indicates whether the network source is an edge type or junction type.
 @since 100.6
 */
@property (nonatomic, assign, readonly) AGSUtilityNetworkSourceType sourceType;

/** The usage type of this utility network source
 This property indicates how the network source is used, such as line, junction, or assembly.
 @since 100.6
 */
@property (nonatomic, assign, readonly) AGSUtilityNetworkSourceUsageType sourceUsageType;

#pragma mark -
#pragma mark methods

/** Gets an associated asset group by name
 Gets one of the asset groups for the utility network source.
 @param name The name of the asset group
 @return An @c AGSUtilityAssetGroup.
 @since 100.6
 */
-(nullable AGSUtilityAssetGroup *)assetGroupWithName:(NSString *)name;

@end

NS_ASSUME_NONNULL_END
