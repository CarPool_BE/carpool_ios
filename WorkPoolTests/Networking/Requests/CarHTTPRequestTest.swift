//
//  CarHTTPRequestTest.swift
//  WorkPoolTests
//
//  Created by Marek Labuzik on 4/6/19.
//  Copyright © 2019 Marek Labuzik. All rights reserved.
//

import XCTest
@testable import WorkPool
import Moya

final class CarHTTPRequestTest: XCTestCase {

    // system under test
    var sut: CarHTTPRequest?

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        sut = nil
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

    // MARK: Get cars tests
    func testThatGetCarsRequestIsGET_HTTPMethod() {
        // given
        let method: Moya.Method = .get

        // when
        sut = CarHTTPRequest.getCars

        // then
        XCTAssertEqual(sut?.method, method)
    }

    func testThatGetCarsHasCarsPath() {
        // given
        let path = "/cars"

        // when
        sut = CarHTTPRequest.getCars
        
        // then
        XCTAssertEqual(sut?.path, path)
    }
    
    func testThatGetCarsTaskIsRequestPlain() {
        // given
        
        //when
        sut = CarHTTPRequest.getCars
        
        // then
        XCTAssertTrue({
            if case .requestPlain? = sut?.task {
                return true
            } else {
                return false
            }
            }())
    }

    func testGetCarsHasIdInURIPath() {
        // given
        let carID = "123"
        let path = "/cars/\(carID)"

        // when
        sut = CarHTTPRequest.getCar(withId: carID)

        // then
        XCTAssertEqual(sut?.path, path)
    }
    
    func testThatGetCarTaskIsRequestPlain() {
        // given
        let carID: String = "121"
        //when
        sut = CarHTTPRequest.getCar(withId: carID)
        
        // then
        XCTAssertTrue({
            if case .requestPlain? = sut?.task {
                return true
            } else {
                return false
            }
            }())
    }

}

// MARK: - delete car
extension CarHTTPRequestTest {
    func testThatDeleteCarRequestIsPATCH_HTTPMethod() {
        // given
        let carID: String = "111"
        let method: Moya.Method = .delete

        // when
        sut = CarHTTPRequest.deleteCar(withId: carID)

        // then
        XCTAssertEqual(sut?.method, method)
    }

    func testDeleteCarHasIdInURIPath() {
        // given
        let carID = "123"
        let path = "/cars/\(carID)"

        // when
        sut = CarHTTPRequest.deleteCar(withId: carID)

        // then
        XCTAssertEqual(sut?.path, path)

    }
    
    func testThatDeleteCarTaskIsRequestPlain() {
        // given
        let carID = "123"
        //when
        sut = CarHTTPRequest.deleteCar(withId: carID)
        
        // then
        XCTAssertTrue({
            if case .requestPlain? = sut?.task {
                return true
            } else {
                return false
            }
            }())
    }
}

// MARK: - create update Car
extension CarHTTPRequestTest {
    func testThatCreateCarRequestIsPATCH_HTTPMethod() {
        // given
        let data: Car = Car.empty()
        let method: Moya.Method = .post

        // when
        sut = CarHTTPRequest.createCar(data)

        // then
        XCTAssertEqual(sut?.method, method)
    }
    
    func testThatCreateCarTaskIsJSONEncodable() {
        // given
        // TODO: nemam na to cas treba to opravit
//        let carData: Car = Car.empty()
//        //when
//        sut = CarHTTPRequest.createCar(carData)
//
//        // then
//        XCTAssertTrue({
//            if case .requestJSONEncodable(_)? = sut?.task {
//                return true
//            } else {
//                return false
//            }
//            }())
    }

    func testThatUpdateCarRequestIsPATCH_HTTPMethod() {
        // given
        let data: Car = Car.empty()
        let method: Moya.Method = .patch

        // when
        sut = CarHTTPRequest.updateCar(data)

        // then
        XCTAssertEqual(sut?.method, method)
    }

    func testUpdateCarHasIdInURIPath() {
        // given
        let carData: Car = Car.empty()
        let path = "/cars" + "/" + String(carData.identifier!)
        // when
        sut = CarHTTPRequest.updateCar(carData)

        // then
        XCTAssertEqual(sut?.path, path)
    }
    
    func testThatUpdateCarTaskIsJSONEncodable() {
        // given
//        let carData: Car = Car.empty()
//        //when
//        sut = CarHTTPRequest.updateCar(carData)
//
//        // then
//        XCTAssertTrue({
//            if case .requestJSONEncodable(_)? = sut?.task {
//                return true
//            } else {
//                return false
//            }
//            }())
    }
}
