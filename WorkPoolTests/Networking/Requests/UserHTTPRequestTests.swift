//
//  UserHTTPRequestTests.swift
//  WorkPoolTests
//
//  Created by Boris Bielik on 30/03/2019.
//  Copyright © 2019 Marek Labuzik. All rights reserved.
//

import XCTest
@testable import WorkPool
import Moya

final class UserHTTPRequestTests: XCTestCase {

    // system under test
    var sut: UserHTTPRequest?

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        sut = nil
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    // MARK: Get users tests
    func testThatGetUsersRequestIsGETHTTPMethod() {
        // given
        let method: Moya.Method = .get

        // when
        sut = UserHTTPRequest.getUsers

        // then
        XCTAssertEqual(sut?.method, method)
    }

    func testThatGetUsersHasUsersPath() {
        // given
        let path = "/users"

        // when
        sut = UserHTTPRequest.getUsers

        // then
        XCTAssertEqual(sut?.path, path)
    }

    func testThatGetUsersHasMyBaseURL() {
        // given
        let baseURL: URL = URL(string: "https://dev.teamride.eu")!

        // when
        sut = UserHTTPRequest.getUsers

        // then
        XCTAssertEqual(sut?.baseURL, baseURL)
    }
    
    func testThatGetUsersTaskIsPlain() {
        // given
        
        //when
        sut = UserHTTPRequest.getUsers
        
        // then
        XCTAssertTrue({
            if case .requestPlain? = sut?.task {
                return true
            } else {
                return false
            }
            }())
    }

}

// MARK: - Create User
extension UserHTTPRequestTests {

    func testThatCreateUserRequestIsPOST_HTTPMethod() {
        // given
        let data: Data = Data()
        let method: Moya.Method = .post

        // when
        sut = UserHTTPRequest.createUser(data)

        // then
        XCTAssertEqual(sut?.method, method)
    }
    
    func testThatCreateUserTaskIsJSONEncodable() {
        // given
        let userData: Data = Data()
        //when
        sut = UserHTTPRequest.createUser(userData)
        
        // then
        XCTAssertTrue({
            if case .requestJSONEncodable(_)? = sut?.task {
                return true
            } else {
                return false
            }
            }())
    }

}

// MARK: - Update User
extension UserHTTPRequestTests {

    func testThatUpdateUserRequestIsPATCH_HTTPMethod() {
        // given
        let data: Data = Data()
        let method: Moya.Method = .patch

        // when
        sut = UserHTTPRequest.updateUser(data)

        // then
        XCTAssertEqual(sut?.method, method)
    }
    
    func testThatUpdateUserTaskIsJSONEncodable() {
        // given
        let userData: Data = Data()
        //when
        sut = UserHTTPRequest.updateUser(userData)
        
        // then
        XCTAssertTrue({
            if case .requestJSONEncodable(_)? = sut?.task {
                return true
            } else {
                return false
            }
            }())
    }
}

// MARK: - Delete user
extension UserHTTPRequestTests {

    func testThatDeleteUserRequestIsDELETE_HTTPMethod() {
        // given
        let userId = "123"
        let method: Moya.Method = .delete

        // when
        sut = UserHTTPRequest.deleteUser(withId: userId)

        // then
        XCTAssertEqual(sut?.method, method)
    }

    func testDeleteUserHasIdInURIPath() {
        // given
        let userId = "123"
        let path = "/users/\(userId)"

        // when
        sut = UserHTTPRequest.deleteUser(withId: userId)

        // then
        XCTAssertEqual(sut?.path, path)

    }
    
    func testThatDeleteUserTaskIsRequestPlain() {
        // given
        let userID: String = "121"
        //when
        sut = UserHTTPRequest.deleteUser(withId: userID)
        
        // then
        XCTAssertTrue({
            if case .requestPlain? = sut?.task {
                return true
            } else {
                return false
            }
            }())
    }
}
