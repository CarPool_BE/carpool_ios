//
//  RideHTTPRequestTest.swift
//  WorkPoolTests
//
//  Created by Marek Labuzik on 4/6/19.
//  Copyright © 2019 Marek Labuzik. All rights reserved.
//

import XCTest
@testable import WorkPool
import CoreLocation
import Moya

final class RideHTTPRequestTest: XCTestCase {

    // system under test
    var sut: RideHTTPRequest?

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        sut = nil
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

    func testThatGetRidesRequestIsGETHTTPMethod() {
        // given
        let method: Moya.Method = .get

        // when
        let emptyGPS = CLLocationCoordinate2D(latitude: 0, longitude: 0)
        sut = RideHTTPRequest.getRides(from: "", fromGps: emptyGPS, toGps: emptyGPS, date: Date())

        // then
        XCTAssertEqual(sut?.method, method)
    }
    
    func testThatGetRidesTaskIsRequestPlain() {
        // given

        //when
        // TODO: nemam na toto cas, treba to opravit
//        let emptyGPS = CLLocationCoordinate2D(latitude: 0, longitude: 0)
//        sut = RideHTTPRequest.getRides(from: "", fromGps:emptyGPS, toGps: emptyGPS, date: Date())
//        let dict:[String: Any] = ["":""]
//
//        // then
//        XCTAssertTrue({
//            if case .requestParameters(parameters: ["":""],
//                                       encoding: URLEncoding.default)? = sut?.task {
//                return true
//            } else {
//                return false
//            }
//            }())
    }

    func testGetRideHasIdInURIPath() {
        // given
        let rideId = 123
        let path = "/rides/\(rideId)"

        // when
        sut = RideHTTPRequest.getRide(withId: rideId)
        
        // then
        XCTAssertEqual(sut?.path, path)
    }
    
    func testThatGetRideTaskIsRequestPlain() {
        // given
        let rideId = 123
        //when
        sut = RideHTTPRequest.getRide(withId: rideId)
        
        // then
        XCTAssertTrue({
            if case .requestPlain? = sut?.task {
                return true
            } else {
                return false
            }
            }())
    }
}

// MARK: - update Ride
extension RideHTTPRequestTest {
    func testThatUpdateRideRequestIsGETHTTPMethod() {
        // given
        let ride = NewRide()
        let method: Moya.Method = .patch

        // when
        sut = RideHTTPRequest.updateRide(ride)

        // then
        XCTAssertEqual(sut?.method, method)
    }

    func testUpdateRideHasIdInURIPath() {
        // given
        let ride = NewRide()
        let path = "/rides"

        // when
        sut = RideHTTPRequest.updateRide(ride)

        // then
        XCTAssertEqual(sut?.path, path)
    }
    
    func testThatUpdateRideTaskIsJSONEncodable() {
        // given
        let ride = NewRide()
        //when
        sut = RideHTTPRequest.updateRide(ride)
        
        // then
        XCTAssertTrue({
            if case .requestJSONEncodable(_)? = sut?.task {
                return true
            } else {
                return false
            }
            }())
    }
}

// MARK: - create Ride
extension RideHTTPRequestTest {
    func testThatCreateRideRequestIsGETHTTPMethod() {
        // given
        let ride = NewRide()
        let method: Moya.Method = .post

        // when
        sut = RideHTTPRequest.createRide(ride)

        // then
        XCTAssertEqual(sut?.method, method)
    }

    func testCreateRideHasIdInURIPath() {
        // given
        let ride = NewRide()
        let path = "/rides"

        // when
        sut = RideHTTPRequest.createRide(ride)

        // then
        XCTAssertEqual(sut?.path, path)
    }
    
    func testThatCreateRideTaskIsJSONEncodable() {
        // given
        let ride = NewRide()

        //when
        sut = RideHTTPRequest.createRide(ride)
        
        // then
        XCTAssertTrue({
            if case .requestCustomJSONEncodable(_, encoder: _)? = sut?.task {
                return true
            } else {
                return false
            }
            }())
    }
}

// MARK: - delete Ride
extension RideHTTPRequestTest {
    func testThatDeleteRideRequestIsPATCH_HTTPMethod() {
        // given
        let rideId = 111
        let method: Moya.Method = .delete

        // when
        sut = RideHTTPRequest.deleteRide(withId: rideId)

        // then
        XCTAssertEqual(sut?.method, method)
    }
    
    func testDeleteRideHasIdInURIPath() {
        // given
        let rideId = 123
        let path = "/rides/\(rideId)"

        // when
        sut = RideHTTPRequest.deleteRide(withId: rideId)

        // then
        XCTAssertEqual(sut?.path, path)
    }
    
    func testThatDeleteRideTaskIsRequestPlain() {
        // given
        let rideId = 123
        //when
        sut = RideHTTPRequest.deleteRide(withId: rideId)
        
        // then
        XCTAssertTrue({
            if case .requestPlain? = sut?.task {
                return true
            } else {
                return false
            }
            }())
    }
}
