//
//  UserProfileHTTPRequestTest.swift
//  WorkPoolTests
//
//  Created by Marek Labuzik on 4/6/19.
//  Copyright © 2019 Marek Labuzik. All rights reserved.
//

import XCTest
@testable import WorkPool
import Moya

final class UserProfileHTTPRequestTest: XCTestCase {

    // system under test
    var sut: UserProfileHTTPRequest?

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        sut = nil
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

    func testThatGetMeProfileRequestIsGET_HTTPMethod() {
        // given
        let method: Moya.Method = .get

        // when
        sut = UserProfileHTTPRequest.getMeProfile

        // then
        XCTAssertEqual(sut?.method, method)
    }
    
    func testGetMeProfileHasURIPath() {
        // given
        let path = "/me"
        
        // when
        sut = UserProfileHTTPRequest.getMeProfile

        // then
        XCTAssertEqual(sut?.path, path)
    }
    
    func testThatGetMeProfileTaskIsRequestPlain() {
        // given
        
        //when
        sut = UserProfileHTTPRequest.getMeProfile
        
        // then
        XCTAssertTrue({
            if case .requestPlain? = sut?.task {
                return true
            } else {
                return false
            }
            }())
    }

    func testThatUpdateProfileRequestIsPATCH_HTTPMethod() {
        // given
        let profileData: User = User.empty()
        let method: Moya.Method = .patch

        // when
        sut = UserProfileHTTPRequest.updateMeProfile(profileData)
        
        // then
        XCTAssertEqual(sut?.method, method)
    }

    func testUpdateMeProfileHasURIPath() {
        // given
        let profileData: User = User.empty()
        let path = "/me"

        // when
        sut = UserProfileHTTPRequest.updateMeProfile(profileData)

        // then
        XCTAssertEqual(sut?.path, path)
    }
    
    func testThatUpdateMeProfileTaskIsJSONEncodable() {
        // given
        // TODO: dorobit
//        let profilData: Data = Data()
//        //when
//        sut = UserProfileHTTPRequest.updateMeProfile(profilData)
//
//        // then
//        XCTAssertTrue({
//            if case .requestJSONEncodable(let data)? = sut?.task {
//                return true
//            } else {
//                return false
//            }
//            }())
    }
}

// MARK: - Login
extension UserProfileHTTPRequestTest {
    func testThatLoginRequestIsGET_HTTPMethod() {
        // given
        let email = "abc@gmail.com"
        let password = "password"
        let deviceData = DeviceInfo(pushID: "",
                                    sandbox: true)
        let method: Moya.Method = .post

        // when
        sut = UserProfileHTTPRequest.login(email: email, pass: password, device: deviceData)

        // then
        XCTAssertEqual(sut?.method, method)
    }

    func testLoginHasURIPath() {
        // given
        let email = "abc@gmail.com"
        let password = "password"
        let deviceData = DeviceInfo(pushID: "",
                                    sandbox: true)
        let path = "/token"

        // when
        sut = UserProfileHTTPRequest.login(email: email, pass: password, device: deviceData)

        // then
        XCTAssertEqual(sut?.path, path)
    }
    
    func testThatLoginTaskIsJSONEncodable() {
        // given
        let email = "abc@gmail.com"
        let password = "password"
        let deviceData = DeviceInfo(pushID: "",
                                    sandbox: true)

        //when
        sut = UserProfileHTTPRequest.login(email: email, pass: password, device: deviceData)
        
        // then
        XCTAssertTrue({
            if case .requestJSONEncodable(_)? = sut?.task {
                return true
            } else {
                return false
            }
            }())
    }
}

// MARK: - logout
extension UserProfileHTTPRequestTest {
    func testThatLogoutRequestIsDELETE_HTTPMethod() {
        // given
        let pushID = "123123"
        let method: Moya.Method = .delete

        // when
        sut = UserProfileHTTPRequest.logout(pushID: pushID)

        // then
        XCTAssertEqual(sut?.method, method)
    }

    func testLogoutHasURIPath() {
        // given
        let path = "/token"
        let pushID = "123123"

        // when
        sut = UserProfileHTTPRequest.logout(pushID: pushID)

        // then
        XCTAssertEqual(sut?.path, path)
    }
    
    func testThatLogoutTaskIsRequestPlain() {
        // TODO: teraz nemam cas treba to dorobit
//        // given
//        let pushID = "123123"
//        //when
//        sut = UserProfileHTTPRequest.logout(pushID: pushID)
//
//        // then
//        XCTAssertTrue({
//            if case .requestPlain? = sut?.task {
//                return true
//            } else {
//                return false
//            }
//            }())
    }
}

// MARK: - get me cars
extension UserProfileHTTPRequestTest {
    func testThatGetMeCatsRequestIsGET_HTTPMethod() {
        // given
        let method: Moya.Method = .get

        // when
        sut = UserProfileHTTPRequest.getMeCars

        // then
        XCTAssertEqual(sut?.method, method)
    }

    func testGetMeCarsHasURIPath() {
        // given
        let path = "/me/cars"

        // when
        sut = UserProfileHTTPRequest.getMeCars

        // then
        XCTAssertEqual(sut?.path, path)
    }
    
    func testThatGetMeCarsTaskIsRequestPlain() {
        // given
        
        //when
        sut = UserProfileHTTPRequest.getMeCars
        
        // then
        XCTAssertTrue({
            if case .requestPlain? = sut?.task {
                return true
            } else {
                return false
            }
            }())
    }
}

// MARK: - create car
extension UserProfileHTTPRequestTest {
    func testThatCreateCatsRequestIsPOST_HTTPMethod() {
        // given
        let carData: Car  = Car.empty()
        let method: Moya.Method = .post

        // when
        sut = UserProfileHTTPRequest.createCar(carData)

        // then
        XCTAssertEqual(sut?.method, method)
    }

    func testCreateCarsHasURIPath() {
        // given
        let carData: Car  = Car.empty()
        let path = "/me/cars"

        // when
        sut = UserProfileHTTPRequest.createCar(carData)

        // then
        XCTAssertEqual(sut?.path, path)
    }
    
    func testThatCreateMeCarTaskIsJSONEncodable() {
        // given
        // TODO: teraz nemam cas treba to dorobit
//        let carData: Car  = Car.empty()
//        //when
//        sut = UserProfileHTTPRequest.createCar(carData)
//
//        // then
//        XCTAssertTrue({
//            if case .requestJSONEncodable(let data)? = sut?.task {
//                return true
//            } else {
//                return false
//            }
//            }())
    }
}

// MARK: - update car
extension UserProfileHTTPRequestTest {
    func testThatUpdateCatsRequestIsPATCH_HTTPMethod() {
        // given
        let carData: Data  = Data()
        let method: Moya.Method = .patch

        // when
        sut = UserProfileHTTPRequest.updateCar(carData)

        // then
        XCTAssertEqual(sut?.method, method)
    }

    func testUpdateCarsHasURIPath() {
        // given
        let carData: Data  = Data()
        let path = "/me/cars"

        // when
        sut = UserProfileHTTPRequest.updateCar(carData)

        // then
        XCTAssertEqual(sut?.path, path)
    }
    
    func testThatUpdateCarTaskIsJSONEncodable() {
        // given
        let carData: Data  = Data()
        //when
        sut = UserProfileHTTPRequest.updateCar(carData)
        
        // then
        XCTAssertTrue({
            if case .requestJSONEncodable(_)? = sut?.task {
                return true
            } else {
                return false
            }
            }())
    }
}

// MARK: - delete car
extension UserProfileHTTPRequestTest {
    func testThatDeleteCarRequestIsDelete_HTTPMethod() {
        // given
        let carID: String = "1111"
        let method: Moya.Method = .delete

        // when
        sut = UserProfileHTTPRequest.deleteCar(withID: carID)

        // then
        XCTAssertEqual(sut?.method, method)
    }

    func testCeleteCarHasURIPath() {
        // given
        let carID: String = "1111"
        let path = "/me/cars/"+carID

        // when
        sut = UserProfileHTTPRequest.deleteCar(withID: carID)

        // then
        XCTAssertEqual(sut?.path, path)
    }
    
    func testThatDeleteMeCarsTaskIsRequestPlain() {
        // given
        let carID: String = "1111"
        //when
        sut = UserProfileHTTPRequest.deleteCar(withID: carID)
        
        // then
        XCTAssertTrue({
            if case .requestPlain? = sut?.task {
                return true
            } else {
                return false
            }
            }())
    }
}

// MARK: - get me rides
extension UserProfileHTTPRequestTest {
    func testThtGetMeRidesRequestIsGET_HTTPMethod() {
        // given
        let method: Moya.Method = .get

        // when
        sut = UserProfileHTTPRequest.getMeRides(of: Date())

        // then
        XCTAssertEqual(sut?.method, method)
    }

    func testGetMeRidesHasURIPath() {
        // given
        let path = "/me/rides"

        // when
        sut = UserProfileHTTPRequest.getMeRides(of: Date())

        // then
        XCTAssertEqual(sut?.path, path)
    }
    
    func testThatGetMeRidesTaskIsRequestPlain() {
        // given
        // TODO: dorobit
//        let date = Date()
//        var dict:[String: Any] = [:]
//        dict["date"] = date.getIso8601String()
//        //when
//        sut = UserProfileHTTPRequest.getMeRides(of: date)
//
//        // then
//        XCTAssertTrue({
//            if case .requestParameters(parameters: dict, encoding: URLEncoding.default)? = sut?.task {
//                return true
//            } else {
//                return false
//            }
//            }())
    }
}

// MARK: - create me ride
extension UserProfileHTTPRequestTest {
    func testThtCreateRideRequestIsPOST_HTTPMethod() {
        // given
        let rideData: Data = Data()
        let method: Moya.Method = .post

        // when
        sut = UserProfileHTTPRequest.createMeRide(rideData)

        // then
        XCTAssertEqual(sut?.method, method)
    }

    func testCreateMeRideHasURIPath() {
        // given
        let rideData: Data = Data()
        let path = "/me/rides"

        // when
        sut = UserProfileHTTPRequest.createMeRide(rideData)

        // then
        XCTAssertEqual(sut?.path, path)
    }
    
    func testThatCreateMeRideTaskIsJSONEncodable() {
        // given
        let rideData: Data = Data()
        //when
        sut = UserProfileHTTPRequest.createMeRide(rideData)
        
        // then
        XCTAssertTrue({
            if case .requestJSONEncodable(_)? = sut?.task {
                return true
            } else {
                return false
            }
            }())
    }
}

// MARK: - update me ride
extension UserProfileHTTPRequestTest {
    func testThatUpdateRideRequestIsPATCH_HTTPMethod() {
        // given
        let rideData: Data = Data()
        let method: Moya.Method = .patch

        // when
        sut = UserProfileHTTPRequest.updateMeRide(rideData)

        // then
        XCTAssertEqual(sut?.method, method)
    }

    func testUpdateRideHasURIPath() {
        // given
        let rideData: Data = Data()
        let path = "/me/rides"

        // when
        sut = UserProfileHTTPRequest.updateMeRide(rideData)

        // then
        XCTAssertEqual(sut?.path, path)
    }
    
    func testThatUpdateMeRideTaskIsJSONEncodable() {
        // given
        let rideData: Data = Data()
        //when
        sut = UserProfileHTTPRequest.updateMeRide(rideData)
        
        // then
        XCTAssertTrue({
            if case .requestJSONEncodable(_)? = sut?.task {
                return true
            } else {
                return false
            }
            }())
    }
}

// MARK: - delete me ride
extension UserProfileHTTPRequestTest {
    func testThtDeleteRideRequestIsDELETE_HTTPMethod() {
        // given
        let rideID: String = "1111"
        let method: Moya.Method = .delete

        // when
        sut = UserProfileHTTPRequest.deleteMeRide(withID: rideID)

        // then
        XCTAssertEqual(sut?.method, method)
    }

    func testDeleteRideHasURIPath() {
        // given
        let rideID: String = "1111"
        let path = "/me/rides/"+rideID

        // when
        sut = UserProfileHTTPRequest.deleteMeRide(withID: rideID)

        // then
        XCTAssertEqual(sut?.path, path)
    }
    
    func testThatDeleteMeRidesTaskIsRequestPlain() {
        // given
        let rideID: String = "1111"
        //when
        sut = UserProfileHTTPRequest.deleteMeRide(withID: rideID)
        
        // then
        XCTAssertTrue({
            if case .requestPlain? = sut?.task {
                return true
            } else {
                return false
            }
            }())
    }
}
