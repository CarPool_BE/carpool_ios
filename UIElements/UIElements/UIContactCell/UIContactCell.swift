//
//  UIContactCell.swift
//  UICustomElements
//
//  Created by Marek Labuzik on 18/06/2019.
//  Copyright © 2019 Marek Labuzik. All rights reserved.
//

import UIKit

public protocol UIContactCellDelegate: class {
    func contactCellView(_ contactCell: UIContactCell, makeCallTo phone: String)
}

public final class UIContactCell: UITableViewCell {
    public weak var delegate: UIContactCellDelegate?
    public var phoneNumber: String?
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet public weak var userName: UILabel!
    @IBOutlet weak var callButton: UIButton!

    public func havePhoneNumber(isExists: Bool) {
        if isExists {
            self.callButton.isHidden = false
        } else {
            self.callButton.isHidden = true
        }
    }

    @IBAction func callButtonPressed(_ sender: UIButton) {
        guard let phoneNumber = self.phoneNumber else {
            return
        }
        self.delegate?.contactCellView(self, makeCallTo: phoneNumber)
    }
}
