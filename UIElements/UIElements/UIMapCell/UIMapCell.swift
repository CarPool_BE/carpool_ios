//
//  UIMapCell.swift
//  UICustomElements
//
//  Created by Marek Labuzik on 25/10/2019.
//  Copyright © 2019 Marek Labuzik. All rights reserved.
//

import UIKit
import MapKit

public protocol UIMapCellDelegate: class {
    func mapCellView(_ mapCell: UIMapCell, didSelect route: MKRoute)
}

final public class UIMapCell: UITableViewCell {
    private var header: MapHeaderView = MapHeaderView()
    private var map: MKMapView = MKMapView()
    private var routes: [MKRoute] = []

    public var delegate: UIMapCellDelegate?

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.map.delegate = self
        self.header.delegate = self
        let mapTap = UITapGestureRecognizer(target: self, action: #selector(mapTapped(_:)))
        self.map.addGestureRecognizer(mapTap)
        self.setupView()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public override var tintColor: UIColor! {
        willSet {
            self.header.tintColor = newValue
        }
    }
    
    public var selectedColor: UIColor? {
        willSet {
            self.header.selectedColor = newValue
        }
    }
    
    public func add(routes: [MKRoute]) {
        for route in self.routes {
            self.map.removeOverlay(route.polyline)
        }

        self.routes = routes.sorted(by: {$0.expectedTravelTime < $1.expectedTravelTime})
        let selectedRoute = self.routes[0]
        for route in self.routes {
            if self.routes[0] == route {
                route.polyline.subtitle = "SELECTED"
                self.map.addOverlay(route.polyline)
            } else {
                route.polyline.subtitle = "NONE"
                self.map.insertOverlay(route.polyline, below: selectedRoute.polyline)
            }
            
            if self.map.overlays.count == 1 {
              self.map.setVisibleMapRect(route.polyline.boundingMapRect,
                                         edgePadding: UIEdgeInsets.init(top: 10.0, left: 10.0, bottom: 10.0, right: 10.0),
                                         animated: false)
            } else {
              let polylineBoundingRect =  self.map.visibleMapRect.union(route.polyline.boundingMapRect)
              self.map.setVisibleMapRect(polylineBoundingRect,
                                         edgePadding: UIEdgeInsets.init(top: 10.0, left: 10.0, bottom: 10.0, right: 10.0),
                                         animated: false)
            }
        }
        self.header.setButtons(routes: self.routes)
    }
    
    @objc private func mapTapped(_ tap: UITapGestureRecognizer) {
        if tap.state == .recognized && tap.state == .recognized {
            let touchPt: CGPoint = tap.location(in: self.map)
            let coord: CLLocationCoordinate2D = self.map.convert(touchPt, toCoordinateFrom: self.map)
            let maxMeters: Double = meters(fromPixel: 22, at: touchPt)
            var nearestDistance: Float = MAXFLOAT
            var nearestPoly: MKPolyline?
            for overlay: MKOverlay in self.map.overlays {
                if (overlay is MKPolyline) {
                    let distance: Float = Float(distanceOf(pt: MKMapPoint(coord), toPoly: overlay as! MKPolyline))
                    if distance < nearestDistance {
                        nearestDistance = distance
                        nearestPoly = overlay as? MKPolyline
                    }
                }
            }
            if Double(nearestDistance) <= maxMeters {
                if nearestPoly != nil {
                    self.setSelected(route: nearestPoly!)
                }
            }
        }
    }
    
    private func setSelected(route selectingRoute: MKPolyline) {
        for i in 0..<self.routes.count {
            self.map.removeOverlay(self.routes[i].polyline)
            if self.routes[i].polyline == selectingRoute {
                self.routes[i].polyline.subtitle = "SELECTED"
                self.header.selectButton(at: i)
                self.delegate?.mapCellView(self, didSelect: self.routes[i])
                self.map.addOverlay(self.routes[i].polyline)
            } else {
                self.routes[i].polyline.subtitle = "NONE"
                self.map.insertOverlay(self.routes[i].polyline, below: selectingRoute)
            }
        }
    }
    
    private func distanceOf(pt: MKMapPoint, toPoly poly: MKPolyline) -> Double {
        var distance: Double = Double(MAXFLOAT)
        for n in 0..<poly.pointCount - 1 {
            let ptA = poly.points()[n]
            let ptB = poly.points()[n + 1]
            let xDelta: Double = ptB.x - ptA.x
            let yDelta: Double = ptB.y - ptA.y
            if xDelta == 0.0 && yDelta == 0.0 {
                // Points must not be equal
                continue
            }
            let u: Double = ((pt.x - ptA.x) * xDelta + (pt.y - ptA.y) * yDelta) / (xDelta * xDelta + yDelta * yDelta)
            var ptClosest: MKMapPoint
            if u < 0.0 {
                ptClosest = ptA
            } else if u > 1.0 {
                ptClosest = ptB
            } else {
                ptClosest = MKMapPoint(x: ptA.x + u * xDelta, y: ptA.y + u * yDelta)
            }
            distance = min(distance, ptClosest.distance(to: pt))
        }
        return distance
    }

    private func meters(fromPixel px: Int, at pt: CGPoint) -> Double {
        let ptB = CGPoint(x: pt.x + CGFloat(px), y: pt.y)
        let coordA: CLLocationCoordinate2D = self.map.convert(pt, toCoordinateFrom: self.map)
        let coordB: CLLocationCoordinate2D = self.map.convert(ptB, toCoordinateFrom: self.map)
        return MKMapPoint(coordA).distance(to: MKMapPoint(coordB))
    }
    
    private func setupView() {
        self.addSubview(self.map)
        self.addSubview(self.header)
        self.setupConstraints()
    }
    
    private func setupConstraints() {
        self.map.translatesAutoresizingMaskIntoConstraints = false
        self.header.translatesAutoresizingMaskIntoConstraints = false
        let mapHeight = UIScreen.main.bounds.size.height*CGFloat(0.65)
        NSLayoutConstraint.activate([
            self.header.heightAnchor.constraint(equalToConstant: 45),
            self.header.topAnchor.constraint(equalTo: self.topAnchor, constant: 0),
            self.header.rightAnchor.constraint(equalTo: self.rightAnchor, constant: 0),
            self.header.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 0),
            
            self.map.topAnchor.constraint(equalTo: self.header.bottomAnchor, constant: 0),
            self.map.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: 0),
            self.map.rightAnchor.constraint(equalTo: self.rightAnchor, constant: 0),
            self.map.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 0),
            self.map.heightAnchor.constraint(equalToConstant: mapHeight)
        ])
    }
}

extension UIMapCell: MKMapViewDelegate {
    public func mapView(_ mapView: MKMapView,
                 rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        let polylineRenderer = MKPolylineRenderer(overlay: overlay)
        if let over = overlay as? MKPolyline {
            if over.subtitle == "SELECTED" {
                polylineRenderer.strokeColor = UIColor.selectRouteColor
            } else {
                polylineRenderer.strokeColor = UIColor.unselectRouteColor
            }
            polylineRenderer.lineWidth = 5
        }
        return polylineRenderer
    }
}

extension UIMapCell: MapHeaderViewDelegate {
    public func mapHeaderView(_ view: MapHeaderView, at index: Int) {
        self.setSelected(route: self.routes[index].polyline)
    }
}

extension UIColor {
    class var selectRouteColor: UIColor? {
        get {
            if #available(iOS 11.0, *) {
                return UIColor(named: "Selected_route_color",
                                in: Bundle(identifier: "eu.teamride.app.UICustomElements"),
                                compatibleWith: nil)
            } else {
                return UIColor(red: 0, green: 166, blue: 255, alpha: 0)
            }
        }
    }
    class var selectRouteBorderColor: UIColor? {
        get {
            if #available(iOS 11.0, *) {
                return UIColor(named: "Selected_route_color_border",
                                in: Bundle(identifier: "eu.teamride.app.UICustomElements"),
                                compatibleWith: nil)
            } else {
                return UIColor(red: 135, green: 165, blue: 249, alpha: 0)
            }
        }
    }
    class var unselectRouteColor: UIColor? {
        get {
            if #available(iOS 11.0, *) {
                return UIColor(named: "Unselected_route_color",
                                in: Bundle(identifier: "eu.teamride.app.UICustomElements"),
                                compatibleWith: nil)
            } else {
                return UIColor(red: 174, green: 204, blue: 254, alpha: 0)
            }
        }
    }
    class var unselectRouteBorderColor: UIColor? {
        get {
            if #available(iOS 11.0, *) {
                return UIColor(named: "Unselected_route_color_border",
                                in: Bundle(identifier: "eu.teamride.app.UICustomElements"),
                                compatibleWith: nil)
            } else {
                return UIColor(red: 154, green: 185, blue: 241, alpha: 0)
            }
        }
    }
}
