//
//  MapHeaderView.swift
//  UICustomElements
//
//  Created by Marek Labuzik on 03/12/2019.
//  Copyright © 2019 Marek Labuzik. All rights reserved.
//

import UIKit
import MapKit

public protocol MapHeaderViewDelegate: class {
    func mapHeaderView(_ view: MapHeaderView, at index: Int)
}

final public class MapHeaderView: UIView {
    var delegate: MapHeaderViewDelegate?
    var stackView: UIStackView = UIStackView()
    var buttons: [UIButton] = []
    var selectedIndex: Int = 0
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        self.setupStackView()
    }
    
    private func setupStackView() {
        self.stackView.axis  = .horizontal
        self.stackView.distribution = .fillProportionally
        self.stackView.alignment = .center
        self.stackView.spacing = 30
        
        self.addSubview(self.stackView)
        self.stackView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            self.stackView.topAnchor.constraint(equalTo: self.topAnchor, constant: 0),
            self.stackView.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -10),
            self.stackView.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 10),
            self.stackView.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: 0)
        ])
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        self.setupStackView()
    }
    
    public override var tintColor: UIColor! {
        willSet {
            for button in self.buttons {
                button.setTitleColor(newValue, for: .normal)
                button.layer.borderColor = newValue.cgColor
            }
        }
    }
    
    public var selectedColor: UIColor? {
        willSet {
            for button in self.buttons {
                button.setTitleColor(newValue, for: .selected)
            }
        }
    }
    
    public func setButtons(routes: [MKRoute]) {
        for button in self.buttons {
            button.removeFromSuperview()
            self.stackView.removeArrangedSubview(button)
        }
        self.buttons.removeAll()
        
        for i in 0..<routes.count {
            let button = UIButton()
            button.tag = i
            button.layer.cornerRadius = 8
            let title = self.stringFromTimeInterval(interval: routes[i].expectedTravelTime)
            button.setTitle(title, for: .normal)
            button.setTitle(title, for: .selected)
            button.setTitleColor(self.selectedColor, for: .selected)
            button.setTitleColor(self.tintColor, for: .normal)
            if routes[i].polyline.subtitle == "SELECTED" {
                self.select(button)
                self.selectedIndex = i
            } else {
                self.deselect(button)
            }
            button.addTarget(self, action: #selector(selectButton(_:)), for: .touchUpInside)
            self.buttons.insert(button, at: i)
            self.stackView.addArrangedSubview(button)
        }
    }
    
    public func selectButton(at index: Int) {
        if index != self.selectedIndex {
            self.deselect(self.buttons[self.selectedIndex])
            self.select(self.buttons[index])
            self.selectedIndex = index
        }
    }
    
    @objc private func selectButton(_ sender: UIButton) {
        self.selectButton(at: sender.tag)
        self.delegate?.mapHeaderView(self, at: sender.tag)
    }
    
    private func select(_ button: UIButton) {
        button.isSelected = true
        button.layer.borderWidth = 1
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 14)
    }
    
    private func deselect(_ button: UIButton) {
        button.isSelected = false
        button.layer.borderWidth = 0
        button.titleLabel?.font = UIFont.systemFont(ofSize: 14)
    }
}

extension MapHeaderView {
    func stringFromTimeInterval(interval: TimeInterval) -> String {
        let interval = Int(interval)
        let seconds = interval % 60
        let minutes = (interval / 60) % 60
        let hours = (interval / 3600)
        if hours == 0 {
            return String(format: "%02d min %02d sec", minutes, seconds)
        } else {
            return String(format: "%02d:%02d:%02d", hours, minutes, seconds)
        }
    }
}
