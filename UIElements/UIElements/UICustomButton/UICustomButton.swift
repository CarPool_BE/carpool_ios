//
//  UICustomButton.swift
//  UICustomElements
//
//  Created by Marek Labuzik on 10/05/2019.
//  Copyright © 2019 Marek Labuzik. All rights reserved.
//

import UIKit

public enum CustomButtonType {
    case ok
    case cancel
    case next
    case create
    case login
    case register
    case registerToRides
    case selectDate
    case cancelRide
    case cancelReservation
}

public class UICustomButton: UIButton {
    var color: UIColor = UIColor()
    var invertedColor: UIColor = UIColor()

    public func set(color: UIColor, invertedColor: UIColor) {
        self.color = color
        self.invertedColor = invertedColor
        self.setupView()
    }

    func setupView() {
        self.layer.borderColor = color.cgColor
        self.layer.cornerRadius = 6
        self.layer.borderWidth = 1
        self.layer.masksToBounds = true
    }
    
    public func setButtonType(type: CustomButtonType) {
        switch type {
        case .ok:
            self.setStyle(withTitle: "OK_Button".localized(), titleColor: self.invertedColor, backgroundColor: self.color)
        case .next:
            self.setStyle(withTitle: "Continue_Button".localized(), titleColor: self.invertedColor, backgroundColor: self.color)
        case .cancel:
            self.setStyle(withTitle: "Cancel_Button".localized(), titleColor: self.color, backgroundColor: self.invertedColor)
        case .create:
            self.setStyle(withTitle: "Create_Button".localized(), titleColor: self.invertedColor, backgroundColor: self.color)
        case .login:
            self.setStyle(withTitle: "Login_Button".localized(), titleColor: self.invertedColor, backgroundColor: self.color)
        case .register:
            self.setStyle(withTitle: "Register_Button".localized(), titleColor: self.invertedColor, backgroundColor: self.color)
        case .registerToRides:
            self.setStyle(withTitle: "Register_toRides_Button".localized(), titleColor: self.invertedColor, backgroundColor: self.color)
        case .cancelReservation:
            self.setStyle(withTitle: "Register_cancelRegistration_Button".localized(), titleColor: self.invertedColor, backgroundColor: self.color)
        case .cancelRide:
            self.setStyle(withTitle: "CancelRide_Button".localized(), titleColor: self.invertedColor, backgroundColor: self.color)
        case .selectDate:
            self.setStyle(withTitle: "SelectDate_Button".localized(), titleColor: self.invertedColor, backgroundColor: self.color)
        }
    }

    func setStyle(withTitle title: String, titleColor: UIColor, backgroundColor: UIColor) {
        self.backgroundColor = backgroundColor
        self.setTitleColor(titleColor, for: .normal)
        self.setTitle(title, for: .normal)
    }
}
