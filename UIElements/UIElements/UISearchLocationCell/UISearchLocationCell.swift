//
//  UISearchLocationCell.swift
//  UICustomElements
//
//  Created by Marek Labuzik on 15/05/2019.
//  Copyright © 2019 Marek Labuzik. All rights reserved.
//

import UIKit

public enum LocationCellType {
    case home
    case work
    case recent
    case actual
    case point

    var image: UIImage {
        var named: String
        switch self {
        case .home:
            named = "home"
        case .work:
            named = "work"
        case .recent:
            named = "time"
        case .actual:
            named = "gps"
        case .point:
            named = "point"
        }
        let bundle = Bundle(for: UISearchLocationCell.self)
        return UIImage(named: named, in: bundle, compatibleWith: nil)!
    }
}

public class UISearchLocationCell: UITableViewCell {
    
    @IBOutlet weak var leftImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    public var type: LocationCellType = .home
    
    public override var tintColor: UIColor! {
        willSet {
            self.leftImageView.tintColor = newValue
        }
    }

    public func set(type: LocationCellType, title: String, subTitle: String) {
        self.titleLabel.text = title
        self.leftImageView.image = type.image.withRenderingMode(.alwaysTemplate)
        self.leftImageView.tintColor = self.tintColor
        self.type = type
    }
}
