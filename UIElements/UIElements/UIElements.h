//
//  UIElements.h
//  UIElements
//
//  Created by Marek Labuzik on 12/04/2019.
//  Copyright © 2019 Marek Labuzik. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for UIElements.
FOUNDATION_EXPORT double UIElementsVersionNumber;

//! Project version string for UIElements.
FOUNDATION_EXPORT const unsigned char UIElementsVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <UIElements/PublicHeader.h>


