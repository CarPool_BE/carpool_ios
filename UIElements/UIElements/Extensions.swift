//
//  Extensions.swift
//  UICustomElements
//
//  Created by Marek Labuzik on 24/06/2019.
//  Copyright © 2019 Marek Labuzik. All rights reserved.
//

import Foundation

extension String {
    func localized() -> String {
        guard let bundle = Bundle(identifier: "eu.teamride.app.UICustomElements") else {
            return NSLocalizedString(self, comment: "")
        }
        return NSLocalizedString(self, tableName: nil, bundle: bundle, value: "", comment: "")
    }
    func unLocalized() -> String {
        print(self)
        return self
    }
}
