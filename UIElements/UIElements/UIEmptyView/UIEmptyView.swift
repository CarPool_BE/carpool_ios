//
//  UIEmptyView.swift
//  UICustomElements
//
//  Created by Marek Labuzik on 15/06/2019.
//  Copyright © 2019 Marek Labuzik. All rights reserved.
//

import UIKit

public protocol UIEmptyViewDelegate: class {
    func emptyViewTextViewDidPressed(_ emptyView: UIEmptyView)
}

public class UIEmptyView: UIView {
    @IBOutlet weak var textLabel: UILabel!
    public weak var delegate: UIEmptyViewDelegate?

    public override func awakeFromNib() {
        self.setupView()
    }

    public func set(text: NSAttributedString) {
        self.textLabel.attributedText = text
    }

    func setupView() {
        self.setUserInteraction()
    }

    func setUserInteraction() {
        let gesture: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(textDidPressed))
        gesture.numberOfTapsRequired = 1
        self.textLabel.isUserInteractionEnabled = true
        self.textLabel.addGestureRecognizer(gesture)
        if #available(iOS 13.0, *) {
            self.backgroundColor = .systemBackground
        }
    }

    @objc func textDidPressed() {
        self.delegate?.emptyViewTextViewDidPressed(self)
    }
}
