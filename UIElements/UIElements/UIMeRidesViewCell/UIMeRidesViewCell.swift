//
//  UISearchRideCellView.swift
//  WorkPool
//
//  Created by Marek Labuzik on 05/05/2019.
//  Copyright © 2019 Marek Labuzik. All rights reserved.
//

import UIKit

public class UIMeRidesViewCell: UITableViewCell, UICollectionViewDelegate, UICollectionViewDataSource {
    @IBOutlet public weak var rideBubbleView: UIView!
    
    @IBOutlet public weak var startRideTimeLabel: UILabel!
    @IBOutlet public weak var startAddressLabel: UILabel!
    @IBOutlet public weak var stopRideTimeLabel: UILabel!
    @IBOutlet public weak var stopAddressLabel: UILabel!
    
    @IBOutlet public weak var startPointImage: UIImageView!
    @IBOutlet public weak var endPointImage: UIImageView!
    @IBOutlet public weak var routePointsImage: UIImageView!
    @IBOutlet public weak var rideType: UIImageView!
    
    @IBOutlet public weak var freePlacesCollection: UICollectionView!
    
    public var rideIdentifier: Int = -1
    
    public var numberOfAllPlacesInRide: Int = 0 {
        didSet {
            self.freePlacesCollection.reloadData()
        }
    }
    public var numberOfFreePlacesInRide: Int = 0 {
        didSet {
            self.freePlacesCollection.reloadData()
        }
    }
    
    public override func layoutSubviews() {
        
        if self.freePlacesCollection.delegate == nil {
            self.freePlacesCollection.delegate = self
            self.freePlacesCollection.dataSource = self
            self.freePlacesCollection.register(UINib(nibName: "PlaceCellView",
                                                     bundle: Bundle(identifier: "eu.teamride.app.UICustomElements")),
                                               forCellWithReuseIdentifier: "PlaceCellView")
            self.freePlacesCollection.semanticContentAttribute = UISemanticContentAttribute.forceRightToLeft
        }
        
        self.rideBubbleView.layer.cornerRadius = 6
        self.rideBubbleView.layer.borderColor = self.tintColor.cgColor
        self.rideBubbleView.layer.borderWidth = 1
        if #available(iOS 13.0, *) {
            self.rideBubbleView.backgroundColor = .systemBackground
        } else {
            self.rideBubbleView.backgroundColor = UIColor(white: 237.0 / 255.0, alpha: 1.0)
        }
    }

    public override var tintColor: UIColor! {
        willSet {
            self.startPointImage.tintColor = newValue
            self.endPointImage.tintColor = newValue
            self.routePointsImage.tintColor = newValue
            self.rideType.tintColor = newValue
        }
    }

    public func isPassenger(isPassenger: Bool) {
        var named = "passenger"
        if isPassenger {
            named = "car"
        }
        let image  = UIImage(named: named,
                             in: Bundle(identifier: "eu.teamride.app.UICustomElements"),
                             compatibleWith: nil)
        self.rideType.image = image
        self.rideType.tintColor = self.tintColor
    }
    
    // MARK: - collection view delegate and data source
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.numberOfAllPlacesInRide
    }
    
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PlaceCellView",
                                                                          for: indexPath) as? PlaceCellView
            else { fatalError("dequeueReusableCell") }
        cell.tintColor = self.tintColor
        cell.setPlaceAsEmpty(isEmpty: indexPath.row < self.numberOfFreePlacesInRide)
        return cell
    }
}

final class PlaceCellView: UICollectionViewCell {
    @IBOutlet weak var image: UIImageView!

    override var tintColor: UIColor! {
        willSet {
            self.image.tintColor = newValue
        }
    }

    func setPlaceAsEmpty(isEmpty: Bool) {
        if isEmpty {
            self.image.image = UIImage(named: "carPlace",
                                       in: Bundle(identifier: "eu.teamride.app.UICustomElements"),
                                       compatibleWith: nil)
        } else {
            self.image.image = UIImage(named: "freeCarPlace",
                                       in: Bundle(identifier: "eu.teamride.app.UICustomElements"),
                                       compatibleWith: nil)
        }
    }
}
