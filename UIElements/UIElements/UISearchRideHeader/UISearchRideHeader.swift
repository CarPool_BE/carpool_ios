//
//  UISEarchRideHeader.swift
//  UICustomElements
//
//  Created by Marek Labuzik on 15/06/2019.
//  Copyright © 2019 Marek Labuzik. All rights reserved.
//

import UIKit
public enum UISearchRideHeaderActionType {
    case reverseButtonPressed
}

public protocol UISearchRideHeaderDelegate: class {
    func searchRideHeader(_ searchRideHeader: UISearchRideHeader, selectedDateChange: Date)
    func searchRideHeader(_ searchRideHeader: UISearchRideHeader, didPerformAction actionType: UISearchRideHeaderActionType)
    func searchRideHeaderDidBeginEditingFromField(_ searchRideHeader: UISearchRideHeader)
    func searchRideHeaderDidBeginEditingToField(_ searchRideHeader: UISearchRideHeader)
}

public class UISearchRideHeader: UITableViewHeaderFooterView, UICustomFieldDelegate, UIDateCollectionViewDelegate {
    public weak var delegate: UISearchRideHeaderDelegate?
    @IBOutlet weak var fromCustomField: UICustomField!
    @IBOutlet weak var toCustomField: UICustomField!
    @IBOutlet weak var dateCollectionView: UIDateCollectionView!
    @IBOutlet weak var reverseButton: UIButton!
    
    public override init(reuseIdentifier: String?) {
        super.init(reuseIdentifier: reuseIdentifier)
    }

    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    public override func awakeFromNib() {
        self.setupView()
    }

    public override var tintColor: UIColor! {
        willSet {
            self.dateCollectionView.tintColor = newValue
            self.reverseButton.tintColor = newValue
            self.fromCustomField.color = newValue
            self.toCustomField.color = newValue
        }
    }
    
    public var color: UIColor = .white {
        willSet {
            self.reverseButton.layer.borderColor = newValue.cgColor
            self.fromCustomField.tintColor = newValue
            self.toCustomField.tintColor = newValue
        }
    }

    func setupView() {
        self.fromCustomField.tag = 0
        self.toCustomField.tag = 1

        self.fromCustomField.delegate = self
        self.toCustomField.delegate = self
        self.dateCollectionView.dateDelegate = self

        self.fromCustomField.setCustomField(type: .home)
        self.fromCustomField.addGuesture()
        self.toCustomField.setCustomField(type: .work)
        self.toCustomField.addGuesture()

        self.reverseButton.clipsToBounds = true
        self.reverseButton.layer.cornerRadius = self.reverseButton.frame.height/2
        self.reverseButton.layer.borderWidth = 1
        self.reverseButton.addTarget(self, action: #selector(reverseButtonPressed), for: .touchUpInside)
        if #available(iOS 13.0, *) {
            self.reverseButton.backgroundColor = .systemBackground
        }
    }

    public func set(from: String, to: String) {
        self.fromCustomField.textField.text = from
        self.toCustomField.textField.text = to
    }

    @objc func reverseButtonPressed() {
        if self.toCustomField.customFieldType == .home {
            self.toCustomField.setCustomField(type: .work)
            self.fromCustomField.setCustomField(type: .home)
        } else {
            self.toCustomField.setCustomField(type: .home)
            self.fromCustomField.setCustomField(type: .work)
        }
        self.delegate?.searchRideHeader(self, didPerformAction: .reverseButtonPressed)
    }

    // MARK: - delegete metohods
    public func customFieldPressed(_ customField: UICustomField) {
        switch customField.tag {
        case 0:
            self.delegate?.searchRideHeaderDidBeginEditingFromField(self)
        case 1:
            self.delegate?.searchRideHeaderDidBeginEditingToField(self)
        default:
            break
        }
    }
    public func customFieldDidBeginEditing(_ customField: UICustomField) {

    }

    public func viewController(_ viewController: UIDateCollectionView, didSelectDate: Date) {
        self.delegate?.searchRideHeader(self, selectedDateChange: didSelectDate)
    }

    public func customField(_ customField: UICustomField, valueDidChange: String) {

    }
}
