//
//  Pickers.swift
//  UICustomElements
//
//  Created by Marek Labuzik on 10/05/2019.
//  Copyright © 2019 Marek Labuzik. All rights reserved.
//

import UIKit

public class UIPickerViewController: UIViewController {
    public var buttonCompletitionAction: ((Date) -> Void)?
    
    private let pickerTitle: UILabel = UILabel()
    
    private let datePicker: UIDatePicker = UIDatePicker()
    private let pickerBgView: UIView = UIView()
    private let okButton: UICustomButton = UICustomButton()
    private let cancelButton: UICustomButton = UICustomButton()

    public init(mode: UIDatePicker.Mode) {
        self.datePicker.datePickerMode = mode
        super.init(nibName: nil, bundle: nil)
        switch mode {
        case .time:
             self.pickerTitle.text = "Start_At".localized()
        case .date:
            self.pickerTitle.text = "Select_Date".localized()
        case .countDownTimer, .dateAndTime:
            break
        @unknown default:
            break
        }
    }
    
    override public func viewDidLoad() {
        self.addSubviews()
        self.setupViews()
        self.setupConstraints()
        super.viewDidLoad()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    public func set(color: UIColor, invertedColor: UIColor) {
        self.okButton.set(color: color, invertedColor: invertedColor)
        self.cancelButton.set(color: color, invertedColor: invertedColor)
    }

    public func completition(completition:@escaping (Date) -> Void) {
        self.buttonCompletitionAction = completition
    }
    func addSubviews() {
        self.view.addSubview(self.pickerBgView)
        self.pickerBgView.addSubview(self.datePicker)
        self.pickerBgView.addSubview(self.pickerTitle)
        self.pickerBgView.addSubview(self.okButton)
        self.pickerBgView.addSubview(self.cancelButton)
    }
    
    public override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        if #available(iOS 13.0, *) {
            if self.traitCollection.userInterfaceStyle == .light {
                self.view.backgroundColor = UIColor.black.withAlphaComponent(0.3)
            } else {
                self.view.backgroundColor = UIColor.white.withAlphaComponent(0.3)
            }
        }
    }
    
    func setupViews() {
        if #available(iOS 13.0, *) {
            self.pickerBgView.backgroundColor = .systemBackground
        } else {
            self.view.backgroundColor = UIColor.black.withAlphaComponent(0.3)
            self.pickerBgView.backgroundColor = .white
        }
        
        self.pickerTitle.font = UIFont.systemFont(ofSize: 20, weight: UIFont.Weight.bold)
        self.pickerTitle.textAlignment = NSTextAlignment.center
        
        if self.datePicker.datePickerMode == UIDatePicker.Mode.date {
            datePicker.minimumDate = Date()
        }
        self.datePicker.locale = Locale.autoupdatingCurrent
        
        self.okButton.addTarget(self, action: #selector(okButtonPressed(sender:)), for: UIControl.Event.touchUpInside)
        self.okButton.setButtonType(type: CustomButtonType.ok)
        
        self.cancelButton.addTarget(self, action: #selector(cancelButtonPressed(sender:)), for: UIControl.Event.touchUpInside)
        self.cancelButton.setButtonType(type: CustomButtonType.cancel)
    }
    
    func setupConstraints() {
        self.pickerBgView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            self.pickerBgView.heightAnchor.constraint(equalToConstant: 333),
            self.pickerBgView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: 0),
            self.pickerBgView.rightAnchor.constraint(equalTo: self.view.rightAnchor, constant: 0),
            self.pickerBgView.leftAnchor.constraint(equalTo: self.view.leftAnchor, constant: 0)
            ])
        self.datePicker.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            self.datePicker.topAnchor.constraint(equalTo: self.pickerBgView.topAnchor, constant: 100),
            self.datePicker.bottomAnchor.constraint(equalTo: self.pickerBgView.bottomAnchor, constant: -100),
            self.datePicker.rightAnchor.constraint(equalTo: self.pickerBgView.rightAnchor, constant: 0),
            self.datePicker.leftAnchor.constraint(equalTo: self.pickerBgView.leftAnchor, constant: 0)
            ])
        self.pickerTitle.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            self.pickerTitle.topAnchor.constraint(equalTo: self.pickerBgView.topAnchor, constant: 43),
            self.pickerTitle.bottomAnchor.constraint(equalTo: self.pickerBgView.bottomAnchor, constant: -266),
            self.pickerTitle.rightAnchor.constraint(equalTo: self.pickerBgView.rightAnchor, constant: 0),
            self.pickerTitle.leftAnchor.constraint(equalTo: self.pickerBgView.leftAnchor, constant: 0)
            ])
        // button width
        let buttonWidth = (UIScreen.main.bounds.width - 16 - 16 - 24)/2
        // button ok
        self.cancelButton.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            self.cancelButton.widthAnchor.constraint(equalToConstant: buttonWidth),
            self.cancelButton.heightAnchor.constraint(equalToConstant: 48),
            self.cancelButton.bottomAnchor.constraint(equalTo: self.pickerBgView.bottomAnchor, constant: -24),
            self.cancelButton.leftAnchor.constraint(equalTo: self.pickerBgView.leftAnchor, constant: 16)
            ])
        // button cancel
        self.okButton.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            self.okButton.widthAnchor.constraint(equalToConstant: buttonWidth),
            self.okButton.heightAnchor.constraint(equalToConstant: 48),
            self.okButton.bottomAnchor.constraint(equalTo: self.pickerBgView.bottomAnchor, constant: -24),
            self.okButton.rightAnchor.constraint(equalTo: self.pickerBgView.rightAnchor, constant: -16),
            self.okButton.leftAnchor.constraint(equalTo: self.cancelButton.rightAnchor, constant: 24)
            ])
    }
    
    // MARK: - actions
    @objc func okButtonPressed(sender: UIButton) {
        guard let buttonAction = self.buttonCompletitionAction else {
            self.dismissView()
            return
        }
        buttonAction(self.datePicker.date)
        self.dismissView()
    }
    
    @objc func cancelButtonPressed(sender: UIButton) {
        
        self.dismissView()
    }
    
    // MARK: -
    func dismissView() {
        self.dismiss(animated: true, completion: nil)
    }
}
