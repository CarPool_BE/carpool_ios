//
//  UIRideDetailHeader.swift
//  UICustomElements
//
//  Created by Marek Labuzik on 18/06/2019.
//  Copyright © 2019 Marek Labuzik. All rights reserved.
//

import UIKit
public enum UIRideDetailHeaderAction {
    case makeCall
}

public protocol UIRideDetailHeaderDelegate: class {
    func headerView(_ view: UIRideDetailHeader, didPerform action: UIRideDetailHeaderAction)
}

public class UIRideDetailHeader: UIView {
    public weak var delegate: UIRideDetailHeaderDelegate?
    @IBOutlet public weak var firstNameLabel: UILabel!
    @IBOutlet public weak var lastNameLabel: UILabel!
    @IBOutlet public weak var carTypeLabel: UILabel!
    @IBOutlet public weak var carNumberAndColorLabel: UILabel!
    @IBOutlet weak var callButton: UIButton!
    
    public override var tintColor: UIColor! {
        willSet {
            self.firstNameLabel.textColor = newValue
            self.lastNameLabel.textColor = newValue
            self.carTypeLabel.textColor = newValue
            self.carNumberAndColorLabel.textColor = newValue
        }
    }
    
    public func showPhoneButtonToCall(show: Bool) {
        self.callButton.isHidden = !show
    }
    
    public override func layoutSubviews() {
        super.layoutSubviews()
        if #available(iOS 13.0, *) {
            self.backgroundColor = .systemBackground
        }
    }
    
    @IBAction func callButtonPressed(_ sender: Any) {
         self.delegate?.headerView(self, didPerform: .makeCall)
    }
}
