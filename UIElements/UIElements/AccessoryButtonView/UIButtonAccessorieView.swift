//
//  UIButtonAccessorieView.swift
//  UICustomElements
//
//  Created by Marek Labuzik on 21/06/2019.
//  Copyright © 2019 Marek Labuzik. All rights reserved.
//

import UIKit

public enum AccessoryButtonViewType {
    case none
    case cancelRide
    case cancelReservation
    case registerRide
    case createRide
    case selectDate

    var buttonType: CustomButtonType {
        switch self {
        case .cancelReservation:
            return .cancelReservation
        case .cancelRide:
            return .cancelRide
        case .createRide:
            return .create
        case .none:
            return .ok
        case .registerRide:
            return .registerToRides
        case .selectDate:
            return .selectDate
        }
    }
}

public protocol AccessoryButtonViewDelegate: class {
    func accessoryButtonPressed(_ view: AccessoryButtonView)
}

final public class AccessoryButtonView: UIView {
    public weak var delegate: AccessoryButtonViewDelegate?
    var button: UICustomButton?
    var isLargePhone: Bool = false

    public init(size: CGSize, isLargePhone: Bool) {
        super.init(frame: CGRect(origin: .zero, size: size))
        self.isLargePhone = isLargePhone
        self.customInit(with: size)
        
        if #available(iOS 13.0, *) {
            self.backgroundColor = .tertiarySystemBackground
        } else {
            self.backgroundColor = .white
        }
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func customInit(with size: CGSize) {
        if self.isLargePhone {
            self.frame = CGRect(origin: .zero, size: CGSize(width: self.frame.width, height: self.frame.height + 16))
        }
        self.button = UICustomButton(frame: CGRect(x: 16, y: 8, width: size.width - 2 * 16, height: 44))
        self.button?.addTarget(self, action: #selector(reserveButtonPressed), for: .touchUpInside)
        self.addSubview(self.button!)
    }

    @objc func reserveButtonPressed() {
        self.delegate?.accessoryButtonPressed(self)
    }

    public func setAccessoryButtonView(type: AccessoryButtonViewType) {
        self.button?.set(color: self.tintColor, invertedColor: .white)
        self.button?.setButtonType(type: type.buttonType)
    }
}
