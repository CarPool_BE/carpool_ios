//
//  UIProfileHeader.swift
//  UICustomElements
//
//  Created by Martin Kurbel on 17/06/2019.
//  Copyright © 2019 Marek Labuzik. All rights reserved.
//

import UIKit

class UIProfileHeader: UIView {
    
    let profileImage = UIImageView()

    override init(frame: CGRect) {
        super.init(frame: frame)
        
        initialize()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func initialize() {
        
    }
}
