//
//  UISwitcherCell.swift
//  UICustomElements
//
//  Created by Marek Labuzik on 12/09/2019.
//  Copyright © 2019 Marek Labuzik. All rights reserved.
//

import UIKit

public protocol UISwitcherCellDelegate: class {
    func switcherCell(_ switcherCell: UISwitcherCell, switchTo: Int)
}

public final class UISwitcherCell: UITableViewCell {
    var switcher: UISegmentedControl?
    public weak var delegate: UISwitcherCellDelegate?
    
    public override func layoutSubviews() {
        super.layoutSubviews()
        if self.switcher == nil {
            self.setupSegmentedControll()
            self.setupCell()
        }
    }
    
    func setupCell() {
        if #available(iOS 13.0, *) {
            self.backgroundColor = .secondarySystemBackground
        }
    }
    
    func setupSegmentedControll() {
        self.switcher = UISegmentedControl(items: ["TabBar_SearchRides".localized(), "TabBar_MePlan".localized()])
        self.switcher?.addTarget(self, action: #selector(segmentChange(_:)), for: .valueChanged)
        self.addSubview(self.switcher!)
        self.setupConstraints()
    }
    
    func setupConstraints() {
        guard let switcher = self.switcher else { return }
        switcher.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            switcher.topAnchor.constraint(equalTo: self.topAnchor, constant: 12),
            switcher.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -12),
            switcher.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -20),
            switcher.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 20)
        ])
    }
    
    public func setSelectedSegmetn(ad index: Int) {
        if self.switcher == nil {
            self.setupSegmentedControll()
            self.setupCell()
        }
        self.switcher?.selectedSegmentIndex = index
    }
    
    @objc func segmentChange(_ segment: UISegmentedControl) {
        self.delegate?.switcherCell(self, switchTo: segment.selectedSegmentIndex)
    }
}
