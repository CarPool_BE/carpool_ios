//
//  UISearchRideCell.swift
//  UICustomElements
//
//  Created by Marek Labuzik on 15/06/2019.
//  Copyright © 2019 Marek Labuzik. All rights reserved.
//

import UIKit

public class UISearchRideCell: UITableViewCell {
    @IBOutlet public weak var startTimeLabel: UILabel!
    @IBOutlet weak var separatorView: UIView!
    @IBOutlet public weak var startAddressLabel: UILabel!
    @IBOutlet public weak var endAddressLabel: UILabel!
    @IBOutlet public weak var driverNameLabel: UILabel!
    public var identifier: Int?
    @IBOutlet var imagesViews: [UIImageView]!
    @IBOutlet weak var bubbleView: UIView!
    
    public override var tintColor: UIColor! {
        willSet {
            self.startTimeLabel.textColor = newValue
            self.startAddressLabel.textColor = newValue
            self.endAddressLabel.textColor = newValue
            self.driverNameLabel.textColor = newValue
            self.separatorView.backgroundColor = newValue
            self.bubbleView.layer.borderColor = newValue.cgColor
            for image in self.imagesViews {
                image.tintColor = newValue
            }
        }
    }
    public override func awakeFromNib() {
        super.awakeFromNib()
        self.setCornerRadius()
    }
    
    func setCornerRadius() {
        self.bubbleView.clipsToBounds = true
        self.bubbleView.layer.cornerRadius = 6
        self.bubbleView.layer.borderWidth = 1
        if #available(iOS 13.0, *) {
            self.bubbleView.backgroundColor = .systemBackground
        }
    }
}
