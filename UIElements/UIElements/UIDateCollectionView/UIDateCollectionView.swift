//
//  UIDateCollectionView.swift
//  UICustomElements
//
//  Created by Marek Labuzik on 11/06/2019.
//  Copyright © 2019 Marek Labuzik. All rights reserved.
//

import UIKit

struct DateViewModel {
    let date: Date
    let dayOfWeek: String
    let numberOfMonthDay: String
    var month: String
    let monthNumber: Int

    init(with date: Date) {
        self.date = date
        self.numberOfMonthDay = DateViewModel.dateToNumberOfDay(date: date)
        self.dayOfWeek = DateViewModel.dateToDayOfWeek(date: date)
        self.month = DateViewModel.dateToMonth(with: date)
        self.monthNumber = DateViewModel.dateToInt(with: date)
    }

    private static func dateToNumberOfDay(date: Date) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd"
        return formatter.string(from: date)
    }
    private static func dateToDayOfWeek(date: Date) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "EEE"
        return formatter.string(from: date)
    }
    private static func dateToMonth(with: Date) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "LLLL"
        let dateString = formatter.string(from: with)
        return dateString
    }

    private static func dateToInt(with: Date) -> Int {
        let formatter = DateFormatter()
        formatter.dateFormat = "M"
        let dateString = Int(formatter.string(from: with)) ?? 0
        return dateString
    }
}

final class DateViewModels {
    private(set) var dictionnaryDates: [DatePickerViewModel] = []
    private(set) var monthNames: [String] = []

    public init(from date: Date = Date()) {
        for identifier in 0...60 {
            guard let date = Calendar.current.date(byAdding: .day,
                                                   value: identifier,
                                                   to: date) else { break }
            let dateModel = DateViewModel(with: date)

            var index = self.monthNames.firstIndex(of: dateModel.month)
            if index == nil {
                self.monthNames.append(dateModel.month)
                index = self.monthNames.firstIndex(of: dateModel.month)
            }

            var pickerViewModel: DatePickerViewModel?
            if self.dictionnaryDates.indices.contains(index!) {
                pickerViewModel = self.dictionnaryDates[index!]
            } else {
                pickerViewModel = DatePickerViewModel(name: dateModel.month)
                self.dictionnaryDates.append(pickerViewModel!)
            }
            pickerViewModel!.dates.append(dateModel)
            self.dictionnaryDates[index!] = pickerViewModel!
        }
    }

    func getNameFor(section: Int) -> String {
        if section > self.monthNames.count {
            return "No valid month"
        }
        let monthName = self.monthNames[section]
        return monthName
    }

    public func getNumberOfSections() -> Int {
        return self.monthNames.count
    }

    public func getNumberOfItemsIn(section: Int) -> Int {
        if section > self.dictionnaryDates.count {
            return 0
        }
        let datePickeViewModel = self.dictionnaryDates[section]

        return datePickeViewModel.dates.count
    }

    public func getViewModelFor(indexPath: IndexPath) -> DateViewModel? {
        if indexPath.section > self.dictionnaryDates.count {
            return DateViewModel(with: Date())
        }
        let datePickeViewModel = self.dictionnaryDates[indexPath.section]
        if indexPath.row > datePickeViewModel.dates.count {
            return DateViewModel(with: Date())
        }
        return datePickeViewModel.dates[indexPath.row]
    }
}

struct DatePickerViewModel {
    let name: String
    var dates: [DateViewModel]

    init(name: String) {
        self.name = name
        self.dates = []
    }
}

final class SectionHeader: UICollectionReusableView {
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.addSubview(self.sectionHeaderlabel)
    }
    var sectionHeaderlabel: UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: 88, height: 28))
}

final class UIDateFlowLayout: UICollectionViewFlowLayout {
    override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        let attributes = super.layoutAttributesForElements(in: rect)?
            .map { $0.copy() } as? [UICollectionViewLayoutAttributes]

        attributes?
            .filter { $0.representedElementCategory == .cell }
            .forEach { attribute in
                let frame = self.collectionView?.bounds ?? .zero
                attribute.frame.origin.y = frame.height - attribute.frame.size.height - 5
        }

        return attributes
    }
}

public protocol UIDateCollectionViewDelegate: class {
    func viewController(_ viewController: UIDateCollectionView, didSelectDate: Date)
}

public final class UIDateCollectionView: UICollectionView, UICollectionViewDataSource,
                            UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {

    let viewModel: DateViewModels = DateViewModels()
    public weak var dateDelegate: UIDateCollectionViewDelegate?

    public override func awakeFromNib() {
        super.awakeFromNib()
        setupView()
    }

    // MARK: - set view
    func setupView() {
        if let flowLayout = self.collectionViewLayout as? UICollectionViewFlowLayout {
            flowLayout.scrollDirection = .horizontal
            flowLayout.sectionHeadersPinToVisibleBounds = true
            flowLayout.sectionInset = UIEdgeInsets(top: 0, left: 14, bottom: 0, right: 0)
            flowLayout.itemSize = CGSize(width: 56, height: 56)
            flowLayout.headerReferenceSize = CGSize(width: 5, height: 28)
        }
        self.register(SectionHeader.self,
                      forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader,
                      withReuseIdentifier: "SectionHeader")
        self.register(UIDateCollectionViewCell.self, forCellWithReuseIdentifier: "UIDateCollectionViewCell")
        self.delegate = self
        self.dataSource = self
        self.reloadData()
        let index = IndexPath(item: 0, section: 0)
        self.selectItem(at: index,
                        animated: false,
                        scrollPosition: .left)
        
        if #available(iOS 13.0, *) {
            self.backgroundColor = .systemBackground
        }
    }

    override public func layoutSubviews() {
        super.layoutSubviews()
    }

    // MARK: - delegate and datasource methods
    public func collectionView(_ collectionView: UICollectionView,
                               viewForSupplementaryElementOfKind kind: String,
                               at indexPath: IndexPath) -> UICollectionReusableView {
        if let sectionHeader = collectionView.dequeueReusableSupplementaryView(ofKind: kind,
                                                                               withReuseIdentifier: "SectionHeader",
                                                                               for: indexPath) as? SectionHeader {
            sectionHeader.sectionHeaderlabel.text = self.viewModel.getNameFor(section: indexPath.section)
            sectionHeader.sectionHeaderlabel.font = UIFont.systemFont(ofSize: 17, weight: .medium)
            if #available(iOS 13.0, *) {
                sectionHeader.sectionHeaderlabel.textColor = .label
                sectionHeader.backgroundColor = .systemBackground
            } else {
                sectionHeader.sectionHeaderlabel.backgroundColor = .white
            }
            return sectionHeader
        }
        return UICollectionReusableView()
    }

    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        let value = self.viewModel.getNumberOfItemsIn(section: section)
        return value
    }

    public func numberOfSections(in collectionView: UICollectionView) -> Int {
        let value = self.viewModel.getNumberOfSections()
        return value
    }

    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "UIDateCollectionViewCell",
                                                            for: indexPath) as? UIDateCollectionViewCell else {
            fatalError()
        }
        guard let model = self.viewModel.getViewModelFor(indexPath: indexPath) else { return cell }
        cell.set(date: model.numberOfMonthDay, day: model.dayOfWeek)
        cell.date = model.date
        cell.tintColor = self.tintColor
        if cell.isSelected {
            cell.isSelected = true
        }
        return cell
    }

    public func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let cell = collectionView.cellForItem(at: indexPath) as? UIDateCollectionViewCell else { fatalError() }
        guard let date = cell.date else { fatalError() }
        cell.isSelected = true
        self.dateDelegate?.viewController(self, didSelectDate: date)
    }

    public func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        guard let cell = collectionView.cellForItem(at: indexPath) as? UIDateCollectionViewCell else { return }
        cell.isSelected = false
    }
}
