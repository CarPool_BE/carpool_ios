//
//  UIDaceCollectionViewCell.swift
//  UICustomElements
//
//  Created by Marek Labuzik on 11/06/2019.
//  Copyright © 2019 Marek Labuzik. All rights reserved.
//

import UIKit
import QuartzCore

final class UIDateCollectionViewCell: UICollectionViewCell {
    private let dateLabel: UILabel = UILabel()
    private let dayLabel: UILabel = UILabel()
    public var date: Date?

    override var isSelected: Bool {
        willSet {
            if newValue {
                self.willSelect()
            } else {
                self.willDeselect()
            }
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.customInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.customInit()
    }
    
    public func set(date: String, day: String) {
        self.dateLabel.text = date
        self.dayLabel.text = day
    }
    
    func customInit() {
        self.addSubview(self.dateLabel)
        self.addSubview(self.dayLabel)
        self.setupConstraints()
        
        self.dateLabel.font = UIFont.systemFont(ofSize: 17, weight: .bold)
        self.dayLabel.font = self.dateLabel.font
        
        self.dateLabel.textAlignment = .center
        self.dayLabel.textAlignment = .center
    }
    
    func setupConstraints() {
        self.dateLabel.translatesAutoresizingMaskIntoConstraints = false
        self.dayLabel.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            self.dateLabel.topAnchor.constraint(equalTo: self.topAnchor, constant: 0),
            self.dateLabel.rightAnchor.constraint(equalTo: self.rightAnchor, constant: 0),
            self.dateLabel.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 0),
            self.dateLabel.heightAnchor.constraint(equalToConstant: self.frame.size.height/2),
            
            self.dayLabel.rightAnchor.constraint(equalTo: self.rightAnchor, constant: 0),
            self.dayLabel.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 0),
            self.dayLabel.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: 0),
            self.dayLabel.topAnchor.constraint(equalTo: self.dateLabel.bottomAnchor, constant: 0)
            ])
    }
    
    func willSelect() {
       self.showBorder(isBorder: true)
        self.set(color: self.tintColor)
    }
    
    func willDeselect() {
        if #available(iOS 13.0, *) {
            self.set(color: .label)
        } else {
            self.set(color: .black)
        }
        
        self.showBorder(isBorder: false)
    }
    
    func set(color: UIColor) {
        self.dayLabel.textColor = color
        self.dateLabel.textColor = color
    }
    
    func showBorder(isBorder: Bool) {
        if isBorder {
            self.layer.borderWidth = 1
        } else {
            self.layer.borderWidth = 0
        }
        
        self.layer.borderColor = self.tintColor.cgColor
        self.layer.cornerRadius = 6
    }
    
}
