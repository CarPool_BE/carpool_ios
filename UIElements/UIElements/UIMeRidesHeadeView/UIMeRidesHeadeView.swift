//
//  UIMeRidesHeadeView.swift
//  UICustomElements
//
//  Created by Marek Labuzik on 17/06/2019.
//  Copyright © 2019 Marek Labuzik. All rights reserved.
//

import UIKit

public protocol UIMeRidesHeadeViewDelegate: class {
    func meRidesHeader(_ meRidesHeader: UIMeRidesHeadeView, selectedDateChange: Date)
}

public class UIMeRidesHeadeView: UITableViewHeaderFooterView, UIDateCollectionViewDelegate {
    @IBOutlet weak var datesCollection: UIDateCollectionView!
    public weak var delegate: UIMeRidesHeadeViewDelegate?
    
    public override var tintColor: UIColor! {
        willSet {
            self.datesCollection.tintColor = newValue
        }
    }
    
    public override func awakeFromNib() {
        self.setupView()
    }
    
    func setupView() {
        if #available(iOS 13.0, *) {
            self.backgroundColor = .systemBackground
        }
        self.datesCollection.dateDelegate = self
    }
    
    public func viewController(_ viewController: UIDateCollectionView, didSelectDate: Date) {
        self.delegate?.meRidesHeader(self, selectedDateChange: didSelectDate)
    }
}
