//
//  UIFormCell.swift
//  UICustomElements
//
//  Created by Marek Labuzik on 09/05/2019.
//  Copyright © 2019 Marek Labuzik. All rights reserved.
//

import UIKit

public class UIFormCell: UITableViewCell {
    @IBOutlet public weak var customField: UICustomField!

    public func setFormCell(type: FieldType) {
        self.customField.setCustomField(type: type)
    }

    public var color: UIColor! {
        didSet {
            self.customField.color = color
        }
    }
    
    public override var tintColor: UIColor! {
        didSet {
            self.customField.tintColor = self.tintColor
        }
    }

    public func setPlaceholder(text: String) {
        var color = UIColor(red: 0, green: 0, blue: 0, alpha: 0.4)
        if #available(iOS 13.0, *) {
            color = .placeholderText
        }
        
        if customField.customFieldType == .carPlaces {
            self.customField.textField.text = ""
            if #available(iOS 13.0, *) {
                color = .label
            } else {
                color = UIColor(red: 0, green: 0, blue: 0, alpha: 0.8)
            }
        }
        
        let string = NSAttributedString(string: text, attributes: [NSAttributedString.Key.foregroundColor: color])
        self.customField.textField.attributedPlaceholder = string
    }
    
    public func setEnableWitingToTextField(isEnable: Bool) {
        self.customField.isUserInteractionEnabled = isEnable
    }
    
    public func setDefaultValue(string: String) {
         self.customField.setDefaultValue(string: string)
    }
}
