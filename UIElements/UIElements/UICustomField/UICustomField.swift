//
//  UICustomField.swift
//  UIElements
//
//  Created by Marek Labuzik on 14/04/2019.
//  Copyright © 2019 Marek Labuzik. All rights reserved.
//

import UIKit

enum FieldViewType {
    case textfield
    case switcher
    case label
    case stepper
}

public enum FieldType {
    case home
    case work
    case time
    case info
    case calendar
    case car
    case returnRide
    case carPlaces
    
    var image: UIImage {
        var named: String
        switch self {
        case .home: named = "home"
        case .work: named = "work"
        case .time: named = "time"
        case .info: named = "info"
        case .calendar: named = "calendar"
        case .car: named = "car"
        case .returnRide: named = "returnRide"
        case .carPlaces: named = "carPlace"
        }
        let bundle = Bundle(for: UICustomField.self)
        return UIImage(named: named, in: bundle, compatibleWith: nil)!
    }
    func type() -> FieldViewType {
        switch self {
            case .home,
                 .work,
                 .time,
                 .calendar,
                 .car:
                return .label
            case .info:
                return .textfield
            case .returnRide:
                return .switcher
            case .carPlaces:
                return .stepper
        }
    }
}

public protocol UICustomFieldDelegate: class {
    func customField(_ customField: UICustomField, valueDidChange: String)
    func customFieldDidBeginEditing(_ customField: UICustomField)
    func customFieldPressed(_ customField: UICustomField)
}

@IBDesignable
public class UICustomField: UIView, UITextFieldDelegate {
    public var indexPath: IndexPath?
    public weak var delegate: UICustomFieldDelegate?
    let bubbleView: UIView = UIView()
    let textField: UITextField = UITextField()
    let image: UIImageView = UIImageView()
    let stepper: UIStepper = UIStepper()
    let valueForStepper: UILabel = UILabel()
    let switcher: UISwitch = UISwitch()

    var customFieldType: FieldType? {
        didSet {
            self.image.image = self.customFieldType?.image
        }
    }
    
    public override var tintColor: UIColor! {
        willSet {
            self.layer.borderColor = newValue.cgColor
        }
    }

    public var color: UIColor! {
        didSet {
            self.image.tintColor = color
            self.stepper.tintColor = color
            self.switcher.onTintColor = color
        }
    }

    public func setDefaultValue(string: String) {
        guard let type = self.customFieldType?.type() else {return}
        switch type {
        case .textfield,
             .switcher,
             .label:
            self.textField.text = string
        case .stepper:
            self.valueForStepper.text = string
            self.setFree(places: Int(string) ?? 0)
        }
    }

    func setCustomField(type: FieldType) {
        self.customFieldType = type
        self.hideUselessViews()
    }

    public override init(frame: CGRect) {
        super.init(frame: frame)
        self.setupView()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setupView()
    }

    func setupView() {
        self.addViewsToSuperView()
        self.setupConstraints()
        self.setupLayerView()
        if #available(iOS 13.0, *) {
            self.backgroundColor = .tertiarySystemBackground
        }
        self.layoutIfNeeded()
    }

    private func addViewsToSuperView() {
        self.addSubview(self.textField)
        self.addSubview(self.image)
        self.addSubview(self.stepper)
        self.addSubview(self.switcher)
        self.addSubview(self.valueForStepper)
        self.textField.delegate = self
        self.stepper.maximumValue = 12
        self.stepper.minimumValue = 0
        self.stepper.addTarget(self,
                               action: #selector(stepperValueChanged(_:)),
                               for: UIControl.Event.valueChanged)
        self.textField.addTarget(self,
                                 action: #selector(textFieldDidChange(_:)),
                                 for: UIControl.Event.editingChanged)
        self.switcher.addTarget(self,
                                 action: #selector(switchDidChange(_:)),
                                 for: UIControl.Event.valueChanged)
    }

    private func setupInputView() {
        let inputView = UIToolbar(frame: CGRect(origin: .zero, size: CGSize(width: UIScreen.main.bounds.width, height: 44)))
        let actionDone = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(hideKeyboard))
        inputView.items = [actionDone]
        self.textField.inputAccessoryView = inputView
    }

    @objc func hideKeyboard() {
        self.textField.resignFirstResponder()
    }

    public func addGuesture() {
        let gesture: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(textFieldDidPressed))
        gesture.numberOfTapsRequired = 1
        self.addGestureRecognizer(gesture)
    }
    
    private func setupLayerView() {
        self.layer.cornerRadius = 6
        self.layer.borderWidth = 1
        if #available(iOS 13.0, *) {
            if (self.traitCollection.userInterfaceStyle == .dark) {
                self.layer.borderWidth = 0
            }
        }
        self.layer.masksToBounds = true
    }

    func hideUselessViews() {
        guard let type = self.customFieldType?.type() else {return}
        switch type {
        case .textfield:
            self.setTexField(isEditable: true)
        case .switcher:
            self.setSwitcher()
        case .label:
            self.setTexField(isEditable: false)
        case .stepper:
            self.setStepper()
        }
    }

    func setTexField(isEditable: Bool) {
        self.switcher.isHidden = true
        self.stepper.isHidden = true
        self.valueForStepper.isHidden = true

        self.image.isHidden = false
        self.textField.isHidden = false
        if isEditable {
            self.setupInputView()
            self.textField.isUserInteractionEnabled = true
        } else {
            self.textField.isUserInteractionEnabled = false
        }
    }

    func setSwitcher() {
        self.switcher.isHidden = false
        self.stepper.isHidden = true
        self.valueForStepper.isHidden = true
        self.textField.isUserInteractionEnabled = false
    }

    func setStepper() {
        self.switcher.isHidden = true
        self.stepper.isHidden = false
        self.valueForStepper.isHidden = false
        self.textField.isUserInteractionEnabled = false
    }

    private func setupConstraints() {
        self.textField.translatesAutoresizingMaskIntoConstraints = false
        self.image.translatesAutoresizingMaskIntoConstraints = false
        self.stepper.translatesAutoresizingMaskIntoConstraints = false
        self.switcher.translatesAutoresizingMaskIntoConstraints = false
        self.valueForStepper.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            self.image.topAnchor.constraint(equalTo: self.topAnchor, constant: 12),
            self.image.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 16),
            self.image.heightAnchor.constraint(equalToConstant: 24),
            self.image.widthAnchor.constraint(equalToConstant: 24),

            self.textField.leftAnchor.constraint(equalTo: self.image.rightAnchor, constant: 16.0),
            self.textField.topAnchor.constraint(equalTo: self.topAnchor, constant: 12.0),
            self.textField.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -8.0),
            self.textField.heightAnchor.constraint(equalToConstant: 24.0),

            self.stepper.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -16),
            self.stepper.centerYAnchor.constraint(equalTo: self.centerYAnchor, constant: 0),
            self.stepper.widthAnchor.constraint(equalToConstant: 94),
            self.stepper.heightAnchor.constraint(equalToConstant: 29),

            self.valueForStepper.rightAnchor.constraint(equalTo: self.stepper.leftAnchor, constant: -20),
            self.valueForStepper.centerYAnchor.constraint(equalTo: self.centerYAnchor, constant: 0),
            self.valueForStepper.heightAnchor.constraint(equalToConstant: 29),
            self.valueForStepper.heightAnchor.constraint(greaterThanOrEqualToConstant: 10),

            self.switcher.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -16),
            self.switcher.centerYAnchor.constraint(equalTo: self.centerYAnchor, constant: 0),
            self.switcher.widthAnchor.constraint(equalToConstant: 49),
            self.switcher.heightAnchor.constraint(equalToConstant: 31)

            ])
    }

    @objc func textFieldDidPressed() {
        self.delegate?.customFieldPressed(self)
    }

    func setFree(places: Int) {
        self.stepper.value = Double(places)
        self.valueForStepper.text = String(places)
    }

    public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let count = textField.text?.count else { return true }
        if  count < 40 {
            return true
        }
        return false
    }
    
    public func textFieldDidBeginEditing(_ textField: UITextField) {
        self.delegate?.customFieldDidBeginEditing(self)
    }
    // MARK: - interactions
    @objc func stepperValueChanged(_ sender: UIStepper) {
        self.valueForStepper.text = String(Int(sender.value))
        self.delegate?.customField(self, valueDidChange: self.valueForStepper.text ?? "")
    }

    @objc func textFieldDidChange(_ sender: UITextField) {
        self.delegate?.customField(self, valueDidChange: sender.text ?? "")
    }

    @objc func switchDidChange(_ sender: UISwitch) {
        self.delegate?.customField(self, valueDidChange: "\(sender.isOn)")
    }
}
