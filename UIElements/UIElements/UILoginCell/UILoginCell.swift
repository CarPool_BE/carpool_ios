//
//  UILoginCell.swift
//  UICustomElements
//
//  Created by Marek Labuzik on 02/06/2019.
//  Copyright © 2019 Marek Labuzik. All rights reserved.
//

import UIKit

public protocol UILoginCellDelegate: class {
    func customField(_ customField: UILoginCell, valueDidChange: String)
}

public class  UILoginCell: UITableViewCell {
    public weak var delegate: UILoginCellDelegate?
    public private(set) var textField: UITextField = UITextField()
    private var maxLenght: Int = 0
    var showPasswordButton: UIButton = UIButton()

    public override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.customInit()
    }
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.customInit()
    }

    func customInit() {
        self.addSubviews()
        self.setupCell()
        self.setConstraints()
    }

    func setupCell() {
        self.showPasswordButton.isHidden = true
        self.textField.delegate = self
        self.textField.addTarget(self,
                                 action: #selector(textFieldDidChange(_:)),
                                 for: UIControl.Event.editingChanged)
        self.showPasswordButton.addTarget(self, action: #selector(showPass), for: .touchUpInside)
    }

    func addSubviews() {
        self.addSubview(self.textField)
        self.addSubview(self.showPasswordButton)
    }

    func setConstraints() {
        self.textField.translatesAutoresizingMaskIntoConstraints = false
        self.showPasswordButton.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            self.textField.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 32),
            self.textField.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -32),
            self.textField.heightAnchor.constraint(equalToConstant: 24),
            self.textField.topAnchor.constraint(equalTo: self.topAnchor, constant: 10),
            self.textField.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: 10),

            self.showPasswordButton.rightAnchor.constraint(equalTo: self.rightAnchor, constant: 16),
            self.showPasswordButton.centerYAnchor.constraint(equalTo: self.centerYAnchor, constant: 0),
            self.showPasswordButton.widthAnchor.constraint(equalToConstant: 22),
            self.showPasswordButton.heightAnchor.constraint(equalToConstant: 15)
            ])
    }

    public func setTintColor(tintColor: UIColor) {
        self.textField.tintColor = tintColor
    }

    public func setPlaceholder(str: String) {
        self.textField.placeholder = str
    }

    public func setCapitalization(type: UITextAutocapitalizationType) {
        self.textField.autocapitalizationType = type
    }

    public func setSecurityEntry(isSecure: Bool) {
        self.textField.isSecureTextEntry = isSecure
        self.showPasswordButton.isHidden = false
    }
    
    public func setMax(length: Int) {
        self.maxLenght = length
    }
    
    public func setKeyboard(type: UIKeyboardType) {
        self.textField.keyboardType = type
    }

    @objc func showPass() {
        self.textField.isSecureTextEntry = !self.textField.isSecureTextEntry
    }

    @objc func textFieldDidChange(_ sender: UITextField) {
        self.delegate?.customField(self, valueDidChange: sender.text ?? "")
    }
}

extension UILoginCell: UITextFieldDelegate {
    public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else { return true }
        let newLength = text.count + string.count - range.length
        return newLength <= self.maxLenght
    }
}
