//
//  UIRegisterRideCell.swift
//  UICustomElements
//
//  Created by Marek Labuzik on 17/06/2019.
//  Copyright © 2019 Marek Labuzik. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

public enum RideStatus {
    case none
    case reserved
    case free
    case full
}

public enum RequestResult {
    case none
    case pending
    case success
    case failure
}

enum StatusViewType {
    case none
    case success
    case failure
}

public class UIRegisterRideCell: UITableViewCell {
    @IBOutlet weak var bubbleView: UIView!
    @IBOutlet var images: [UIImageView]!
    @IBOutlet weak var separatorView: UIView!
    
    @IBOutlet public weak var startTimeLabel: UILabel!
    @IBOutlet public weak var endTimeLabel: UILabel!
    @IBOutlet public weak var startAddressLabel: UILabel!
    @IBOutlet public weak var endAddressLabel: UILabel!
    @IBOutlet public weak var noteLabel: UILabel!
    @IBOutlet weak var statusRideCell: UILabel!
    
    var statusView: UIView?
    
    public var rideId: Int = -1
    private var acitvityView: UIView?
    private var activityIndicator: NVActivityIndicatorView?
    public var isRideSelected: Bool? {
        willSet {
            guard let isSelected = newValue else { return }
            if isSelected {
                self.setSelected()
            } else {
                self.setDeseleted()
            }
        }
    }
    
    public var result: RequestResult? {
        willSet {
            guard let newResult = newValue else { return }
            switch newResult {
            case .failure:
                self.stopAnimating()
                self.prepareStatusView(type: .failure)
            case .pending:
                self.startAnimating()
            case .success:
                self.stopAnimating()
                self.prepareStatusView(type: .success)
            case .none:
                break
            }
            
        }
    }
    
    public override var tintColor: UIColor! {
        didSet {
            self.setDeseleted()
        }
    }
    
    public var disableColor: UIColor = .black {
        didSet {
            self.setDisabled()
        }
    }
    
    public override func awakeFromNib() {
        super.awakeFromNib()
        self.setCornerRadius()
        self.statusRideCell.isHidden = true
    }
    
    public func setRide(status: RideStatus) {
        switch status {
        case .none:
            self.statusRideCell.isHidden = true
        case .free:
            self.statusRideCell.isHidden = false
            self.statusRideCell.text = "Ride_free".localized()
        case .full:
            self.statusRideCell.isHidden = false
            self.statusRideCell.text = "Ride_full".localized()
        case .reserved:
            self.statusRideCell.isHidden = false
            self.statusRideCell.text = "Ride_reserved".localized()
        }
    }
    
    func setCornerRadius() {
        self.bubbleView.clipsToBounds = true
        self.bubbleView.layer.cornerRadius = 6
        self.bubbleView.layer.borderWidth = 1
    }
    
    func setDisabled() {
        self.statusRideCell.textColor = self.disableColor
        self.startTimeLabel.textColor = self.disableColor
        self.startAddressLabel.textColor = self.disableColor
        self.endTimeLabel.textColor = self.disableColor
        self.endAddressLabel.textColor = self.disableColor
        self.noteLabel.textColor = self.disableColor
        self.separatorView.backgroundColor = self.disableColor
        for image in self.images {
            image.tintColor = self.disableColor
        }
        
        if #available(iOS 13.0, *) {
            self.bubbleView.backgroundColor = .systemBackground
        } else {
            self.bubbleView.backgroundColor = .white
        }
    }

    func setSelected() {
        self.statusRideCell.textColor = .white
        self.startTimeLabel.textColor = .white
        self.startAddressLabel.textColor = .white
        self.endTimeLabel.textColor = .white
        self.endAddressLabel.textColor = .white
        self.noteLabel.textColor = .white
        self.separatorView.backgroundColor = .white
        for image in self.images {
            image.tintColor = .white
        }
        self.bubbleView.backgroundColor = self.tintColor
    }

    func setDeseleted() {
        self.bubbleView.layer.borderColor = self.tintColor.cgColor
        self.statusRideCell.textColor = self.tintColor
        self.separatorView.backgroundColor = self.tintColor
        self.startTimeLabel.textColor = self.tintColor
        self.startAddressLabel.textColor = self.tintColor
        self.endTimeLabel.textColor = self.tintColor
        self.endAddressLabel.textColor = self.tintColor
        self.noteLabel.textColor = self.tintColor
        for image in self.images {
            image.tintColor = self.tintColor
        }
        if #available(iOS 13.0, *) {
            self.bubbleView.backgroundColor = .systemBackground
        } else {
            self.bubbleView.backgroundColor = .white
        }
    }

    public func setAnimation(type: NVActivityIndicatorType, color: UIColor) {
        self.activityIndicator = nil
        if self.acitvityView != nil {
            self.acitvityView?.removeFromSuperview()
            self.acitvityView = nil
        }
        self.acitvityView = UIView(frame: self.frame)
        let activitySize = CGSize(width: 40, height: 40)
        let activityPoint = CGPoint(x: (self.frame.width - activitySize.width) / 2,
                                    y: (self.frame.height - activitySize.height) / 2 )
        
        self.activityIndicator = NVActivityIndicatorView(frame: CGRect(origin: activityPoint, size: activitySize),
                                                         type: type,
                                                         color: color,
                                                         padding: nil)
        self.acitvityView!.backgroundColor = UIColor.black.withAlphaComponent(0.2)
        self.acitvityView!.addSubview(self.activityIndicator!)
    }
    
    public func startAnimating() {
        self.acitvityView?.frame = self.frame
        self.activityIndicator?.startAnimating()
        guard let activityView = self.acitvityView else { return }
        self.addSubview(activityView)
        self.bringSubviewToFront(activityView)
    }

    public func stopAnimating() {
        self.activityIndicator?.stopAnimating()
        self.acitvityView?.removeFromSuperview()
        self.activityIndicator = nil
        self.acitvityView = nil
    }
    
    func prepareStatusView(type: StatusViewType) {
        if self.statusView != nil {
            self.statusView?.removeFromSuperview()
        }
        self.statusView = UIView(frame: self.bubbleView.frame)
        self.statusView?.layer.cornerRadius = self.bubbleView.layer.cornerRadius
        
        self.statusView?.backgroundColor = UIColor.black.withAlphaComponent(0.2)
        
        let imageSize: CGSize = CGSize(width: 40, height: 40)
        let imagePoint: CGPoint = CGPoint(x: (self.bubbleView.frame.size.width - imageSize.width)/2,
                                         y: (self.bubbleView.frame.size.height - imageSize.height)/2)
        let imageView = UIImageView(frame: CGRect(origin: imagePoint, size: imageSize))
        
        switch type {
        case .failure:
            imageView.image = UIImage(named: "Failure_checkMark",
                                      in: Bundle(identifier: "eu.teamride.app.UICustomElements"),
                                      compatibleWith: nil)
        case .success:
            imageView.image = UIImage(named: "Success_checkMark",
                                      in: Bundle(identifier: "eu.teamride.app.UICustomElements"),
                                      compatibleWith: nil)
        case .none: break
        }
        imageView.image?.withRenderingMode(.alwaysTemplate)
        imageView.tintColor = .white
        imageView.backgroundColor = .black
        imageView.layer.cornerRadius = self.bubbleView.layer.cornerRadius
        self.statusView?.addSubview(imageView)
        self.addSubview(self.statusView!)
    }
}
