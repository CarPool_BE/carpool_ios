//
//  WorkPoolAppearance.swift
//  WorkPool
//
//  Created by Marek Labuzik on 30/05/2019.
//  Copyright © 2019 Marek Labuzik. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

final class MeAppearance {
    class func setupApp() {
        UINavigationBar.appearance().tintColor = .mainGreen
        UITabBar.appearance().tintColor = .mainGreen
    }
    static var activityIndicatorType: NVActivityIndicatorType {
        return .ballPulse
    }
}

extension UINavigationController {
    open override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationBar.isTranslucent = false
        self.navigationBar.shadowImage = UIImage()
    }
}

extension UIColor {
    @nonobjc class var placeholderColor: UIColor {
        return UIColor(white: 0.0, alpha: 0.4)
    }
    @nonobjc class var black20: UIColor {
        return UIColor(white: 0.0, alpha: 0.2)
    }
    @nonobjc class var black80: UIColor {
        return UIColor(white: 0.0, alpha: 0.8)
    }
    @nonobjc class var mainGreen: UIColor {
        return UIColor(red: 106.0 / 255.0, green: 177.0 / 255.0, blue: 135.0 / 255.0, alpha: 1.0)
    }
    @nonobjc class var steel: UIColor {
        return UIColor(red: 142.0 / 255.0, green: 142.0 / 255.0, blue: 147.0 / 255.0, alpha: 1.0)
    }
}
