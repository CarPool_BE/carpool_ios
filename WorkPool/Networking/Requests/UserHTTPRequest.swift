//
//  UserHTTPRequest.swift
//  WorkPool
//
//  Created by Boris Bielik on 30/03/2019.
//  Copyright © 2019 Marek Labuzik. All rights reserved.
//

import Foundation
import Moya

extension Provider {
    static let user: MoyaProvider<UserHTTPRequest> = NetworkingClient.provider()
}

enum UserHTTPRequest: TargetType {
    case getUsers
    case getUser(withId: String)
    case getUserRides(withId: String)
    case createUser(Data)
    case updateUser(Data)
    case deleteUser(withId: String)

    var baseURL: URL {
        return URLSettings.baseURL
    }

    var method: Moya.Method {
        switch self {
        case .getUsers, .getUser, .getUserRides:
            return .get
        case .updateUser:
            return .patch
        case .deleteUser:
            return .delete
        case .createUser:
            return .post
        }
    }

    var headers: [String: String]? {
        let token = AppData.meData.auth.accessToken?.token ?? ""
        return ["Authorization": "Bearer " + token]
    }

    var path: String {
        switch self {
        case .getUser(let identifier),
             .deleteUser(let identifier):
            return userPath + "/" + identifier
        case .getUserRides(let identifier):
            return userPath + "/" + identifier + userRidesPath
        case .getUsers, .updateUser, .createUser:
            return userPath
        }
    }

    var sampleData: Data {
        return Data()
    }

    var parameterEncoding: Moya.ParameterEncoding {
        return JSONEncoding.default
    }

    var task: Task {
        switch self {
        case .updateUser(let userData), .createUser(let userData):
            return .requestJSONEncodable(userData)
        case .getUsers, .getUser, .getUserRides:
            return .requestPlain
        case .deleteUser:
            return .requestPlain
        }
    }

}

extension UserHTTPRequest {
    var userPath: String {
        return "/users"
    }
    var userRidesPath: String {
        return "/rides"
    }
}
