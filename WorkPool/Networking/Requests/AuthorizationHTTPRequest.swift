//
//  Autorization.swift
//  WorkPool
//
//  Created by Marek Labuzik on 07/06/2019.
//  Copyright © 2019 Marek Labuzik. All rights reserved.
//

import Foundation
import Moya

extension Provider {
    static let authorization: MoyaProvider<AuthorizationHTTPRequest> = NetworkingClient.provider(requiresAuth: false)
}

enum AuthorizationHTTPRequest: TargetType {
    case getAccessToken(with: RefreshToken)
    
    var baseURL: URL {
        return URLSettings.baseURL
    }
    
    var path: String {
        return "/token"
    }
    
    var method: Moya.Method {
        return .get
    }
    
    var sampleData: Data {
        return Data()
    }
    
    var task: Task {
        switch self {
        case .getAccessToken(let refreshToken):
            return .requestParameters(parameters: ["refresh_token": refreshToken.token], encoding:URLEncoding.default)
        }
    }

    var headers: [String: String]? {
        return [:]
    }
}
