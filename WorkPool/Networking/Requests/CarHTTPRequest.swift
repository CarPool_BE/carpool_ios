//
//  CarHTTPRequest.swift
//  WorkPool
//
//  Created by Marek Labuzik on 4/4/19.
//  Copyright © 2019 Marek Labuzik. All rights reserved.
//

import Foundation
import Moya

extension Provider {
    static let car: MoyaProvider<CarHTTPRequest> = NetworkingClient.provider()
}

enum CarHTTPRequest: TargetType {
    
    case getCars
    case getCar(withId: String)
    case createCar(Car)
    case updateCar(Car)
    case deleteCar(withId: String)
    
    var baseURL: URL {
        return URLSettings.baseURL
    }
    
    var method: Moya.Method {
        switch self {
        case .getCars, .getCar:
            return .get
        case .updateCar:
            return .patch
        case .deleteCar:
            return .delete
        case .createCar:
            return .post
        }
    }
    
    var headers: [String: String]? {
        let token = AppData.meData.auth.accessToken?.token ?? ""
        return ["Authorization": "Bearer " + token]
    }
    
    var path: String {
        switch self {
        case .getCar(let identifier), .deleteCar(let identifier):
            return carPath + "/" + identifier
        case .getCars, .createCar:
            return carPath
        case .updateCar(let car):
            return carPath + "/" + String(car.identifier ?? 0)
        }
    }
    
    var sampleData: Data {
        return Data()
    }
    
    var parameterEncoding: Moya.ParameterEncoding {
        return JSONEncoding.default
    }
    
    var task: Task {
        switch self {
        case .updateCar(let car):
            let encoder = JSONEncoder()
            encoder.dateEncodingStrategy = .formatted(DateFormatter.iso8601Full)
            return .requestCustomJSONEncodable(car, encoder: encoder)
        case .createCar(let car):
            let encoder = JSONEncoder()
            encoder.dateEncodingStrategy = .formatted(DateFormatter.iso8601Full)
            return .requestCustomJSONEncodable(car, encoder: encoder)
        case .getCars, .getCar:
            return .requestPlain
        case .deleteCar:
            return .requestPlain
        }
    }
    
}

extension CarHTTPRequest {
    var carPath: String {
        return "/cars"
    }
}
