//
//  UserProfileHTTPRequest.swift
//  WorkPool
//
//  Created by Marek Labuzik on 4/6/19.
//  Copyright © 2019 Marek Labuzik. All rights reserved.
//

import Foundation
import Moya

extension Provider {
    static let profile: MoyaProvider<UserProfileHTTPRequest> = NetworkingClient.provider()
}

enum UserProfileHTTPRequest: TargetType, AccessTokenAuthorizable {
    
    case login(email: String, pass: String, device: DeviceInfo)
    case register(Register)
    case logout(pushID: String)
    case getMeProfile
    case updateMeProfile(User)
    case getMeCars
    case createCar(Car)
    case updateCar(Data)
    case deleteCar(withID: String)
    case getMeRides(of: Date)
    case createMeRide(Data)
    case updateMeRide(Data)
    case deleteMeRide(withID: String)

    var baseURL: URL {
        return URLSettings.baseURL
    }
    
    var authorizationType: AuthorizationType {
        switch self {
        case .login:
            return .basic
        case .deleteMeRide,
             .logout,
             .getMeProfile,
             .updateMeProfile,
             .getMeCars,
             .createCar,
             .updateCar,
             .deleteCar,
             .getMeRides,
             .createMeRide,
             .updateMeRide:
            return .bearer
        case .register:
            return .none
        }
    }

    var method: Moya.Method {
        switch self {
        case .getMeProfile, .getMeCars, .getMeRides:
            return .get
        case .updateMeProfile, .updateCar, .updateMeRide:
            return .patch
        case .deleteCar, .deleteMeRide, .logout:
            return .delete
        case .login, .createMeRide, .createCar, .register:
            return .post
        }
    }
    
    var headers: [String: String]? {
        switch self {
        case .login(let mail, let pass, _):
            let str = mail + ":" + pass
            return ["Authorization": "Basic " + str.toBase64()]
        case .deleteMeRide,
             .register,
             .logout,
             .getMeProfile,
             .updateMeProfile,
             .getMeCars,
             .createCar,
             .updateCar,
             .deleteCar,
             .getMeRides,
             .createMeRide,
             .updateMeRide:
            return nil
        }
        
    }
    
    var path: String {
        switch self {
        case .login:
            return loginPath
        case .logout:
            return loginPath
        case .register:
            return registerPath
        case .getMeProfile, .updateMeProfile:
            return meProfilePath
        case .getMeCars, .createCar, .updateCar:
            return meCarsPath
        case .deleteCar(let carID):
            return meCarsPath + "/" + carID
        case .getMeRides, .createMeRide, .updateMeRide:
            return meRidesPath
        case .deleteMeRide(let rideID):
            return meRidesPath + "/" + rideID
        }
    }

    var sampleData: Data {
        return Data()
    }
    
    var parameterEncoding: Moya.ParameterEncoding {
        return JSONEncoding.default
    }

    var task: Task {
        switch self {
        case .login(_, _, let deviceData):
            return .requestJSONEncodable(deviceData)
        case .register(let registerData):
            return .requestJSONEncodable(registerData)
        case .createMeRide(let rideData),
             .updateMeRide(let rideData):
            return .requestJSONEncodable(rideData)
        case .updateCar(let carData):
            return .requestJSONEncodable(carData)
        case .createCar(let car):
            let encoder = JSONEncoder()
            encoder.dateEncodingStrategy = .formatted(DateFormatter.iso8601Full)
            return .requestCustomJSONEncodable(car, encoder: encoder)
        case .deleteCar,
             .deleteMeRide,
             .getMeCars,
             .getMeProfile:
            return .requestPlain
        case .logout(let pushID):
            let dict: [String: Any] = ["push_id": pushID]
            return .requestParameters(parameters: dict, encoding: URLEncoding.default)
        case .getMeRides(let date):
            var dict: [String: Any] = [:]
            dict["date"] = date.getIso8601String()
            return .requestParameters(parameters: dict, encoding: URLEncoding.default)
        case .updateMeProfile(let profileData):
            let encoder = JSONEncoder()
            encoder.dateEncodingStrategy = .formatted(DateFormatter.iso8601Full)
            return .requestCustomJSONEncodable(profileData, encoder: encoder)
        }
    }

}

extension UserProfileHTTPRequest {
    var loginPath: String {
        return "/token"
    }
    var registerPath: String {
        return "/users"
    }
    var meProfilePath: String {
        return "/me"
    }
    var meCarsPath: String {
        return "/me/cars"
    }
    var meRidesPath: String {
        return "/me/rides"
    }
}
