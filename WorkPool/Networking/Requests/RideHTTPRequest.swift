//
//  RideHTTPRequest.swift
//  WorkPool
//
//  Created by Marek Labuzik on 4/4/19.
//  Copyright © 2019 Marek Labuzik. All rights reserved.
//

import Foundation
import Moya
import CoreLocation

extension Provider {
    static let ride: MoyaProvider<RideHTTPRequest> = NetworkingClient.provider()
}

enum RideHTTPRequest: TargetType, AccessTokenAuthorizable {
    
    case getRides(from: String, to: String, fromGps: CLLocationCoordinate2D, toGps: CLLocationCoordinate2D, date: Date)
    case getRide(withId: Int)
    case createRide(NewRide)
    case updateRide(NewRide)
    case deleteRide(withId: Int)
    case registerToRide(rideId: Int, userId: Int)
    case removeFromRide(rideId: Int, userId: Int)
    
    var baseURL: URL {
        return URLSettings.baseURL
    }
    
    var authorizationType: AuthorizationType {
        return .bearer
    }
    
    var method: Moya.Method {
        switch self {
        case .registerToRide:
            return .put
        case .getRides, .getRide:
            return .get
        case .updateRide:
            return .patch
        case .deleteRide,
             .removeFromRide:
            return .delete
        case .createRide:
            return .post
        }
    }
    
    var headers: [String: String]? {
//        let token = AppData.meData.auth.accessToken?.token ?? ""
//        return ["Authorization":"Bearer " + token]
        return nil
    }
    
    var path: String {
        switch self {
        case .registerToRide(let rideID, let userID),
             .removeFromRide(let rideID, let userID):
            return ridePath + "/" + String(rideID) + addPassengetPath + "/" + String(userID)
        case .getRide(let identifier), .deleteRide(let identifier):
            return ridePath + "/" + String(identifier)
        case .getRides, .updateRide, .createRide:
            return ridePath
        }
    }
    
    var sampleData: Data {
        return Data()
    }
    
    var parameterEncoding: Moya.ParameterEncoding {
        return JSONEncoding.default
    }
    
    var task: Task {
        switch self {
        case .updateRide(let userData):
            return .requestJSONEncodable(userData)
        case .createRide(let userData):
            let encoder = JSONEncoder()
            encoder.dateEncodingStrategy = .formatted(DateFormatter.iso8601Full)
            return .requestCustomJSONEncodable(userData, encoder: encoder)
        case .getRides(from: let from, to: let to, fromGps: let fromGPS, toGps: let toGPS, date: let date):
            var dict: [String: Any] = [:]
            dict["from"] = from
            dict["to"] = to
            dict["gps_from"] = String(fromGPS.longitude) + ", " + String(fromGPS.latitude)
            dict["gps_to"] = String(toGPS.longitude) + ", " + String(toGPS.latitude)
            dict["date"] = date.getIso8601String()
            return .requestParameters(parameters: dict, encoding: URLEncoding.default)
        case .getRide,
             .registerToRide:
            return .requestPlain
        case .deleteRide,
             .removeFromRide:
            return .requestPlain
        }
    }
    
}

extension RideHTTPRequest {
    var ridePath: String {
        return "/rides"
    }
    var addPassengetPath: String {
        return "/passenger"
    }
    var rideWithRadiusPath: String {
        return "/rides/inMeRadius"
    }
}
