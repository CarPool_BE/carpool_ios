//
//  Service.swift
//  WorkPool
//
//  Created by Marek Labuzik on 12/06/2019.
//  Copyright © 2019 Marek Labuzik. All rights reserved.
//

import Foundation
import Moya
import RxSwift

protocol Service: class {

    var disposeBag: DisposeBag { get }
    var errorBehaviour: BehaviorSubject<RequestErrors> { get }

    func processTokenExpireError(_ error: Error)
    func start()
    func stop()
}

extension Service {
    func processTokenExpireError(_ error: Error) {
        let moyaError = DecodeErrors.httpMoyaError(error: error)
        switch moyaError {
        case .accessTokenExpire:
            errorBehaviour.onNext(moyaError)
        default:
            break
        }
    }

    func request<T: Model, Target: TargetType>(request: Target,
                                               apiClient: MoyaProvider<Target>,
                                               model: T.Type,
                                               completion: @escaping (Result<T, RequestErrors>) -> Void) {
        apiClient.rx
            .request(request)
            .filterSuccessfulStatusCodes()
            .retry(1)
            .map { response -> T in
                do {
                    let decoder = JSONDecoder()
                    decoder.dateDecodingStrategy = .formatted(DateFormatter.iso8601Full)
                    return try decoder.decode(T.self, from: response.data)
                } catch let error {
                    print(error)
                    completion(.failure(.decodeError))
                }
                return model.empty()
            }
            .subscribe(onSuccess: { (models) in
                completion(.success(models))
            }, onError: { [weak self] (error) in
                 guard let self = self else { return }
                let err = DecodeErrors.httpMoyaError(error: error)
                completion(.failure(err))
                self.processTokenExpireError(error)
            }).disposed(by: disposeBag)
    }

    func requestWithoutMaping<Target: TargetType>(request: Target,
                                                  apiClient: MoyaProvider<Target>,
                                                  completion: @escaping (Result<Bool, RequestErrors>) -> Void) {
        apiClient.rx
            .request(request)
            .filterSuccessfulStatusCodes()
            .retry(1)
            .subscribe(onSuccess: { (response) in
                switch response.statusCode {
                case 201:
                    completion(.success(true))
                case 200:
                    completion(.success(true))
                default:
                    completion(.failure(.httpError))
                }
            }) { [weak self] (error) in
                guard let self = self else { return }
                let err = DecodeErrors.httpMoyaError(error: error)
                completion(.failure(err))
                self.processTokenExpireError(error)
            }.disposed(by: self.disposeBag)
    }
    
}
