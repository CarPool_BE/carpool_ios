//
//  MeService.swift
//  WorkPool
//
//  Created by Marek Labuzik on 05/05/2019.
//  Copyright © 2019 Marek Labuzik. All rights reserved.
//

import Foundation
import Moya
import RxSwift

final class MeProfileService: Service {
    var errorBehaviour: BehaviorSubject<RequestErrors> = BehaviorSubject(value: .httpError)
    
    func start() {

    }
    
    func stop() {

    }
    
    let disposeBag = DisposeBag()
    let apiClient = Provider.profile
    
    func getMeProfile(completion: @escaping (Result<User, RequestErrors>) -> Void) {
        request(request: UserProfileHTTPRequest.getMeProfile,
                apiClient: apiClient,
                model: User.self) { result in
                    completion(result)
        }
    }
    
    func getMeRides(of day: Date, completion: @escaping (Result<Rides, RequestErrors>) -> Void) {
        request(request: UserProfileHTTPRequest.getMeRides(of: day),
                apiClient: self.apiClient,
                model: Rides.self) { result in
                    completion(result)
        }
    }
    
    func getMeCars(completion: @escaping (Result<Cars, RequestErrors>) -> Void) {
        request(request: UserProfileHTTPRequest.getMeCars,
                apiClient: self.apiClient,
                model: Cars.self) { result in
                    completion(result)
        }
    }
    
    func logout(completion: @escaping (Result<Bool, RequestErrors>) -> Void) {
        guard let pushToken = PushNotifications.mePush.deviceID else {
            completion(.failure(.emptyParameters))
            return
        }
        self.requestWithoutMaping(request: UserProfileHTTPRequest.logout(pushID: pushToken),
                                  apiClient: self.apiClient) { result in
                                    completion(result)
        }
    }

    func login(data: Login, device: DeviceInfo,
               completion: @escaping (Result< Dictionary<String, Any>, RequestErrors>) -> Void) {
        let loginRequest = UserProfileHTTPRequest.login(email: data.email, pass: data.password, device: device)

        self.apiClient.rx
            .request(loginRequest)
            .filterSuccessfulStatusCodes()
            .map { response -> Dictionary<String, Any> in
                do {
                    let data = try JSONSerialization.jsonObject(with: response.data, options: [])
                    guard let dictData = data as? Dictionary<String, Any> else { return ["": ""] }
                    return dictData
                } catch {
                    print(error.localizedDescription)
                    return ["": ""]
                }
            }
            .subscribe(onSuccess: { (response) in
                completion(.success(response))
            }) { (err) in
                let error = DecodeErrors.httpMoyaError(error: err)
                completion(.failure(error))
            }.disposed(by: self.disposeBag)
    }

    func register(data: Register, completion: @escaping (Result<Bool, RequestErrors>) -> Void) {
        requestWithoutMaping(request: UserProfileHTTPRequest.register(data),
                             apiClient: self.apiClient) { result in
                                completion(result)
        }
    }

    func updateMy(profile: User, completion: @escaping (Result<Bool, RequestErrors>) -> Void) {
        requestWithoutMaping(request: UserProfileHTTPRequest.updateMeProfile(profile),
                             apiClient: self.apiClient) { result in
                                completion(result)
        }
    }
}
