//
//  CarService.swift
//  WorkPool
//
//  Created by Marek Labuzik on 15/06/2019.
//  Copyright © 2019 Marek Labuzik. All rights reserved.
//

import Foundation
import Moya
import RxSwift

final class CarService: Service {
    func start() {

    }

    func stop() {

    }

    func provider() -> MoyaProvider<CarHTTPRequest> {
        return Provider.car
    }

    var errorBehaviour: BehaviorSubject<RequestErrors> = BehaviorSubject(value: .httpError)
    let disposeBag = DisposeBag()

    func add(car: Car, completion: @escaping (Result<Bool, RequestErrors>) -> Void) {
        requestWithoutMaping(request: CarHTTPRequest.createCar(car),
                             apiClient: self.provider()) { result in
                                completion(result)
        }
    }

    func update(car: Car, completion: @escaping (Result<Bool, RequestErrors>) -> Void) {
        requestWithoutMaping(request: CarHTTPRequest.updateCar(car),
                             apiClient: self.provider()) { result in
                                completion(result)
        }
    }
}
