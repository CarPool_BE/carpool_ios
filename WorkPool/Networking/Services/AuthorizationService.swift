//
//  AuthorizationService.swift
//  WorkPool
//
//  Created by Marek Labuzik on 07/06/2019.
//  Copyright © 2019 Marek Labuzik. All rights reserved.
//

import Foundation
import Result
import Moya
import RxSwift

private extension MoyaProvider {

    func start() {
        self.manager.startRequestsImmediately = true
    }

    func stop() {
        self.manager.startRequestsImmediately = false
    }
}

final class AuthorizationService {

    func startServices() {
        Provider.user.start()
        Provider.car.start()
        Provider.profile.start()
        Provider.ride.start()
    }

    func stopServices() {
        Provider.user.stop()
        Provider.car.stop()
        Provider.profile.stop()
        Provider.ride.stop()
    }

    let disposeBag = DisposeBag()
    let apiClient = Provider.authorization

    func observe(service: Service) {
        service.errorBehaviour
            .asObservable()
            .filter {
                    $0 == .accessTokenExpire ||
                    $0 == .userNameAndPasswordNotMatch
            }
            .subscribe(onNext: { [weak self] (_) in
                guard let refreshToken = AppData.meData.auth.refreshToken else { return }
                self?.stopServices()
                self?.renewAccessToken(refresh: refreshToken ,
                                       completion: { result in
                                        switch result {
                                        case .success(let accessToken):
                                            AppData.meData.auth.accessToken = accessToken
                                            self?.startServices()
                                        case .failure:
                                            break
                                        }
                })
            }, onError: { (error) in
                print(error)
            }).disposed(by: self.disposeBag)
    }
    
    func renewAccessToken(refresh: RefreshToken,
                          completion: @escaping (Result<AccessToken, RequestErrors>) -> Void) {

        let tokenRequest = AuthorizationHTTPRequest.getAccessToken(with: refresh)

        self.apiClient.rx
            .request(tokenRequest)
            .filterSuccessfulStatusCodes()
            .map { response -> AccessToken? in
                do {
                    let decoder = JSONDecoder()
                    decoder.dateDecodingStrategy = .formatted(DateFormatter.iso8601Full)
                    return try decoder.decode(AccessToken.self, from: response.data)
                } catch {
                    completion(.failure(.httpError))
                    print("error: \(error)")
                    return nil
                }
            }
            .subscribe(onSuccess: { (newAccesToken) in
                if let token = newAccesToken {
                    completion(.success(token))
                } else {
                    completion(.failure(.httpError))
                }
            }) { (err) in
                let error = DecodeErrors.httpMoyaError(error: err)
                completion(.failure(error))
            }.disposed(by: self.disposeBag)
    }
}

// MARK: - AccessTokenAuthorizable
/// A protocol for controlling the behavior of `AccessTokenPlugin`.
public protocol AccessTokenAuthorizable {

    /// Represents the authorization header to use for requests.
    var authorizationType: AuthorizationType { get }
}

// MARK: - AuthorizationType
/// An enum representing the header to use with an `AccessTokenPlugin`
public enum AuthorizationType {
    /// No header.
    case none

    /// The `"Basic"` header.
    case basic

    /// The `"Bearer"` header.
    case bearer

    /// Custom header implementation.
    case custom(String)

    public var value: String? {
        switch self {
        case .none: return nil
        case .basic: return "Basic"
        case .bearer: return "Bearer"
        case .custom(let customValue): return customValue
        }
    }
}

// MARK: - AccessTokenPlugin
/**
 A plugin for adding basic or bearer-type authorization headers to requests. Example:
 ```
 Authorization: Basic <token>
 Authorization: Bearer <token>
 Authorization: <Сustom> <token>
 ```
 */
public struct AccessTokenPlugin: PluginType {

    /// A closure returning the access token to be applied in the header.
    public let tokenClosure: () -> String?

    /**
     Initialize a new `AccessTokenPlugin`.
     - parameters:
     - tokenClosure: A closure returning the token to be applied in the pattern `Authorization: <AuthorizationType> <token>`
     */
    public init(tokenClosure: @escaping () -> String?) {
        self.tokenClosure = tokenClosure
    }

    /**
     Prepare a request by adding an authorization header if necessary.
     - parameters:
     - request: The request to modify.
     - target: The target of the request.
     - returns: The modified `URLRequest`.
     */
    public func prepare(_ request: URLRequest, target: TargetType) -> URLRequest {

        guard let authorizable = target as? AccessTokenAuthorizable else { return request }

        let authorizationType = authorizable.authorizationType
        var request = request

        switch authorizationType {
        case .basic, .bearer, .custom:
            if let value = authorizationType.value {
                guard let token = tokenClosure() else { break }
                let authValue = value + " " + token
                request.addValue(authValue, forHTTPHeaderField: "Authorization")
            }
        case .none:
            break
        }

        return request
    }
}
