//
//  SearchRideService.swift
//  WorkPool
//
//  Created by Marek Labuzik on 05/05/2019.
//  Copyright © 2019 Marek Labuzik. All rights reserved.
//

import Foundation
import Moya
import RxSwift
import CoreLocation

final class RideService: Service {
    func start() {

    }
    
    func stop() {
        
    }
    
    var errorBehaviour: BehaviorSubject<RequestErrors> = BehaviorSubject(value: .httpError)

    let disposeBag = DisposeBag()
    let apiClient = Provider.ride
    
    func getRides(from: String,
                  to: String,
                  fromGps: CLLocationCoordinate2D,
                  toGps: CLLocationCoordinate2D,
                  date: Date,
                  completion: @escaping (Result<Rides, RequestErrors>) -> Void) {
        if !from.isEmpty {
            request(request: RideHTTPRequest.getRides(from: from, to: to, fromGps: fromGps, toGps: toGps, date: date),
                    apiClient: apiClient,
                    model: Rides.self) { (result) in
                        completion(result)
            }
        } else {
            completion(.failure(.emptyParameters))
        }

    }
    
    func deregisterFromRide(with rideID: Int, userId: Int, completion: @escaping (Result<Bool, RequestErrors>) -> Void ) {
        self.requestWithoutMaping(request: RideHTTPRequest.removeFromRide(rideId: rideID, userId: userId),
                                  apiClient: self.apiClient) { result in
                                    completion(result)
        }
    }
    
    func removeMyRide(with rideID: Int, completion: @escaping (Result<Bool, RequestErrors>) -> Void ) {
        self.requestWithoutMaping(request: RideHTTPRequest.deleteRide(withId: rideID),
                                  apiClient: self.apiClient) { result in
                                    completion(result)
        }
    }
    
    func registerToRide(with rideID: Int, completion: @escaping (Result<Bool, RequestErrors>) -> Void) {
        guard let userId = AppData.meData.profile?.identifier else {
            completion(.failure(.emptyParameters))
            return
        }
        self.requestWithoutMaping(request: RideHTTPRequest.registerToRide(rideId: rideID, userId: userId),
                                  apiClient: self.apiClient) { result in
                                    completion(result)
        }
    }
    
    func add(newRide: NewRide, completion: @escaping (Result<Bool, RequestErrors>) -> Void) {
        let createRideRequest = RideHTTPRequest.createRide(newRide)

        self.apiClient.rx
            .request(createRideRequest)
            .filterSuccessfulStatusCodes()
            .subscribe(onSuccess: { (response) in
                switch response.statusCode {
                case 201:
                    completion(.success(true))
                default:
                    completion(.failure(.httpError))
                }
            }) { [weak self] (err) in
                self?.processTokenExpireError(err)
                completion(.failure(.httpError))
            }.disposed(by: self.disposeBag)
    }
    
    func getRide(wtihID: Int, completion: @escaping (Result<Ride, RequestErrors>) -> Void) {
        self.request(request: RideHTTPRequest.getRide(withId: wtihID),
                     apiClient: self.apiClient,
                     model: Ride.self) { result in
                        completion(result)
        }
    }
}
