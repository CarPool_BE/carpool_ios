//
//  UserService.swift
//  WorkPool
//
//  Created by Marek Labuzik on 02/05/2019.
//  Copyright © 2019 Marek Labuzik. All rights reserved.
//

import Foundation
import Moya
import RxSwift

final class UserService: Service {
    func start() {

    }
    
    func stop() {
        
    }
    
    func provider() -> MoyaProvider<UserHTTPRequest> {
        return Provider.user
    }

    var errorBehaviour: BehaviorSubject<RequestErrors> = BehaviorSubject(value: .httpError)
    let disposeBag = DisposeBag()
    
    public func getUsers(from: Int = 50, completion: @escaping (Result<Users, RequestErrors>) -> Void) {
        request(request: UserHTTPRequest.getUsers,
                apiClient: Provider.user,
                model: Users.self) { result in
                    completion(result)
        }
    }
    
    public func getUser(with userID: Int = 50, completion: @escaping (Result<User, RequestErrors>) -> Void) {
        request(request: UserHTTPRequest.getUser(withId: String(userID)),
                apiClient: Provider.user,
                model: User.self) { result in
                    completion(result)
        }
    }
    
    public func getUserRides(with userID: Int = 50, completion: @escaping (Result<Rides, RequestErrors>) -> Void) {
        request(request: UserHTTPRequest.getUserRides(withId: String(userID)),
                apiClient: Provider.user,
                model: Rides.self) { result in
                    completion(result)
        }
    }
}
