//
//  NetworkingClient.swift
//  WorkPool
//
//  Created by Boris Bielik on 30/03/2019.
//  Copyright © 2019 Marek Labuzik. All rights reserved.
//

import Foundation
import Moya

enum RequestErrors: Error {    
    case none
    case httpError
    case decodeError
    case badAccess
    case userNameAndPasswordNotMatch
    case wrongData
    case accessTokenExpire
    case dataIsExists
    case internalServerError
    case emptyParameters
    case notFound
    case notAllowed

    var errorDescription: String {
        switch self {
        case .accessTokenExpire:
            break
        case .badAccess:
            break
        case .dataIsExists:
            break
        case .decodeError:
            break
        case .emptyParameters:
            break
        case .httpError:
            break
        case .internalServerError:
            break
        case .none:
            break
        case .notAllowed:
            break
        case .notFound:
            break
        case .userNameAndPasswordNotMatch:
            break
        case .wrongData:
            break
        }
        return ""
    }
    
    func registerConvert() -> AppError {
        switch self {
        case .decodeError, .emptyParameters, .internalServerError, .notAllowed:
            return .retry
        case .accessTokenExpire, .badAccess, .httpError, .none:
            return .none
        case .userNameAndPasswordNotMatch:
            return .badCredentials
        case .wrongData:
            return .checkDataAndRetry
        case .dataIsExists:
            return .emailOrIdIsUsed
        case .notFound:
            return .idNotExists
        }
    }
    
    func convertToAppError() -> AppError {
        switch self {
        case .accessTokenExpire, .badAccess, .httpError, .none, .notFound:
            return .none
        case .decodeError, .emptyParameters, .internalServerError, .notAllowed:
            return .retry
        case .dataIsExists:
            return .alreadyExists
        case .userNameAndPasswordNotMatch:
            return .badCredentials
        case .wrongData:
            return .checkDataAndRetry
        }
    }
}

final class DecodeErrors {
    class func httpMoyaError(error: Error) -> RequestErrors {
        guard let err = error as? MoyaError  else { return  .httpError}

        guard let code = err.response?.statusCode else { return  .httpError }
        switch code {
        case 400:
            return .wrongData
        case 401:
            return .accessTokenExpire
        case 403:
            return .userNameAndPasswordNotMatch
        case 404:
            return .notFound
        case 405:
            return .notAllowed
        case 409:
            return .dataIsExists
        case 500:
            return .internalServerError
        default:
            return .httpError
        }
    }
}

struct URLSettings {
    #if DEBUG
    static let baseURL: URL = URL(string: "https://test.teamride.eu")!
    #else
    static let baseURL: URL = URL(string: "https://test.teamride.eu")!
    #endif
}

enum Provider {}

public final class NetworkingClient {
    // Client
    static func provider<T: TargetType>(requiresAuth: Bool = true) -> MoyaProvider<T> {
        var plugins: [PluginType] = [NetworkLoggerPlugin()]
        if requiresAuth {
            plugins.append(AccessTokenPlugin(tokenClosure: {AppData.meData.auth.accessToken?.token}))
        }
        return MoyaProvider<T>(plugins: plugins)
    }
}
