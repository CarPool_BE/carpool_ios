//
//  DateTimeExtensions.swift
//  WorkPool
//
//  Created by Marek Labuzik on 06/05/2019.
//  Copyright © 2019 Marek Labuzik. All rights reserved.
//

import Foundation

extension DateFormatter {
    static let iso8601Full: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        formatter.calendar = Calendar(identifier: .iso8601)
        formatter.timeZone = TimeZone(secondsFromGMT: 0)
        formatter.locale = Locale(identifier: "en_US_POSIX")
        return formatter
    }()
}

extension Date {
    func getTimeString() -> String {
        let formatter: DateFormatter = DateFormatter()
        formatter.dateFormat = "HH:mm"
        return formatter.string(from: self)
    }

    func getDateString() -> String {
        let formatter: DateFormatter = DateFormatter()
        formatter.dateFormat = "dd.MM. EEEE"
        return formatter.string(from: self)
    }

    func getIso8601String() -> String {
        return DateFormatter.iso8601Full.string(from: self)
    }
}
