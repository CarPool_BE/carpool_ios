//
//  DeviceExtension.swift
//  WorkPool
//
//  Created by Marek Labuzik on 20/06/2019.
//  Copyright © 2019 Marek Labuzik. All rights reserved.
//

import UIKit

enum UIDeviceType {
    case unknown
    case iPhone5
    case iPhone6_8
    case iPhone6_8_plus
    case iPhoneX_Xs
    case iPhoneXs_Max
    case iPhoneXr
    case iPad
    
    var isLarge: Bool {
        switch self {
        case .unknown,
             .iPhone5,
             .iPhone6_8,
             .iPhone6_8_plus:
            return false
        case .iPhoneX_Xs,
             .iPhoneXs_Max,
             .iPhoneXr:
            return true
        case .iPad:
            return false
        }
    }
}

extension UIDevice {
    var deviceType: UIDeviceType {
        if UIDevice().userInterfaceIdiom == .phone {
            switch UIScreen.main.nativeBounds.height {
            case 1136:
                return .iPhone5
            case 1334:
                return .iPhone6_8
            case 1920, 2208:
                return .iPhone6_8_plus
            case 2436:
                return .iPhoneX_Xs
            case 2688:
                return .iPhoneXs_Max
            case 1792:
                return .iPhoneXr
            default:
                return .unknown
            }
        }
        return .iPad
    }
}
