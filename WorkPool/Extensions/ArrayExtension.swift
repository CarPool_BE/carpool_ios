//
//  ArrayExtension.swift
//  WorkPool
//
//  Created by Marek Labuzik on 14/05/2019.
//  Copyright © 2019 Marek Labuzik. All rights reserved.
//

import Foundation

extension Array {
    func toString() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd.MM."
        var returnString: String = ""
        if let arr: [Date] = self as? [Date] {
            for date in arr {
                let dateString = dateFormatter.string(from: date)
                if returnString == "" {
                    returnString = dateString
                } else {
                    returnString += ", " + dateString
                }
            }
        }
        return returnString
    }
}

extension Collection {
    /// Returns the element at the specified index if it is within bounds, otherwise nil.
    subscript (safe index: Index) -> Element? {
        return indices.contains(index) ? self[index] : nil
    }
}
