//
//  WLabel.swift
//  WorkPool
//
//  Created by Martin Kurbel on 18/06/2019.
//  Copyright © 2019 Marek Labuzik. All rights reserved.
//

import UIKit

final class WLabel: UILabel {
    
    convenience init(style: UIFont.Weight = .regular,
                     size: CGFloat = 15,
                     color: UIColor = Color.black,
                     align: NSTextAlignment = .left,
                     lines: Int = 1,
                     scale: CGFloat = 1
        ) {
        self.init()
        
        self.font = UIFont.systemFont(ofSize: size, weight: style)
        self.textColor = color
        self.textAlignment = align
        self.numberOfLines = lines
        self.lineBreakMode = lines == 1 ? .byTruncatingTail : .byWordWrapping
        self.minimumScaleFactor = scale
        self.adjustsFontSizeToFitWidth = scale != 1
    }
}
