//
//  RouteExtensions.swift
//  WorkPool
//
//  Created by Marek Labuzik on 15/10/2019.
//  Copyright © 2019 Marek Labuzik. All rights reserved.
//

import MapKit

public extension MKMultiPoint {
    var coordinates: [CLLocationCoordinate2D] {
        var coords = [CLLocationCoordinate2D](repeating: kCLLocationCoordinate2DInvalid,
                                              count: pointCount)

        getCoordinates(&coords, range: NSRange(location: 0, length: pointCount))

        return coords
    }
}
