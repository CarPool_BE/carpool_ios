//
//  StringExtensions.swift
//  WorkPool
//
//  Created by Marek Labuzik on 14/05/2019.
//  Copyright © 2019 Marek Labuzik. All rights reserved.
//

import Foundation

extension String {
    func localized() -> String {
        return NSLocalizedString(self, comment: "")
    }
    
    func unLocalized() -> String {
        print(self)
        return self
    }
    
    func fromBase64() -> String? {
        guard let data = Data(base64Encoded: self) else {
            return nil
        }
        return String(data: data, encoding: .utf8)
    }
    
    func toBase64() -> String {
        return Data(self.utf8).base64EncodedString()
    }
}
