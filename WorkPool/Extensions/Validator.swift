//
//  Validator.swift
//  WorkPool
//
//  Created by Martin Kurbel on 25/06/2019.
//  Copyright © 2019 Marek Labuzik. All rights reserved.
//

import Foundation

class Validator {
    
    func validateEmail(_ email: String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailTest = NSPredicate(format: "SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: email)
    }
    
    func validateName(_ name: String) -> Bool {
        return name.count > 1
    }
    
    func validAddress(_ address: String) -> Bool {
        return address.count > 1
    }
    
    func validSPZ(_ spz: String) -> Bool {
        return spz.count > 1 && spz.count <= 8
    }
    
    func validPhoneNumber(_ number: String) -> Bool {
        let PHONE_REGEX = "^([+]?\\d{1,2})?(\\d{3}){2}\\d{4}$"
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
        let result =  phoneTest.evaluate(with: number)
        return result
    }
}
