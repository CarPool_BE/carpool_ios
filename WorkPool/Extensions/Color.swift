//
//  Color.swift
//  WorkPool
//
//  Created by Martin Kurbel on 18/06/2019.
//  Copyright © 2019 Marek Labuzik. All rights reserved.
//

import UIKit

extension UIColor {
    
    static func rgbColor(_ red: Int, _ green: Int, _ blue: Int, _ alpha: CGFloat = 1) -> UIColor {
        return UIColor(
            red: CGFloat(red)/255.0,
            green: CGFloat(green)/255.0,
            blue: CGFloat(blue)/255.0,
            alpha: alpha
        )
    }
}

final class Color {
    
    static let black            = UIColor.rgbColor(0, 0, 0, 0.8)
    static let placeholder      = UIColor.rgbColor(0, 0, 0, 0.4)
    static let green            = UIColor.rgbColor(106, 177, 135)
    static let buttonBlue       = UIColor.rgbColor(0, 122, 255)
}
