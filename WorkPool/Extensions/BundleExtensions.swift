//
//  BundleExtensions.swift
//  WorkPool
//
//  Created by Marek Labuzik on 17/06/2019.
//  Copyright © 2019 Marek Labuzik. All rights reserved.
//

import UIKit

extension Bundle {
    static var customElements: Bundle? {
        return Bundle(identifier: "eu.teamride.app.UICustomElements") ?? nil
    }
}
