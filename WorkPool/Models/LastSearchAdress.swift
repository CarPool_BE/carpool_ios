//
//  LastSearchAdress.swift
//  WorkPool
//
//  Created by Marek Labuzik on 28/07/2019.
//  Copyright © 2019 Marek Labuzik. All rights reserved.
//

import Foundation
import CoreLocation

struct LastSearchAdress {
    var title: String
    var latitude: Double
    var longitude: Double

    func gpsPosition() -> CLLocationCoordinate2D {
        return CLLocationCoordinate2D(latitude: self.latitude, longitude: self.longitude)
    }
    
    init(title: String, latitude: Double, longitude: Double) {
        self.title = title
        self.latitude = latitude
        self.longitude = longitude
    }

    init(title: String, gpsPosition: CLLocationCoordinate2D) {
        self.title = title
        self.latitude = gpsPosition.latitude
        self.longitude = gpsPosition.longitude
    }

    init?(dictionary: [String: String]) {
        guard let title = dictionary["title"],
            let lat = dictionary["latitude"],
            let lon = dictionary["longitude"]
            else { return nil }
        guard let latitude = Double(lat),
            let longitude = Double(lon)
            else { return nil }

        self.init(title: title, latitude: latitude, longitude: longitude)
    }

    var propertyListRepresentation: [String: String] {
        return ["title": title, "latitude": String(latitude), "longitude": String(longitude)]
    }
}
