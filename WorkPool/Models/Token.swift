//
//  Token.swift
//  WorkPool
//
//  Created by Marek Labuzik on 06/06/2019.
//  Copyright © 2019 Marek Labuzik. All rights reserved.
//

import Foundation

struct RefreshToken: DataConvertible, Hashable, Encodable, Model {
    var token: String
    var expire: Date

    init() {
        self.token = ""
        self.expire = Date(timeIntervalSince1970: 0)
    }

    init(with dict: Dictionary <String, Any>) {
        self.token = dict["refresh_token"] as? String ?? ""
        let dateIsoString = dict["refresh_token_expires_in"] as? String ?? ""
        self.expire = DateFormatter.iso8601Full.date(from: dateIsoString) ?? Date(timeIntervalSince1970: 0)
    }

    enum CodingKeys: String, CodingKey {
        case token = "refresh_token"
        case expire = "refresh_token_expires_in"
    }

    static func empty<T>() -> T where T: Model {
        let refreshToken = RefreshToken()
        guard let emptyRefreshToken = refreshToken as?T else {
            fatalError()
        }
        return emptyRefreshToken
    }
}

struct AccessToken: DataConvertible, Hashable, Encodable {
    var token: String
    var expire: Date

    init() {
        self.token = ""
        self.expire = Date(timeIntervalSince1970: 0)
    }

    init(with dict: Dictionary <String, Any>) {
        self.token = dict["acces_token"] as? String ?? ""
        let dateIsoString = dict["acces_token_expires_in"] as? String ?? ""
        self.expire = DateFormatter.iso8601Full.date(from: dateIsoString) ?? Date(timeIntervalSince1970: 0)
    }

    enum CodingKeys: String, CodingKey {
        case token = "acces_token"
        case expire = "acces_token_expires_in"
    }

    static func empty<T>() -> T where T: Model {
        let accessToken = AccessToken()
        guard let epmtyAccessToken = accessToken as?T else {
            fatalError()
        }
        return epmtyAccessToken
    }
}

struct StructToDict {
    func toDict() -> [String: Any] {
        var dict = [String: Any]()
        let otherSelf = Mirror(reflecting: self)
        for child in otherSelf.children {
            if let key = child.label {
                dict[key] = child.value
            }
        }
        return dict
    }
}

struct JSON {
    static let encoder = JSONEncoder()
}
extension Encodable {
    subscript(key: String) -> Any? {
        return dictionary?[key]
    }
    var dictionary: [String: Any]? {
        return (try? JSONSerialization.jsonObject(with: JSON.encoder.encode(self))) as? [String: Any] ?? nil
    }
}
