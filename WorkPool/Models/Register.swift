//
//  Register.swift
//  WorkPool
//
//  Created by Marek Labuzik on 05/06/2019.
//  Copyright © 2019 Marek Labuzik. All rights reserved.
//

import Foundation

struct Register: DataConvertible, Hashable, Model {
    var name: String
    var workID: String
    var email: String
    var password: String

    enum CodingKeys: String, CodingKey {
        case name = "Name"
        case workID = "CDSID"
        case email = "Email"
        case password = "Password"
    }
    static func empty<T>() -> T where T: Model {
        let register = Register(name: "", workID: "", email: "", password: "")
        guard let emptyRegister = register as?T else {
            fatalError()
        }
        return emptyRegister
    }
}
