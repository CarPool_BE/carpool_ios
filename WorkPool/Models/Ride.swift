//
//  Ride.swift
//  WorkPool
//
//  Created by Marek Labuzik on 4/6/19.
//  Copyright © 2019 Marek Labuzik. All rights reserved.
//

import Foundation

struct Rides: DataConvertible, Hashable, Model {
    let rides: [Ride]
    
    static func empty<T>() -> T where T: Model {
        let rides = Rides(rides: [])
        guard let tRides = rides as? T else {
            fatalError()
        }
        return tRides
    }

    init(rides: [Ride]) {
        self.rides = rides
    }
    
    enum CodingKeys: String, CodingKey {
        case rides = "Rides"
    }
}

struct Ride: DataConvertible, Hashable, Model {
    let identifier: Int?
    let fromAddress, toAddress, note: String?
    let startDate, endDate: Date?
    let rideWithUsers: Int?
    let numberOfPlaces: Int
    let lon, lat: String?
    let driver: User?
    let car: Car?
    let usersDetail: [User]?
    
    enum CodingKeys: String, CodingKey {
        case identifier = "ID"
        case fromAddress = "From"
        case toAddress = "To"
        case startDate = "StartDate"
        case endDate = "EndDate"
        case note = "Note"
        case numberOfPlaces = "NumberOfPlaces"
        case rideWithUsers = "RideWithUsers"
        case lon = "Lon"
        case lat = "Lat"
        case driver = "User"
        case usersDetail = "RideWithUsersDetail"
        case car = "Car"
    }

    static func empty<T>() -> T where T: Model {
        let ride = Ride(identifier: nil, fromAddress: nil, toAddress: nil, note: nil, startDate: nil, endDate: nil,
                        rideWithUsers: -1, numberOfPlaces: -1, lon: nil, lat: nil, driver: nil, car: nil, usersDetail: nil)
        guard let emptyRide = ride as?T else {
            fatalError()
        }
        return emptyRide
    }
}
