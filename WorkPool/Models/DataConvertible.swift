import Foundation

/// Default Converter
struct DefaultConverter: Converter {
    public init() {}
}

/// Converter protocol allows:
/// - converting JSON data into `Codable` model
/// - converting `Codable` model into JSON data
protocol Converter {

    /// Creates `Codable` model from given JSON data
    ///
    /// - Parameter data: JSON data
    /// - Returns: Model
    /// - Throws: JSON Parsing error
    func from<T: Codable>(_ data: Data) throws -> T

    /// Creates JSON encoded data from given `Codable` model
    ///
    /// - Parameter model: `Codable` model
    /// - Returns: JSON data
    /// - Throws: Encoding error
    func toData<T: Codable>(_ model: T) throws -> Data
}

extension Converter {

    /// Creates `Codable` model from given JSON data
    ///
    /// - Parameter data: JSON data
    /// - Returns: Model
    /// - Throws: JSON Parsing error
    func from<T: Codable>(_ data: Data) throws -> T {
        return try JSONDecoder().decode(T.self, from: data)
    }

    /// Creates JSON encoded data from given `Codable` model
    ///
    /// - Parameter model: `Codable` model
    /// - Returns: JSON data
    /// - Throws: Encoding error
    func toData<T: Codable>(_ model: T) throws -> Data {
        return try JSONEncoder().encode(model)
    }
}

/// DataConvertible
protocol DataConvertible: Codable {

    /// Initializer for data
    ///
    /// - Parameter data: JSON encoded data
    /// - Throws: Throws error when data can't be parsed
    init(data: Data) throws

    /// Returns encoded Data
    ///
    /// - Returns: Data
    /// - Throws: Throws error if data can't be encoded
    func data() throws -> Data
}

extension DataConvertible {

    /// Initializer for data
    ///
    /// - Parameter data: JSON encoded data
    /// - Throws: Throws error when data can't be parsed
    init(data: Data) throws {
        self = try DefaultConverter().from(data)
    }

    /// Returns encoded Data
    ///
    /// - Returns: Data
    /// - Throws: Throws error if data can't be encoded
    func data() throws -> Data {
        return try DefaultConverter().toData(self)
    }
}
