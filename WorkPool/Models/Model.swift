//
//  Model.swift
//  WorkPool
//
//  Created by Marek Labuzik on 12/06/2019.
//  Copyright © 2019 Marek Labuzik. All rights reserved.
//

import Foundation
import Moya
import RxSwift

protocol Model: Codable {
    static func empty<T: Model>() -> T
}
