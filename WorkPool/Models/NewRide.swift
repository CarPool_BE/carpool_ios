//
//  NewRide.swift
//  WorkPool
//
//  Created by Marek Labuzik on 28/05/2019.
//  Copyright © 2019 Marek Labuzik. All rights reserved.
//

import Foundation
import MapKit

struct NewRide: DataConvertible, Hashable, Model {
    var from, to, note, returnNote, encodedPolyline: String
    var fromLon, fromLat, toLon, toLat: Double
    var date: [Date]
    var time, timeETA: Date
    var returnTime, returnTimeETA: Date?
    var isReturn: Bool
    var numberOfPlaces, returnNumberOfPlaces, userID, carID: Int

    init() {
        let emptyDate = Date(timeIntervalSince1970: 0)
        from =  ""
        to = ""
        note = ""
        returnNote = ""
        encodedPolyline = ""
        fromLon =  0.0
        fromLat = 0.0
        toLon = 0.0
        toLat = 0.0
        date = [emptyDate]
        time = emptyDate
        timeETA = emptyDate
        returnTime = emptyDate
        returnTimeETA = emptyDate
        isReturn = false
        numberOfPlaces = -1
        returnNumberOfPlaces = -1
        userID = -1
        carID = -1
    }

    init(withViewModel model: NewRideViewModel, profile: User) {
        self.from = model.from
        self.fromLon = model.fromLon
        self.fromLat = model.fromLat
        self.to = model.to
        self.toLon = model.toLon
        self.toLat = model.toLat
        self.note = model.note
        self.returnNote = model.returnNote
        encodedPolyline = model.encodedPolyline
        self.date = model.dates
        if let time = model.time {
            self.time = time
        } else {
            self.time = Date(timeIntervalSince1970: 0)
        }
        self.returnTime = model.returnTime
        self.isReturn = model.isReturn
        self.numberOfPlaces = model.numberOfPlaces
        self.returnNumberOfPlaces = model.returnNumberOfPlaces
        self.carID = model.carID
        self.userID = profile.identifier
        if let time = model.timeETA {
            self.timeETA = time
        } else {
            self.timeETA = Date(timeIntervalSince1970: 0)
        }
        self.returnTimeETA = model.returnTimeEta
    }

    enum CodingKeys: String, CodingKey {
        case from = "From"
        case to = "To"
        case fromLon = "FromLon"
        case fromLat = "FromLat"
        case toLon = "ToLon"
        case toLat = "ToLat"
        case date = "Date"
        case time = "Time"
        case timeETA = "TimeETA"
        case returnTime = "ReturnTime"
        case returnTimeETA = "ReturnTimeETA"
        case isReturn = "IsReturn"
        case note = "Note"
        case returnNote = "ReturnNote"
        case numberOfPlaces = "NumberOfPlaces"
        case returnNumberOfPlaces = "ReturnNumberOfPlaces"
        case userID = "User_ID"
        case carID = "Car_ID"
        case encodedPolyline = "Route"
    }

    static func empty<T>() -> T where T: Model {
        let newRide = NewRide()
        guard let emptyNewRide = newRide as?T else {
            fatalError()
        }
        return emptyNewRide
    }
}
