//
//  Car.swift
//  WorkPool
//
//  Created by Marek Labuzik on 4/6/19.
//  Copyright © 2019 Marek Labuzik. All rights reserved.
//

import Foundation

struct Cars: Model {
    let cars: [Car]

    static func empty<T>() -> T where T: Model {
        let cars = Cars(cars: [])
        guard let tCars = cars as? T else {
            fatalError()
        }
        return tCars
    }

    init(cars: [Car]) {
        self.cars = cars
    }
}

struct Car: DataConvertible, Hashable, Model {
    var identifier: Int?
    var spz, brand, model, color: String?
    var numberOfPlaces, carConsumption: Int?
    var fuelType: Int?

    init(identifier: Int?,
         spz: String?, brand: String?,
         model: String?, color: String?,
         numberOfPlaces: Int?, carConsumption: Int?,
         fuelType: Int?) {
        self.identifier = identifier
        self.spz = spz
        self.brand = brand
        self.model = model
        self.color = color
        self.numberOfPlaces = numberOfPlaces
        self.carConsumption = carConsumption
        self.fuelType = fuelType
    }

    init() {
        self.identifier = nil
        self.spz = nil
        self.brand = nil
        self.model = nil
        self.color = nil
        self.numberOfPlaces = nil
        self.carConsumption = nil
        self.fuelType = nil
    }

    enum CodingKeys: String, CodingKey {
        case identifier = "ID"
        case spz = "SPZ"
        case brand = "Brand"
        case model = "Model"
        case color = "Color"
        case numberOfPlaces = "NumberOfPlaces"
        case carConsumption = "CarConsumption"
        case fuelType = "FuelType"
    }

    static func empty<T>() -> T where T: Model {
        let car = Car(identifier: -1, spz: "", brand: "", model: "", color: "", numberOfPlaces: -1, carConsumption: -1, fuelType: -1)
        guard let emptyCar = car as?T else {
            fatalError()
        }
        return emptyCar
    }
}
