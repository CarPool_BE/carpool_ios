//
//  Login.swift
//  WorkPool
//
//  Created by Marek Labuzik on 05/06/2019.
//  Copyright © 2019 Marek Labuzik. All rights reserved.
//

import Foundation

struct Login: DataConvertible, Hashable, Model {
    var email: String
    var password: String

    enum CodingKeys: String, CodingKey {
        case email = "Email"
        case password = "Password"
    }

    static func empty<T>() -> T where T: Model {
        let login = Login(email: "", password: "")
        guard let emptyLogin = login as?T else {
            fatalError()
        }
        return emptyLogin
    }
}

struct DeviceInfo: DataConvertible, Hashable, Model {
    var pushID: String
    var sandbox: Bool
    let osType: String = "iOS"
    
    var asDictionary: [String: Any] {
        let mirror = Mirror(reflecting: self)
        let dict = Dictionary(uniqueKeysWithValues: mirror.children.lazy.map({ (label: String?, value: Any) -> (String, Any)? in
            guard label != nil else { return nil }
            return (label!, value)
        }).compactMap { $0 })
        return dict
    }
    
    enum CodingKeys: String, CodingKey {
        case pushID = "PushID"
        case sandbox = "Sandbox"
        case osType = "OsType"
    }

    static func empty<T>() -> T where T: Model {
        let device = DeviceInfo(pushID: "", sandbox: false)
        guard let emptyDevice = device as?T else {
            fatalError()
        }
        return emptyDevice
    }
}
