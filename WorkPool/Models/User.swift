//
//  User.swift
//  WorkPool
//
//  Created by Boris Bielik on 30/03/2019.
//  Copyright © 2019 Marek Labuzik. All rights reserved.
//

import Foundation
import CoreLocation

struct ProfileValidator {
    enum ProfileValidatorError: Error {
        case none
        case invalidEmail
        case nameWrongFormat
        case invalidPhoneNumber
        case brandIsEmpty
        case carNumberIsEmpty
        case colorIsEmpty

        var localizedError: String {
            switch self {
            case .none:
                return "Wrong_unknown".localized()
            case .invalidEmail:
                return "Wrong_EmailFormat".localized()
            case .nameWrongFormat:
                return "Wrong_NameFormat".localized()
            case .invalidPhoneNumber:
                return "Wrong_PhoneNumber".localized()
            case .brandIsEmpty:
                return "Wrong_brand_isEmpty".localized()
            case .carNumberIsEmpty:
                return "Wrong_carNumber_isEmpty".localized()
            case .colorIsEmpty:
                return "Wrong_Color_isEmpty".localized()
            }
        }
    }
    
    func validate(updating profile: User) throws -> Bool {
        if let email = profile.email, !email.isEmpty {
            guard  LoginValidator.isValidEmail(testStr: email) else {
                throw ProfileValidatorError.invalidEmail
            }
        }
        
        guard  LoginValidator.isValidName(string: profile.name) else {
            throw ProfileValidatorError.nameWrongFormat
        }
        
        if let phone = profile.phoneNumber, !phone.isEmpty {
            guard  LoginValidator.isValidPhoneNumber(number: phone) else {
                throw ProfileValidatorError.invalidPhoneNumber
            }
        }
        
        if let car = profile.car {
            if car.brand?.isEmpty ?? true == false ||
                car.spz?.isEmpty ?? true == false ||
                car.color?.isEmpty ?? true == false {
                
                guard car.brand?.isEmpty == false else {
                    throw ProfileValidatorError.brandIsEmpty
                }
                guard car.spz?.isEmpty == false else {
                    throw ProfileValidatorError.carNumberIsEmpty
                }
                guard car.color?.isEmpty == false else {
                    throw ProfileValidatorError.colorIsEmpty
                }
            }
        }

        return true
    }
    
}

struct Users: Model {
    let users: [User]
    
    static func empty<T>() -> T where T: Model {
        let users = Users(users: [])
        guard let tUsers = users as? T else {
            fatalError()
        }
        return tUsers
    }
    
    init(users: [User]) {
        self.users = users
    }
}

struct User: DataConvertible, Hashable, Model {
    let identifier: Int
    var cdsid: String?
    var name: String
    var email: String?
    var home: String?
    var phoneNumber: String?
    let createdAt: String?
    var car: Car?
    var homeLon: Double?
    var homeLat: Double?

    func getHomeGPS() -> CLLocationCoordinate2D {
        if let doubleLat = homeLat, let doubleLon = homeLon {
            return CLLocationCoordinate2D(latitude: doubleLat, longitude: doubleLon)
        }
        return CLLocationCoordinate2D(latitude: 0, longitude: 0)
    }
    
    mutating func sethHomeCoordinates(_ coordinates: CLLocationCoordinate2D) {
        self.homeLat = coordinates.latitude
        self.homeLon = coordinates.longitude
    }
    
    enum CodingKeys: String, CodingKey {
        case identifier = "ID"
        case home = "City"
        case name = "Name"
        case email = "Email"
        case phoneNumber = "Phone"
        case createdAt = "Created"
        case car = "Car"
        case homeLon = "FromLon"
        case homeLat = "FromLat"
        case cdsid = "CDSID"
    }
    
    static func empty<T>() -> T where T: Model {
        let user = User(identifier: -1,
                        cdsid: "",
                        name: "",
                        email: "",
                        home: "",
                        phoneNumber: "",
                        createdAt: "",
                        car: nil,
                        homeLon: nil,
                        homeLat: nil)
        guard let emptyUser = user as?T else {
            fatalError()
        }
        return emptyUser
    }
    
    func validateInputs() -> Bool {
        
        let valid = Validator()
        
        if !valid.validateEmail(email ?? "") || !valid.validPhoneNumber(phoneNumber ?? "") {
            return false
        }
        if name.isEmpty || home?.isEmpty ?? true {
            return false
        }
        if car?.brand?.isEmpty ?? true || car?.color?.isEmpty ?? true || !valid.validSPZ(car?.spz ?? "") {
            return false
        }
        
        return true
    }
}
