//
//  User.swift
//  WorkPool
//
//  Created by Boris Bielik on 30/03/2019.
//  Copyright © 2019 Marek Labuzik. All rights reserved.
//

import Foundation
import CoreLocation

struct Users: Model {
    let users: [User]

    static func empty<T>() -> T where T: Model {
        let users = Users(users: [])
        guard let tUsers = users as? T else {
            fatalError()
        }
        return tUsers
    }

    init(users: [User]) {
        self.users = users
    }
}

struct User: DataConvertible, Hashable, Model {
    let identifier: Int
    let name: String
    let email: String?
    let home: String?
    let phoneNumber: String?
    let createdAt: String?
    let car: Car?
    let homeLon: Double?
    let homeLat: Double?
    let homeGPS: GpsPosition?

    func getHomeGPS() -> CLLocationCoordinate2D {
        if let gps = self.homeGPS {
            if let lat = gps.latitude, let lon = gps.longitude {
                if let doubleLat = Double(lat), let doubleLon = Double(lon) {
                    return CLLocationCoordinate2D(latitude: doubleLat, longitude: doubleLon)
                }
            }
        }
        return CLLocationCoordinate2D(latitude: 0, longitude: 0)
    }

    enum CodingKeys: String, CodingKey {
        case identifier = "ID"
        case home = "City"
        case name = "Name"
        case email = "Email"
        case phoneNumber = "Phone"
        case createdAt = "Created"
        case car = "Car"
        case homeLon = "homeLon"
        case homeLat = "homeLat"
        case homeGPS = "homeGPS"
    }

    static func empty<T>() -> T where T: Model {
        let user = User(identifier: -1,
                        name: "",
                        email: "",
                        home: "",
                        phoneNumber: "",
                        createdAt: "",
                        car: nil,
                        homeLon: nil,
                        homeLat: nil,
                        homeGPS: nil)
        guard let emptyUser = user as?T else {
            fatalError()
        }
        return emptyUser
    }
}

struct GpsPosition: DataConvertible, Hashable {
    let latitude, longitude: String?

    enum CodingKeys: String, CodingKey {
        case latitude = "latitude"
        case longitude = "longitude"
    }
}
