//
//  ShareRideCoordinator.swift
//  WorkPool
//
//  Created by Marek Labuzik on 14/10/2019.
//  Copyright © 2019 Marek Labuzik. All rights reserved.
//

import UIKit

final class ShareRideCoordinator: Coordinator {
    var isVisible: Bool = false

    var presentationType: PresentationType?

    var childCoordinators: [Coordinator] = []

    var onClose: (() -> Void)?

    var myRideDetailViewController: MeRideDetailViewController?
    var rootViewController: UINavigationController?

    let containerVC: ContainerViewController
    let rideID: Int
    let userID: Int

    var shareRideViewModel: RideViewModel? {
        willSet {
            self.myRideDetailViewController?.viewModel = newValue
        }
    }

    init(containerVC: ContainerViewController, rideID: String, userID: String) {
        self.containerVC = containerVC
        self.rideID = Int(rideID) ?? 0
        self.userID = Int(userID) ?? 0
    }

    private func getData() {
        DetailRideList().getRideDetail(for: self.rideID) { result in
            switch result {
            case .failure:
                self.showAlertRideNotExists()
            case .success(let rideDetail):
                self.shareRideViewModel = rideDetail
                self.myRideDetailViewController!.stopAnimating()
            }
        }
    }

    private func showAlertRideNotExists() {

    }

    func start(with presentationType: PresentationType = .modal) {
        self.presentationType = presentationType

        self.myRideDetailViewController = MeRideDetailViewController()
        self.myRideDetailViewController!.isRideFormShareWeb = true
        self.rootViewController = UINavigationController(rootViewController: self.myRideDetailViewController!)
        self.myRideDetailViewController!.delegate = self

        switch presentationType {
        case .push, .present:
            break
        case .modal:
            self.containerVC.present(self.rootViewController!, animated: true, completion: nil)
        }
        self.isVisible = true
        self.myRideDetailViewController!.startAnimating()
        self.getData()
    }
}

extension ShareRideCoordinator: MeRideDetailViewControllerDelegate {
    func presentationControllerDidDismissViewController(_ contactCell: MeRideDetailViewController) {
        self.close()
    }

    func viewController(_ viewController: MeRideDetailViewController, didPerform action: MyRideDetailAction) {

    }

    func viewController(_ contactCell: MeRideDetailViewController, makeCallTo phone: String) {
        let url = URL(string: "tel://\(phone)")!
        UIApplication.shared.open(url, options: [:], completionHandler: nil)
    }
}
