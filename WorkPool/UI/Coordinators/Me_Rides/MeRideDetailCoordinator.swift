//
//  MeRideDetailCoordinator.swift
//  WorkPool
//
//  Created by Marek Labuzik on 18/06/2019.
//  Copyright © 2019 Marek Labuzik. All rights reserved.
//

import UIKit

final class MeRideDetailCoordinator: Coordinator {
    var isVisible: Bool = false
    
    var presentationType: PresentationType?
    
    var childCoordinators: [Coordinator] = []
    
    var viewModel: RideViewModel? {
        willSet {
            guard let controller = self.myRideDetailViewController else { return }
            controller.viewModel = newValue
            self.viewModel?.errorHandler = { error in
                ErrorHandler.show(with: error, on: controller)
            }
        }
    }

    let list: DetailRideList = DetailRideList()
    
    var onClose: (() -> Void)?
    
    private(set) var myRideDetailViewController: MeRideDetailViewController?
    private(set) var navigationController: UINavigationController
    private(set) var rideID: Int
    
    init(_ navigationController: UINavigationController, rideID: Int) {
        self.navigationController = navigationController
        self.rideID = rideID
        self.getData()
    }
    
    func start(with presentationType: PresentationType = .push) {
        self.presentationType = presentationType
        
        self.myRideDetailViewController = MeRideDetailViewController()
        self.myRideDetailViewController?.delegate = self
        
        switch presentationType {
        case .push:
            navigationController.pushViewController(self.myRideDetailViewController!, animated: true)
        case .present, .modal:
            navigationController.show(self.myRideDetailViewController!, sender: nil)
        }
        self.isVisible = true
    }
    
    func close() {
        guard let type = self.presentationType else {return}
        switch type {
        case .push:
            self.navigationController.popViewController(animated: true)
        case .present, .modal:
            self.navigationController.dismiss(animated: true, completion: nil)
        }
        if let close = self.onClose {
            close()
        }
        self.isVisible = false
    }
    
    func getData() {
        self.myRideDetailViewController?.startAnimating()
        self.list.getRideDetail(for: self.rideID, completion: {[weak self] result in
            guard let self = self else { return }
            switch result {
            case .success(let data):
                self.viewModel = data
            case .failure:
                self.viewModel?.errorHandler?(.retry)
            }
            self.myRideDetailViewController?.stopAnimating()
        })
    }
    
    func getShareLink() -> URL? {
        guard let viewModel = self.viewModel,
            let rideIdentifier = viewModel.model.identifier,
            let driver = viewModel.driver else {
            return nil
        }
        var url = String(format: "https://www.teamride.eu/share/ride.php?rideID=%@&userID=%@&token=",
                         String(rideIdentifier),
                         String(driver.identifier))

        let token = String(format: "&driver=%@&fromTime=%@&toTime=%@&from=%@&to=%@&car=%@&color=%@&seats=%@",
                           String(format: "%@ %@", driver.firstName, driver.lastName),
                           String(viewModel.model.startDate?.timeIntervalSince1970 ?? 0),
                           String(viewModel.model.endDate?.timeIntervalSince1970 ?? 0),
                           viewModel.from,
                           viewModel.to,
                           String(format: "%@, %@", driver.carBrand, driver.carNumber),
                           driver.carColor,
                           String(viewModel.numberOfPlaces)
                    )
        let encodedString = token.toBase64()
        url.append(encodedString)

        return URL(string: url)!
    }
}

extension MeRideDetailCoordinator: MeRideDetailViewControllerDelegate {
    func presentationControllerDidDismissViewController(_ contactCell: MeRideDetailViewController) {
        
    }

    func viewController(_ contactCell: MeRideDetailViewController, makeCallTo phone: String) {
        let url = URL(string: "tel://\(phone)")!
        UIApplication.shared.open(url, options: [:], completionHandler: nil)
    }

    func viewController(_ viewController: MeRideDetailViewController, didPerform action: MyRideDetailAction) {
        guard let rideID = self.viewModel?.model.identifier,
             let userID = AppData.meData.profile?.identifier
            else { return }
        
        switch action {
        case .cancelRide:
            self.myRideDetailViewController?.startAnimating()
            AppData.meData.auth.rideService.removeMyRide(with: rideID) { [weak self] result in
                guard let self = self else { return }
                switch result {
                case .success:
                    self.close()
                case .failure:
                    self.viewModel?.errorHandler?(.retry)
                }
                self.myRideDetailViewController?.stopAnimating()
            }
        case .removeFromRide:
            self.myRideDetailViewController?.startAnimating()
            AppData.meData.auth.rideService.deregisterFromRide(with: rideID, userId: userID) { [weak self] result in
                guard let self = self else { return }
                switch result { 
                case .success:
                    self.close()
                case .failure:
                    self.viewModel?.errorHandler?(.retry)
                }
                self.myRideDetailViewController?.stopAnimating()
            }
        case .none:
            break
        case .shareRide:
            var sharedItem: [URL] = []
            if let url = self.getShareLink() {
                sharedItem.append(url)
            }
            
            let activityViewController = UIActivityViewController(activityItems: sharedItem as [Any], applicationActivities: [])
            if #available(iOS 13.0, *) {
                activityViewController.view.backgroundColor = .secondarySystemBackground
            } 
            activityViewController.popoverPresentationController?.barButtonItem = self.myRideDetailViewController?.navigationItem.rightBarButtonItem
            self.myRideDetailViewController?.present(activityViewController, animated: true, completion: nil)
            break
        }
    }
}
