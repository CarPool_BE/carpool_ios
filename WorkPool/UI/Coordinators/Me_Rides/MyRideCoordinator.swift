//
//  MyRideCoordinator.swift
//  WorkPool
//
//  Created by Marek Labuzik on 13/05/2019.
//  Copyright © 2019 Marek Labuzik. All rights reserved.
//

import UIKit

struct FilterContext {
    var date: Date = Date()
}

final class MyRideCoordinator: NSObject, Coordinator {
    var presentationType: PresentationType?

    var onClose: (() -> Void)?

    var childCoordinators: [Coordinator] = []
    private var createCoordinator: CreateRideCoordinator?
    private var datailCoordinator: MeRideDetailCoordinator?
    var filterMeRides = FilterContext()
    
    var viewModel: [RideViewModel]? {
        willSet {
            self.myRideViewController?.rides = newValue ?? []
            self.myRideViewController?.reloadData()
        }
    }
    
    var isVisible: Bool = false
    
    private(set) var myRideViewController: MeRidesViewController?
    private(set) var navigationController: UINavigationController?
    
    func start() -> UINavigationController {
        
        self.myRideViewController = MeRidesViewController()
        self.myRideViewController?.deledate = self
        self.myRideViewController?.tabBarItem = UITabBarItem(title: "TabBar_MePlan".localized(),
                                                        image: UIImage(named: "TabMeRides"),
                                                        tag: 1)
        
        self.navigationController = UINavigationController(rootViewController: self.myRideViewController!)
        self.navigationController?.delegate = self
        return self.navigationController!
    }
    
    func getData() {
        self.myRideViewController?.startAnimating()
        AppData.meData.auth.profileService.getMeRides(of: self.filterMeRides.date) { [weak self] result in
            guard let self = self else { return }
            switch result {
            case .success(let rides):
                self.viewModel = rides.rides.map { RideViewModel(model: $0) }
            case .failure(let err):
                if err == .notFound {
                    self.viewModel = []
                }
                print(err)
            }
            self.myRideViewController?.stopAnimating()
        }
    }
}

extension MyRideCoordinator: MeRidesViewControllerDelegate {
    func viewController(_ meRidesViewController: MeRidesViewController, showDetailFor rideID: Int) {
        self.datailCoordinator?.onClose?()
        
        self.datailCoordinator = MeRideDetailCoordinator(self.navigationController!, rideID: rideID)
        self.childCoordinators.append(self.datailCoordinator!)
        self.datailCoordinator?.onClose = { [weak self] in
            guard let self = self else {return}
            if let idx = self.childCoordinators.firstIndex(where: { $0 === self.datailCoordinator }) {
                self.childCoordinators.remove(at: idx)
                self.datailCoordinator = nil
            }
        }
        self.datailCoordinator?.start()
    }

    func viewController(_ meRidesViewController: MeRidesViewController, didPerform action: MeRidesActionType) {
        switch action {
        case .getNewData:
            self.getData()
        case .createNewRide:
            guard let navigationController = self.navigationController else {
                preconditionFailure("Nemam navigation")
            }
            
            self.createCoordinator?.onClose?()
            
            self.createCoordinator = CreateRideCoordinator(navigationController)
            childCoordinators.append(self.createCoordinator!)
            self.createCoordinator?.onClose = { [weak self] in
                guard let self =  self else {return}
                if let idx = self.childCoordinators.firstIndex(where: { $0 === self.createCoordinator }) {
                    self.childCoordinators.remove(at: idx)
                    self.createCoordinator = nil
                }
            }
            self.createCoordinator?.start()
//            self.createCoordinator?.updateViewModel(date: self.filterMeRides.date)
        }
    }
    
    func viewController(_ meRidesViewController: MeRidesViewController, didUpdate date: Date) {
        self.filterMeRides.date = date
        self.getData()
    }
}

extension MyRideCoordinator: UINavigationControllerDelegate {
    
    func navigationController(_ navigationController: UINavigationController, willShow viewController: UIViewController, animated: Bool) {
        if viewController is MeRidesViewController && self.createCoordinator != nil {
            self.createCoordinator?.onClose?()
        }
        if viewController is MeRidesViewController && self.datailCoordinator != nil {
            self.datailCoordinator?.onClose?()
        }
    }
}
