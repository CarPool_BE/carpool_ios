//
//  TabBarCoordinator.swift
//  WorkPool
//
//  Created by Marek Labuzik on 11/05/2019.
//  Copyright © 2019 Marek Labuzik. All rights reserved.
//

import Foundation
import UIKit

final class TabBarCoordinator: Coordinator {
    var presentationType: PresentationType?
    
    var onClose: (() -> Void)?
    
    var isVisible: Bool = false

    private(set) var childCoordinators: [Coordinator] = []

    let containerVC: ContainerViewController
    let tabBar = UITabBarController()
    
    init(containerVC: ContainerViewController) {
        self.containerVC = containerVC
    }
    
    func start() {
        // search
        let searchRideCoordinator = SearchRideCoordinator()
        let searchRideNC = searchRideCoordinator.start()
        childCoordinators.append(searchRideCoordinator)
        
        // my ride
        let myRideCoordinator = MyRideCoordinator()
        let myRideNC = myRideCoordinator.start()
        childCoordinators.append(myRideCoordinator)
        
        // profile
        let profileCoordinator = ProfileCoordinator()
        let profileNC = profileCoordinator.start()
        profileCoordinator.onClose = { [weak self] in
            guard let self = self else {return}
            self.childCoordinators.removeAll()
            self.close()
        }
        childCoordinators.append(profileCoordinator)
    
        tabBar.viewControllers = [searchRideNC, myRideNC, profileNC]
        tabBar.selectedIndex = AppData.meData.getDefaultScreen()
        tabBar.modalPresentationStyle = .fullScreen
        containerVC.viewControllers = [tabBar]
        containerVC.selectViewController(at: 0, animated: false)
        containerVC.modalPresentationStyle = .fullScreen
        
        isVisible = true
        childCoordinators = childCoordinators.map { coordinator in
            coordinator.isVisible = true
            return coordinator
        }
    }
    
    func close() {
        if let close = self.onClose {
            close()
        }
        self.isVisible = false
    }

    public func showRide(rideID: String, userID: String) {
        let shareCoordinator = ShareRideCoordinator(containerVC: self.containerVC, rideID: rideID, userID: userID)
        shareCoordinator.start()
    }
}
