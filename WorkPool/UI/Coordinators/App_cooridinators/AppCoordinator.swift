//
//  AppCoordinator.swift
//  WorkPool
//
//  Created by Marek Labuzik on 13/05/2019.
//  Copyright © 2019 Marek Labuzik. All rights reserved.
//

import UIKit

protocol Coordinator: class {
    var isVisible: Bool {get set}
    var presentationType: PresentationType? {get set}
    var childCoordinators: [Coordinator] {get}
    var onClose: (() -> Void)? { get set }
    func close()
}

extension Coordinator {
    func close() {
        if let close = self.onClose {
            close()
        }
    }
}

enum PresentationType {
    case push
    case present
    case modal
}

/// Window ma 1 navigation controller
/// navigation controller ma container view controller
/// Login sa vzdy zobrazuje nad navigation conrollerom (window.rootViewController)
/// TabBar je
final class AppCoordinator: Coordinator {
    var presentationType: PresentationType?
    
    var onClose: (() -> Void)?
    
    var isVisible: Bool = false
    let navigationController: UINavigationController
    let containerViewController: ContainerViewController

    private(set) var childCoordinators: [Coordinator] = []
    
    init() {
        self.containerViewController = ContainerViewController(viewControllers: [])
        self.containerViewController.modalPresentationStyle = .fullScreen
        self.navigationController = UINavigationController(rootViewController: containerViewController)
        self.navigationController.modalPresentationStyle = .fullScreen
        self.navigationController.setNavigationBarHidden(true, animated: false)
        self.navigationController.view.backgroundColor = UIColor.white
    }
    
    func start() {

        // Prihlaseny uzivatel
        if AppData.meData.auth.getSignInToken() != nil {
            let tabBarCoordinator = TabBarCoordinator(containerVC: containerViewController)
            self.childCoordinators.append(tabBarCoordinator)
            tabBarCoordinator.onClose = { [weak self] in
                guard let self = self else {return}
                if let idx = self.childCoordinators.firstIndex(where: { $0 === tabBarCoordinator }) {
                    self.showLogin(in: self.navigationController,
                                   animated: true,
                                   presentationType: .present,
                                   onClose: { [weak self] _ in
                        guard let self = self else {return}
                        self.childCoordinators.remove(at: idx)
                        self.close()
                        self.start()
                    })
                }
            }
            tabBarCoordinator.start()

        // Uzivatel nie je prihlaseny
        } else {
            showLoginInModal()
        }

        isVisible = true
    }

    public func showRide(rideID: String, userID: String) {
        if let coordinator = self.childCoordinators.last as? TabBarCoordinator {
            coordinator.showRide(rideID: rideID, userID: userID)
        }
    }

    func showLoginInModal() {
        let loginCoordinator = showLogin(in: self.navigationController,
                                         animated: true,
                                         presentationType: .present,
                                         onClose: { [weak self] loginCoordinator in
                                            guard let self = self else {return}
                                            if let idx = self.childCoordinators.firstIndex(where: { $0 === loginCoordinator }) {
                                                self.childCoordinators.remove(at: idx)
                                                self.close()
                                                self.start()
                                            }
        })
        self.childCoordinators.append(loginCoordinator)
        loginCoordinator.onClose = { [weak self] in
            guard let self =  self else {return}
            if let idx = self.childCoordinators.firstIndex(where: { $0 === loginCoordinator }) {
                self.childCoordinators.remove(at: idx)
            }
        }
    }

    @discardableResult
    func showLogin(in navController: UINavigationController,
                   animated: Bool,
                   presentationType: PresentationType,
                   onClose: @escaping (LoginCoordinator?) -> Void) -> LoginCoordinator {
        let loginCoordinator = LoginCoordinator(navigationController: navController)
        loginCoordinator.presentationType = presentationType
        loginCoordinator.onClose = {
            onClose(loginCoordinator)
        }
        loginCoordinator.start(animated: animated)

        return loginCoordinator
    }

    func close() {
        if let visible = containerViewController.visibleViewController as? UINavigationController {
            visible.setViewControllers([], animated: false)
        }
        childCoordinators = []
    }
}
