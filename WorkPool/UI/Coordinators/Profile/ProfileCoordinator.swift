//
//  ProfileCoordinator.swift
//  WorkPool
//
//  Created by Marek Labuzik on 13/05/2019.
//  Copyright © 2019 Marek Labuzik. All rights reserved.
//

import UIKit

final class ProfileCoordinator: Coordinator {
    var presentationType: PresentationType?
    
    var onClose: (() -> Void)?
    
    var childCoordinators: [Coordinator] = []
    
    var isVisible: Bool = false
    
    private(set) var profileViewController: MeProfileViewController?
    private(set) var navigationController: UINavigationController?
    
    func start() -> UINavigationController {
        
        if #available(iOS 13.0, *) {
            profileViewController = MeProfileViewController(style: .insetGrouped)
        } else {
            profileViewController = MeProfileViewController()
        }
        self.profileViewController?.profileDelegate = self
        profileViewController?.tabBarItem = UITabBarItem(title: "TabBar_Profile".localized(),
                                                         image: UIImage(named: "TabProfile"),
                                                         tag: 2)
        
        navigationController = UINavigationController(rootViewController: profileViewController!)
        self.isVisible = true
        return navigationController!
    }
    
    func close() {
        if let close = self.onClose {
            close()
        }
        self.isVisible = false
    }
}

extension ProfileCoordinator: MeProfileViewControllerDelegate {
    func viewController(_ viewController: MeProfileViewController, setDefatulScreen index: Int) {
        AppData.meData.saveDefaultScreen(index: index)
    }
    
    func viewController(_ viewController: MeProfileViewController, didPerform action: MeProfileActionType) {
        switch action {
        case .logout:
            AppData.meData.reset()
            self.close()
        }
    }
}
