//
//  LoginCoordinator.swift
//  WorkPool
//
//  Created by Marek Labuzik on 31/05/2019.
//  Copyright © 2019 Marek Labuzik. All rights reserved.
//

import UIKit

final class LoginCoordinator: Coordinator, LoginViewControllerDelegate {

    var isVisible: Bool = false

    enum Present {
        case viewController(UIViewController)
        case navigationController(UINavigationController)
    }

    var presentationType: PresentationType?

    private(set) var childCoordinators: [Coordinator] = []

    var onClose: (() -> Void)?

    private(set) var loginViewController: LoginViewController?
    let navigationController: UINavigationController
    private(set) var onboardingModel: OnboardingViewModel?

    #if DEBUG
    var loginModel: LoginViewModel = LoginViewModel(name: "DEBUG", workID: "", email: "Jako@hrasko333.sk", password: "ahoj", reTypePassword: "", errorHandler: nil)
    #else
    var loginModel: LoginViewModel = LoginViewModel()
    #endif

    var present: Present?

    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }

    func start(animated: Bool) {
        let loginViewController = setupLoginViewController()
        loginViewController.modalPresentationStyle = .fullScreen
        self.navigationController.setNavigationBarHidden(false, animated: false)
        self.navigationController.modalPresentationStyle = .fullScreen

        if presentationType == .some(.push) {
            self.navigationController.show(loginViewController, sender: nil)
            self.present = .viewController(loginViewController)
        } else {
            let navigationController = UINavigationController(rootViewController: loginViewController)
            navigationController.modalPresentationStyle = .fullScreen
            self.navigationController.present(navigationController, animated: animated, completion: nil)
            self.present = .navigationController(navigationController)
        }
        self.isVisible = true
    }

    func close() {
        if let present = present {
            switch present {
            case .navigationController(let navController):
                completionClose()
                navController.dismiss(animated: true, completion: nil)
            case .viewController(let loginVC):
                loginVC.dismiss(animated: true, completion: { [weak self] in
                    self?.completionClose()
                })
            }
        }
    }

    // MARK: Setup login view controller
    private func setupLoginViewController() -> LoginViewController {
        let loginViewController = LoginViewController(style: .plain)
        self.loginViewController = loginViewController
        self.loginModel.errorHandler = { error in
            ErrorHandler.show(with: error, on: loginViewController)
        }
        self.loginViewController?.model = self.loginModel
        self.loginViewController?.delegate = self
        self.loginViewController?.modalPresentationStyle = .fullScreen
        return loginViewController
    }

    private func completionClose() {
        if let close = self.onClose {
            self.isVisible = false
            self.navigationController.setNavigationBarHidden(true, animated: false)
            close()
        }
    }

    // MARK: Login View Controller Delegate
    func viewController(_ viewController: LoginViewController, didPerformAction: LoginActionType) {
        switch didPerformAction {
        case .showPrivacyPolicy:
            let privacyView = WebContent()
            privacyView.page = .policy
            self.loginViewController?.navigationController?.pushViewController(privacyView, animated: true)
        case .registerButtonPressed,
             .loginButtonPressed:
            let isLogin = viewController.isShowingLogin()
            let validator = LoginValidator()
            do {
                let isValid = try validator.validate(registration: self.loginModel, forLogin: isLogin)
                if isValid {
                    if isLogin {
                        self.login()
                    } else {
                        self.register()
                    }
                }
            } catch let error as LoginValidator.LoginValidatorError {
                self.loginModel.errorHandler?(error.convertToAppError())
            } catch {
                NSLog("Untracked login error: \(error)")
            }
        }
    }

    func login() {
        self.loginViewController?.startAnimating()
        var isSandox = false
        #if DEBUG
        isSandox = true
        #endif

        let loginData = Login(email: self.loginModel.email,
                              password: self.loginModel.password)
        let deviceData = DeviceInfo(pushID: PushNotifications.mePush.deviceID ?? "",
                                    sandbox: isSandox)
        AppData.meData.auth.profileService.login(data: loginData, device: deviceData) { [weak self] result in
            guard let self = self else { return }
            switch result {
            case .failure(let error):
                self.loginModel.errorHandler?(error.convertToAppError())
            case .success(let response):
                AppData.meData.modelToUpdate = self.onboardingModel
                AppData.meData.auth.accessToken = AccessToken(with: response)
                AppData.meData.auth.refreshToken = RefreshToken(with: response)
                self.close()
            }
            self.loginViewController?.stopAnimating()
        }
    }

    func register() {
        self.loginViewController?.startAnimating()
        let data = Register(name: self.loginModel.name,
                            workID: self.loginModel.workID,
                            email: self.loginModel.email,
                            password: self.loginModel.password)
        AppData.meData.auth.profileService.register(data: data) { [weak self] result in
            guard let self = self else { return }
            self.loginViewController?.stopAnimating()
            switch result {
            case .failure(let error):
                self.loginModel.errorHandler?(error.registerConvert())
            case .success:
                self.showOnboarding()
            }
        }
    }

    func viewController(_ viewController: LoginViewController, valueDidChange value: String, forRow: LoginTableViewModel) {
        switch forRow {
        case .email:
            self.loginModel.email = value
        case .name:
            self.loginModel.name = value
        case .password:
            self.loginModel.password = value
        case .retypePassword:
            self.loginModel.reTypePassword = value
//        case .workID:
//            self.loginModel.workID = value
        }
    }
    
    func showOnboarding() {
        guard let loginVC = self.loginViewController else { return }
        let onboardingCoordinator = OnboardingCoordinator(loginVC)
        self.childCoordinators.append(onboardingCoordinator)
        onboardingCoordinator.onClose = {[weak self] in
            guard let self = self else {return}
            self.onboardingModel = onboardingCoordinator.model
            if let idx = self.childCoordinators.firstIndex(where: { $0 === onboardingCoordinator }) {
                self.childCoordinators.remove(at: idx)
                self.login()
            }
        }
        onboardingCoordinator.start()
    }
}
