//
//  OnboardingCoordinator.swift
//  WorkPool
//
//  Created by Marek Labuzik on 05/07/2019.
//  Copyright © 2019 Marek Labuzik. All rights reserved.
//

import UIKit
import OnboardKit

struct OnboardingViewModel {
    var address: String
    var lat: Double
    var lon: Double
    var phoneNumber: String
    var car: Car

    init() {
        self.address = ""
        self.lat = 0
        self.lon = 0
        self.phoneNumber = ""
        self.car = Car.empty()
    }

    init(address: String, lat: Double, lon: Double, phoneNumber: String, car: Car) {
        self.address = address
        self.lat = lat
        self.lon = lon
        self.phoneNumber = phoneNumber
        self.car = car
    }
}

final class OnboardingCoordinator: Coordinator {
    var presentationType: PresentationType?

    var isVisible: Bool = false

    var childCoordinators: [Coordinator] = []

    var onClose: (() -> Void)?

    var onboardingPages: [OnboardPage]?
    var model: OnboardingViewModel
    var onboardingController: OnboardViewController?
    var loginController: UIViewController
    var onboardingNavigationController: UINavigationController?

    init(_ loginController: UIViewController) {
        self.loginController = loginController
        self.model = OnboardingViewModel()
    }

    func start() {
        self.createOnboardingController()
        guard let onboarding  = self.onboardingController else { return }
        self.onboardingNavigationController = UINavigationController(rootViewController: onboarding)
        self.onboardingNavigationController?.modalPresentationStyle = .fullScreen

        self.onboardingController?.delegate = self
        loginController.show(self.onboardingNavigationController!, sender: nil)
        self.isVisible = true
    }

    func close() {
        self.loginController.dismiss(animated: true, completion: nil)
        if let close = self.onClose {
            self.isVisible = false
            close()
        }
    }
    
    func createOnboardingController() {
        let boldTitleFont = UIFont.systemFont(ofSize: 28.0, weight: .bold)
        let mediumTextFont = UIFont.systemFont(ofSize: 17.0, weight: .regular)

        let styling: OnboardViewController.ButtonStyling = { button in
            let image = UIImage(named: "Home")
            button.setImage(image, for: .normal)
            button.tintColor = .mainGreen
            button.setTitleColor(.mainGreen, for: .normal)
            button.titleLabel?.font = UIFont.systemFont(ofSize: 20.0, weight: .bold)
        }
        
        var appearanceConfiguration = OnboardViewController.AppearanceConfiguration(tintColor: .mainGreen,
                                                                                    titleColor: .black80,
                                                                                    textColor: .black80,
                                                                                    backgroundColor: .white,
                                                                                    titleFont: boldTitleFont,
                                                                                    textFont: mediumTextFont,
                                                                                    actionButtonStyling: styling,
                                                                                    imageColor: .mainGreen)
        
        if #available(iOS 13.0, *) {
            appearanceConfiguration = OnboardViewController.AppearanceConfiguration(tintColor: .mainGreen,
                                                                                        titleColor: .secondaryLabel,
                                                                                        textColor: .secondaryLabel,
                                                                                        backgroundColor: .systemBackground,
                                                                                        titleFont: boldTitleFont,
                                                                                        textFont: mediumTextFont,
                                                                                        actionButtonStyling: styling,
                                                                                        imageColor: .mainGreen)
        }
        self.createPages()
        guard let pages = self.onboardingPages else { return }
        self.onboardingController = OnboardViewController(pageItems: pages,
                                                          appearanceConfiguration: appearanceConfiguration)
        self.onboardingController?.modalPresentationStyle = .fullScreen
        self.onboardingController?.delegate = self
    }
    
    func createPages() {
        let pageOne = OnboardPage(title: "GoWorkWithColleague_title".localized(),
                                  imageName: "OnboardingCar",
                                  description: "GoWorkWithColleague_subTitle".localized())
        
        let pageTwo = OnboardPage(title: "FindRide_title".localized(),
                                  imageName: "OnboardingSearch",
                                  description: "FindRide_subTitle".localized())
        
        let pageThree = OnboardPage(title: "DrriveToWork_title".localized(),
                                    imageName: "OnboardingDrive",
                                    description: "DrriveToWork_subTitle".localized())
        
        let pageFour = OnboardPage(title: "SetupYourAddress_title".localized(),
                                   imageName: "OnboardingHome",
                                   description: "SetupYourAddress_subTitle".localized(),
                                   advanceButtonTitle: "Done_button".localized(),
                                   actionButtonTitle: "SetupYourAddress_selectAddressButton".localized(),
                                   action: { [weak self] _ in
                                    guard let self = self else {return}
                                    self.showAddressPicker()
        })
        
        self.onboardingPages = [pageOne, pageTwo, pageThree, pageFour]
    }

    func showAddressPicker() {
        let vc = SearchAddressController()
        vc.color = .mainGreen
        vc.delegate = self
        vc.type = .from
        self.onboardingNavigationController?.pushViewController(vc, animated: true)
    }
}

extension OnboardingCoordinator: SearchAddressControllerDelate {
    func viewController(_ viewController: SearchAddressController, didPerformAction: SearchAddressActionType) {
        self.onboardingNavigationController?.popViewController(animated: true)
    }

    func viewController(_ viewController: SearchAddressController, type: SearchAddressType, point: PointInfo) {
        self.model.address = point.title
        self.model.lon = point.position.longitude
        self.model.lat = point.position.latitude
        self.onboardingNavigationController?.popViewController(animated: true)
        self.onboardingController?.updateViewController(at: 3, updateButton: self.model.address)
    }
}

extension OnboardingCoordinator: OnboardingDelegate {
    func onboardViewController(_ controller: OnboardViewController, didPerform action: OnboardActionType) {
        self.close()
    }
}
