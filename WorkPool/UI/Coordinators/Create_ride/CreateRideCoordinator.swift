//
//  CreateRideCoordinator.swift
//  WorkPool
//
//  Created by Marek Labuzik on 23/05/2019.
//  Copyright © 2019 Marek Labuzik. All rights reserved.
//

import Foundation
import UIKit
import MapKit
import Crashlytics
import UICustomElements
import Polyline

final class CreateRideCoordinator: Coordinator {
    var presentationType: PresentationType?
    private var etaTime: TimeInterval?
    var onClose: (() -> Void)?
    var isVisible: Bool = false
    var isFromSearchController: Bool = false
    private var createRideViewController: CreateRideViewController?
    var childCoordinators: [Coordinator] = []

    private(set) var navigationController: UINavigationController

    private var viewModel: NewRideViewModel? {
        didSet {
            self.createRideViewController?.model = self.viewModel
            self.viewModel?.errorHandler = { error in
                if let coordinator = self.createRideViewController {
                    ErrorHandler.show(with: error, on: coordinator)
                }
            }
        }
    }

    init(_ navigationController: UINavigationController) {
        self.navigationController = navigationController
    }

    func start(with presentationType: PresentationType = .push) {
        guard let profile = AppData.meData.profile else {
            if let close = self.onClose {
                close()
            }
            return
        }
        self.presentationType = presentationType
        self.createRideViewController = CreateRideViewController(style: .grouped)
        self.createRideViewController?.delegate = self

        self.viewModel = NewRideViewModel(meProfileData: profile,
                                          lastFindingPoint: AppData.meData.lastSearch,
                                          workGPS: AppData.meData.workGPS,
                                          workAddress: AppData.meData.workAddress)
        switch presentationType {
        case .push:
            navigationController.pushViewController(createRideViewController!, animated: true)
        case .present, .modal:
            navigationController.show(createRideViewController!, sender: nil)
        }
        self.tryUpdateData()
    }
    
    func updateViewModel(date: Date) {
        self.viewModel?.set(dates: [date])
    }

    func close() {
        guard let type = self.presentationType else {return}
        switch type {
        case .push:
            self.navigationController.popViewController(animated: true)
        case .present, .modal:
            self.navigationController.dismiss(animated: true, completion: nil)
        }
        if let close = self.onClose {
            close()
        }
    }
}

extension CreateRideCoordinator: CreateRideViewControllerDelegate {
    func viewController(_ viewController: CreateRideViewController, didPerformAction actionType: CreateRideActionType) {
        switch actionType {
        case .unknown:
            break
        case .didSelectAddressTo:
            self.didSelectAddress(.to)
        case .didSelectAddressFrom:
            self.didSelectAddress(.from)
        case .didSelectDate:
            self.didSelectDate()
        case .didSelectTime:
            self.presentTimePicker(.oneWayType)
        case .didSelectReturnTime:
            self.presentTimePicker(.returnRide)
        case .didSelectCar:
            self.didSelectCar()
        case .didCreateRideButtonPressed:
            self.createRide()
        case .didSetOneWayRide:
            self.viewModel?.isReturn = false
        case .didSetReturnRide:
            self.viewModel?.isReturn = true
        }
    }
    
    private func didSelectDate() {
        let selectDate = SelectDateViewController()
        selectDate.delegate = self
        self.navigationController.pushViewController(selectDate, animated: true)
        selectDate.loadSelectedDates(dates: self.viewModel?.dates)
    }
    
    private func presentTimePicker(_ type: WayType) {
        let controller = UIPickerViewController(mode: UIDatePicker.Mode.time)
        if #available(iOS 13.0, *) {
            controller.set(color: .mainGreen, invertedColor: .systemBackground)
        } else {
            controller.set(color: .mainGreen, invertedColor: .white)
        }
        controller.completition { [weak self] (time) in
            guard let self = self else {return}
            switch type {
            case .oneWayType:
                self.viewModel?.time = time
            case .returnRide:
                self.viewModel?.returnTime = time
            }
            self.tryUpdateData()
        }
        self.presentModal(vc: controller)
    }
    
    private func didSelectAddress(_ type: SearchAddressType) {
        let vc = SearchAddressController()
        vc.showingContent = .showWork
        if AppData.meData.profile?.home != "" {
            vc.showingContent = .showWorkAndHome
        }
        vc.color = .mainGreen
        vc.type = type
        vc.delegate = self
        self.navigationController.pushViewController(vc, animated: true)
    }
    
    private func createRide() {
        guard let viewModel = self.viewModel else { return }
        let validator = NewRideValidator()
        
        do {
            let isDone = try validator.validate(ride: viewModel)
            if isDone {
                let newRide = NewRide(withViewModel: viewModel,
                                      profile: AppData.meData.profile ?? User.empty())
                AppData.meData.rideList.add(newRide: newRide) { (result) in
                    switch result {
                    case .failure(let error):
                        viewModel.errorHandler?(error.convertToAppError())
                    case .success:
                        if self.isFromSearchController {
                            self.sohowActionAlert()
                        } else {
                            self.close()
                        }
                    }
                }
            }
        } catch let error {
            guard let error = error as? NewRideValidator.ValidatorError else { return }
            viewModel.errorHandler?(error.convertToAppError())
        }
    }
    
    private func sohowActionAlert() {
        let alert = UIAlertController(title: "CreateRide_success_action_title".localized(), message: "CreateRide_success_action_message".localized(), preferredStyle: .alert)
        
        let actionShowSearch = UIAlertAction(title: "CreateRide_success_action_showSearch".localized(), style: .default) {[weak self] _ in
            guard let self = self else {return}
            self.close()
        }
        
        let actionShowMeRides = UIAlertAction(title: "CreateRide_success_action_showMyRides".localized(), style: .default) {[weak self] _ in
            guard let self = self,
                let tabBarController = self.createRideViewController?.tabBarController
                else {return}
            tabBarController.selectedIndex = 1
            self.close()
        }
        
        alert.addAction(actionShowSearch)
        alert.addAction(actionShowMeRides)
        
        self.createRideViewController?.present(alert, animated: true, completion: nil)
    }
    
    private func presentModal(vc: UIViewController) {
        vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        self.navigationController.parent!.present(vc, animated: true, completion: nil)
    }

    func viewController(_ viewController: CreateRideViewController, valueDidChange: String, forRow: CreateRideRows) {
        switch forRow {
        case .addressFrom, .addressTo, .date, .time, .car:
            break
        case .returnDate, .returnTime:
            break
        case .freePlaces:
            self.viewModel?.numberOfPlaces = Int(valueDidChange) ?? -1
        case .returnNote:
            self.viewModel?.returnNote = valueDidChange
        case .note:
            self.viewModel?.note = valueDidChange
        case .returnFreePlaces:
            self.viewModel?.returnNumberOfPlaces = Int(valueDidChange) ?? -1
        case .none:
            break
        }
    }

    func exist(date: Date, inArray: [Date]) -> Bool {
        for dateInArray in inArray {
            if dateInArray.compare(date) != .orderedSame {
                return true
            }
        }
        return false
    }

    func tryUpdateData() {
        if let lat = self.viewModel?.fromLat,
            let lon = self.viewModel?.fromLon,
            let toLat = self.viewModel?.toLat,
            let toLon = self.viewModel?.toLon {

            let fromGPS = CLLocationCoordinate2D(latitude: Double(lat),
                                                 longitude: Double(lon))
            let toGPS = CLLocationCoordinate2D(latitude: Double(toLat),
                                               longitude: Double(toLon))
            self.getRoutes(from: fromGPS, to: toGPS)
        }
        if let time = self.viewModel?.time,
            let eta = self.etaTime {
            self.viewModel?.timeETA = time.addingTimeInterval(eta)
        }
        if let returnTime = self.viewModel?.returnTime,
            let eta = self.etaTime {
            self.viewModel?.returnTime = returnTime.addingTimeInterval(eta)
        }
    }

    private func getRoutes(from: CLLocationCoordinate2D,
                            to: CLLocationCoordinate2D) {
        let request = MKDirections.Request()
        request.source = MKMapItem(placemark: MKPlacemark(coordinate: from, addressDictionary: nil))
        request.destination = MKMapItem(placemark: MKPlacemark(coordinate: to, addressDictionary: nil))
        request.requestsAlternateRoutes = true
        request.transportType = .automobile
        
        let directionsTo = MKDirections(request: request)

        directionsTo.calculate { [weak self] (response, _) in
            guard let self = self else { return }
            guard var routes = response?.routes else { return }
            if (routes.count > 0) {
                routes.sort(by: {$0.expectedTravelTime < $1.expectedTravelTime})
                self.createRideViewController?.add(routes: routes)
                if let route = routes.first {
                    self.etaTime = route.expectedTravelTime
                    self.setRoute(poly: route.polyline.coordinates)
                }
            }
        }
    }
    
    private func setRoute(poly: [CLLocationCoordinate2D]) {
        let polyne = Polyline(coordinates: poly)
        self.viewModel?.encodedPolyline = polyne.encodedPolyline
    }

    private func didSelectCar() {
        guard (self.viewModel?.carSPZ) == "" else { return }

        CreateEditCarViewController().createCar(on: self.createRideViewController) { [weak self] result in
            guard let self = self else { return }
            switch result {
            case .failure(let error):
                self.viewModel?.errorHandler?(error.convertToAppError())
            case .success(let car):
                AppData.meData.profile?.car = car
                self.viewModel?.carID = car.identifier ?? -1
                self.viewModel?.carSPZ = car.spz ?? ""
                self.tryUpdateData()
            }
        }
    }
}

extension CreateRideCoordinator: SelectDateViewControllerDelegate {
    func viewController(_ viewController: SelectDateViewController, selected dates: [Date]) {
        self.viewModel?.set(dates: dates)
        self.navigationController.popViewController(animated: true)
        self.tryUpdateData()
    }
}

extension CreateRideCoordinator: SearchAddressControllerDelate {
    func viewController(_ viewController: SearchAddressController, didPerformAction: SearchAddressActionType) {
        self.navigationController.popViewController(animated: true)
        self.tryUpdateData()
    }

    func viewController(_ viewController: SearchAddressController, type: SearchAddressType, point: PointInfo) {
        switch type {
        case .from:
            self.viewModel?.fromLat = point.position.latitude
            self.viewModel?.fromLon = point.position.longitude
            self.viewModel?.from = point.title
        case .to:
            self.viewModel?.toLat = point.position.latitude
            self.viewModel?.toLon = point.position.longitude
            self.viewModel?.to = point.title
        }
        self.tryUpdateData()
        self.navigationController.popViewController(animated: true)
    }
    
    func viewController(_ viewController: CreateRideViewController, routeDidChange route: MKRoute) {
        let coordinates = route.polyline.coordinates
        self.setRoute(poly: coordinates)
        self.viewModel?.timeETA = self.viewModel?.time?.addingTimeInterval(route.expectedTravelTime)
        self.viewModel?.returnTimeEta = self.viewModel?.returnTime?.addingTimeInterval(route.expectedTravelTime)
    }
}
