//
//  SearchRideDetialCoordinator.swift
//  WorkPool
//
//  Created by Marek Labuzik on 18/06/2019.
//  Copyright © 2019 Marek Labuzik. All rights reserved.
//

import UIKit

final class SearchRideDetialCoordinator: Coordinator {
    var isVisible: Bool = false
    
    var presentationType: PresentationType?
    
    var childCoordinators: [Coordinator] = []
    var driverID: Int = -1
    var viewModel: SearchRideDetalViewModel? {
        didSet {
            self.searchRideDetailViewController?.viewModel = self.viewModel
            self.searchRideDetailViewController?.scrollTo(ride: self.initialSelectedRide)
            if self.viewModel!.errorHandler == nil {
                self.viewModel!.errorHandler = { error in
                    ErrorHandler.show(with: error, on: self.searchRideDetailViewController, completion: {_ in
                        if error == .succsess {
                            self.close()
                        }
                    })
                }
            }
        }
    }
    
    var onClose: (() -> Void)?
    
    private(set) var searchRideDetailViewController: SearchRideDetailViewController?
    private(set) var navigationController: UINavigationController
    private var initialSelectedRide: Int = 0
    
    init(_ navigationController: UINavigationController, userID: Int, rideID: Int) {
        self.navigationController = navigationController
        self.viewModel = SearchRideDetalViewModel()
        self.driverID = userID
        self.initialSelectedRide = rideID
        self.getData()
    }
    
    func start(with presentationType: PresentationType = .push) {
        self.presentationType = presentationType
        self.searchRideDetailViewController = SearchRideDetailViewController()
        self.searchRideDetailViewController?.deledate = self
        
        switch presentationType {
        case .push:
            navigationController.pushViewController(self.searchRideDetailViewController!, animated: true)
        case .present, .modal:
            navigationController.show(self.searchRideDetailViewController!, sender: nil)
        }
        self.isVisible = true
    }
    
    func close() {
        guard let type = self.presentationType else {return}
        switch type {
        case .push:
            self.navigationController.popViewController(animated: true)
        case .present, .modal:
            self.navigationController.dismiss(animated: true, completion: nil)
        }
        if let close = self.onClose {
            close()
        }
        self.isVisible = false
    }
    
    func getData() {
        let model = DetailSearchRideViewModel()
        model.getUser(with: self.driverID, completion: { [weak self] result in
            guard let self = self else { return }
            switch result {
            case .success(let model):
                self.viewModel?.setUser(user: model)
            case .failure:
                break
            }
        })
        model.getUserRides(with: self.driverID, completion: { [weak self] result in
            guard let self = self else { return }
            switch result {
            case .success(let model):
                self.viewModel?.setRides(rides: model)
            case .failure:
                break
            }
        })
    }

}

extension SearchRideDetialCoordinator: SearchRideDetailViewControllerDelegate {
    func viewController(_ viewController: SearchRideDetailViewController, selectCellAt indexPath: IndexPath) {
        self.viewModel?.select(at: indexPath)
    }
    
    func viewController(_ viewController: SearchRideDetailViewController, didPerform action: SearchRideDetailActionType) {
        switch action {
        case .registerRidesFromList:
            guard let model = self.viewModel else { return }
            var succsessIndexis: [IndexPath] = []
            var failureIndexis: [IndexPath] = []
            let rides = model.getSelectedRides()
            for ride in rides {
                guard let indexPath = model.getIndexPathFor(rideID: ride.model.identifier) else { return                }
                self.viewModel?.setRideRequestResult(.pending, at: indexPath)
                ride.registerRide { [weak self] result in
                    guard let self = self else { return }
                    switch result {
                    case .success:
                        self.viewModel?.setRideState(.reserved, at: indexPath)
                        self.viewModel?.setRideRequestResult(.success, at: indexPath)
                        succsessIndexis.append(indexPath)
                    case .failure(let error):
                        self.viewModel?.setRideRequestResult(.failure, at: indexPath)
                        if error == .dataIsExists {
                            self.viewModel?.setRideState(.full, at: indexPath)
                        }
                        failureIndexis.append(indexPath)
                    }
                
                    ///clean array
                    if ride.model.identifier == rides.last?.model.identifier {
                        if succsessIndexis.count == rides.count { ///succsess
                            model.errorHandler?(.succsess)
                        } else if succsessIndexis.count + failureIndexis.count == rides.count { /// failure
                            model.errorHandler?(.retry)
                        }
                    }
                }
            }
        }
    }
}
