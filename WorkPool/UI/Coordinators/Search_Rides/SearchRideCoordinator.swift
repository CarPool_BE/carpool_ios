//
//  SearchRideCoordinator.swift
//  WorkPool
//
//  Created by Marek Labuzik on 13/05/2019.
//  Copyright © 2019 Marek Labuzik. All rights reserved.
//

import UIKit

final class SearchRideCoordinator: NSObject, Coordinator {
    var presentationType: PresentationType?
    
    var onClose: (() -> Void)?
    
    var childCoordinators: [Coordinator] = []
    
    var isVisible: Bool = false
    private var createCoordinator: CreateRideCoordinator?
    private var datailCoordinator: SearchRideDetialCoordinator?

    private(set) var searchRideViewController: SearchRideViewController?
    private(set) var navigationController: UINavigationController?

    private var viewModel: RidesViewModel? {
        didSet {
            searchRideViewController?.viewModel = viewModel
        }
    }
    
    func start() -> UINavigationController {
        self.searchRideViewController = SearchRideViewController()

        let rideList = AppData.meData.rideList
        self.viewModel = RidesViewModel(rideList: rideList)

        searchRideViewController?.viewModel = viewModel
        searchRideViewController?.delegate = self
        searchRideViewController?.tabBarItem = UITabBarItem(title: "TabBar_SearchRides".localized(),
                                                            image: UIImage(named: "TabSearch"),
                                                            tag: 0)
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(updateContext),
                                               name: NSNotification.Name(rawValue: "UserProfileUpdate"),
                                               object: nil)
        
        navigationController = UINavigationController(rootViewController: searchRideViewController!)
        self.navigationController?.delegate = self
        return navigationController!
    }

    // MARK: - actions
    @objc func updateContext() {
        let rideList = AppData.meData.rideList
        self.viewModel = RidesViewModel(rideList: rideList)
        self.getData()
    }
    
    func getData() {
        self.searchRideViewController?.startAnimating()
        self.viewModel?.getRides { [weak self](result) in
            guard let self = self else { return }
            switch result {
            case .success:
                self.searchRideViewController?.reloadData()
            case .failure:
                self.searchRideViewController?.reloadData()
            }
            self.searchRideViewController?.stopAnimating()
        }
        self.searchRideViewController?.reloadData()
    }
}

// MARK: Search Ride View Controller Delegate

extension SearchRideCoordinator: SearchRideViewControllerDelegate {
    func viewController(_ viewController: SearchRideViewController, showDetailFor userID: Int, andRide: Int) {
        self.datailCoordinator?.onClose?()
        self.datailCoordinator = SearchRideDetialCoordinator(self.navigationController!, userID: userID, rideID: andRide)
        self.childCoordinators.append(self.datailCoordinator!)
        self.datailCoordinator?.onClose = { [weak self] in
            guard let self =  self else {return}
            if let idx = self.childCoordinators.firstIndex(where: { $0 === self.datailCoordinator }) {
                self.childCoordinators.remove(at: idx)
                self.datailCoordinator = nil
            }
        }
        self.datailCoordinator?.start()
    }
    
    func viewController(_ viewController: SearchRideViewController, didUpdate date: Date) {
        self.viewModel?.searchContext.date = date
        self.getData()
    }

    func viewController(_ viewController: SearchRideViewController, didPerformAction actionType: SearchRideActionType) {
        switch actionType {
        case .createRide:
            guard let navigationController = navigationController else {
                preconditionFailure("Nemam navigation")
            }

            createCoordinator?.onClose?()

            createCoordinator = CreateRideCoordinator(navigationController)
            createCoordinator?.isFromSearchController = true
            childCoordinators.append(createCoordinator!)
            createCoordinator?.onClose = { [weak self] in
                guard let self =  self else {return}
                if let idx = self.childCoordinators.firstIndex(where: { $0 === self.createCoordinator }) {
                    self.childCoordinators.remove(at: idx)
                    self.createCoordinator = nil
                }
            }
            createCoordinator?.start()
//            createCoordinator?.updateViewModel(date: self.viewModel?.searchContext.date ?? Date())
        case .getNewData:
            self.getData()
        case .didSelectAddressFrom:
            let vc = SearchAddressController()
            vc.showingContent = .showWork
            if AppData.meData.profile?.home != "" {
                vc.showingContent = .showWorkAndHome
            }
            vc.delegate = self
            vc.type = .from
            self.navigationController?.pushViewController(vc, animated: true)
        case .didSelectAddressTo:
            let vc = SearchAddressController()
            vc.showingContent = .showWork
            if AppData.meData.profile?.home != "" {
                vc.showingContent = .showWorkAndHome
            }
            vc.delegate = self
            vc.type = .to
            self.navigationController?.pushViewController(vc, animated: true)
        case .didSelectReverse:
            if let model = self.viewModel {
                let value = model.searchContext.from
                let gpsValue = model.searchContext.fromGps
                model.searchContext.from = model.searchContext.to
                model.searchContext.fromGps = model.searchContext.toGps
                model.searchContext.to = value
                model.searchContext.toGps = gpsValue
                self.searchRideViewController?.updeteHeader()
                self.getData()
            }
        }
    }
}

extension SearchRideCoordinator: SearchAddressControllerDelate {
    func viewController(_ viewController: SearchAddressController, type: SearchAddressType, point: PointInfo) {
        switch type {
        case .from:
            self.viewModel?.searchContext.fromGps = point.position
            self.viewModel?.searchContext.from = point.title
            let newSearch = LastSearchAdress(title: point.title, gpsPosition: point.position)
            AppData.meData.lastSearch = newSearch
        case .to:
            self.viewModel?.searchContext.toGps = point.position
            self.viewModel?.searchContext.to = point.title
        }

        self.searchRideViewController?.updeteHeader()
        self.navigationController?.popViewController(animated: true)
    }

    func viewController(_ viewController: SearchAddressController, didPerformAction: SearchAddressActionType) {
        self.navigationController?.popViewController(animated: true)
    }
}

extension SearchRideCoordinator: UINavigationControllerDelegate {

    func navigationController(_ navigationController: UINavigationController, willShow viewController: UIViewController, animated: Bool) {
        if viewController is SearchRideViewController && self.createCoordinator != nil {
            self.createCoordinator?.onClose?()
        }
        if viewController is MeRidesViewController && self.datailCoordinator != nil {
            self.datailCoordinator?.onClose?()
        }
    }
}
