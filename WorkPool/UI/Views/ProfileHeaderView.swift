//
//  ProfileHeaderView.swift
//  WorkPool
//
//  Created by Martin Kurbel on 20/06/2019.
//  Copyright © 2019 Marek Labuzik. All rights reserved.
//

import UIKit

final class ProfileHeaderView: UIView {
    
    private let profileImage = UIImageView()
    private let nameLabel = UILabel()
    private let changePictureButton = UIButton()
    
    var user: User? = nil {
        didSet {
            nameLabel.text = user?.name
        }
    }
    
    var isEditing = false {
        didSet {
//            changePictureButton.isHidden = !isEditing
            nameLabel.isHidden = isEditing
        }
    }
    
    var onButtonTap: (() -> Void)?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initialize()
        setConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func initialize() {
        
        nameLabel.font = UIFont.systemFont(ofSize: 17, weight: .medium)
        
        profileImage.backgroundColor = .gray
        profileImage.clipsToBounds = true
        profileImage.layer.cornerRadius = 34
        profileImage.image = UIImage(named: "profilePlaceholder")
        
        changePictureButton.isHidden = true
        changePictureButton.titleLabel?.font = UIFont.systemFont(ofSize: 15, weight: .medium)
        changePictureButton.setTitleColor(Color.buttonBlue, for: .normal)
        changePictureButton.setTitle("Change_Photo".localized(), for: .normal)
        
        changePictureButton.addTarget(self, action: #selector(didTapButton), for: .touchUpInside)
    }
    
    private func setConstraints() {
        
        self.addSubview(profileImage)
        profileImage.snp.makeConstraints { (make) in
            make.centerY.equalToSuperview()
            make.left.equalToSuperview().offset(32)
            make.width.height.equalTo(68)
        }
        
        self.addSubview(nameLabel)
        nameLabel.snp.makeConstraints { (make) in
            make.centerY.equalToSuperview()
            make.left.equalTo(profileImage.snp.right).offset(16)
            make.right.equalToSuperview().inset(16)
        }
        
        self.addSubview(changePictureButton)
        changePictureButton.snp.makeConstraints { (make) in
            make.centerY.equalToSuperview()
            make.left.equalTo(profileImage.snp.right).offset(16)
            make.right.equalToSuperview().inset(16)
        }
    }
    
    @objc private func didTapButton() {
        self.onButtonTap?()
    }
}
