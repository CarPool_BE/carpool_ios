//
//  ProfileEditCell.swift
//  WorkPool
//
//  Created by Martin Kurbel on 21/06/2019.
//  Copyright © 2019 Marek Labuzik. All rights reserved.
//

import UIKit

class ProfileEditCell: UITableViewCell {
    
    let textField = UITextField()
    
    var placeholder: String = "" {
        didSet {
            textField.placeholder = placeholder
        }
    }
    
    var dataText: String? = "" {
        didSet {
            textField.text = dataText
        }
    }
    
    var onTextChange: ((String) -> Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        initialize()
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        initialize()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func initialize() {
        if #available(iOS 13.0, *) {
            self.backgroundColor = .secondarySystemBackground
        } 
        textField.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
        textField.clearButtonMode = .whileEditing
        self.contentView.addSubview(textField)
        
        textField.snp.makeConstraints { (make) in
            make.top.bottom.equalToSuperview()
            make.left.right.equalToSuperview().inset(16)
        }
    }
    
    @objc private func textFieldDidChange() {
        self.onTextChange?(self.textField.text ?? "")
    }
}
