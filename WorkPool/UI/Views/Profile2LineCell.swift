//
//  Profile2LineCell.swift
//  WorkPool
//
//  Created by Martin Kurbel on 20/06/2019.
//  Copyright © 2019 Marek Labuzik. All rights reserved.
//

import UIKit

final class Profile2LineCell: UITableViewCell {
    
    private let infoLabel = WLabel.init(style: .medium, size: 13, color: Color.placeholder)
    private let dataTextLabel = WLabel.init(style: .medium, size: 17)
    
    var placeholder: String = "" {
        didSet {
            infoLabel.text = placeholder
        }
    }
    
    var dataText: String? = "" {
        didSet {
            dataTextLabel.text = dataText
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        initialize()
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        initialize()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func initialize() {
        
        self.selectionStyle = .none
        
        if #available(iOS 13.0, *) {
            self.dataTextLabel.textColor = .label
            self.infoLabel.textColor = .secondaryLabel
            self.backgroundColor = .secondarySystemBackground
        }
        self.contentView.addSubview(infoLabel)
        infoLabel.snp.makeConstraints { (make) in
            make.left.right.equalToSuperview().inset(16)
            make.top.equalToSuperview().offset(14)
        }
        
        self.contentView.addSubview(dataTextLabel)
        dataTextLabel.snp.makeConstraints { (make) in
            make.left.right.equalToSuperview().inset(16)
            make.height.greaterThanOrEqualTo(20)
            make.top.equalTo(infoLabel.snp.bottom).offset(2)
            make.bottom.equalToSuperview().inset(8)
        }
    }
}
