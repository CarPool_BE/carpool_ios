//
//  ActivityView.swift
//  WorkPool
//
//  Created by Marek Labuzik on 12/07/2019.
//  Copyright © 2019 Marek Labuzik. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class ActivityView: UIView {
    var activityIndicatorView: NVActivityIndicatorView?
    
    func startAnimating() {
        self.backgroundColor = .black20
        let activitySize = CGSize(width: 40, height: 40)
        let activityPoint = CGPoint(x: (self.frame.width - activitySize.width) / 2,
                                    y: (self.frame.height - activitySize.height) / 2 )
        self.activityIndicatorView = NVActivityIndicatorView(frame: CGRect(origin: activityPoint, size: activitySize),
                                                             type: MeAppearance.activityIndicatorType,
                                                             color: .mainGreen,
                                                             padding: nil)
        self.activityIndicatorView?.startAnimating()
        self.addSubview(self.activityIndicatorView!)
    }
    
    func stopAnimating() {
        self.activityIndicatorView?.stopAnimating()
        self.activityIndicatorView?.removeFromSuperview()
        self.activityIndicatorView = nil
    }
}
