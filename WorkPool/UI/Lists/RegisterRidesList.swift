//
//  RegisterRidesList.swift
//  WorkPool
//
//  Created by Marek Labuzik on 19/06/2019.
//  Copyright © 2019 Marek Labuzik. All rights reserved.
//

import Foundation

final class RegisterRidesList {
    var list: RegisterRideViewModel
    
    init(with list: RegisterRideViewModel) {
        self.list = list
    }
    
    func addRegisterMeToRides(completion: @escaping (Result<RegisterRideViewModel, RequestErrors>) -> Void) {
        for registerRide in self.list.rideIds {
            
            AppData.meData.auth.rideService.registerToRide(with: registerRide) { [weak self] result in
                guard let self = self else { return }
                switch result {
                case .success:
                    self.list.succuessRegistrationRidesIds.append(registerRide)
                case .failure:
                    self.list.failedRegistrationRidesIds.append(registerRide)
                }
                if self.list.succuessRegistrationRidesIds.count == self.list.rideIds.count {
                    completion(.success(self.list))
                } else if self.list.failedRegistrationRidesIds.count + self.list.succuessRegistrationRidesIds.count == self.list.rideIds.count {
                    completion(.success(self.list))
                }
            }
        }
    }

}
