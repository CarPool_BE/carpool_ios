//
//  RideList.swift
//  WorkPool
//
//  Created by Marek Labuzik on 11/05/2019.
//  Copyright © 2019 Marek Labuzik. All rights reserved.
//

import Foundation
import CoreLocation

final class RideList {
    enum RideListError: LocalizedError {
        case failedToGetRides(from: String, fromGps: CLLocationCoordinate2D, toGps: CLLocationCoordinate2D, date: Date, error: Error)
        
        var localizedDescription: String {
            switch self {
            case .failedToGetRides(from: let from, fromGps: let fromGps, toGps: let toGps, date: let date, error: let error):
                return "Failed to fetch rides from \(from) gps form \(fromGps) to \(toGps) date \(date), reason: \(error)"
            }
        }
    }

    func getRides(from: String,
                  to: String,
                  fromGps: CLLocationCoordinate2D,
                  toGps: CLLocationCoordinate2D,
                  date: Date,
                  completion: @escaping (Result<Rides, RequestErrors>) -> Void) {
        AppData.meData.auth.rideService.getRides(from: from,
                                                 to: to,
                                                 fromGps: fromGps,
                                                 toGps: toGps,
                                                 date: date,
                                                 completion: { result in
                                                    switch result {
                                                    case .success(let rides):
                                                        completion(.success(rides))
                                                    case .failure(let error):
//                                                        let err = RideListError.failedToGetRides(from: from,
//                                                                                                   fromGps: fromGps,
//                                                                                                   toGps:toGps,
//                                                                                                   date: date,
//                                                                                                   error: error)
                                                        completion(.failure(error))
                                                    }
        })
    }

    func add(newRide: NewRide, completion: @escaping (Result<Bool, RequestErrors>) -> Void) {
        AppData.meData.auth.rideService.add(newRide: newRide) { result in
            switch result {
            case let .failure(err):
                completion(.failure(err))
            case let .success(resp):
                completion(.success(resp))
            }
        }
    }
}
