//
//  WebContent
//  WorkPool
//
//  Created by Marek Labuzik on 15/08/2019.
//  Copyright © 2019 Marek Labuzik. All rights reserved.
//

import UIKit
import WebKit

enum PresentingPage {
    case policy
    case eula
    
    var url: URL {
        get {
            switch self {
            case .policy:
                return URL(string: "https://www.teamride.eu/privacy.html")!
            case .eula:
                return URL(string: "https://www.teamride.eu/eula.html")!
            }
        }
    }
    
    var title: String {
        get {
            switch self {
            case .policy:
                return "PRIVACY POLICY"
            case .eula:
                return "TERMS OF SERVICE"
            }
        }
    }
}

final class WebContent: UIViewController, WKNavigationDelegate {
    var webView: WKWebView?
    var page: PresentingPage?
    var activityView: ActivityView?
    
    override func loadView() {
        super.loadView()
        self.webView = WKWebView()
        self.webView?.navigationDelegate = self
        self.view = self.webView
        
        self.activityView = ActivityView(frame: self.view.bounds)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        guard let webView = self.webView,
            let page = self.page
            else { return }
        
        let url = page.url
        webView.load(URLRequest(url: url))
        self.title = page.title
        self.activityView?.startAnimating()
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        self.activityView?.stopAnimating()
    }
}
