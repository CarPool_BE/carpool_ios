//
//  LoginViewController.swift
//  WorkPool
//
//  Created by Marek Labuzik on 31/05/2019.
//  Copyright © 2019 Marek Labuzik. All rights reserved.
//

import UICustomElements
import UIKit
import NVActivityIndicatorView

enum LoginActionType {
    case registerButtonPressed
    case loginButtonPressed
    case showPrivacyPolicy
}

protocol LoginViewControllerDelegate: class {
    func viewController(_ viewController: LoginViewController, didPerformAction: LoginActionType)
    func viewController(_ viewController: LoginViewController, valueDidChange: String, forRow: LoginTableViewModel)
}

final class LoginViewController: UITableViewController, UILoginCellDelegate {
    weak var delegate: LoginViewControllerDelegate?
    var segmentControll: UISegmentedControl?
    var activityIndicatorView: ActivityView?
    public var model: LoginViewModel?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupTableView()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setupSegmentControll()
    }

    func setupTableView() {
        self.tableView.rowHeight = 44
        self.tableView.register(UILoginCell.self, forCellReuseIdentifier: "UILoginCell")
        self.tableView.separatorInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 16)
        if #available(iOS 11, *) {
            self.tableView.separatorInsetReference = .fromAutomaticInsets
        }
        tableView.reloadData()
    }

    func setupSegmentControll() {
        let segmentItems = ["Login".localized(), "Register".localized()]
        self.segmentControll = UISegmentedControl(items: segmentItems)
        self.segmentControll?.selectedSegmentIndex = 0
        self.segmentControll?.tintColor = .mainGreen
        self.segmentControll?.addTarget(self,
                                        action: #selector(segmentControllDidChange(segmentControll:)),
                                        for: .valueChanged)
        if let navBar = self.navigationController?.navigationBar {
            navBar.topItem?.titleView = self.segmentControll
        }
    }
    
    func startAnimating() {
        if self.activityIndicatorView == nil {
            self.activityIndicatorView = ActivityView(frame: self.view.bounds)
            self.activityIndicatorView?.startAnimating()
            self.view.addSubview(self.activityIndicatorView!)
        }
    }
    
    func stopAnimating() {
        self.activityIndicatorView?.stopAnimating()
        self.activityIndicatorView?.removeFromSuperview()
        self.activityIndicatorView = nil
    }

    func isShowingLogin() -> Bool {
        if self.segmentControll?.selectedSegmentIndex == 0 {
            return true
        } else {
            return false
        }
    }

    @objc func segmentControllDidChange(segmentControll: UISegmentedControl) {
        self.tableView.reloadData()
    }

    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        guard let model = self.model else { return 88 }
        return CGFloat(model.getGetHeightInsetForTable(isLogin: self.isShowingLogin()))
    }

    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return UIView(frame: CGRect(origin: .zero,
                                    size: CGSize(width: self.tableView.frame.width, height: 80)))
    }

    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 96
    }

    override func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let view = UIView(frame: CGRect(origin: CGPoint.zero,
                                        size: CGSize(width: UIScreen.main.bounds.width,
                                                     height: 96)))
        let button = UICustomButton(frame: CGRect(x: 16,
                                                  y: 8,
                                                  width: view.frame.width - 32,
                                                  height: view.frame.height - 32))
        
        let privacyPolicyLabel = UILabel(frame: CGRect(x: 16, y: button.frame.height + 12, width: view.frame.width - 32, height: view.frame.height - button.frame.height))
        privacyPolicyLabel.numberOfLines = 0
        privacyPolicyLabel.textAlignment = .center
        privacyPolicyLabel.lineBreakMode = .byWordWrapping
        let fromatedString = String(format: "%@%@", "PrivacyPolicy_text".localized(), "PrivacyPolicy_Link_policy".localized())
        let atributedText = NSMutableAttributedString(string: fromatedString)
        let buttonRange = (fromatedString as NSString).range(of: "PrivacyPolicy_Link_policy".localized())
        atributedText.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.mainGreen, range: buttonRange)
        atributedText.addAttribute(NSAttributedString.Key.font, value: UIFont.systemFont(ofSize: 13, weight: UIFont.Weight.light), range: NSRange(location: 0, length: atributedText.length))
        privacyPolicyLabel.attributedText = atributedText
        
        let gesture: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(textDidPressed))
        gesture.numberOfTapsRequired = 1
        privacyPolicyLabel.isUserInteractionEnabled = true
        privacyPolicyLabel.addGestureRecognizer(gesture)

        button.addTarget(self, action: #selector(loginButtonPressed), for: .touchUpInside)
        button.set(color: .mainGreen, invertedColor: .white)
        if let model = self.model {
            button.setButtonType(type: model.getButtonType(isLogin: self.isShowingLogin()))
        } else {
            button.setButtonType(type: .ok)
        }
        view.addSubview(button)
        view.addSubview(privacyPolicyLabel)

        return view
    }

    @objc func textDidPressed() {
        self.delegate?.viewController(self, didPerformAction: .showPrivacyPolicy)
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let model = self.model else { return 0 }
        return model.getNumberOfRows(isLogin: self.isShowingLogin())
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "UILoginCell") as? UILoginCell else {
            fatalError("Misconfigured cell type, UISearchLocationCell!")
        }
        let row = LoginTableViewModel(rawValue: indexPath.row)!
        cell.setTintColor(tintColor: .mainGreen)
        cell.setPlaceholder(str: row.getPlaceholder())
        cell.setCapitalization(type: row.getCapitalizationType())
        cell.setSecurityEntry(isSecure: row.isSecureTextfield())
        cell.setMax(length: row.getMaxLenght())
        cell.setKeyboard(type: row.getKeyboardType())

        #if DEBUG
        if row == .email {
            cell.textField.text = model?.email
        } else if row == .password {
            cell.textField.text = model?.password
        }

        if segmentControll?.selectedSegmentIndex == 1 {
            if row == .retypePassword {
                cell.textField.text = model?.password
            }
        }
        #endif
        cell.delegate = self
        cell.tag = indexPath.row
        cell.selectionStyle = .none
        if indexPath.row == 0 {
            cell.textField.resignFirstResponder()
            cell.textField.becomeFirstResponder()
        }
        return cell
    }

    @objc func loginButtonPressed() {
        if self.isShowingLogin() {
            self.delegate?.viewController(self, didPerformAction: .loginButtonPressed)
        } else {
            self.delegate?.viewController(self, didPerformAction: .registerButtonPressed)
        }
    }

    func customField(_ customField: UILoginCell, valueDidChange: String) {
        let row = LoginTableViewModel(rawValue: customField.tag)!
        self.delegate?.viewController(self, valueDidChange: valueDidChange, forRow: row)
    }
}
