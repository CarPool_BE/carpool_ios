//
//  MeProfileViewController.swift
//  WorkPool
//
//  Created by Marek Labuzik on 09/05/2019.
//  Copyright © 2019 Marek Labuzik. All rights reserved.
//

import UIKit
import SnapKit
import UICustomElements

enum MeProfileActionType {
    case logout
}

protocol MeProfileViewControllerDelegate: class {
    func viewController(_ viewController: MeProfileViewController, didPerform action: MeProfileActionType)
    func viewController(_ viewController: MeProfileViewController, setDefatulScreen index: Int)
}

final class MeProfileViewController: UITableViewController {
    
    weak var profileDelegate: MeProfileViewControllerDelegate?
    var userData: User?
    var activityView: ActivityView?

    var inEditMode = false {
        didSet {
            self.tableView.reloadData()
            headerView.isEditing = inEditMode
        }
    }
    
    private let headerView = ProfileHeaderView()
    
    private var sections = [MeProfileSection]()
    private var rowsAboutMe = [MeProfileInfo]()
    private var rowsVehicle = [MeProfileVehicleInfo]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        setupData()
        setupNavigationBar()
        setupTableView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        setupData()
        self.tableView.reloadData()
    }
    
    private func setupView() {
        if #available(iOS 13.0, *) {
            self.tableView.backgroundColor = .systemBackground
        }
    }
    
    private func setupData() {
        
        userData = AppData.meData.profile
        if userData?.car == nil {
            userData?.car = Car()
        }
        headerView.user = userData
        
        sections = [.aboutMe, .vehicleInfo, .defaultScreen, .logout]
//        rowsAboutMe = [.phoneNumber, .address, .email, .cdsid]
        rowsAboutMe = [.phoneNumber, .address, .email]
        rowsVehicle = MeProfileVehicleInfo.allCases
    }
    
    private func setupNavigationBarAppearance() {
        
        let navBar = self.navigationController?.navigationBar
        
        navBar?.backgroundColor = .white
        navBar?.isTranslucent = false
        navBar?.setBackgroundImage(UIImage(), for: .default)
        navBar?.shadowImage = UIImage()
    }
    
    private func setupNavigationBar() {
        
        let editNavButton = UIBarButtonItem(title: "Edit_Button".localized(), style: .plain, target: self, action: #selector(edit))
        editNavButton.tintColor = Color.green
        self.navigationItem.rightBarButtonItem = editNavButton
    }
    
    private func setupTableView() {
        
        self.tableView.tableFooterView = UIView()
        let frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: 100)
        headerView.frame = frame
        self.tableView.tableHeaderView = headerView
        
        weak var welf = self
        headerView.onButtonTap = {
            welf?.changeProfilePhoto()
        }
        
        self.tableView.register(Profile2LineCell.self, forCellReuseIdentifier: "Profile2LineCell")
        self.tableView.register(ProfileEditCell.self, forCellReuseIdentifier: "ProfileEditCell")
        self.tableView.register(UITableViewCell.self, forCellReuseIdentifier: "UITableViewCell")
        self.tableView.register(UISwitcherCell.self, forCellReuseIdentifier: "UISwitcherCell")
    }
    
    // Configure cells
    private func configureCell(_ cell: UITableViewCell, at indexPath: IndexPath) {
        
        switch sections[indexPath.section] {
        case .aboutMe:
            setupAboutCells(cell, row: indexPath.row)
        case .vehicleInfo:
            setupVehicleCells(cell, row: indexPath.row)
        case .defaultScreen:
            self.setupDefaultScreenCells(cell, row: indexPath.row)
            break
        case .logout:
            cell.textLabel?.font = UIFont.systemFont(ofSize: 13)
            cell.textLabel?.text = "logout".localized()
            
            if #available(iOS 13.0, *) {
                cell.textLabel?.textColor = .secondaryLabel
                cell.backgroundColor = .secondarySystemBackground
            } else {
                cell.textLabel?.textColor = Color.placeholder
            }
        }
    }
    
    private func setupDefaultScreenCells(_ cell: UITableViewCell, row: Int) {
        if let cell = cell as? UISwitcherCell {
            cell.setSelectedSegmetn(ad: AppData.meData.getDefaultScreen())
        }
    }
    
    private func setupAboutCells(_ cell: UITableViewCell, row: Int) {
        weak var welf = self
        if let cell = cell as? Profile2LineCell {
            cell.placeholder = rowsAboutMe[row].localizedPlaceholder
            
            switch rowsAboutMe[row] {
            case .phoneNumber:
                cell.dataText = userData?.phoneNumber
            case .address:
                cell.dataText = userData?.home
            case .email:
                cell.dataText = userData?.email
//            case .cdsid:
//                if let id = userData?.cdsid {
//                    cell.dataText = String(id)
//                }
            default:
                break
            }
        }
        
        if let cell = cell as? ProfileEditCell {
            cell.placeholder = rowsAboutMe[row].localizedPlaceholder
            
            switch rowsAboutMe[row] {
            case .name:
                cell.dataText = userData?.name
                cell.onTextChange = { text in
                    welf?.userData?.name = text
                }
            case .phoneNumber:
                cell.dataText = userData?.phoneNumber
                cell.onTextChange = { text in
                    welf?.userData?.phoneNumber = text
                }
            case .address:
                cell.textField.isUserInteractionEnabled = false
                cell.accessoryType = .disclosureIndicator
                cell.dataText = userData?.home
                cell.onTextChange = { text in
                    welf?.userData?.home = text
                }
            case .email:
                cell.dataText = userData?.email
                cell.onTextChange = { text in
                    welf?.userData?.email = text
                }
            }
        }
    }
    
    private func setupVehicleCells(_ cell: UITableViewCell, row: Int) {
        
        weak var welf = self
        if let cell = cell as? Profile2LineCell {
            cell.placeholder = rowsVehicle[row].localizedPlaceholder
            
            switch rowsVehicle[row] {
            case .licensePlate:
                cell.dataText = userData?.car?.spz
            case .vehicleColor:
                cell.dataText = userData?.car?.color
            case .vehicleBrand:
                cell.dataText = userData?.car?.brand
            }
        }
        
        if let cell = cell as? ProfileEditCell {
            cell.placeholder = rowsVehicle[row].localizedPlaceholder
            
            switch rowsVehicle[row] {
            case .licensePlate:
                cell.dataText = userData?.car?.spz
                cell.onTextChange = { text in
                    welf?.userData?.car?.spz = text
                }
            case .vehicleColor:
                cell.dataText = userData?.car?.color
                cell.onTextChange = { text in
                    welf?.userData?.car?.color = text
                }
            case .vehicleBrand:
                cell.dataText = userData?.car?.brand
                cell.onTextChange = { text in
                    welf?.userData?.car?.brand = text
                }
            }
        }
    }
    
    @objc private func edit() {
        
        if inEditMode {
            
            guard let user = userData else { return }
            let validator = ProfileValidator()
            do {
                let isValid = try validator.validate(updating: user)
                if isValid {
                    self.startAnimating()
                    AppData.meData.auth.profileService.updateMy(profile: user) { [weak self] result in
                        guard let self = self else { return }
                        switch result {
                        case .success:
                            let oldProfile = AppData.meData.profile
                            AppData.meData.profile = self.userData
                            AppData.meData.profile?.car = oldProfile?.car
                            self.endEdit()
                        case .failure(let error):
                            print(error)
                        }
                    }
                    guard let car = user.car else { return }
                    self.tryUpdateOrCreate(car: car)
                }
            } catch let error {
                if let err = error as? ProfileValidator.ProfileValidatorError {
                    let alert = UIAlertController.alert(withTitle: "Validate_Error_title".localized(),
                                                        subtitle: err.localizedError,
                                                        buttonTitle: "OK_Button".localized())
                    self.present(alert, animated: true, completion: nil)
                }
            }
        }
        
        let endEditNavButton = UIBarButtonItem(title: "Cancel_Button".localized(), style: .plain, target: self, action: #selector(endEdit))
        self.navigationItem.leftBarButtonItem = endEditNavButton
        
        self.navigationItem.rightBarButtonItem?.title = "Save_Button".localized()
        
        sections = [.aboutMe, .vehicleInfo]
        rowsAboutMe = [.name, .phoneNumber, .email, .address]
        
        inEditMode = true
    }

    func tryUpdateOrCreate(car: Car) {
        if car.identifier == -1 {
            AppData.meData.auth.carService.add(car: car) { result in
                switch result {
                case .success:
                    AppData.meData.profile?.car = car
                case .failure: break
                }
            }
        } else {
            AppData.meData.auth.carService.update(car: car) { result in
                switch result {
                case .success:
                    AppData.meData.profile?.car = car
                case .failure: break
                }
            }
        }
    }
    
    @objc private func endEdit() {
        setNavBarToDefault()
        headerView.user = userData
        sections = [.aboutMe, .vehicleInfo, .logout]
//        rowsAboutMe = [.phoneNumber, .address, .email, .cdsid]
        rowsAboutMe = [.phoneNumber, .address, .email]
        self.stopAnimating()
        inEditMode = false
        self.tableView.reloadData()
    }
    
    private func setNavBarToDefault() {
        self.navigationItem.rightBarButtonItem?.title = "Edit_Button".localized()
        self.navigationItem.leftBarButtonItem = nil
    }
    
    private func changeProfilePhoto() {
        
    }
    
    private func showAddressVC() {
        let vc = SearchAddressController()
        vc.showingContent = .showWork
        if AppData.meData.profile?.home != "" {
            vc.showingContent = .showWorkAndHome
        }
        vc.color = .mainGreen
        vc.delegate = self
        vc.type = .from
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    private func logoutUser() {
        AppData.meData.auth.logout { result in
            switch result {
            case .success:
                self.profileDelegate?.viewController(self, didPerform: .logout)
            case .failure:
                break
            }
        }
    }

    func startAnimating() {
        if self.activityView == nil {
            self.activityView = ActivityView(frame: self.tableView.bounds)
            self.activityView?.startAnimating()
            self.tableView.addSubview(self.activityView!)
        }
    }

    func stopAnimating() {
        self.activityView?.stopAnimating()
        self.activityView?.removeFromSuperview()
        self.activityView = nil
    }
}

extension MeProfileViewController {
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        if self.isEditing {
            return sections.count - 2
        }
        return sections.count
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch sections[section] {
        case .aboutMe:
            return self.rowsAboutMe.count
        case .vehicleInfo:
            return self.rowsVehicle.count
        case .defaultScreen:
            return 1
        case .logout:
            return 1
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 32
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return sections[section].localizedTitle
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cellType = inEditMode ? "ProfileEditCell" : "Profile2LineCell"
        
        switch sections[indexPath.section] {
        case .aboutMe:
            return tableView.dequeueReusableCell(withIdentifier: cellType, for: indexPath)
        case .vehicleInfo:
            return tableView.dequeueReusableCell(withIdentifier: cellType, for: indexPath)
        case .defaultScreen:
            let cell = self.tableView.dequeueReusableCell(withIdentifier: "UISwitcherCell", for: indexPath) as! UISwitcherCell
            cell.delegate = self
            return cell
        default:
            return tableView.dequeueReusableCell(withIdentifier: "UITableViewCell", for: indexPath)
        }
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        self.configureCell(cell, at: indexPath)
    }
    
    override func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        if let headerView = view as? UITableViewHeaderFooterView {
            headerView.textLabel?.font = UIFont.systemFont(ofSize: 13)
            if #available(iOS 13.0, *) {
                headerView.textLabel?.textColor = .secondaryLabel
                headerView.backgroundColor = .secondarySystemBackground
            } else {
                headerView.textLabel?.textColor = Color.placeholder
            }
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        switch sections[indexPath.section] {
        case .aboutMe:
            if rowsAboutMe[indexPath.row] == .address {
                if self.inEditMode {
                    showAddressVC()
                }
            }
        case .logout:
            self.logoutUser()
        default:
            break
        }
    }
}

extension MeProfileViewController: SearchAddressControllerDelate {
    func viewController(_ viewController: SearchAddressController, didPerformAction: SearchAddressActionType) {
         self.navigationController?.popViewController(animated: true)
    }
    
    func viewController(_ viewController: SearchAddressController, type: SearchAddressType, point: PointInfo) {
        AppData.meData.profile?.home = point.title
        AppData.meData.profile?.sethHomeCoordinates(point.position)
        self.navigationController?.popViewController(animated: true)
    }
}

extension MeProfileViewController: UISwitcherCellDelegate {
    func switcherCell(_ switcherCell: UISwitcherCell, switchTo: Int) {
        self.profileDelegate?.viewController(self, setDefatulScreen: switchTo)
    }
}

enum MeProfileSection: Int, CaseIterable {
    case aboutMe
    case vehicleInfo
    case logout
    case defaultScreen
    
    var localizedTitle: String? {
        switch self {
        case .aboutMe:
            return "My_informations".localized()
        case .vehicleInfo:
            return "Vehicle".localized()
        case .defaultScreen:
            return "DefaultScreen_title".localized()
        case .logout:
            return nil
        }
    }
}

enum DefaultScreen: Int, CaseIterable {
    case switcher
}

enum MeProfileInfo: Int, CaseIterable {
    case name
    case phoneNumber
    case address
    case email
//    case cdsid
    
    var localizedPlaceholder: String {
        switch self {
        case .name:
            return "Meno".unLocalized()
        case .phoneNumber:
            return "Phone_Number".localized()
        case .address:
            return "My_Address".localized()
        case .email:
            return "email".localized()
//        case .cdsid:
//            return "Work_Number".localized()
        }
    }
}

enum MeProfileVehicleInfo: Int, CaseIterable {
    case licensePlate
    case vehicleColor
    case vehicleBrand
    
    var localizedPlaceholder: String {
        switch self {
        case .licensePlate:
            return "Car_Number".localized()
        case .vehicleColor:
            return "Car_Color".localized()
        case .vehicleBrand :
            return "Car_Brand".localized()
        }
    }
}
