//
//  SelectDateViewController.swift
//  WorkPool
//
//  Created by Marek Labuzik on 24/06/2019.
//  Copyright © 2019 Marek Labuzik. All rights reserved.
//

import UIKit
import UICustomElements
import KDCalendar

protocol SelectDateViewControllerDelegate: class {
    func viewController(_ viewController: SelectDateViewController, selected dates: [Date])
}

final class SelectDateViewController: UIViewController {
    weak var delegate: SelectDateViewControllerDelegate?
    var calendarView: CalendarView?
    
    var accessoryButonView: AccessoryButtonView?

    var selectedDates: [Date] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupCalendarView()
        self.setupAccessorieView()
        self.setupView()
    }
    
    func setupCalendarView() {
        var bgColor: UIColor = .white
        var selectedDateTextColor: UIColor = .black
        var weekDeyColor: UIColor = .black
        var cellTextColorDefault: UIColor = .black
        
        var cellColorToday: UIColor = .white
        var headerTextColor: UIColor = .black80
        var cellEventColor: UIColor = .black
        if #available(iOS 13.0, *) {
            bgColor = UIColor.systemBackground
            selectedDateTextColor = .label
            weekDeyColor = .label
            cellTextColorDefault = .secondaryLabel
            
            cellColorToday = UIColor.tertiarySystemBackground
            cellEventColor = UIColor.label
            headerTextColor = .label
        }
        
        let size = CGSize(width: self.view.frame.width, height: self.view.frame.width)
        self.calendarView                           = CalendarView()
        self.calendarView?.frame                    = CGRect(origin: .zero, size: size)
        CalendarView.Style.cellShape                = .bevel(6.0)
        CalendarView.Style.cellColorDefault         = UIColor.clear
        CalendarView.Style.cellColorToday           = cellColorToday
        CalendarView.Style.cellSelectedBorderColor  = .mainGreen
        CalendarView.Style.cellSelectedTextColor    = selectedDateTextColor
        CalendarView.Style.cellEventColor           = cellEventColor
        CalendarView.Style.headerTextColor          = headerTextColor
        CalendarView.Style.headerBackgroundColor    = bgColor
        CalendarView.Style.cellTextColorDefault     = cellTextColorDefault
        CalendarView.Style.cellTextColorToday       = .mainGreen
        CalendarView.Style.cellTextColorWeekend     = weekDeyColor
        CalendarView.Style.firstWeekday             = .monday
        self.calendarView!.dataSource = self
        self.calendarView!.delegate = self

        self.calendarView!.direction = .vertical
        self.calendarView!.multipleSelectionEnable = true
        self.calendarView!.marksWeekends = true

        self.calendarView!.backgroundColor = bgColor
        self.view.backgroundColor = bgColor
        self.view.addSubview(self.calendarView!)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.inputAccessoryView?.isHidden = true
        self.tabBarController?.tabBar.isHidden = true
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.inputAccessoryView?.isHidden = false
        self.tabBarController?.tabBar.isHidden = false
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        guard let calendar = self.calendarView else { return }
        calendar.setDisplayDate(Date())

        for date in self.selectedDates {
            calendar.selectDate(date)
        }
    }

    func setupView() {
        
    }

    public func loadSelectedDates(dates: [Date]?) {
        guard let dates = dates else { return }
        self.selectedDates = dates
    }

    func setupAccessorieView() {
        let height: CGFloat = 44 + 16 + 8
        let size: CGSize = CGSize(width: self.view.frame.size.width, height: height)
        self.accessoryButonView = AccessoryButtonView(size: size, isLargePhone: UIDevice.current.deviceType.isLarge)
        self.accessoryButonView?.tintColor = .mainGreen
        self.accessoryButonView?.setAccessoryButtonView(type: .selectDate)
        self.accessoryButonView?.delegate = self
    }

    func updateAccessorieView() {
        if self.selectedDates.count > 0 {
            self.inputAccessoryView?.isHidden = false
        } else {
            self.inputAccessoryView?.isHidden = true
        }
    }

    override var canBecomeFirstResponder: Bool {
        return true
    }

    override var inputAccessoryView: UIView? {
        return self.accessoryButonView
    }
}

extension SelectDateViewController: AccessoryButtonViewDelegate {
    func accessoryButtonPressed(_ view: AccessoryButtonView) {
        self.delegate?.viewController(self, selected: self.selectedDates.sorted())
    }
}

extension SelectDateViewController: CalendarViewDelegate, CalendarViewDataSource {
    func calendar(_ calendar: CalendarView, didScrollToMonth date: Date) { }
    func calendar(_ calendar: CalendarView, didLongPressDate date: Date) { }

    func calendar(_ calendar: CalendarView, canSelectDate date: Date) -> Bool {
        guard let calendar = self.calendarView else { return false }
        var dateComponents = DateComponents()
        dateComponents.day = -1
        let today = Date()
        let yesterday = calendar.calendar.date(byAdding: dateComponents, to: today)!
        if yesterday.timeIntervalSince1970 < date.timeIntervalSince1970 {
            return true
        }
        return false
    }

    func calendar(_ calendar: CalendarView, didSelectDate date: Date, withEvents events: [CalendarEvent]) {
        let dateFound = self.selectedDates.filter { $0 == date }
        if dateFound.count == 0 {
            self.selectedDates.append(date)
        }
        self.updateAccessorieView()
    }
    
    func calendar(_ calendar: CalendarView, didDeselectDate date: Date) {
        self.selectedDates = self.selectedDates.filter { $0 != date }
        self.updateAccessorieView()
    }
    
    func calendar(_ calendar: CalendarView, didLongPressDate date: Date, withEvents events: [CalendarEvent]?) {
        
    }
    
    func headerString(_ date: Date) -> String? {
        return nil
    }
    
    func startDate() -> Date {
        return Date()
    }
    
    func endDate() -> Date {
        guard let calendar = self.calendarView else { return Date() }
        var dateComponents = DateComponents()
        dateComponents.month = 2
        let today = Date()
        let twoMonthsFromNow = calendar.calendar.date(byAdding: dateComponents, to: today)!
        
        return twoMonthsFromNow
    }
}
