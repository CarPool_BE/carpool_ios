//
//  SearchAddressController.swift
//  WorkPool
//
//  Created by Marek Labuzik on 10/05/2019.
//  Copyright © 2019 Marek Labuzik. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation
import UICustomElements
import ArcGIS

public struct PointInfo {
    var title: String
    var position: CLLocationCoordinate2D
    var result: AGSSuggestResult?
    init(title: String?, location: CLLocationCoordinate2D?) {
        self.title = title ?? ""
        self.position = location ?? CLLocationCoordinate2D(latitude: 0, longitude: 0)
    }
}

enum SearchError: Error {
    case emptyStreetOrCity

    var appError: AppError {
        switch self {
        case .emptyStreetOrCity:
            return .emptyStreetOrCity
        }
    }
}

public enum SearchAddressActionType {
    case cancelButtonPressed
}
public enum SearchAddressType {
    case from
    case to
}

public enum WayType {
    case oneWayType
    case returnRide
}

public enum ShowTableCells {
    case none
    case showWork
    case showWorkAndHome
}

public protocol SearchAddressControllerDelate: class {
    func viewController(_ viewController: SearchAddressController, didPerformAction: SearchAddressActionType)
    func viewController(_ viewController: SearchAddressController, type: SearchAddressType, point: PointInfo)
}

final public class SearchAddressController: UITableViewController, ResultsSearchControllerDelegate {
    public weak var delegate: SearchAddressControllerDelate?
    var searchController: UISearchController!
    let request = MKLocalSearch.Request()
    var search: MKLocalSearch?
    var type: SearchAddressType?
    var searchArc: AGSLocatorTask = AGSLocatorTask(url: URL(string: "https://geocode.arcgis.com/arcgis/rest/services/World/GeocodeServer")!)
    var haveLicence: Bool = false
    var showingContent: ShowTableCells?
    
    var color: UIColor? {
        willSet {
            self.resultController.tintColor = newValue
            self.tableView.reloadData()
        }
    }

    private let resultController: ResultsSearchController = ResultsSearchController()
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        self.resultController.resultDelegate = self
        self.tableView.register(UINib(nibName: "UISearchLocationCell", bundle: Bundle(identifier: "eu.teamride.app.UICustomElements")),
                                forCellReuseIdentifier: "UISearchLocationCell")
        self.tableView.tableFooterView = UIView()
        self.setupSearchController()
        self.licence()
    }

    public override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.searchController.searchBar.becomeFirstResponder()
    }

    override public func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        tableView.contentInset = UIEdgeInsets(top: 10, left: 0, bottom: 0, right: 0)
    }
    
    override public func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.view.setNeedsLayout()
        navigationController?.view.layoutIfNeeded()
    }

    override public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch self.showingContent {
        case .some(.none):
            return 0
        case .some(.showWork):
            return 1
        case .some(.showWorkAndHome):
            return 2
        case .none:
            return 0
        }
    }
    
    override public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "UISearchLocationCell") as? UISearchLocationCell else {
            fatalError("Misconfigured cell type, UISearchLocationCell!")
        }
        cell.tintColor = .mainGreen
        self.setCell(cell: cell, at: indexPath)

        return cell
    }
    
    func setCell(cell: UISearchLocationCell, at indexPath: IndexPath) {
        if self.showingContent == .some(.showWorkAndHome) {
            switch indexPath.row {
            case 0:
                cell.set(type: .home, title: "home".localized(), subTitle: "")
            case 1:
                cell.set(type: .work, title: "work".localized(), subTitle: "")
            default: break
            }
        }
        if self.showingContent == .some(.showWork) {
            switch indexPath.row {
            case 0:
                cell.set(type: .work, title: "work".localized(), subTitle: "")
            default: break
            }
        }
    }

    override public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let cell: UISearchLocationCell = tableView.cellForRow(at: indexPath) as? UISearchLocationCell else {return}
        switch cell.type {
        case .home:
            let position = PointInfo(title: AppData.meData.profile?.home, location: AppData.meData.profile?.getHomeGPS())
            if let type = self.type {
                self.delegate?.viewController(self, type: type, point: position)
            }
        case .work:
            let position = PointInfo(title: AppData.meData.workAddress, location: AppData.meData.workGPS)
            if let type = self.type {
                self.delegate?.viewController(self, type: type, point: position)
            }
        case .actual:break
        case .point, .recent:break
        }
    }

    func viewController(_ viewController: ResultsSearchController, point: PointInfo) {
        if let type = self.type {
            self.delegate?.viewController(self, type: type, point: point)
        }
    }
}

extension SearchAddressController: UISearchControllerDelegate, UISearchBarDelegate {
    func setupSearchController() {
        self.navigationItem.hidesBackButton = true
        self.searchController = UISearchController(searchResultsController: self.resultController)
        self.searchController.delegate = self
        self.searchController.searchResultsUpdater = self
        self.searchController.hidesNavigationBarDuringPresentation = false
        self.searchController.searchBar.sizeToFit()
        self.searchController.searchBar.delegate = self
       
        let searchBar = self.searchController.searchBar
        searchBar.sizeToFit()
        searchBar.placeholder = "Search_placeholder".localized()
        searchBar.showsCancelButton = true
        searchBar.isTranslucent = true
        searchBar.barTintColor = .mainGreen
        searchBar.searchBarStyle = .minimal
        self.navigationItem.titleView = searchController.searchBar
        self.definesPresentationContext = true
    }
    public func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.delegate?.viewController(self, didPerformAction: .cancelButtonPressed)
    }
}

extension SearchAddressController: UISearchResultsUpdating {
    
    public func updateSearchResults(for searchController: UISearchController) {
        guard let text = searchController.searchBar.text else { return }
        if self.haveLicence {
            self.searchAddress(with: text)
        } else {
            self.searchAddressFormiOS(with: text)
        }
    }
    
    func searchAddressFormiOS(with text: String) {
        if let search = self.search {
            if search.isSearching {
                self.search?.cancel()
            }
        }
        
        self.request.naturalLanguageQuery = text
        self.request.region = self.getRegion()
        self.search = MKLocalSearch(request: request)
        self.search?.start { response, _ in
            guard let response = response else {
                return
            }
            print(response.mapItems)
            var list: [PointInfo] = []
            for result in response.mapItems {
                let place = result.placemark
                let neme = place.name ?? ""
                let locality = place.locality ?? ""
                let placeTitle = neme + ", " + locality
                let data = PointInfo(title: placeTitle, location: place.location?.coordinate)
                list.append(data)
            }
            self.resultController.reloadData(data: list)
            self.tableView.reloadData()
        }
    }
    
    func getRegion() -> MKCoordinateRegion {
        // region slovensko
        let coordinate = CLLocationCoordinate2D(latitude: 48.73055555555555, longitude: 19.4572222222222)
        let width = CLLocationDistance(exactly: 250*1000)
        let height = CLLocationDistance(exactly: 70*1000)
        return MKCoordinateRegion(center: coordinate, latitudinalMeters: height!, longitudinalMeters: width!)
    }
    
    func searchAddress(with address: String) {
        if address.count < 3 {
            return
        }
        let params = AGSSuggestParameters()
        params.countryCode = "SVK"
        self.searchArc.suggest(withSearchText: address, completion: { (results, err) in
            guard let results = results else { return }

            var list: [PointInfo] = []
            for result in results {
                var data = PointInfo(title: result.label, location: nil)
                data.result = result
                list.append(data)
            }
            self.resultController.reloadData(data: list)
            self.tableView.reloadData()
            if err != nil {
                print("error \(err!.localizedDescription)")
            }
        })
    }
    
    func licence() {
        do {
            let licence = try AGSArcGISRuntimeEnvironment.setLicenseKey("runtimelite,1000,rud5454485029,none,6PB3LNBHPDCM2T8AG162")
            print("Licence status: \(licence.licenseStatus.rawValue)")
            switch licence.licenseStatus {
            case .expired,
                 .invalid,
                 .loginRequired:
                self.haveLicence = false
            case .valid:
                self.haveLicence = true
            @unknown default:
                break
            }
        } catch let error {
            self.haveLicence = false
            print(error.localizedDescription)
        }
        
    }
}

protocol ResultsSearchControllerDelegate: class {
    func viewController(_ viewController: ResultsSearchController, point: PointInfo)
}

final class ResultsSearchController: UITableViewController {
    var fetchData: [PointInfo] = []
    var selectedPointInfo: PointInfo?
    var searchArc: AGSLocatorTask = AGSLocatorTask(url: URL(string: "https://geocode.arcgis.com/arcgis/rest/services/World/GeocodeServer")!)
    weak var resultDelegate: ResultsSearchControllerDelegate?
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.tableFooterView = UIView()
        self.tableView.rowHeight = 64
        self.tableView.register(UINib(nibName: "UISearchLocationCell", bundle: Bundle(identifier: "eu.teamride.app.UICustomElements")),
                                forCellReuseIdentifier: "UISearchLocationCell")
    }
    
    var tintColor: UIColor?

    func reloadData(data: [PointInfo]) {
        self.fetchData.removeAll()
        self.fetchData = data
        self.tableView.reloadData()
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.fetchData.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let data = self.fetchData[indexPath.row]
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "UISearchLocationCell") as? UISearchLocationCell else {
            fatalError("Misconfigured cell type, UISearchLocationCell!")
        }
        cell.tintColor = .mainGreen
        cell.set(type: .point, title: data.title, subTitle: "")
        return cell
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let data = self.fetchData[indexPath.row]
        if let result = data.result {
            self.findCoordinates(for: result) { [weak self] result in
                guard let self = self else { return }
                switch result {
                case .success(let pointInfo):
                    self.resultDelegate?.viewController(self, point: pointInfo)
                case .failure(let error):
                    ErrorHandler.show(with: error.appError, on: self, withBtn: "Continue_Button".localized()) { _ in
                        if self.selectedPointInfo != nil {
                            self.resultDelegate?.viewController(self, point: self.selectedPointInfo!)
                        }
                    }
                    break
                }
            }
        } else {
            self.resultDelegate?.viewController(self, point: data)
        }
        
    }
    
    func findCoordinates(for result: AGSSuggestResult, completition: @escaping (Result<PointInfo, SearchError>) -> Void) {
        let params = AGSGeocodeParameters()
        params.resultAttributeNames = ["City", "StAddr", "District"]
        self.searchArc.geocode(with: result, parameters: params) { (geocodeResults, _) in
            guard let geocodeReesults = geocodeResults,
                let firsGeocode = geocodeReesults.first,
                let cordinate = firsGeocode.displayLocation?.toCLLocationCoordinate2D()
                else { return }
            self.selectedPointInfo = PointInfo(title: result.label, location: cordinate)
            guard let attributes = firsGeocode.attributes,
                let city = attributes["City"] as? String,
                let street = attributes["StAddr"] as? String
                else {
                    completition(.success(self.selectedPointInfo!))
                    return
                }
            if !city.isEmpty && !street.isEmpty {
                if let district = attributes["District"] as? String {
                    self.selectedPointInfo?.title = street + ", " + district
                } else {
                    self.selectedPointInfo?.title = street + ", " + city
                }
                completition(.success(self.selectedPointInfo!))
            } else {
                completition(.failure(.emptyStreetOrCity))
            }
        }
    }
}
