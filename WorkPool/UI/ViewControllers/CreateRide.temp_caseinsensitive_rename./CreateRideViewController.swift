//
//  CreateRideViewController.swift
//  WorkPool
//
//  Created by Marek Labuzik on 23/04/2019.
//  Copyright © 2019 Marek Labuzik. All rights reserved.
//

import UIKit
import UICustomElements
import MapKit

enum CrateRideSection: Int, CaseIterable {
    case oneWayRide
    case returnRide

    var localizedTitle: String? {
        switch self {
        case .oneWayRide:
            return "OneWayRide".localized()
        case .returnRide:
            return "ReturnWayRide".localized()
        }
    }
}

protocol CreateRideRowProtocol {
    var type: FieldType { get }
    var localizedPlaceholder: String { get }
    var userInteraction: Bool { get }
    var valueChangeFor: CreateRideRows { get }
    func getData(fromModel model: NewRideViewModel) -> String
}

enum OneWayRide: Int, CaseIterable, CreateRideRowProtocol {
    case addressFrom
    case addressTo
    case date
    case time
    case note
    case car
    case freePlaces
    case map

    var type: FieldType {
        switch self {
        case .addressFrom:
            return .home
        case .addressTo:
            return .work
        case .date:
            return .calendar
        case .time:
            return .time
        case .note:
            return .info
        case .car:
            return .car
        case .freePlaces:
            return .carPlaces
        case .map:
            return .home
        }
    }

    var valueChangeFor: CreateRideRows {
        switch self {
        case .addressFrom:
            return .addressFrom
        case .addressTo:
            return .addressTo
        case .date:
            return .date
        case .time:
            return .time
        case .note:
            return .note
        case .car:
            return .car
        case .freePlaces:
            return .freePlaces
        case .map:
            return .addressFrom
        }
    }

    var localizedPlaceholder: String {
        switch self {
        case .addressFrom:
            return "Me_Location_Placeholder".localized()
        case .addressTo:
            return "Me_Destination_Placeholder".localized()
        case .car:
            return "Car_Placeholder".localized()
        case .date:
            return "Date_Placeholder".localized()
        case .freePlaces:
            return "Free_Places_Placeholder".localized()
        case .note:
            return "Note_Placeholder".localized()
        case .time:
            return "Time_Placeholder".localized()
        case .map:
            return ""
        }
    }

    var userInteraction: Bool {
        switch self {
        case .addressFrom: return false
        case .addressTo: return false
        case .car: return false
        case .date: return false
        case .freePlaces: return false
        case .note: return true
        case .time: return false
        case .map: return false
        }
    }

    func getData(fromModel model: NewRideViewModel) -> String {
        switch self {
        case .addressFrom:
            return model.from
        case .addressTo:
            return model.to
        case .car:
            return model.carSPZ
        case .date:
            return model.dates.toString()
        case .freePlaces:
            return String(model.numberOfPlaces)
        case .time:
            var returnString = ""
            if let time = model.time {
                returnString = time.getTimeString()
            }
            return returnString
        case .note:
            return model.note
        case .map:
            return ""
        }
    }
}

enum ReturnRide: Int, CaseIterable, CreateRideRowProtocol {
    case returnTime
    case returnNote
    case returnFreePlaces
    case map

    var valueChangeFor: CreateRideRows {
        switch self {
        case .returnTime:
            return .returnTime
        case .returnNote:
            return .returnNote
        case .returnFreePlaces:
            return .returnFreePlaces
        case .map:
            return .returnTime
        }
    }

    var type: FieldType {
        switch self {
        case .returnTime:
            return .time
        case .returnNote:
            return .info
        case .returnFreePlaces:
            return .carPlaces
        case .map:
            return .time
        }
    }

    var localizedPlaceholder: String {
        switch self {
        case .returnFreePlaces:
            return "Free_Places_Placeholder".localized()
        case .returnNote:
            return "Note_Placeholder".localized()
        case .returnTime:
            return "Time_Placeholder".localized()
        case .map:
            return ""
        }
    }

    var userInteraction: Bool {
        switch self {
        case .returnFreePlaces: return false
        case .returnNote: return true
        case .returnTime: return false
        case .map: return false
        }
    }

    func getData(fromModel model: NewRideViewModel) -> String {
        switch self {
        case .returnFreePlaces:
            return String(model.returnNumberOfPlaces)
        case .returnTime:
            var returnString = ""
            if let time = model.returnTime {
                returnString = time.getTimeString()
            }
            return returnString
        case .returnNote:
            return model.returnNote
        case .map:
            return ""
        }
    }
}

enum CreateRideRows: Int {
    case none
    case addressFrom
    case addressTo
    case date
    case time
    case note
    case car
    case freePlaces
    case returnDate
    case returnTime
    case returnNote
    case returnFreePlaces
}

enum CreateRideActionType {
    case unknown
    case didSelectAddressTo
    case didSelectAddressFrom
    case didSelectDate
    case didSelectTime
    case didSelectReturnTime
    case didSelectCar
    case didCreateRideButtonPressed
    case didSetReturnRide
    case didSetOneWayRide
}

protocol CreateRideViewControllerDelegate: class {
    func viewController(_ viewController: CreateRideViewController, didPerformAction: CreateRideActionType)
    func viewController(_ viewController: CreateRideViewController, valueDidChange: String, forRow: CreateRideRows)
    func viewController(_ viewController: CreateRideViewController, routeDidChange route: MKRoute)
}

final class CreateRideViewController: UITableViewController {
    weak var delegate: CreateRideViewControllerDelegate?
    
    private var segmentControll: UISegmentedControl?
    private var accessoryButonView: AccessoryButtonView?
    private var sections = [CrateRideSection]()
    private var oneWayRide = [OneWayRide]()
    private var returnRide = [ReturnRide]()
    private var mapCell: UIMapCell?
    
    private(set) var isTyping: Bool = false

    var model: NewRideViewModel? {
        didSet {
            if self.isTyping == false {
                self.tableView.reloadData()
            }
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Create_Ride_Title".localized()
        self.setupTableView()
        self.setupSegmentControll()
        sections = CrateRideSection.allCases
        oneWayRide = OneWayRide.allCases
        returnRide = ReturnRide.allCases
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.tabBar.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.tabBarController?.tabBar.isHidden = false
        self.mapCell?.delegate = nil
        self.mapCell = nil
    }
    
    func setupSegmentControll() {
        self.segmentControll = UISegmentedControl(items: ["OneWayRide".localized(), "ReturnWayRide".localized()])
        self.segmentControll?.tintColor = .mainGreen
        self.segmentControll?.selectedSegmentIndex = 0
        self.segmentControll?.addTarget(self, action: #selector(segmentControllWillChange), for: .valueChanged)
        self.navigationItem.titleView = self.segmentControll
    }
    
    @objc func segmentControllWillChange() {
        self.tableView.reloadData()
        if self.segmentControll?.selectedSegmentIndex == 0 {
            self.delegate?.viewController(self, didPerformAction: .didSetOneWayRide)
        } else {
            self.delegate?.viewController(self, didPerformAction: .didSetReturnRide)
        }
    }

    func setupTableView() {
        self.tableView.backgroundColor = .white
        self.tableView.rowHeight = UITableView.automaticDimension
        self.tableView.register(UINib(nibName: "UIFormCell",
                                      bundle: Bundle(identifier: "eu.teamride.app.UICustomElements")),
                                forCellReuseIdentifier: "UIFormCell")
        self.tableView.register(UINib(nibName: "UIFormSteperCellView",
                                      bundle: Bundle(identifier: "eu.teamride.app.UICustomElements")),
                                forCellReuseIdentifier: "UIFormSteperCellView")
        self.tableView.register(UIMapCell.self, forCellReuseIdentifier: "UIMapCell")
        self.tableView.separatorStyle = UITableViewCell.SeparatorStyle.none
        if #available(iOS 13.0, *) {
            self.tableView.backgroundColor = .secondarySystemBackground
        }
    }
    
    override var canBecomeFirstResponder: Bool {
        return true
    }
    
    override var inputAccessoryView: UIView? {
        if self.accessoryButonView == nil {
            self.setupAccessorieView()
        }
        return self.accessoryButonView
    }
    
    func setupAccessorieView() {
        let height: CGFloat = 44 + 16 + 8
        let size: CGSize = CGSize(width: self.view.frame.size.width, height: height)
        self.accessoryButonView = AccessoryButtonView(size: size, isLargePhone: UIDevice.current.deviceType.isLarge)
        self.accessoryButonView?.tintColor = .mainGreen
        self.accessoryButonView?.setAccessoryButtonView(type: .createRide)
        self.accessoryButonView?.delegate = self
    }
    
    public func add(routes: [MKRoute]) {
        if self.mapCell != nil {
            self.mapCell!.add(routes: routes)
        }
    }

}

extension CreateRideViewController {
    override func numberOfSections(in tableView: UITableView) -> Int {
        if self.segmentControll?.selectedSegmentIndex == 0 {
            return 1
        } else {
            return 2
        }
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch sections[section] {
        case .oneWayRide:
            if self.segmentControll?.selectedSegmentIndex == 0 {
                return self.oneWayRide.count
            } else {
                return self.oneWayRide.count - 1
            }
        case .returnRide:
            return self.returnRide.count
        }
    }

    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let myLabel = UILabel()
        myLabel.frame = CGRect(x: 16, y: 5, width: UIScreen.main.bounds.width, height: 20)
        myLabel.font = UIFont.boldSystemFont(ofSize: 17)
        
        myLabel.text = self.sections[section].localizedTitle
        
        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 30))
        headerView.addSubview(myLabel)

        if #available(iOS 13.0, *) {
            myLabel.textColor = .secondaryLabel
            headerView.backgroundColor = .secondarySystemBackground
        } else {
            myLabel.textColor = .black80
        }
        
        return headerView
    }

    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 30
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell: UITableViewCell?
        var row: CreateRideRowProtocol?
        
        switch sections[indexPath.section] {
        case .oneWayRide:
            row = OneWayRide(rawValue: indexPath.row)!
            if self.oneWayRide[indexPath.row] == .map {
                cell = tableView.dequeueReusableCell(withIdentifier: "UIMapCell")
            } else {
                cell = tableView.dequeueReusableCell(withIdentifier: "UIFormCell")
            }
        case .returnRide:
            if self.returnRide[indexPath.row] == .map {
                cell = tableView.dequeueReusableCell(withIdentifier: "UIMapCell")
            } else {
                cell = tableView.dequeueReusableCell(withIdentifier: "UIFormCell")
            }
            row = ReturnRide(rawValue: indexPath.row)!
        }
        
        if let cell = cell as? UIFormCell, row != nil {
            cell.customField.delegate = self
            cell.customField.indexPath = indexPath
            cell.setFormCell(type: row!.type)
            cell.setPlaceholder(text: row!.localizedPlaceholder)
            cell.color = .mainGreen
            cell.tintColor = .black20
            if let model = self.model {
                cell.setDefaultValue(string: row!.getData(fromModel: model))
            }
            cell.selectionStyle = .none
            return cell
        } else if let cell = cell as? UIMapCell {
            if #available(iOS 13.0, *) {
                cell.tintColor = .secondaryLabel
                cell.selectedColor = .label
            } else {
                cell.tintColor = .black20
                cell.selectedColor = .black
            }
            cell.delegate = self
            self.mapCell = cell
            return cell
        } else {
            return UITableViewCell()
        }
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var action: CreateRideActionType = .unknown
        self.isTyping = false

        switch sections[indexPath.section] {
            case .oneWayRide:
                let row = OneWayRide(rawValue: indexPath.row)!
                switch row {
                case .addressFrom:
                    action = .didSelectAddressFrom
                case .addressTo:
                    action = .didSelectAddressTo
                case .car:
                    action = .didSelectCar
                case .date:
                    action = .didSelectDate
                case .time:
                    action = .didSelectTime
                case .freePlaces:
                    action = .unknown
                case .note:
                    action = .unknown
                case .map:
                    action = .unknown
            }
            case .returnRide:
                let row = ReturnRide(rawValue: indexPath.row)!
                switch row {
                case .returnTime:
                    action = .didSelectReturnTime
                case .returnFreePlaces:
                    action = .unknown
                case .returnNote:
                    action = .unknown
                case .map:
                    action = .unknown
                }
        }
        self.delegate?.viewController(self, didPerformAction: action)
    }
}

extension CreateRideViewController: UIMapCellDelegate {
    func mapCellView(_ mapCell: UIMapCell, didSelect route: MKRoute) {
        self.delegate?.viewController(self, routeDidChange: route)
    }
}

extension CreateRideViewController: AccessoryButtonViewDelegate {
    func accessoryButtonPressed(_ view: AccessoryButtonView) {
        self.delegate?.viewController(self, didPerformAction: .didCreateRideButtonPressed)
    }
}

extension CreateRideViewController: UICustomFieldDelegate {
    func customField(_ customField: UICustomField, valueDidChange: String) {
        var row: CreateRideRows = .none
        guard let indexPath = customField.indexPath else { return }
        switch sections[indexPath.section] {
        case .oneWayRide:
            row = OneWayRide(rawValue: indexPath.row)?.valueChangeFor ?? .none
        case .returnRide:
            row = ReturnRide(rawValue: indexPath.row)?.valueChangeFor ?? .none
        }
        self.delegate?.viewController(self, valueDidChange: valueDidChange, forRow: row)
    }
    
    func customFieldDidBeginEditing(_ customField: UICustomField) {
        self.isTyping = true
    }
    
    func customFieldPressed(_ customField: UICustomField) { }
}
