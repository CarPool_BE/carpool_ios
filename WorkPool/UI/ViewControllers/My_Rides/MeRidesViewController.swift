//
//  MeRidesViewController.swift
//  WorkPool
//
//  Created by Marek Labuzik on 05/05/2019.
//  Copyright © 2019 Marek Labuzik. All rights reserved.
//

import UIKit
import UICustomElements

enum MeRidesActionType {
    case getNewData
    case createNewRide
}

protocol MeRidesViewControllerDelegate: class {
    func viewController(_ meRidesViewController: MeRidesViewController, didPerform action: MeRidesActionType)
    func viewController(_ meRidesViewController: MeRidesViewController, didUpdate date: Date)
    func viewController(_ meRidesViewController: MeRidesViewController, showDetailFor rideID: Int)
}

extension MeRidesViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let count = self.rides.count
        self.showEmptyTable(isEmpty: !(count > 0))
        return count
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if #available(iOS 11, *) {
            return UITableView.automaticDimension
        } else {
            return 152
        }
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "UIMeRidesViewCell",
                                                       for: indexPath) as? UIMeRidesViewCell
            else { fatalError("DequeueReusableCell failed") }
        let rideData = self.rides[indexPath.row]

        cell.startAddressLabel.text = rideData.from
        cell.startRideTimeLabel.text = rideData.startTime
        cell.stopAddressLabel.text = rideData.to
        cell.stopRideTimeLabel.text = rideData.endTime
        cell.numberOfAllPlacesInRide = rideData.numberOfPlaces
        cell.numberOfFreePlacesInRide = rideData.rideWithUsers
        cell.rideIdentifier = rideData.model.identifier ?? -1
        cell.isPassenger(isPassenger: rideData.isDriver)

        cell.tintColor = .mainGreen
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let cell = tableView.cellForRow(at: indexPath) as? UIMeRidesViewCell else { return }
        self.deledate?.viewController(self, showDetailFor: cell.rideIdentifier)
    }
}

final class MeRidesViewController: UIViewController, UIEmptyViewDelegate, UIMeRidesHeadeViewDelegate {
    weak var deledate: MeRidesViewControllerDelegate?
    var tableView: UITableView = UITableView(frame: CGRect(x: 0, y: 0, width: 100, height: 100), style: .plain)
    var activityView: ActivityView?
    var headerView: UIMeRidesHeadeView?
    var rides: [RideViewModel] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "TabBar_MePlan".localized()
        self.setupTableView()
        self.addCreateButton()
        self.setupHeader()
        self.setupConstraints()
        if #available(iOS 13.0, *) {
            self.view.backgroundColor = .systemBackground
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.loadData()
    }
    
    func setupTableView() {
        self.view.backgroundColor = .white
        self.tableView.dataSource = self
        self.tableView.delegate = self
        self.tableView.separatorStyle = UITableViewCell.SeparatorStyle.none
        self.tableView.rowHeight = UITableView.automaticDimension
        self.tableView.register(UINib(nibName: "UIMeRidesViewCell", bundle: Bundle.customElements),
                                forCellReuseIdentifier: "UIMeRidesViewCell")
    }

    func setupConstraints() {
        guard let header = self.headerView else { return }
        self.view.addSubview(header)
        self.view.addSubview(self.tableView)

        self.tableView.translatesAutoresizingMaskIntoConstraints = false
        header.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            header.topAnchor.constraint(equalTo: self.view.topAnchor, constant: 0),
            header.rightAnchor.constraint(equalTo: self.view.rightAnchor, constant: 0),
            header.leftAnchor.constraint(equalTo: self.view.leftAnchor, constant: 0),
            header.bottomAnchor.constraint(equalTo: self.tableView.topAnchor, constant: 0),

            self.tableView.rightAnchor.constraint(equalTo: self.view.rightAnchor, constant: 0),
            self.tableView.leftAnchor.constraint(equalTo: self.view.leftAnchor, constant: 0),
            self.tableView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: 0)
            ])
    }
    
    func addCreateButton() {
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Create_Ride_Title".localized(),
                                                                 style: .plain,
                                                                 target: self,
                                                                 action: #selector(pushCreateRideVC))
    }

    func startAnimating() {
        if self.activityView == nil {
            self.activityView = ActivityView(frame: self.tableView.bounds)
            self.activityView?.startAnimating()
            self.tableView.addSubview(self.activityView!)
        }
    }

    func stopAnimating() {
        self.activityView?.stopAnimating()
        self.activityView?.removeFromSuperview()
        self.activityView = nil
    }
    
    @objc func pushCreateRideVC() {
        self.deledate?.viewController(self, didPerform: .createNewRide)
    }
    
    func reloadData() {
        self.tableView.reloadData()
    }
    
    private func loadData() {
        self.deledate?.viewController(self, didPerform: .getNewData)
    }

    func setupHeader() {
        guard let header = Bundle.customElements?.loadNibNamed("UIMeRidesHeadeView",
                                                               owner: self,
                                                               options: nil)?.first as? UIMeRidesHeadeView else { return }
        header.tintColor = .mainGreen
        header.delegate = self
        self.headerView = header
    }

    // MARK: - UIEmptyView
    func showEmptyTable(isEmpty: Bool) {
        if isEmpty {
            guard let view = Bundle.customElements?.loadNibNamed("UIEmptyView",
                                                                 owner: self,
                                                                 options: nil)?.first as? UIEmptyView else { return }
            let string = NSMutableAttributedString(string: "MyRides_Empty_text".localized())
            let actionString = NSMutableAttributedString(string: "MyRides_Empty_action".localized())
            actionString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.mainGreen, range: NSRange(location: 0, length: actionString.length))
            string.append(NSAttributedString(string: "\n"))
            string.append(actionString)
            view.set(text: string)
            view.delegate = self
            self.tableView.backgroundView = view
        } else {
            self.tableView.backgroundView = nil
        }
    }
    
    func emptyViewTextViewDidPressed(_ emptyView: UIEmptyView) {
        self.pushCreateRideVC()
    }
    
    func meRidesHeader(_ meRidesHeader: UIMeRidesHeadeView, selectedDateChange: Date) {
        self.deledate?.viewController(self, didUpdate: selectedDateChange)
    }
}
