//
//  MeRideDetailViewController.swift
//  WorkPool
//
//  Created by Marek Labuzik on 18/06/2019.
//  Copyright © 2019 Marek Labuzik. All rights reserved.
//

import UIKit
import UICustomElements

enum MyRideDetailAction {
    case none
    case removeFromRide
    case cancelRide
    case shareRide
}

protocol MeRideDetailViewControllerDelegate: class {
    func viewController(_ viewController: MeRideDetailViewController, didPerform action: MyRideDetailAction)
    func viewController(_ contactCell: MeRideDetailViewController, makeCallTo phone: String)
    func presentationControllerDidDismissViewController(_ contactCell: MeRideDetailViewController)
}

final class MeRideDetailViewController: UIViewController {
    weak var delegate: MeRideDetailViewControllerDelegate?
    var accessoryButonView: AccessoryButtonView?
    var tableView: UITableView = UITableView()
    var headerView: UIRideDetailHeader?
    var activityView: ActivityView?
    var isRideFormShareWeb: Bool = false

    var viewModel: RideViewModel? {
        didSet {
            self.tableView.reloadData()
            self.updateHeader()
            self.updateAccessoryView()
            if self.isRideFormShareWeb {
                self.setRideFormShareWeb()
            }
        }
    }
    
    override var canBecomeFirstResponder: Bool {
        if self.viewModel != nil {
            self.showShareButton()
            return true
        } else {
            self.hideShareButton()
            return false
        }
    }

    override var inputAccessoryView: UIView? {
        return self.accessoryButonView
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupHeaderView()
        self.setupTable()
        self.setupAccessorieView()
        self.setupFrames()
        self.presentationController?.delegate = self
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.tabBar.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.tabBarController?.tabBar.isHidden = false

        if self.isMovingFromParent || self.isBeingDismissed {//back button pressed
            print("BACK button pressed")
        }
    }
    
    func hideShareButton() {
        self.navigationController?.navigationItem.rightBarButtonItem = nil
    }
    
    func showShareButton() {
        let shareButton = UIBarButtonItem(barButtonSystemItem: .action, target: self, action: #selector(showShareMenu))
        self.navigationItem.rightBarButtonItem = shareButton
    }
    
    @objc func showShareMenu() {
        self.delegate?.viewController(self, didPerform: .shareRide)
    }
    
    func setDoneButton() {
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(donePressed))
    }
    
    @objc func donePressed() {
        self.dismissVC()
    }
    
    func setRideFormShareWeb() {
        self.setDoneButton()
        guard let model = self.viewModel,
            let date = model.model.startDate else {
            return
        }
        if  date < Date() {
            UIAlertController.topMostControllerShowAlert(withTitle: "Error_title".localized(), subtitle: "RideExpire".localized(), buttonTitle: "OK_Button".localized()) { _ in
                self.dismissVC()
            }
        }
    }
    
    func dismissVC() {
        self.dismiss(animated: true, completion: nil)
        self.delegate?.presentationControllerDidDismissViewController(self)
    }

    func setupTable() {
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.tableFooterView = UIView()
        self.tableView.separatorStyle = .none
        self.tableView.register(UINib(nibName: "UIRegisterRideCell", bundle: Bundle.customElements),
                                forCellReuseIdentifier: "UIRegisterRideCell")
        self.tableView.register(UINib(nibName: "UIContactCell", bundle: Bundle.customElements),
                                forCellReuseIdentifier: "UIContactCell")
        self.view.addSubview(self.tableView)
    }

    func setupHeaderView() {
        guard let header = Bundle.customElements?.loadNibNamed("UIRideDetailHeader",
                                                               owner: self,
                                                               options: nil)?.first as? UIRideDetailHeader else { return }
        self.headerView = header
        var frame = header.frame
        frame.size.width = UIScreen.main.bounds.width
        self.headerView?.frame = frame
        self.headerView?.backgroundColor = .white
        self.headerView?.firstNameLabel.text = self.viewModel?.driver?.firstName ?? ""
        self.headerView?.lastNameLabel.text = self.viewModel?.driver?.lastName ?? ""
        self.headerView?.carTypeLabel.text = self.viewModel?.driver?.carBrand
        self.headerView?.carNumberAndColorLabel.text = (self.viewModel?.driver?.carNumber ?? "") + " " + (self.viewModel?.driver?.carColor ?? "")
        if self.viewModel?.driver?.phoneNumber != nil  &&
            self.viewModel?.isPassenger == true  &&
            self.isRideFormShareWeb == false {
            self.headerView?.showPhoneButtonToCall(show: true)
        } else {
            self.headerView?.showPhoneButtonToCall(show: false)
        }
        self.view.addSubview(self.headerView!)
    }

    func updateHeader() {
        if let driver = self.viewModel?.driver {
            self.headerView?.firstNameLabel.text = driver.firstName
            self.headerView?.lastNameLabel.text = driver.lastName
            self.headerView?.carTypeLabel.text = driver.carBrand
            self.headerView?.carNumberAndColorLabel.text = driver.carNumber + ", " + driver.carColor
            if self.viewModel?.driver?.phoneNumber != nil  &&
                self.viewModel?.isPassenger == true {
                self.headerView?.showPhoneButtonToCall(show: true)
            } else {
                self.headerView?.showPhoneButtonToCall(show: false)
            }
        }
    }

    func setupAccessorieView() {
        let height: CGFloat = 44 + 16 + 8
        let size: CGSize = CGSize(width: self.view.frame.size.width, height: height)
        self.accessoryButonView = AccessoryButtonView(size: size, isLargePhone: UIDevice.current.deviceType.isLarge)
        self.accessoryButonView?.tintColor = .mainGreen
        self.accessoryButonView?.setAccessoryButtonView(type: .cancelRide)
        self.accessoryButonView?.delegate = self
    }
    
    func updateAccessoryView() {
        guard let model = self.viewModel else { return }
        if !model.isDriver {
            self.accessoryButonView?.setAccessoryButtonView(type: .cancelReservation)
        } else {
            self.accessoryButonView?.setAccessoryButtonView(type: .cancelRide)
        }
        self.becomeFirstResponder()
    }

    func setupFrames() {
        guard let header = self.headerView else { return }
        self.tableView.frame = CGRect(x: 0, y: header.frame.height,
                                      width: UIScreen.main.bounds.width, height: self.view.frame.height - header.frame.height)
    }

    func startAnimating() {
        if self.activityView == nil {
            self.activityView = ActivityView(frame: self.tableView.bounds)
            self.activityView?.startAnimating()
            self.tableView.addSubview(self.activityView!)
        }
    }

    func stopAnimating() {
        self.activityView?.stopAnimating()
        self.activityView?.removeFromSuperview()
        self.activityView = nil
    }
}

extension MeRideDetailViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let model = self.viewModel else {
            return 0
        }
        return model.users.count + 1
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let model = self.viewModel else { fatalError() }
        if indexPath.row == 0 {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "UIRegisterRideCell") as? UIRegisterRideCell
                else { fatalError() }
            cell.startTimeLabel.text = model.startTime
            cell.startAddressLabel.attributedText = model.startAddress
            cell.endTimeLabel.text = model.endTime
            cell.endAddressLabel.attributedText = model.endAddress
            cell.noteLabel.text = model.note
            cell.isSelected = false
            cell.tintColor = .mainGreen
            cell.selectionStyle = .none
            return cell
        } else {
            let row = indexPath.row - 1
            let passenger = model.users[row]
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "UIContactCell") as? UIContactCell
                else { fatalError() }
            if passenger.lastName.isEmpty {
                cell.userName.text = passenger.firstName
            } else {
                cell.userName.text = passenger.firstName + ", " + passenger.lastName
            }

            cell.phoneNumber = passenger.phoneNumber
            if !model.isPassenger && passenger.phoneNumber != nil && self.isRideFormShareWeb == false {
                cell.havePhoneNumber(isExists: true)
            } else {
                cell.havePhoneNumber(isExists: false)
            }
            cell.selectionStyle = .none
            cell.delegate = self
            return cell
        }
    }
}

extension MeRideDetailViewController: UIContactCellDelegate {
    func contactCellView(_ contactCell: UIContactCell, makeCallTo phone: String) {
        self.delegate?.viewController(self, makeCallTo: phone)
    }
}

extension MeRideDetailViewController: AccessoryButtonViewDelegate {
    func accessoryButtonPressed(_ view: AccessoryButtonView) {
        guard let model = self.viewModel else { return }
        var action: MyRideDetailAction = .none
        if model.isDriver {
            action = .cancelRide
        } else {
            action = .removeFromRide
        }
        self.delegate?.viewController(self, didPerform: action)
    }
}

extension MeRideDetailViewController: UIAdaptivePresentationControllerDelegate {
    func presentationControllerDidDismiss(_ presentationController: UIPresentationController) {
        self.delegate?.presentationControllerDidDismissViewController(self)
    }
}
