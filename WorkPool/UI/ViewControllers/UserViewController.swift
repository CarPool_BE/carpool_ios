//
//  UserViewController.swift
//  WorkPool
//
//  Created by Marek Labuzik on 02/05/2019.
//  Copyright © 2019 Marek Labuzik. All rights reserved.
//

import UIKit

final class UserViewController: UITableViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        self.getUsers()
    }
    
    // MARK: - data
    var users: [User] = []
    
    func getUsers(from: Int = 50) {
        AppData.meData.auth.userService.getUsers { result in
            switch result {
            case .success(let users):
                self.users = users.users
                self.tableView.reloadData()
            case .failure:
                break
            }
        }
    }
    
    // MARK: - TableViewDataSource
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.users.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .subtitle, reuseIdentifier: "cell")
        return cell
    }
}
