//
//  ViewControllerContainer.swift
//  WorkPool
//
//  Created by Marek Labuzik on 15/07/2019.
//  Copyright © 2019 Marek Labuzik. All rights reserved.
//

import Foundation
import UIKit

protocol ViewControllerContainer {

    /// visible view controller
    var visibleViewController: UIViewController? {get}

    /// visible index
    var visibleIndex: Int? {get}

    /// all view controllers
    var viewControllers: [UIViewController] {get set}

    /// sets view controller
    func setViewController(_ viewController: UIViewController, with frame: CGRect, animated: Bool, completion: (() -> Void)?) -> Bool
}

extension ViewControllerContainer where Self: UIViewController {

    var visibleIndex: Int? {
        guard let viewController = visibleViewController else {
            return nil
        }

        return viewControllers.firstIndex(of: viewController)
    }

    var visibleViewController: UIViewController? {
        return children.last
    }

    @discardableResult func setViewController(_ viewController: UIViewController, with frame: CGRect, animated: Bool, completion: (() -> Void)? = nil) -> Bool {
        if children.contains(viewController) {
            print("view controller is already child")
            return false
        }

        let disappearingViewController = visibleViewController

        viewController.willMove(toParent: self)

        //starting positon of new view
        if let disappearingView = disappearingViewController?.view {
            self.view.insertSubview(viewController.view, aboveSubview: disappearingView)
        } else {
            self.view.addSubview(viewController.view)
        }
        viewController.view.alpha = animated ? 0 : 1
        viewController.view.frame = frame

        addChild(viewController)

        disappearingViewController?.willMove(toParent: nil)
        disappearingViewController?.beginAppearanceTransition(false, animated: animated)
        disappearingViewController?.view.tintAdjustmentMode = .normal

        let animationBlock = { () -> Void in
            viewController.view.alpha = 1
        }

        let completionBlock: (Bool) -> Void = { [weak self] finished in
            //call view did disappear on disappearing child
            disappearingViewController?.view.removeFromSuperview()
            viewController.didMove(toParent: nil)
            disappearingViewController?.endAppearanceTransition()
            disappearingViewController?.removeFromParent()

            viewController.didMove(toParent: self)
            completion?()
        }

        if animated {
            UIView.animate(withDuration: 0.25,
                           delay: 0,
                           options: [.transitionCrossDissolve],
                           animations: animationBlock,
                           completion: completionBlock
            )
        } else {
            animationBlock()
            completionBlock(true)
        }

        return true
    }
}

protocol PresentedInContainer {
    var container: ContainerViewController {get set}
}

class ContainerViewController: UIViewController, ViewControllerContainer {

    var visibleViewController: UIViewController?
    var viewControllers: [UIViewController]

    override var childForStatusBarStyle: UIViewController? {
        return visibleViewController
    }

    override var childForStatusBarHidden: UIViewController? {
        return visibleViewController
    }

    override var prefersStatusBarHidden: Bool {
        return visibleViewController?.prefersStatusBarHidden ?? false
    }

    override var preferredStatusBarUpdateAnimation: UIStatusBarAnimation {
        return .slide
    }

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }

    init(viewControllers: [UIViewController]) {
        self.viewControllers = viewControllers
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func updateVisibleChildHeight(_ height: CGFloat) {
        guard let _ = visibleViewController else {
            return
        }

        var frame = self.view.bounds
        frame.size.height = height
        self.view.frame = frame
    }

    private func safeFrame() -> CGRect {
        return self.view.bounds
    }

    func isVisibleController(_ viewController: UIViewController) -> Bool {
        if visibleViewController == viewController {
            return true
        }

        return visibleViewController?.children.first(where: { $0 == viewController }) != nil
    }

    func selectViewController(at index: Int, animated: Bool, completion: (() -> Void)? = nil) {
        guard let viewController = viewControllers[safe: index] else {
            return
        }

        setViewController(viewController, with: safeFrame(), animated: animated, completion: completion)
        visibleViewController = viewController
    }

    private func viewController(for viewController: UIViewController) -> UIViewController? {
        return viewControllers.first { (vc) -> Bool in
            if vc == viewController {
                return true
            }
            return vc.children.first(where: { $0 == viewController }) != nil
        }
    }

    func close(viewController: UIViewController, animated: Bool, completion: @escaping (Bool) -> Void) {
        guard let vcToRemove = self.viewController(for: viewController), vcToRemove == visibleViewController else {
            completion(false)
            return
        }

        viewControllers.removeElement(vcToRemove)
        if let presentingViewController = viewControllers.last, let index = self.viewControllers.firstIndex(of: presentingViewController) {
            selectViewController(at: index, animated: animated, completion: {
                completion(true)
            })
        }
    }
}

extension Array where Element: Equatable {

    @discardableResult public mutating func removeElement(_ element: Element) -> Int? {
        let firstIndex = self.firstIndex { $0 == element }
        self = filter { $0 != element }
        return firstIndex
    }

    public mutating func removeElements(_ elements: [Element]) {
        if elements.count == 0 {
            return
        }

        self = filter { return !elements.contains($0) }
    }
}
