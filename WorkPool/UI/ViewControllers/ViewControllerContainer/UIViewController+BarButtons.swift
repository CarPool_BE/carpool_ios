//
//  UIViewController+BarButtons.swift
//  WorkPool
//
//  Created by Marek Labuzik on 15/07/2019.
//  Copyright © 2019 Marek Labuzik. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {

    func barButtonItem(withTitle title: String, color: UIColor, fontWeight: UIFont.Weight, target: Any?, action: Selector) -> UIBarButtonItem {
        let barButtonItem = UIBarButtonItem(title: title,
                                            style: .plain,
                                            target: target ?? self,
                                            action: action
        )

//        let font = Font.font(with: fontWeight, size: 16)
//        let attributes = TextAttributes.attributes(font: font, color: color)
//        barButtonItem.setTitleTextAttributes(attributes, for: .normal)
//        barButtonItem.setTitleTextAttributes(attributes, for: .highlighted)
//
//        let disabledAttributes = TextAttributes.attributes(font: font, color: Color.lightGray)
//        barButtonItem.setTitleTextAttributes(disabledAttributes, for: .disabled)

        return barButtonItem
    }

    func barButtonItem(withImage image: UIImage, target: Any? = nil, action: Selector) -> UIBarButtonItem {
        return UIBarButtonItem(image: image, style: .plain, target: target ?? self, action: action)
    }

    func setLeftBarButtonItem(with image: UIImage, target: Any? = nil, action: Selector) {
        let leftBarButtonItem = UIBarButtonItem(image: image, style: .plain, target: target ?? self, action: action)
        self.navigationItem.leftBarButtonItem = leftBarButtonItem
    }

    func setLeftBarButtonItem(withTitle title: String, color: UIColor = UIColor.orange, fontWeight: UIFont.Weight = .regular, target: Any? = nil, action: Selector) {
        self.navigationItem.leftBarButtonItem = barButtonItem(withTitle: title, color: color, fontWeight: fontWeight, target: target, action: action)
        self.navigationItem.leftBarButtonItem!.tintColor = color
    }

//    @objc func setCancelButton(target: Any? = nil, action: Selector) {
//        setLeftBarButtonItem(withTitle: NSLocalizedString("BUTTON_CANCEL", "general cancel button title"), target: target, action: action)
//    }
//
//    @objc func setCloseButton(target: Any? = nil, action: Selector) {
//        setLeftBarButtonItem(withTitle: NSLocalizedString("BUTTON_CLOSE", "general close button title"), target: target, action: action)
//    }

    @objc enum BackButtonStyle: Int {
        case back
        case close
    }

    @objc enum BackButtonColorStyle: Int {
        case white
        case black
    }

    //objc
    @objc func setBackButton(_ style: BackButtonStyle, colorStyle: BackButtonColorStyle, action: Selector) {
        setBackButton(style, colorStyle: colorStyle, target: self, action: action)
    }

    func setBackButton(_ style: BackButtonStyle, colorStyle: BackButtonColorStyle, target: Any?, action: Selector) {
        var image: UIImage
        switch style {
        case .back:
            image = #imageLiteral(resourceName: "back")
        case .close:
            image = #imageLiteral(resourceName: "close")
        }

        var color: UIColor
        switch colorStyle {
        case .white:
            color = UIColor.white
        case .black:
            color = UIColor.gray
        }

        guard let navigationController = self.navigationController, navigationController.viewControllers.count > 1 else {
            self.setLeftBarButtonItem(with: image, target: target, action: action)
            self.navigationItem.leftBarButtonItem?.imageInsets = UIEdgeInsets(top: 0, left: -5, bottom: 0, right: 0)
            self.navigationItem.leftBarButtonItem?.tintColor = UIColor.gray
            return
        }

        let count = navigationController.viewControllers.count

        let previousVC = self.navigationController?.viewControllers[count - 2]
        previousVC?.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: target, action: action)
        navigationController.navigationBar.tintColor = color
        navigationController.navigationBar.backIndicatorImage = image
        navigationController.navigationBar.backIndicatorTransitionMaskImage = image
    }

    func changeImagePosition(image: UIImage?, origin: CGPoint) -> UIImage? {
        let size = image?.size ?? .zero

        UIGraphicsBeginImageContextWithOptions(size, false, 2)
        image?.draw(in: CGRect(origin: origin, size: size))

        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage
    }

    @discardableResult func setRightBarButtonItem(withTitle title: String, color: UIColor = UIColor.orange, fontWeight: UIFont.Weight = .regular, target: Any? = nil, action: Selector) -> UIBarButtonItem {
        let rightBarButton = barButtonItem(withTitle: title, color: color, fontWeight: fontWeight, target: target ?? self, action: action)
        self.navigationItem.rightBarButtonItem = rightBarButton
        self.navigationItem.rightBarButtonItem!.tintColor = color

        return rightBarButton
    }

    func setRightBarButtonItem(withImage image: UIImage, action: Selector) {
        self.navigationItem.rightBarButtonItem = barButtonItem(withImage: image, action: action)
    }
}
