//
//  CreateEditCarViewController.swift
//  WorkPool
//
//  Created by Marek Labuzik on 11/07/2019.
//  Copyright © 2019 Marek Labuzik. All rights reserved.
//

import UIKit

struct CarValidator {
    enum CarValidatorError: Error {
        case noName
        case emptyCarNumber
        case lenghtCarNumber
        case emptyCarColor
        case emptyCarBrand
        
        var localizedError: String {
            switch self {
            case .noName:
                return "CheckDataAndRetry".localized()
            case .emptyCarNumber:
                return "".localized()
            case .lenghtCarNumber:
                return "".localized()
            case .emptyCarColor:
                return "".localized()
            case .emptyCarBrand:
                return "".localized()
            }
        }
    }
    
    func validate(registration car: Car) throws -> Bool {
        guard car.brand?.isEmpty == false else {
            throw CarValidatorError.emptyCarBrand
        }
        
        guard car.spz?.isEmpty == false else {
            throw CarValidatorError.emptyCarNumber
        }
        
        guard car.spz?.count != 8 else {
            throw CarValidatorError.lenghtCarNumber
        }
        
        guard car.color?.isEmpty == false else {
            throw CarValidatorError.emptyCarColor
        }
        return true
    }

}

final class CreateEditCarViewController: NSObject {
    let RISTRICTED_CHARACTERS = "'*=+[]\\|;:'\",<>/?%"
    
    func createCar(on viewController: UIViewController?, completition: @escaping (Result<Car, RequestErrors>) -> Void ) {
        guard let controller = viewController else {
            completition(.failure(.none))
            return
        }
        
        let alert = UIAlertController(title: "CrateCar_Title".localized(),
                                      message: "CreateCar_Message".localized(),
                                      preferredStyle: .alert)
        alert.addTextField { [weak self] field in
            field.delegate = self
            field.placeholder = "Car_Number_Placeholder".localized()
            field.tag = 8
        }
        alert.addTextField { [weak self] field in
            field.delegate = self
            field.placeholder = "Car_Color_Placeholder".localized()
            field.tag = 20
        }
        alert.addTextField { [weak self] field in
            field.delegate = self
            field.placeholder = "Car_Brand_Placeholder".localized()
            field.tag = 64
        }
        
        let cancelAction = UIAlertAction(title: "Cancel_Button".localized(), style: UIAlertAction.Style.cancel, handler: nil)
        let okAction = UIAlertAction(title: "OK_Button".localized(), style: .default) { _ in
            var car = Car()
            car.spz = alert.textFields?[0].text
            car.color = alert.textFields?[1].text
            car.brand = alert.textFields?[2].text
            let validator = CarValidator()
            do {
                let isValid = try validator.validate(registration: car)
                if isValid {
                    AppData.meData.auth.carService.add(car: car, completion: { result in
                        switch result {
                        case .success:
                            AppData.meData.auth.refreshProfile(completion: { resul in
                                switch resul {
                                case .failure:
                                    alert.title = "CheckDataAndRetry".localized()
                                    controller.present(alert, animated: true, completion: nil)
                                case .success(let newCar):
                                    completition(.success(newCar))
                                }
                            })

                        case .failure(let error):
                            if error == .dataIsExists {
                                alert.title = "CarData_IsExists".localized()
                                controller.present(alert, animated: true, completion: nil)
                            } else {
                                alert.title = "CheckDataAndRetry".localized()
                                controller.present(alert, animated: true, completion: nil)
                            }
                        }
                    })
                }
            } catch let error {
                if let err = error as? CarValidator.CarValidatorError {
                    alert.title = err.localizedError
                    controller.present(alert, animated: true, completion: nil)
                } else {
                    alert.title = "CheckDataAndRetry".localized()
                    controller.present(alert, animated: true, completion: nil)
                }
            }
        }
        alert.addAction(okAction)
        alert.addAction(cancelAction)
        
        controller.present(alert, animated: true, completion: nil)
    }
}

extension CreateEditCarViewController: UITextFieldDelegate {

    public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let set = CharacterSet(charactersIn: RISTRICTED_CHARACTERS)
        let inverted = set.inverted
        let filtered = string.components(separatedBy: inverted).joined(separator: "")
        if filtered == string {
            return false
        }
        guard let text = textField.text else { return true }
        
        let newLength = text.count + string.count - range.length
        return newLength <= textField.tag
    }
}
