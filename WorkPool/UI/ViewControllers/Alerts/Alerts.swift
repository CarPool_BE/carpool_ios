//
//  Alerts.swift
//  WorkPool
//
//  Created by Marek Labuzik on 05/06/2019.
//  Copyright © 2019 Marek Labuzik. All rights reserved.
//

import UIKit

extension UIAlertController {

    class func alert(withTitle: String, subtitle: String, buttonTitle: String) -> UIAlertController {
        let alert = UIAlertController(title: withTitle, message: subtitle, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: buttonTitle, style: .cancel, handler: nil))
        return alert
    }
    
    class func alert(withTitle: String, subtitle: String, buttonTitle: String, handler: ((UIAlertAction) -> Void)?) -> UIAlertController {
        let alert = UIAlertController(title: withTitle, message: subtitle, preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: buttonTitle, style: .cancel) { action in
            if handler != nil {
                handler!(action)
            }
        })
        return alert
    }

    class func topMostControllerShowAlert(withTitle: String, subtitle: String, buttonTitle: String, handler: ((UIAlertAction) -> Void)?) {
        if var topController = UIApplication.shared.keyWindow?.rootViewController {
            while let presentedViewController = topController.presentedViewController {
                topController = presentedViewController
            }

            let alert = UIAlertController(title: withTitle, message: subtitle, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: buttonTitle, style: .cancel, handler: handler))
            topController.present(alert, animated: true, completion: nil)
        }
    }
}
