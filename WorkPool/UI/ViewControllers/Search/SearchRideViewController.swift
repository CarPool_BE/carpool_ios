//
//  SearchRideViewController.swift
//  WorkPool
//
//  Created by Marek Labuzik on 05/05/2019.
//  Copyright © 2019 Marek Labuzik. All rights reserved.
//

import UIKit
import UICustomElements

enum SearchRideActionType {
    case didSelectAddressFrom
    case didSelectAddressTo
    case didSelectReverse
    case createRide
    case getNewData
}

protocol SearchRideViewControllerDelegate: class {
    func viewController(_ viewController: SearchRideViewController, didPerformAction: SearchRideActionType)
    func viewController(_ viewController: SearchRideViewController, didUpdate date: Date)
    func viewController(_ viewController: SearchRideViewController, showDetailFor userID: Int, andRide: Int)
}

final class SearchRideViewController: UIViewController, UITableViewDelegate, UITableViewDataSource,
                                        UISearchRideHeaderDelegate, UIEmptyViewDelegate {
    weak var delegate: SearchRideViewControllerDelegate?
    var headerView: UISearchRideHeader?
    var tableView: UITableView = UITableView(frame: CGRect(x: 0, y: 0, width: 100, height: 100), style: .plain)
    var activityView: ActivityView?
    var viewModel: RidesViewModel? {
        didSet {
            if isViewLoaded {
                self.reloadData()
            }
            self.updeteHeader()
        }
    }
    
    private var rides: [RideViewModel] {
        return self.viewModel?.viewModels ?? []
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "TabBar_SearchRides".localized()
        self.setupTableView()
        self.updeteHeader()
        self.addCreateButton()
        self.setupViews()
        self.setupHeaderView()
        self.setupConstraints()
    }

    func setupViews() {
        guard let header = Bundle.customElements?.loadNibNamed("UISearchRideHeader",
                                                               owner: self,
                                                               options: nil)?.first as? UISearchRideHeader else { return }
        self.headerView = header
        if #available(iOS 13.0, *) {
            self.headerView?.color = .secondaryLabel
            self.view.backgroundColor = .systemBackground
        } else {
            self.headerView?.color = .black20
            self.view.backgroundColor = .white
        }
        
        self.view.addSubview(self.tableView)
        self.view.addSubview(self.headerView!)
        
    }

    func setupConstraints() {
        guard let header = self.headerView else { return }
        self.tableView.translatesAutoresizingMaskIntoConstraints = false
        header.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            header.topAnchor.constraint(equalTo: self.view.topAnchor, constant: 0),
            header.rightAnchor.constraint(equalTo: self.view.rightAnchor, constant: 0),
            header.leftAnchor.constraint(equalTo: self.view.leftAnchor, constant: 0),
            header.bottomAnchor.constraint(equalTo: self.tableView.topAnchor, constant: 0),

            self.tableView.rightAnchor.constraint(equalTo: self.view.rightAnchor, constant: 0),
            self.tableView.leftAnchor.constraint(equalTo: self.view.leftAnchor, constant: 0),
            self.tableView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: 0)
            ])
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.delegate?.viewController(self, didPerformAction: .getNewData)
        self.updeteHeader()
    }
    
    func addCreateButton() {
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Create_Ride_Title".localized(),
                                                                 style: .plain,
                                                                 target: self,
                                                                 action: #selector(pushCreateRideVC))
    }

    func startAnimating() {
        if self.activityView == nil {
            self.activityView = ActivityView(frame: self.tableView.bounds)
            self.activityView?.startAnimating()
            self.tableView.addSubview(self.activityView!)
        }
    }

    func stopAnimating() {
        self.activityView?.stopAnimating()
        self.activityView?.removeFromSuperview()
        self.activityView = nil
    }

    @objc func pushCreateRideVC() {
        delegate?.viewController(self, didPerformAction: .createRide)
    }
    
    func setupTableView() {
        self.tableView.alwaysBounceVertical = false
        self.tableView.separatorStyle = UITableViewCell.SeparatorStyle.none
        self.tableView.rowHeight = UITableView.automaticDimension
        self.tableView.estimatedSectionHeaderHeight = UITableView.automaticDimension
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(UINib(nibName: "UISearchRideCell",
                                      bundle: Bundle.customElements),
                                forCellReuseIdentifier: "UISearchRideCell")
        if #available(iOS 13.0, *) {
            self.tableView.backgroundColor = .systemBackground
        }
    }

    func updeteHeader() {
        self.headerView?.set(from: self.viewModel?.searchContext.from ?? "", to: self.viewModel?.searchContext.to ?? "")
    }

    func reloadData() {
        self.tableView.alwaysBounceVertical = true
        self.tableView.reloadData()
        if self.tableView.numberOfRows(inSection: 0) > 0 {
            self.tableView.scrollToRow(at: IndexPath(row: 0, section: 0), at: .middle, animated: true)
        }
        self.tableView.alwaysBounceVertical = false
        self.updeteHeader()
    }

    func setupHeaderView() {
        self.headerView!.tintColor = .mainGreen
        self.headerView!.delegate = self
        self.updeteHeader()
        self.headerView?.backgroundColor = .white
    }

    // MARK: - table view deledate and data source
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if #available(iOS 11, *) {
            return UITableView.automaticDimension
        } else {
            return 152
        }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let count = self.rides.count
        self.showEmptyTable(isEmpty: !(count > 0))
        return count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "UISearchRideCell",
                                                       for: indexPath) as? UISearchRideCell
            else { fatalError("DequeueReusableCell failed") }
        let viewModel = self.rides[indexPath.row]
        cell.startAddressLabel.text = viewModel.from
        cell.startTimeLabel.text = viewModel.startTime
        cell.endAddressLabel.text = viewModel.to
        cell.driverNameLabel.text = ""
        if let drvier = viewModel.driver {
            cell.driverNameLabel.text = drvier.firstName + " " + drvier.lastName
        }
        cell.identifier = viewModel.model.driver?.identifier
        cell.tintColor = .mainGreen
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let viewModel = self.rides[indexPath.row]
        guard let cell = tableView.cellForRow(at: indexPath) as? UISearchRideCell,
              let userID = cell.identifier,
              let rideID = viewModel.model.identifier else {
            return
        }
        
        self.delegate?.viewController(self, showDetailFor: userID, andRide: rideID)
    }

    // MARK: -
    func showEmptyTable(isEmpty: Bool) {
        if isEmpty {
            guard let view = Bundle.customElements?.loadNibNamed("UIEmptyView",
                                                                 owner: self,
                                                                 options: nil)?.first as? UIEmptyView else { return }
            let string = NSMutableAttributedString(string: "SearchRides_Empty_text".localized())
            let actionString = NSMutableAttributedString(string: "SearchRides_Empty_action".localized())
            actionString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.mainGreen, range: NSRange(location: 0, length: actionString.length))
            if #available(iOS 13.0, *) {
                string.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.label, range: NSRange(location: 0, length: string.length))
                view.backgroundColor = .systemBackground
            }
            string.append(NSAttributedString(string: "\n"))
            string.append(actionString)

            view.set(text: string)
            view.delegate = self
            self.tableView.backgroundView = view
        } else {
            self.tableView.backgroundView = nil
        }
    }

    // MARK: - UISearchRideHeaderDelegate methods
    func emptyViewTextViewDidPressed(_ emptyView: UIEmptyView) {
        self.pushCreateRideVC()
    }
    func searchRideHeader(_ searchRideHeader: UISearchRideHeader, didPerformAction actionType: UISearchRideHeaderActionType) {
        self.delegate?.viewController(self, didPerformAction: .didSelectReverse)
    }
    func searchRideHeader(_ searchRideHeader: UISearchRideHeader, selectedDateChange: Date) {
        self.delegate?.viewController(self, didUpdate: selectedDateChange)
    }

    func searchRideHeader(_ searchRideHeader: UISearchRideHeader, valueFromDidChange: String) {
    }

    func searchRideHeader(_ searchRideHeader: UISearchRideHeader, valueToDidChange: String) {
    }

    func searchRideHeaderDidBeginEditingFromField(_ searchRideHeader: UISearchRideHeader) {
        delegate?.viewController(self, didPerformAction: .didSelectAddressFrom)
    }

    func searchRideHeaderDidBeginEditingToField(_ searchRideHeader: UISearchRideHeader) {
        delegate?.viewController(self, didPerformAction: .didSelectAddressTo)
    }
}
