//
//  SearchRideDetailViewController.swift
//  WorkPool
//
//  Created by Marek Labuzik on 18/06/2019.
//  Copyright © 2019 Marek Labuzik. All rights reserved.
//

import UIKit
import UICustomElements

enum SearchRideDetailActionType {
    case registerRidesFromList
}

enum ReservationResultState {
    case failure
    case success
}

protocol SearchRideDetailViewControllerDelegate: class {
    func viewController(_ viewController: SearchRideDetailViewController, selectCellAt indexPath: IndexPath)

    func viewController(_ viewController: SearchRideDetailViewController, didPerform action: SearchRideDetailActionType)
}

final class SearchRideDetailViewController: UIViewController {
    weak var deledate: SearchRideDetailViewControllerDelegate?
    
    var accessoryButonView: AccessoryButtonView?
    var selectedCount: Int = 0
    var isInitialScrolled: Bool = true
    var tableView: UITableView = UITableView()
    var headerView: UIRideDetailHeader?
    
    var viewModel: SearchRideDetalViewModel? {
        didSet {
            self.tableView.reloadData()
            self.updateHeader()
        }
    }
    
    override var canBecomeFirstResponder: Bool {
        return true
    }
    
    override var inputAccessoryView: UIView? {
        return self.accessoryButonView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupTableView()
        self.setupHeader()
        self.setupAccessorieView()
        self.setupFrames()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.inputAccessoryView?.isHidden = true
        self.tabBarController?.tabBar.isHidden = true
        self.updateInputAccesorieView()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.tabBarController?.tabBar.isHidden = false
    }
    
    public func scrollTo(ride: Int) {
        if self.isInitialScrolled {
            guard let model = self.viewModel,
                let indexPath = model.getIndexPathFor(rideID: ride) else {
                    return
            }
            self.isInitialScrolled = false
            self.selectCell(at: indexPath)
            self.tableView.scrollToRow(at: indexPath, at: .top, animated: false)
        }
    }
    
    func setupTableView() {
        self.tableView.separatorStyle = .none
        self.tableView.allowsMultipleSelection = false
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(UINib(nibName: "UIRegisterRideCell",
                                      bundle: Bundle.customElements),
                                forCellReuseIdentifier: "UIRegisterRideCell")
        
        self.view.addSubview(self.tableView)
    }
    
    func setupHeader() {
        guard let header = Bundle.customElements?.loadNibNamed("UIRideDetailHeader",
                                                               owner: nil,
                                                               options: nil)?.first as? UIRideDetailHeader else { return }
        self.headerView = header
        self.headerView?.firstNameLabel.text = ""
        self.headerView?.lastNameLabel.text = ""
        self.headerView?.carTypeLabel.text = ""
        self.headerView?.carNumberAndColorLabel.text = ""
        self.view.addSubview(self.headerView!)
    }
    
    func updateHeader() {
        if let driver = self.viewModel?.driver {
            self.headerView?.firstNameLabel.text = driver.firstName
            self.headerView?.lastNameLabel.text = driver.lastName
            self.headerView?.carTypeLabel.text = driver.carBrand
            self.headerView?.carNumberAndColorLabel.text = driver.carNumber + ", " + driver.carColor
        }
    }
    
    func setupAccessorieView() {
        let height: CGFloat = 44 + 16 + 8
        let size: CGSize = CGSize(width: self.view.frame.size.width, height: height)
        self.accessoryButonView = AccessoryButtonView(size: size, isLargePhone: UIDevice.current.deviceType.isLarge)
        self.accessoryButonView?.tintColor = .mainGreen
        self.accessoryButonView?.setAccessoryButtonView(type: .registerRide)
        self.accessoryButonView?.delegate = self
    }
    
    func setupFrames() {
        guard let header = self.headerView else {
            return
        }
        let navBarHeight = (navigationController?.navigationBar.frame.size.height ?? 0) + UIApplication.shared.statusBarFrame.size.height
        self.tableView.frame = CGRect(x: 0, y: header.frame.height,
                                      width: UIScreen.main.bounds.size.width,
                                      height: self.view.frame.height - navBarHeight - header.frame.height)
    }
}

extension SearchRideDetailViewController: AccessoryButtonViewDelegate {
    func accessoryButtonPressed(_ view: AccessoryButtonView) {
        self.deledate?.viewController(self, didPerform: .registerRidesFromList)
    }
}

extension SearchRideDetailViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        guard let model = self.viewModel else {
            return ""
        }
        return model.getName(for: section)
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        view.backgroundColor = .white
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let model = self.viewModel else {
            return 0
        }
        return model.numberOfRows(in: section)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        guard let model = self.viewModel else {
            return 0
        }
        return model.numberOfSections()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "UIRegisterRideCell") as? UIRegisterRideCell else {
            fatalError()
        }
        guard let model = self.viewModel else { return cell }
        guard let ride = model.getRide(at: indexPath),
            let rideID = ride.model.identifier else { return cell }

        if ride.isPassenger {
            cell.disableColor = .black20
        } else {
            cell.tintColor = .mainGreen
        }
        
        cell.startAddressLabel.attributedText =  ride.startAddress
        cell.endAddressLabel.attributedText = ride.endAddress
        cell.startTimeLabel.text = ride.startTime
        cell.endTimeLabel.text = ride.endTime
        cell.noteLabel.text = ride.note
        cell.rideId = rideID
        cell.result = ride.rideRequestResult.resultForCell
        cell.isRideSelected = ride.isSelected
        cell.setAnimation(type: MeAppearance.activityIndicatorType, color: .mainGreen)
        self.setRide(state: ride.rideState, forCell: cell)
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.selectCell(at: indexPath)
    }
    
    func setRide(state: RideState, forCell cell: UIRegisterRideCell) {
        switch state {
        case .full:
            cell.setRide(status: .full)
        case .none:
            cell.setRide(status: .none)
        case .free:
            cell.setRide(status: .free)
        case .reserved:
            cell.setRide(status: .reserved)
        }
    }
    
    func startAnimation(for indexPath: IndexPath) {
        guard let cell = tableView.cellForRow(at: indexPath) as? UIRegisterRideCell else { return }
        cell.startAnimating()
    }

    func stopAnimation(for indexPath: IndexPath) {
        guard let cell = tableView.cellForRow(at: indexPath) as? UIRegisterRideCell else { return }
        cell.stopAnimating()
    }
    
    private func selectCell(at indexPath: IndexPath) {
        guard let model = self.viewModel,
            let ride = model.getRide(at: indexPath) else { return }
        if ride.isPassenger { return }
        self.deledate?.viewController(self, selectCellAt: indexPath)

        if !ride.isSelected {
            self.selectedCount += 1
        } else {
            self.selectedCount -= 1
        }
        self.updateInputAccesorieView()
    }
    
    func updateCell(at indexPath: IndexPath) {
        self.tableView.beginUpdates()
        self.tableView.reloadRows(at: [indexPath], with: .none)
        self.tableView.endUpdates()
        self.updateInputAccesorieView()
    }
    
    private func updateInputAccesorieView() {
        var inset = self.tableView.contentInset
        
        guard let inputView = self.accessoryButonView else {
            return
        }
        if self.selectedCount > 0 {
            self.inputAccessoryView?.isHidden = false
            inset.bottom = inputView.frame.size.height
        } else {
            self.inputAccessoryView?.isHidden = true
            inset.bottom = 0
        }
        self.tableView.contentInset = inset
//        self.tableView.layoutIfNeeded()
    }
}
