//
//  DetailSearchRideViewModel.swift
//  WorkPool
//
//  Created by Marek Labuzik on 19/06/2019.
//  Copyright © 2019 Marek Labuzik. All rights reserved.
//

import Foundation

struct SearchRideDetail {
    var rides: [RideViewModel]
    var drver: UserViewModel
    
    init() {
       self.rides = []
        self.drver = UserViewModel.empty()
    }
}

struct SearchRideDetalViewModel {
    var dayRides: [DayRides]
    var sectionName: [String]
    var driver: UserViewModel
    var errorHandler: ((AppError) -> Void)?
    init() {
        self.dayRides = []
        self.sectionName = []
        self.driver = UserViewModel.empty()
    }

    mutating func setUser(user: UserViewModel) {
        self.driver = user
    }

    mutating func setRides(rides: [RideViewModel]) {
        self.removeAll()

        for ride in rides {
            let sectionName = self.sectionName.filter { $0 == ride.dateString }
            if sectionName.count == 1 {
                guard let index = self.sectionName.firstIndex(of: sectionName.first!) else { continue }
                self.dayRides[index].rides.append(ride)
            } else {
                var data = DayRides(date: ride.model.startDate)
                data.rides.append(ride)

                self.dayRides.append(data)
                self.sectionName.append(ride.dateString)
            }
        }
    }

    mutating func removeAll() {
        self.dayRides.removeAll()
        self.sectionName.removeAll()
    }
    
    mutating func select(rideId: Int) {
        guard let indexPath = self.getIndexPathFor(rideID: rideId)
            else { return        }
        self.select(at: indexPath)
    }
    
    mutating func select(at indexPath: IndexPath) {
        guard var ride = getRide(at: indexPath)
            else { return }
        ride.isSelected = !ride.isSelected
        self.update(ride: ride, at: indexPath)
    }
    
    mutating func setRideRequestResult(_ requestResult: RideRequestResult, at indexPath: IndexPath) {
        guard var ride = getRide(at: indexPath)
            else { return }
        ride.rideRequestResult = requestResult
        self.update(ride: ride, at: indexPath)
    }
    
    mutating func setRideState(_ state: RideState, at indexPath: IndexPath) {
        guard var ride = getRide(at: indexPath)
            else { return        }
        ride.rideState = state
        self.update(ride: ride, at: indexPath)
    }
    
    func getSelectedRides() -> [RideViewModel] {
        var selected: [RideViewModel] = []
        for dayRide in self.dayRides {
            let filtred: [RideViewModel] = dayRide.rides.filter { $0.isSelected == true }
            selected.append(contentsOf: filtred)
        }
        return selected
    }
    
    func getIndexPathFor(rideID: Int?) -> IndexPath? {
        guard self.dayRides.count > 0 else { return nil }
        for section in 1...self.numberOfSections() {
            let sectionNumber = section - 1
            for row in 1...self.numberOfRows(in: sectionNumber) {
                let rowNumber = row - 1
                let indexPath = IndexPath(row: rowNumber, section: sectionNumber)
                guard self.exist(indexPath: indexPath) else { return nil }

                guard let ride = self.getRide(at: indexPath),
                    let rideIdentifier = ride.model.identifier else {
                    break
                }
                if rideIdentifier == rideID {
                    return indexPath
                }
            }
        }
        return nil
    }

    func exist(indexPath: IndexPath ) -> Bool {
        if self.dayRides.count < indexPath.section {
            return false
        }
        if self.dayRides[indexPath.section].rides.count < indexPath.row {
            return false
        }
        return true
    }

    func getRide(at indexPath: IndexPath) -> RideViewModel? {
        guard self.isExist(indexPath: indexPath) else {
            return nil
        }
        return self.dayRides[indexPath.section].rides[indexPath.row]
    }

    mutating func update(ride: RideViewModel, at indexPath: IndexPath) {
        guard self.isExist(indexPath: indexPath) else {
            return
        }
        self.dayRides[indexPath.section].rides[indexPath.row] = ride
    }

    func getName(for section: Int) -> String {
        return self.sectionName[section]
    }

    func numberOfRows(in section: Int) -> Int {
        return self.dayRides[section].rides.count
    }

    func numberOfSections() -> Int {
        return self.dayRides.count
    }
    
    func isExist(indexPath: IndexPath) -> Bool {
        if self.dayRides.count < indexPath.section {
            return false
        }
        if self.dayRides[indexPath.section].rides.count < indexPath.row {
            return false
        }
        return true
    }
}

struct DayRides {
    var rides: [RideViewModel]
    var date: Date
    init(date: Date?) {
        self.rides = []
        self.date = date ?? Date(timeIntervalSince1970: 0)
    }
}

final class DetailSearchRideViewModel {
    private(set) var model: SearchRideDetalViewModel = SearchRideDetalViewModel()
    var errorHandler: ((AppError) -> Void)?
    
    func getUser(with userID: Int, completion: @escaping (Result<UserViewModel, RequestErrors>) -> Void) {
        AppData.meData.auth.userService.getUser(with: userID) { result in
            switch result {
            case .success(let user):
                completion(.success(UserViewModel(with: user)))
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }

    func getUserRides(with userID: Int, completion: @escaping (Result<[RideViewModel], RequestErrors>) -> Void) {
        AppData.meData.auth.userService.getUserRides(with: userID) { result in
            switch result {
            case .success(let rideList):
                let rides = rideList.rides.map { RideViewModel(model: $0) }
                completion(.success(rides))
            case .failure(let error):
                if error == .notFound {
                    completion(.success([]))
                } else {
                    completion(.failure(error))
                }
            }
        }
    }
    
}
