//
//  DetailRideViewModel.swift
//  WorkPool
//
//  Created by Marek Labuzik on 19/06/2019.
//  Copyright © 2019 Marek Labuzik. All rights reserved.
//

import Foundation

struct RideDetail: Model {
    init(with: Ride) {
        
    }
    
    static func empty<T>() -> T where T: Model {
        let rideDetail = RideDetail(with: Ride.empty())
        guard let emptyRideDetail = rideDetail as?T else {
            fatalError()
        }
        return emptyRideDetail
    }
}

final class DetailRideList {

    func getRideDetail(for rideID: Int, completion: @escaping (Result<RideViewModel, RequestErrors>) -> Void) {
        AppData.meData.auth.rideService.getRide(wtihID: rideID) { result in
            switch result {
            case .success(let ride):
                let rideView = RideViewModel(model: ride)
                completion(.success(rideView))
            case .failure(let err):
                if err == .notFound {
                    let rideView = RideViewModel(model: Ride.empty())
                    completion(.success(rideView))
                }
                completion(.failure(err))
            }
        }
    }
}
