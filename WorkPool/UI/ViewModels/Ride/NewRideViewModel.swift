//
//  NewRideViewModel.swift
//  WorkPool
//
//  Created by Marek Labuzik on 28/05/2019.
//  Copyright © 2019 Marek Labuzik. All rights reserved.
//

import Foundation
import MapKit

struct NewRideValidator {
    
    enum ValidatorError: Error {
        case modelError
        case noAddressFrom
        case noAddressTo
        case noNote
        case noReturnNote
        case noTime
        case noReturnTime
        case noDates
        case noCar
        
        func convertToAppError() -> AppError {
            switch self {
            case .modelError:
                return .modelError
            case .noAddressFrom:
                return .noAddressFrom
            case .noAddressTo:
                return .noAddressTo
            case .noCar:
                return .noCar
            case .noDates:
                return .noDates
            case .noNote:
                return .noNote
            case .noReturnNote:
                return .noReturnNote
            case .noReturnTime:
                return .noReturnTime
            case .noTime:
                return .noTime
            }
        }
    }
    
    func validate(ride: NewRideViewModel) throws -> Bool {
        guard ride.from.isEmpty == false else {
            throw ValidatorError.noAddressFrom
        }
        
        guard ride.to.isEmpty == false else {
            throw ValidatorError.noAddressTo
        }
        
        guard ride.dates.count != 0 else {
            throw ValidatorError.noDates
        }
        
        guard ride.time != nil else {
            throw ValidatorError.noTime
        }
        
        guard ride.carID != -1 else {
            throw ValidatorError.noCar
        }
        
        if ride.isReturn {
            guard ride.returnTime != nil else {
                throw ValidatorError.noReturnTime
            }
        }
        
        return true
    }
    
    static func getLocalizedError(with err: ValidatorError) -> String {
        var errorString: String =  ""
        switch err {
        case .modelError:
            errorString = "Unknown_Error"
        case .noAddressFrom:
            errorString = "NoAddressFrom"
        case .noAddressTo:
            errorString = "NoAddressTo"
        case .noNote:
            errorString = "NoNote"
        case .noReturnNote:
            errorString = "NoRetunrNote"
        case .noTime:
            errorString = "NoTime"
        case .noReturnTime:
            errorString = "NoReturnTime"
        case .noDates:
            errorString = "NoDate"
        case .noCar:
            errorString = "NotCar"
        }
        return errorString.localized()
    }
}

struct NewRideViewModel {
    var from: String
    var fromLon: Double
    var fromLat: Double
    var to: String
    var toLon: Double
    var toLat: Double
    private(set) var dates: [Date]
    var time: Date?
    var timeETA: Date?
    var note: String
    var carID: Int
    var carSPZ: String
    var numberOfPlaces: Int
    var isReturn: Bool
    var returnTime: Date?
    var returnTimeEta: Date?
    var returnNote: String
    var returnNumberOfPlaces: Int
    
    var encodedPolyline: String
    
    var errorHandler: ((AppError) -> Void)?

    init(meProfileData: User, lastFindingPoint: LastSearchAdress?, workGPS: CLLocationCoordinate2D, workAddress: String) {
        if let point = lastFindingPoint {
            self.from = point.title
            let position = point.gpsPosition()
            self.fromLat = position.latitude
            self.fromLon = position.longitude
        } else {
            self.from = meProfileData.home ?? ""
            self.fromLat = meProfileData.homeLat ?? 0.0
            self.fromLon = meProfileData.homeLon ?? 0.0
        }

        self.to = workAddress
        self.toLat = workGPS.latitude
        self.toLon = workGPS.longitude

        if let carID = meProfileData.car?.identifier {
            self.carID = carID
        } else {
            self.carID = -1
        }
        if let carSPZ = meProfileData.car?.spz {
            self.carSPZ = carSPZ

        } else {
            self.carSPZ = ""
        }

        if let numberOfPlaces = meProfileData.car?.numberOfPlaces {
            self.numberOfPlaces = numberOfPlaces
            self.returnNumberOfPlaces = numberOfPlaces
        } else {
            self.numberOfPlaces = 3
            self.returnNumberOfPlaces = 3
        }

        self.note = ""
        self.returnNote = ""
        self.isReturn = false
        self.dates = []
        self.encodedPolyline = ""
    }
    
    mutating func set(dates: [Date]) {
        self.dates.removeAll()
        self.dates = dates
    }
}
