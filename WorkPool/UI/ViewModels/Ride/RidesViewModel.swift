//
//  RidesViewModel.swift
//  WorkPool
//
//  Created by Marek Labuzik on 11/05/2019.
//  Copyright © 2019 Marek Labuzik. All rights reserved.
//

import Foundation
import CoreLocation

struct RidesSearchContext {
    var from: String = AppData.meData.profile?.home ?? ""
    var fromGps: CLLocationCoordinate2D = AppData.meData.profile?.getHomeGPS() ?? CLLocationCoordinate2D()

    var to: String = AppData.meData.workAddress
    var toGps: CLLocationCoordinate2D = AppData.meData.workGPS
    var date: Date = Date()
}

final class RidesViewModel {
    let rideList: RideList
    
    private(set) var viewModels: [RideViewModel] = []
    
    var searchContext: RidesSearchContext = RidesSearchContext()
    
    init(rideList: RideList) {
        self.rideList = rideList
        if let lasSearch = AppData.meData.lastSearch {
            self.searchContext.from = lasSearch.title
            self.searchContext.fromGps = lasSearch.gpsPosition()
        }
    }
    
    func getRides(completion: @escaping (Result<Rides, RequestErrors>) -> Void) {
        self.viewModels.removeAll()
        rideList.getRides(from: searchContext.from,
                          to: searchContext.to,
                          fromGps: searchContext.fromGps,
                          toGps: searchContext.toGps,
                          date: searchContext.date) { [weak self] (result) in
            guard let self = self else { return }
            switch result {
            case .success(let rides):
                self.viewModels = rides.rides.map { RideViewModel(model: $0) }
                completion(.success(rides))
            case .failure(let error):
                if error == .notFound {
                    self.viewModels = []
                }
                print(error)
                completion(.failure(error))
            }
        }
    }
}
