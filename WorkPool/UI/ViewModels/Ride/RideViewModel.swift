//
//  RideViewModel.swift
//  WorkPool
//
//  Created by Marek Labuzik on 11/05/2019.
//  Copyright © 2019 Marek Labuzik. All rights reserved.
//

import Foundation
import MapKit
import UICustomElements

struct GpsPositionString: DataConvertible, Hashable {
    var latitude: String = ""
    var longitude: String = ""
    init() {
        self.latitude = ""
        self.longitude = ""
    }
    init(latitude: String, longitude: String) {
        self.latitude = latitude
        self.longitude = longitude
    }
    init(coordinate: CLLocationCoordinate2D) {
        self.latitude = String(coordinate.latitude)
        self.longitude = String(coordinate.longitude)
    }
}

enum RideState {
    case none
    case reserved
    case free
    case full
}

enum RideRequestResult {
    case none
    case pending
    case success
    case failure
    
    var resultForCell: RequestResult {
        switch self {
        case .failure:
            return .failure
        case .none:
            return .none
        case .pending:
            return .pending
        case .success:
            return .success
        }
    }
}

struct RideViewModel {
    let model: Ride
    var from: String
    var startAddress: NSAttributedString
    var to: String
    var endAddress: NSAttributedString
    var dateString: String
    var startTime: String
    var endTime: String
    var numberOfPlaces: Int
    var rideWithUsers: Int
    var note: String
    var isSelected: Bool
    var users: [UserViewModel]
    var driver: UserViewModel?
    var isDriver: Bool
    var isPassenger: Bool
    var rideState: RideState
    var rideRequestResult: RideRequestResult
    
    var errorHandler: ((AppError) -> Void)?

    init(model: Ride) {
        self.model = model
        self.from = model.fromAddress ?? ""
        self.to = model.toAddress ?? ""
        self.dateString = model.startDate?.getDateString() ?? ""
        self.startTime = model.startDate?.getTimeString() ?? ""
        self.endTime = model.endDate?.getTimeString() ?? ""
        self.numberOfPlaces = model.numberOfPlaces
        self.rideWithUsers = model.rideWithUsers ?? 0
        self.note = model.note ?? ""
        self.startAddress = NSAttributedString(string: self.from.replacingOccurrences(of: ", ", with: "\n"))
        self.endAddress = NSAttributedString(string: self.to.replacingOccurrences(of: ", ", with: "\n"))
        self.isSelected = false
        self.users = []
        if let users = model.usersDetail {
            for user in users {
                self.users.append(UserViewModel(with: user))
            }
        }
        if let driver = model.driver {
            self.driver = UserViewModel(with: driver)
        }
        let myUserID = AppData.meData.profile?.identifier
        if driver?.identifier ==  myUserID {
            self.isDriver = true
        } else {
            self.isDriver = false
        }
        let passenger = self.users.filter({ $0.identifier == myUserID })
        if passenger.count == 1 {
            self.isPassenger = true
        } else {
            self.isPassenger = false
        }
        self.rideState = .none
        if self.isPassenger {
            self.rideState = .reserved
        } else if self.users.count == model.numberOfPlaces {
            self.rideState = .full
        } else if self.users.count < model.numberOfPlaces {
            self.rideState = .free
        }
        self.rideRequestResult = .none
    }

    func registerRide(completion: @escaping (Result<Bool, RequestErrors>) -> Void) {
        guard let rideID = self.model.identifier else { return }
        AppData.meData.auth.rideService.registerToRide(with: rideID) { result in
            completion(result)
        }
    }
}
