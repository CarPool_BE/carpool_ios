//
//  RegisterRideViewModel.swift
//  WorkPool
//
//  Created by Marek Labuzik on 19/06/2019.
//  Copyright © 2019 Marek Labuzik. All rights reserved.
//

import Foundation

struct RegisterRideViewModel: Model {
    var rideIds: [Int]
    var succuessRegistrationRidesIds: [Int]
    var failedRegistrationRidesIds: [Int]
    var driverID: Int
    
    static func empty<T>() -> T where T: Model {
        let registerRide = RegisterRideViewModel(rideIds: [], succuessRegistrationRidesIds: [],
                                                 failedRegistrationRidesIds: [], driverID: -1)
        guard let tRide = registerRide as? T else {
            fatalError()
        }
        return tRide
    }
}
