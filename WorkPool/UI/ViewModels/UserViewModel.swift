//
//  UserViewModel\.swift
//  WorkPool
//
//  Created by Marek Labuzik on 20/06/2019.
//  Copyright © 2019 Marek Labuzik. All rights reserved.
//

import Foundation

struct UserViewModel: Model {
    var nameArr: [String]
    var identifier: Int
    var firstName: String
    var lastName: String
    var phoneNumber: String?
    var carBrand: String
    var carNumber: String
    var carColor: String
    
    init(with user: User) {
        self.nameArr = user.name.split { $0 == " " }.map(String.init)
        
        self.firstName = user.name
        self.lastName = ""
        
        if nameArr.count > 0 {
            self.firstName = nameArr[0].trimmingCharacters(in: .whitespacesAndNewlines)
        }
        if nameArr.count > 1 {
            var lasNameArr = nameArr
            lasNameArr.remove(at: 0)
            for name in lasNameArr {
                self.lastName = self.lastName + " " + name.trimmingCharacters(in: .whitespacesAndNewlines)
            }
        }
        
        self.phoneNumber = user.phoneNumber
        self.identifier = user.identifier
        self.carBrand = user.car?.brand ?? ""
        self.carNumber = user.car?.spz ?? ""
        self.carColor = user.car?.color ?? ""
    }
    
    static func empty<T>() -> T where T: Model {
        let userViewModel = UserViewModel(with: User.empty())
        guard let meptyUserViewModel = userViewModel as?T else {
            fatalError()
        }
        return meptyUserViewModel
    }
}
