//
//  RegisterViewModel.swift
//  WorkPool
//
//  Created by Marek Labuzik on 31/05/2019.
//  Copyright © 2019 Marek Labuzik. All rights reserved.
//

import Foundation
import UIKit
import UICustomElements

struct LoginValidator {
    enum LoginValidatorError: Error {
        case noName
        case noWorkID
        case noEmail
        case invalidEmail
        case noPassword
        case noReTypePassword
        case passwordNoMatch
        case passworMinLenght
        case passwordCriteria
        case workIDLenght
        
        func convertToAppError() -> AppError {
            switch self {
            case .invalidEmail:
                return .invalidEmail
            case .noEmail:
                return .noEmail
            case .noName:
                return .noName
            case .noPassword:
                return .noPassword
            case .noReTypePassword:
                return .noReTypePassword
            case .noWorkID:
                return .noWorkID
            case .passwordNoMatch:
                return .passwordNoMatch
            case .passworMinLenght:
                return .passworMinLenght
            case .passwordCriteria:
                return .passwordCriteria
            case .workIDLenght:
                return .workIDLenght
            }
        }
    }

    func validate(registration: LoginViewModel, forLogin: Bool) throws -> Bool {
        guard registration.email.isEmpty == false else {
            throw LoginValidatorError.noEmail
        }

        guard LoginValidator.isValidEmail(testStr: registration.email) else {
            throw LoginValidatorError.invalidEmail
        }

        guard registration.password.isEmpty == false else {
            throw LoginValidatorError.noPassword
        }

        if forLogin == false {
            guard registration.reTypePassword.isEmpty == false else {
                throw LoginValidatorError.noReTypePassword
            }

            guard registration.password == registration.reTypePassword else {
                throw LoginValidatorError.passwordNoMatch
            }

            guard registration.name.isEmpty == false else {
                throw LoginValidatorError.noName
            }

//            guard registration.workID.isEmpty == false else {
//                throw LoginValidatorError.noWorkID
//            }
//
//            guard registration.workID.count == 8 else {
//                throw LoginValidatorError.workIDLenght
//            }
            
            guard registration.password.count > 6 else {
                throw LoginValidatorError.passworMinLenght
            }
            guard LoginValidator.isValidPassword(testStr: registration.password) else {
                throw LoginValidatorError.passwordCriteria
            }
        }

        return true
    }

    public static func isValidEmail(testStr: String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"

        let emailTest = NSPredicate(format: "SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    public static func isValidPassword(testStr: String) -> Bool {
        let passwordRegex = "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)[a-zA-Z\\d\\W]{6,}$"

        let emailTest = NSPredicate(format: "SELF MATCHES %@", passwordRegex)
        return emailTest.evaluate(with: testStr)
    }
    
    public static func isValidName(string: String) -> Bool {
        if string.count < 3 {
            return false
        } else if string.count > 250 {
            return false
        }
        return true
    }
    
    public static func isValidPhoneNumber(number: String) -> Bool {
        let phoneRegex = "^([+]?[\\s0-9]+)?(\\d{3}|[(]?[0-9]+[)])?([-]?[\\s]?[0-9])+$"
        
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", phoneRegex)
        return phoneTest.evaluate(with: number)
    }
}

struct LoginViewModel {
    var name: String = ""
    var workID: String = ""
    var email: String = ""
    var password: String = ""
    var reTypePassword: String = ""
    var errorHandler: ((AppError) -> Void)?

    func getNumberOfRows(isLogin: Bool) -> Int {
        return isLogin ? 2 : 4
    }

    func getGetHeightInsetForTable(isLogin: Bool) -> CGFloat {
        return isLogin ? 173 : 88
    }

    func getButtonType(isLogin: Bool) -> CustomButtonType {
        return isLogin ? .login : .register
    }
}

enum LoginTableViewModel: Int {
    case email
    case password
    case retypePassword
    case name
//    case workID

    func getPlaceholder() -> String {
        switch self {
        case .email:
            return "Enter_email_placeholder".localized()
        case .name:
            let name = "First_Name_Placeholder".localized()
            let lastName = "Last_Name_Placeholder".localized()
            let string = name + ", " + lastName
            return string
        case .password:
            return "Enter_Pwd_Placeholder".localized()
        case .retypePassword:
            return "ReType_Pwd_Placeholder".localized()
//        case .workID:
//            return "Work_Number".localized()
        }
    }

    func getCapitalizationType() -> UITextAutocapitalizationType {
        switch self {
        case .name:
            return .words
        case .password,
             .retypePassword,
//             .workID,
             .email:
            return .none
        }
    }

    func isSecureTextfield() -> Bool {
        switch self {
        case .email,
             .name:
//             .workID:
            return false
        case .password,
             .retypePassword:
            return true
        }
    }
    
    func getMaxLenght() -> Int {
        switch self {
        case .email:
            return 320
        case .name:
            return 250
        case .password,
             .retypePassword:
            return 32
//        case .workID:
//            return 8
        }
    }
    
    func getKeyboardType() -> UIKeyboardType {
        switch self {
        case .email:
            return .emailAddress
        case .password,
             .name,
             .retypePassword:
            return .default
//        case .workID:
//            return .numberPad
        }
    }
}
