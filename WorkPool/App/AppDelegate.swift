//
//  AppDelegate.swift
//  WorkPool
//
//  Created by Marek Labuzik on 3/29/19.
//  Copyright © 2019 Marek Labuzik. All rights reserved.
//

import UIKit
import Moya
import RxSwift
import Fabric
import Crashlytics

@UIApplicationMain
final class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?
    private var appCoordinator: AppCoordinator?

    let meData = AppData.meData

    func application(_ application: UIApplication,
                     didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        Fabric.with([Crashlytics.self])
        PushNotifications.mePush.registerForPushNotifications()
        MeAppearance.setupApp()

        appCoordinator = AppCoordinator()

        self.window = UIWindow(frame: UIScreen.main.bounds)
        self.window?.rootViewController = appCoordinator?.navigationController
        self.window?.makeKeyAndVisible()

        appCoordinator?.start()
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state.
        // This can occur for certain types of temporary interruptions
        // (such as an incoming phone call or SMS message) or when the user quits
        // the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and
        // invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data,
        // invalidate timers, and store enough application state
        // information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution,
        // this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state;
        // here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        PushNotifications.mePush.clearNotificationsAndBadge()
        // Restart any tasks that were paused (or not yet started)
        // while the application was inactive. If the application
        // was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate.
        // See also applicationDidEnterBackground:.
    }

    // MARK: - push notifications
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        PushNotifications.mePush.didRegisterForRemoteNotifications(with: deviceToken)
    }

    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        PushNotifications.mePush.didFailToRegisterForRemoteNotifications(withError: error)
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey: Any] = [:]) -> Bool {
        if (AppData.meData.auth.refreshToken != nil) {
            guard let components = NSURLComponents(url: url, resolvingAgainstBaseURL: true),
                let params = components.queryItems else {
                    print("Invalid URL")
                    return false
            }
            
            if let userID = params.first(where: { $0.name == "userID" })?.value,
                let rideID = params.first(where: { $0.name == "rideID" })?.value {
                if AppData.meData.auth.isLoggedOn() {
                    self.appCoordinator?.showRide(rideID: rideID, userID: userID)
                } else {
                    UIAlertController.topMostControllerShowAlert(withTitle: "Error_title".localized(), subtitle: "LoginToRegisterRide".localized(), buttonTitle: "OK_Button".localized(), handler: nil)
                }
                return true
            } else {

                return false
            }
        } else {
            return false
        }
    }
    
    func application(_ application: UIApplication, willContinueUserActivityWithType userActivityType: String) -> Bool {
        return true
    }
    
    func application(_ application: UIApplication, continue userActivity: NSUserActivity, restorationHandler: @escaping ([UIUserActivityRestoring]?) -> Void) -> Bool {
        return true
    }
    
    func application(_ application: UIApplication,
                     didReceiveRemoteNotification userInfo: [AnyHashable: Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        if application.applicationState == .active {
            if let aps = userInfo["aps"] as? NSDictionary {
                if let alert = aps["alert"] as? NSDictionary {
                    if let message = alert["message"] as? String {
                        UIAlertController.topMostControllerShowAlert(withTitle: "", subtitle: message, buttonTitle: "OK_Button".localized(), handler: nil)
                    }
                }
            }
        }
        completionHandler(.noData)
    }

    func restartApp() {
        self.meData.reset()
        self.appCoordinator?.start()
    }
}
