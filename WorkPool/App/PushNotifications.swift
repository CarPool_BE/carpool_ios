//
//  PushNotificaions.swift
//  WorkPool
//
//  Created by Marek Labuzik on 05/06/2019.
//  Copyright © 2019 Marek Labuzik. All rights reserved.
//

import UserNotifications
import UIKit

public class PushNotifications {
    static public let mePush = PushNotifications()

    private(set) var deviceID: String?

    func registerForPushNotifications() {
        UNUserNotificationCenter.current()
            .requestAuthorization(options: [.alert, .sound, .badge]) {
                [weak self] granted, _ in

                print("Permission granted: \(granted)")
                guard granted else { return }
                self?.getNotificationSettings()
        }
        UIApplication.shared.registerForRemoteNotifications()
    }

    func getNotificationSettings() {
        UNUserNotificationCenter.current().getNotificationSettings { settings in
            print("Notification settings: \(settings)")
        }
    }
    
    func clearNotificationsAndBadge() {
        let center = UNUserNotificationCenter.current()
        center.removeAllDeliveredNotifications()
        UIApplication.shared.applicationIconBadgeNumber = 0
    }
    
    func registerLocalNotifications() {
        UNUserNotificationCenter.current()
            .requestAuthorization(options: [.alert, .sound, .badge]) {
                granted, _ in
                print("Permission granted: \(granted)")
        }
    }

    func didRegisterForRemoteNotifications(with deviceToken: Data) {
        let tokenParts = deviceToken.map { data in String(format: "%02.2hhx", data) }
        let token = tokenParts.joined()
        self.deviceID = token
    }

    func didFailToRegisterForRemoteNotifications(withError error: Error) {
        let errorCode = (error as NSError).code
        if errorCode == 3010 {
            self.deviceID = "Simulator"
        }
        print("Failed to register: \(error)")
    }
}
