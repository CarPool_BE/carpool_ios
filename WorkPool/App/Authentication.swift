//
//  Authentication.swift
//  WorkPool
//
//  Created by Marek Labuzik on 12/06/2019.
//  Copyright © 2019 Marek Labuzik. All rights reserved.
//

import Foundation
import UIKit

final class Authentication: Resettable {
    func reset() {
        accessToken = nil
        refreshToken = nil
        self.refreshTokenInMemmory = nil
    }
    
    private var refreshTokenInMemmory: RefreshToken?

    var refreshToken: RefreshToken? { // TODO: prerobit na keychain
        set {
            guard let value = newValue else {
                KeychainWrapper.standard.removeAllKeys()
                return
            }
            KeychainWrapper.standard.set(value.token, forKey: "RefreshToken")
            KeychainWrapper.standard.set(Int(value.expire.timeIntervalSince1970), forKey: "RefreshToken_expire")
        }
        get {
            if let token = self.refreshTokenInMemmory {
                return token
            }
            guard let token = KeychainWrapper.standard.string(forKey: "RefreshToken"),
                  let expire = KeychainWrapper.standard.integer(forKey: "RefreshToken_expire")
                else { return nil }

            var refreshToken = RefreshToken()
            refreshToken.token = token
            refreshToken.expire = Date(timeIntervalSince1970: Double(expire))
            self.refreshTokenInMemmory = refreshToken
            return refreshToken
        }
    }
    var accessToken: AccessToken? {
        didSet {
            self.getData()
        }
    }

    let profileService: MeProfileService = MeProfileService()
    let authService: AuthorizationService = AuthorizationService()
    let rideService: RideService = RideService()
    let userService: UserService = UserService()
    let carService: CarService = CarService()
    
    // MARK: - fuctions
    init() {
        self.authService.observe(service: self.profileService)
        self.authService.observe(service: self.rideService)
        self.authService.observe(service: self.userService)
        if let token = self.refreshToken {
            self.authService.renewAccessToken(refresh: token) { [weak self] result in
                guard let self = self else { return }
                switch result {
                case .success(let accesToken):
                    self.accessToken = accesToken
                case .failure:
                    guard let appDelegate: AppDelegate = UIApplication.shared.delegate as? AppDelegate
                        else { return }
                    self.reset()
                    appDelegate.restartApp()
                    break
                }
            }
        }
    }
    
    func logout(completion: @escaping (Result<Bool, RequestErrors>) -> Void) {
        self.profileService.logout { result in
            switch result {
            case .success:
                UserDefaults.standard.removeObject(forKey: "RefreshToken")
                UserDefaults.standard.synchronize()
            case .failure:
                break
            }
            completion(result)
        }
        
    }

    func getData() {
        self.profileService.getMeProfile { result in
            switch result {
            case .success(let user):
                AppData.meData.profile = user
            case .failure(let error):
                print("Error: \(error)")
            }
        }
    }

    func refreshProfile(completion: @escaping (Result<Car, RequestErrors>) -> Void) {
        self.profileService.getMeProfile { result in
            switch result {
            case .success(let user):
                AppData.meData.profile = user
                guard let car = user.car else {
                    completion(.failure(.notFound))
                    return
                }
                completion(.success(car))
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }

    public func getSignInToken() -> String? {
        return refreshToken?.token
    }

    public func isLoggedOn() -> Bool {
        if refreshToken?.token == nil {
            return false
        } else {
            return true
        }
    }
}
