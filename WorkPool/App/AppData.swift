//
//  AppData.swift
//  WorkPool
//
//  Created by Marek Labuzik on 09/05/2019.
//  Copyright © 2019 Marek Labuzik. All rights reserved.
//

import Foundation
import Moya
import MapKit
import CommonCrypto

protocol Resettable {
    func reset()
}

final class AppData {
    
    static let shared = AppData()

    var resetables: [Resettable] = []

    // MARK: - properties
    static public let meData = AppData()
    public let auth = Authentication()

    public var modelToUpdate: OnboardingViewModel?
    
    var profile: User? {
        didSet {
            self.updateProfil()
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "UserProfileUpdate"), object: nil, userInfo: nil)
        }
    }

    var lastSearch: LastSearchAdress? {
        set {
            if let newSearch = newValue {
                UserDefaults.standard.set(newSearch.propertyListRepresentation, forKey: "LastSearch")
                UserDefaults.standard.synchronize()
            }
        }
        get {
            guard let lastSearch = UserDefaults.standard.object(forKey: "LastSearch") as? [String: String] else {
                return nil
            }
            return LastSearchAdress(dictionary: lastSearch)
        }
    }
    
//    public let workAddress: String = "Jaguar Landrover, Nitra"
//    public let workGPS: CLLocationCoordinate2D = CLLocationCoordinate2DMake(48.346067, 18.049028)

    public let workAddress: String = "Továrenská 12, Bratislava"
    public let workGPS: CLLocationCoordinate2D = CLLocationCoordinate2DMake(48.1445966, 17.1242701)
    
    let rideList = RideList()

    init() {
        resetables = [
            auth
        ]
    }

    func reset() {
        profile = nil
        resetables.forEach { $0.reset() }
    }

    func updateProfil() {
        if self.profile != nil {
            guard let model = self.modelToUpdate else { return }
            guard var profile = self.profile else {return}
            profile.car = model.car
            profile.home = model.address
            profile.homeLat = model.lat
            profile.homeLon = model.lon
            profile.phoneNumber = model.phoneNumber
            
            AppData.meData.auth.profileService.updateMy(profile: profile, completion: { [weak self] result in
                switch result {
                case .failure: break
                case .success:
                    self?.modelToUpdate = nil
                    self?.profile = profile
                }
            })
        }
    }
    
    func getDefaultScreen() -> Int {
        return UserDefaults.standard.integer(forKey: "DefaultScreen")
    }
    
    func saveDefaultScreen(index: Int) {
        UserDefaults.standard.set(index, forKey: "DefaultScreen")
        UserDefaults.standard.synchronize()
    }

}
