//
//  ErrorHandler.swift
//  WorkPool
//
//  Created by Marek Labuzik on 11/07/2019.
//  Copyright © 2019 Marek Labuzik. All rights reserved.
//

import UIKit

enum AppError: Error {
    case none
    case succsess
    case retry
    case checkDataAndRetry
    case badCredentials
    case alreadyExists
    // LoginValidator error
    case noWorkID
    case noEmail
    case noName
    case invalidEmail
    case noPassword
    case noReTypePassword
    case passwordNoMatch
    case passworMinLenght
    case passwordCriteria
    case workIDLenght
    case emailOrIdIsUsed
    case idNotExists
    //createRideValidator error
    case modelError
    case noAddressFrom
    case noAddressTo
    case noNote
    case noReturnNote
    case noTime
    case noReturnTime
    case noDates
    case noCar
    //searchRide
    case emptyStreetOrCity

    case onboardingAddressEmpty
    
    var showAlert: Bool {
        if self == .none {
            return false
        }
        return true
    }
    
    var localizedError: String {
        switch self {
        case .none:
            return "General_Error".localized()
        case .succsess:
            return "Request_Success".localized()
        case .alreadyExists: return "User_IsExists".localized()
        case .badCredentials: return "Bad_Credentials".localized()
        case .checkDataAndRetry: return "CheckDataAndRetry".localized()
        case .retry: return "Please_Retry".localized()
        case .invalidEmail:
            return "InvalidEmail".localized()
        case .noEmail:
            return "Email_field_isEpmty".localized()
        case .noName:
            return "Neme_field_isEpmty".localized()
        case .noPassword:
            return "Password_field_isEpmty".localized()
        case .noReTypePassword:
            return "ReTypePassword_field_isEpmty".localized()
        case .noWorkID:
            return "WorkID_field_isEpmty".localized()
        case .passwordNoMatch:
            return "PasswordNoMatch_field_isEpmty".localized()
        case .passworMinLenght:
            return "PasswordMinLenght".localized()
        case .passwordCriteria:
            return "PasswordCriteria".localized()
        case .workIDLenght:
            return "WorkIDWrongLenght".localized()
        case .modelError:
            return "Unknown_Error".localized()
        case .noAddressFrom:
            return "NoAddressFrom".localized()
        case .noAddressTo:
            return "NoAddressTo".localized()
        case .noNote:
            return "NoNote".localized()
        case .noReturnNote:
            return "NoRetunrNote".localized()
        case .noTime:
            return "NoTime".localized()
        case .noReturnTime:
            return "NoReturnTime".localized()
        case .noDates:
            return "NoDate".localized()
        case .noCar:
            return "NotCar".localized()
        case .emptyStreetOrCity:
            return "EmptyStreetOrCity".localized()
        case .onboardingAddressEmpty:
            return "OnboardingAddressEmpty".localized()
        case .emailOrIdIsUsed:
            return "Registration_failure_email_id".localized()
        case .idNotExists:
            return "Registration_failure_workng_pNumber".localized()
        }
    }
}

final class ErrorHandler {
    public static func show(with error: AppError, on controller: UIViewController?) {
        guard let controller = controller else {
            return
        }
        ErrorHandler.show(tile: "Error_title".localized(), message: error.localizedError, on: controller)
    }
    
    public static func show(with error: AppError,
                            on controller: UIViewController?,
                            withBtn title: String,
                            completionBtn: ((UIAlertAction) -> Void)?) {
        guard let controller = controller else { return }
        var alertTitle = "Error_title".localized()
        if error == .succsess {
            alertTitle = "Request_Success_title".localized()
        }
        
        let alert = UIAlertController.alert(withTitle: alertTitle,
                                            subtitle: error.localizedError,
                                            buttonTitle: "OK_Button".localized(),
                                            handler: nil)
        
        let action = UIAlertAction(title: title, style: .default) { act in
            if completionBtn != nil {
                completionBtn!(act)
            }
        }
        alert.addAction(action)
        controller.present(alert, animated: true, completion: nil)
    }
    
    public static func show(with error: AppError, on controller: UIViewController?, completion: ((UIAlertAction) -> Void)? = nil) {
        guard let controller = controller else {
            return
        }
        var alertTitle = "Error_title".localized()
        if error == .succsess {
            alertTitle = "Request_Success_title".localized()
        }
        let alert = UIAlertController.alert(withTitle: alertTitle,
                                            subtitle: error.localizedError,
                                            buttonTitle: "OK_Button".localized(),
                                            handler: { action in
                                                if completion != nil {
                                                    completion!(action)
                                                }
        })
        controller.present(alert, animated: true, completion: nil)
    }

    private static func show(tile: String, message: String, on controller: UIViewController) {
        let alert = UIAlertController.alert(withTitle: tile,
                                            subtitle: message,
                                            buttonTitle: "OK_Button".localized())
        controller.present(alert, animated: true, completion: nil)
    }
}
